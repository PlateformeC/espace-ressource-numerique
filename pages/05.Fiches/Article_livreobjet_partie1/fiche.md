---
title: 'LIVRE-OBJET, OBJET-LIVRE (partie 1)'
media_order: IMG_0888.webp
date: '01.09.2019 00:00'
taxonomy:
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'livre-objet'

    author:
    - 'Yanaïta Araguas'
aura:
  pagetype: article
  description: "Livre-objet ? Objet-livre ? Différents formats de livres peuvent se cacher derrière ces appellations : livres pop up, livre à tirettes, à disques , livres accordéon, “arquelinades”, livres-tunnels, livres à images transparentes, à illusions d’optique etc. Chercher à définir la notion de livre-objet nous a amené à prendre en compte la difficulté de faire rentrer cette terminologie dans des cases."
  image: IMG_0888.webp
  category:
    - recherches

---

*« Rien de pire pour un livre que de n’être pas pris en main. Il a beau se poser là avec sa couverture, son format, son épaisseur, tant que personne ne le touche, il n’est qu’un objet inerte et muet.»* Liliane Cheilan

## C'est quoi un livre?

Pour la plupart des occidentaux de classe moyenne, les livres sont des objets courants. Ils nous entourent mais semblent parfois devenus invisibles par leur banalité. Mais, qu’est-ce qu’un livre concrètement ? Quelle est sa fonction ? Sa forme est-elle constante, d’un livre de poche à une édition de la Pléiade, d’un guide touristique à un livre de cuisine ? Quel est le fil conducteur ?

*Livre : Mot latin, liber. Assemblage de feuilles portant un texte, réunies en un volume, reliées ou brochées.* Petit Larousse 2014

Un livre c’est un objet physique que l’on tient dans sa main. Il a un poids que l’on sent quand on le porte dans son sac. Il peut avoir l’odeur de la poussière et du vieux papier. Il peut être doux au toucher. Son aspect contribue à nous faire entrer dans un univers, une ambiance qui lui est propre, et que nous pourrons invoquer mentalement en souvenir, indissociable du contenu lui même. Que l’on change, au grès d’une réédition, l’image de couverture ou le type de papier et voilà notre expérience de lecture toute différente car, bien que ce qu’il raconte soit souvent sa raison d’être, la forme du livre a une grande importance dans le rapport que nous entretenons avec lui. Un livre c’est aussi un vecteur de transmission, un outil de mémoire et d’information, un espace de rêverie séculaire. Grâce à cet outil extraordinaire, le lecteur capitalise de l’expérience et emmagasine des données, des savoirs, de la culture. Ce qui est en jeu lorsque nous lisons, relie intimement nos mains, nos sens et notre cerveau. Notre conscience se déploie bien au-delà de l’objet livre en une réunion de matérialité et d’immatérialité, dont l’assemblage s’est construit au fil des siècles de la main et de la mémoire des hommes.

## Brève histoire de l’objet-livre, évolutions, révolutions

Le livre depuis ses origines est par essence changeant, mouvant, versatile. Son aspect s’est adapté en fonction des cultures, des usages, des matériaux-ressources, des techniques de production, au fil du temps et des expérimentations de ceux qui le fabriquent.

Aux origines, des tablettes et des cônes gravés parfois intransportables mais qui ont traversé les siècles, des rouleaux de papyrus fragiles et légers sur lesquels la lecture se faisait par défilement, et les premiers codices[1] (carnets assemblés avec des pages de papyrus ou de parchemin, objets finalement assez similaires à nos livres actuels), chez les Grecs et les Romains mais aussi chez les Mayas de l’autre coté de l’océan.

![livreobjet_1.jpeg](livreobjet_1.jpeg?forceresize=100%)
*Codex Guta-Sintram, 1154 – Photo Claude TRUONG-NGOC*

Au Moyen âge apparaissent les premiers livres à système. Ce sont des dispositifs inventés pour permettre de transmettre une information complexe. On trouve par exemple des manuels d’anatomie, composés de languettes à soulever, pour s’initier aux mystères du corps humain ou des livres d’astronomie à volvelles[2] comme le Cosmographicus Liber, en 1524, permettant de comprendre le mouvement des corps célestes en actionnant des disques mobiles.

![livreobjet_2.jpeg](livreobjet_2.jpeg?forceresize=100%)
*Image provenant de : www.headlesschicken.ca/eng204/texts/CosmographicusLiber*

![livreobjet_3.jpeg](livreobjet_3.jpeg?forceresize=100%)
*Image provenant de : www.bonhams.com/auctions/18411/lot/6044*

![livreobjet_4.webp](livreobjet_4.webp?forceresize=100%)
*Flap Anatomy, 2011 – Photo by Mark Zupan, Senior Graphic Designer for Duke University Libraries.*

Alors que livre fut longtemps un objet rare, recopié à la main et peu diffusé, les débuts de l’imprimerie en Orient et Occident vont changer la donne.

Les livres se répandent, leur usage se démocratise.

On commence à ressentir que l’objet s’adapte à ceux qui les utilisent.

En 1677 est publiée La confession coupée, un livre à glisser dans sa poche, composé de centaines de languettes de papier, sur lesquelles sont répertoriés les péchés, à détacher et à donner à son confesseur. Trois siècles plus tard, sur la même forme, le livre de Raymond Quenaud, Cent milliards de poèmes, datant de 1961, permettra de créer des sonnets aléatoires en combinant les languettes.

![livreobjet_5.jpeg](livreobjet_5.jpeg?forceresize=100%)
*Photo: Le Devoir Annik MH de Carufel*

![livreobjet_6.jpeg](livreobjet_6.jpeg?forceresize=100%)
*Image provenant de : www.pedagogie.ac-aix-marseille.fr*

Vers 1765, voient le jour en Angleterre les premiers « movable books », également nommés « Arlequinades », dans lesquels le déploiement de rabats génère différentes possibilités de récits superposés.

Au XIXème siècle, apparaissent les livres-tunnels, les livres à tirettes, à disques, les livres à images transparentes, à illusions d’optique, dans un contexte où l’intérêt pour l’image animée est alimenté par des innovations telles que la chronophotographie, la lanterne magique, ainsi que les tout débuts du cinéma et du dessin animé. La place de l’enfant dans la société est également en train de changer et l’éducation passe, pour les classes aisées, par la lecture, une littérature «jeunesse» émergeant peu à peu.

Durant le dernier tiers du XIXème siècle, Lothar Meggendorfer, illustrateur allemand et inventeur de génie, crée le premier livre animé moderne comme cadeau de Noël à son fils. À l’intérieur se cachent de nombreux mécanismes articulés complexes animant les personnages.

![livreobjet_7.jpeg](livreobjet_7.jpeg?forceresize=100%)
*Lothar Meggendorfer, illustration from Travels of Little Lord Thumb and His Man Damien, 1891*

![livreobjet_8.jpeg](livreobjet_8.jpeg?forceresize=100%)
*‘The Library,’ a tunnel book from about 1730 with hand-colored intaglio illustrations.*

Dans les années 1940/50, le livre d’artiste connaît un essor nouveau sous une forme beaucoup plus avant-gardiste avec, comme figure emblématique, Bruno Munari. Ce plasticien milanais, également graphiste, designer, directeur de la collection Tutti bambini chez Einaudi, était également très concerné par la question de la transmission au jeune public, ce qui l’a amené à la création en 1977 du premier laboratorio per bambini à Milan, un laboratoire d’expérimentation de l’art visuel par les enfants avec une méthode innovante basée sur l’expérience :
*« Les arts visuels ne doivent pas être racontés avec des mots, ils doivent être expérimentés: les mots sont oubliés, l’expérience ne l’est pas. »*

![livreobjet_9.jpeg](livreobjet_9.jpeg?forceresize=100%)
*Image provenant de : http://blog.marshmallow-games.com*

![livreobjet_10.jpeg](livreobjet_10.jpeg?forceresize=100%)
*Image provenant de : https://libricalzelunghe.it/2016/04/08/fare-esperienza-della-realta-attraverso-i-libri-catalogo/*

Dans cet esprit, il concevra de nombreux livres révolutionnant l’expérience du lecteur. Parmi ceux-là, les Livres illisibles[3] (ouvrages sans texte, suite de jeux de formes et de couleurs), et Prélivres[4] (boîte-bibliothèque contenant 12 livres carrés encastrés dans un coffret, jouant sur les matières, les formes, proposant une narration sensorielle inédite). On sent encore aujourd’hui l’écho de son œuvre dans le travail de nombreux artistes du livre contemporain, comme Katsumi Komagata.

![livreobjet_11.jpeg](livreobjet_11.jpeg?forceresize=100%)
*Photo : Anaïs Beaulieu / Les Trois Ourses*


Dans les années 50 à 70, des collectifs comme Fluxus produiront de nombreux livres hybrides en écho à Marcel Duchamp et sa Boîte en valise (1914). Vient ensuite en parallèle la diffusion à grande échelle du phénomène «pop-up»[5] pour le plaisir des petits et des grands. Ces livres animés aux figures surgissantes impressionnantes créent une relation active avec le lecteur, comme dans La maison hantée de Jan Pienkowski ou parfois plus contemplative, par exemple dans A cloud ou Little tree de Katsumi Komagata. Qu’ils soient foisonnants dans la forme, parfois au détriment du fond comme chez Robert Sabuda (ingénieur papier de génie : Alice au pays des merveilles, Le magicien d’Oz), ou Votjech Kubasta(Le petit chaperon rouge) ou minimalistes comme le superbe ABC3D de Marion Bataille, on laisse opérer la magie, on oublie sa propre réalité pour se projeter dans le monde fictionnel du livre. Et lorsqu’il se fait sculpture comme dans 600 pastilles noires, de David Carter, le livre et l’œuvre d’art semblent se fondre parfaitement dans ce terme de livre-objet.

<iframe width="560" height="315" src="https://www.youtube.com/embed/gYLVih7T3lo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

On voit également, à l’ère du numérique, apparaître de nouveaux usages et de nouvelles formes. Les liseuses en sont un exemple. Un livre en forme de tour de Babel, réunissant potentiellement tous les livres du monde en un seul… Des livres dont la matérialité est toute différente. Sauront-ils résister au temps à la manière des cônes historiques sumériens ? Les artistes sont nombreux à s’emparer aujourd’hui du livre augmenté, issu par exemple de l’utilisation conjointe d’un livre et d’une tablette, comme la collection Histoires Animées parue chez Albin Michel Jeunesse, où la réalité augmentée se place au service de la narration, apportant des niveaux supplémentaires de lecture, ou encore dans Avec quelques briques, livre de Vincent Godeau, adapté par Cléa Dieudonné et Mathilde Fournier. Aujourd’hui encore, moins rare, moins précieux, plus accessible, le livre continue de muter et de nouvelles formes apparaissent, parfois difficiles à qualifier, à classifier.

<iframe title="vimeo-player" src="https://player.vimeo.com/video/324933885" width="640" height="426" frameborder="0" allowfullscreen></iframe>

<iframe title="vimeo-player" src="https://player.vimeo.com/video/81093905" width="640" height="360" frameborder="0" allowfullscreen></iframe>

## Livre-objet, livre d’artiste, livre hybride, Fanzine graphique… Tentative de définition, préciser les termes…

![index.webp](index.webp?forceresize=100%)
*Image provenant de : https://www.binocheetgiquello.com/lot/12290/2305785*

![livreobjet_13.jpeg](livreobjet_13.jpeg?forceresize=100%)


Lorsque nous sortons de la définition première du livre, nous peinons souvent à trouver un qualificatif adapté. Que penser d’un livre d’où sortent des monstres de papier (Encyclodino -Robert Sabuda), d’un livre qui se déplie en accordéon pour atteindre parfois 4 ou 5 mètres de long (Un livre pour toi – Květa Pacovská ou La Prose du Transsibérien -1913 – par Sonia Delaunay et Blaise Cendrars), d’un livre en ombrocinéma, demandant l’intervention d’une grille imprimée sur plastique transparent qui, actionnée de droite à gauche, animera les pages (Poémotion, Takahiro Kurashima ou New York en pyjamarama – Frédérique Bertrand et Michaël Leblond) ?

<iframe width="560" height="315" src="https://www.youtube.com/embed/WiDN-kkCVcQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Un livre sans texte, sans couverture, sans reliure est-il encore un livre ?

Faudrait-il encore désigner ainsi un livre qui perdrait sa fonction, ne pouvant être lu qu’au prix d’efforts substantiels, tel Fleurs de saisons, ce livre minuscule à lire avec une loupe (Toppan printing) ou le gigantesque Patrie aimée de sept tonnes et deux mètres de haut (conçu par Vinicios Leoncio pour dénoncer le poids de la bureaucratie au Brésil) ? Comment lire un livre où le temps suspend son cours de la première à la dernière page (Cependant – Paul Cox)? Que dire enfin, et ou classer, les livres composites comme ceux du Mouvement Fluxus ?

![livreobjet_14.jpeg](livreobjet_14.jpeg?forceresize=100%)
*Cependant, Éditions du Seuil, 2002.*

Alexandre Mare, commissaire d’exposition, écrit pour Art press dans son article Petite géographie du livre d’artiste :

  *« Il est difficile de délimiter la catégorie à laquelle appartient le livre d’artiste car sa définition et son champ de production fluctuent. Objet éditorial et artistique, il est à la lisière entre deux mondes, où le lecteur devient spectateur. À la différence du livre qui peut être identifié comme objet culturel et comme marchandise, le livre d’artiste est pris dans des contradictions ontologiques et historiques qui empêchent d’en fixer la définition : il peut aussi bien s’intituler livre-sculpture, livre peint, portfolio, livre unique, magazine d’artiste, fanzine d’artiste, artist’s book, book of artist, ouvrage de bibliophilie, livre illustré, livre d’atelier, etc.»*

Marie Françoise Quignard, dans son livre Bibliothèques/ Lieux d’art contemporain. Quels partenariats ? propose de réfléchir en ces termes :

    *« À l’extrême fin du XXe siècle, parler du livre d’artiste c’est accepter sa polysémie, ne pas prendre la partie pour le tout, le limitant à une période ou à un mode. C’est donc accepter l’ambiguïté du terme et admettre ses différentes significations afin de brosser un tableau le plus vaste possible de ses différentes manifestations.»*

Ces livres exceptionnels, quels que soient leurs noms, ont amené de nouvelles façons de produire, de fabriquer et de concevoir. Éditeurs, imprimeurs, artistes ont du investir autrement leur terrain de jeu pour rendre possibles ces nouvelles formes. Véritables chercheurs, ils repoussent ensemble les limites et les contraintes qui leurs sont propres pour nos offrir ces objets magnifiques.


[1] Codex au singulier

[2] La volvelle est une carte tournante, utilisée pour l’astronomie, la navigation ou encore la médecine. Il s’agit d’un instrument de calcul. Elle met en forme les principes de l’astrolabe dans un livre.

[3] Libri illeggibili

[4] Prelibri

[5] livre animé, livre à système
