---
title: CNC-SentierBattu
media_order: PC020608.JPG
type_ressource: text
feature_image: PC020608.JPG
license: cc-by-nc-sa
date: '01-01-2016 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: article
    description: 'Cette machine sert au fraisage et à la découpe.'
    image: PC020608.JPG
metadata:
    description: 'Cette machine sert au fraisage et &agrave; la d&eacute;coupe.'
    'og:url': 'https://ressources.pingbase.net/fiches/cnc_sentier_battu'
    'og:type': article
    'og:title': 'CNC-SentierBattu | PING Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert au fraisage et &agrave; la d&eacute;coupe.'
    'og:image': 'https://ressources.pingbase.net/fiches/cnc_sentier_battu/PC020608.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '960'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'CNC-SentierBattu | PING Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert au fraisage et &agrave; la d&eacute;coupe.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/cnc_sentier_battu/PC020608.JPG'
    'article:published_time': '2016-01-01T00:00:00+01:00'
    'article:modified_time': '2020-12-02T09:21:46+01:00'
    'article:author': Ping
---

*Cette machine sert au fraisage et à la découpe.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/CNC-SentierBattu?classes=btn)
