---
title: '[MAKER À COEUR] Jean-Pierre, le récupérateur frénétique'
media_order: jeanpierre-870x550.png
type_ressource: text
feature_image: jeanpierre-870x550.png
license: cc-by-nc-sa
date: '01-11-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'À l''Atelier partagé du Breil, Jean-Pierre, nous dévoile sa passion : la réparation !'
    image: jeanpierre-870x550.png
metadata:
    description: '&Agrave; l''Atelier partag&eacute; du Breil, Jean-Pierre, nous d&eacute;voile sa passion : la r&eacute;paration !'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_jean_pierre_le_recuperateur_frenetique'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Jean-Pierre, le r&eacute;cup&eacute;rateur fr&eacute;n&eacute;tique | Espace Ressources Num&eacute;riques'
    'og:description': '&Agrave; l''Atelier partag&eacute; du Breil, Jean-Pierre, nous d&eacute;voile sa passion : la r&eacute;paration !'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_jean_pierre_le_recuperateur_frenetique/jeanpierre-870x550.png'
    'og:image:type': image/png
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Jean-Pierre, le r&eacute;cup&eacute;rateur fr&eacute;n&eacute;tique | Espace Ressources Num&eacute;riques'
    'twitter:description': '&Agrave; l''Atelier partag&eacute; du Breil, Jean-Pierre, nous d&eacute;voile sa passion : la r&eacute;paration !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_jean_pierre_le_recuperateur_frenetique/jeanpierre-870x550.png'
    'article:published_time': '2017-11-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:56:08+01:00'
    'article:author': Ping
---

**Bonjour Jean-Pierre ! Peux-tu te présenter ?**

Allez curriculum vitae (rire). Je m’appelle Jean-Pierre Hardy, je suis né à La Rochelle et j’ai grandi dans un environnement où mon père accumulait beaucoup de choses, ce que j’ai continué à faire jusqu’à maintenant. Je me rappelle d’un solex et d’un siège tournant à 8 francs. Les premiers bricolages c’était autour de ça avec un petit moteur d’essuie-glaces sous le siège tournant pour le faire bouger, avec des boutons ! Bref. J’ai fait un IUT de Mesures physiques – Électro-technique qui n’a pas spécialement abouti. J’aimais les moteurs mais les mesures physiques c’était bien trop élevé pour moi.

Après le service militaire, il a fallu travailler. Je suis donc arrivé à Nantes. À l’époque ça n’était pas très compliqué de trouver du travail et j’ai commencé dans un petit magasin, rue Paul Bellamy, qui vendait de la Hi-Fi. J’étais au service après-vente donc j’allais réparer des machines un peu partout. C’est d’ailleurs à ce moment là que j’ai entendu pour la première fois le mot « obsolescence », c’était à peu près en 1975. Je suis ensuite rentré dans une autre entreprise de photocopieurs après avoir rencontré un monsieur qui y était revendeur, et j’y suis resté plus ou moins 20 ans. Finalement, la retraite est arrivée, ça a été un moment difficile, de grand stress. Le corps ne suivait plus et je devais me concentrer sur quelque chose pour ne pas rester à rien faire ! Je me suis donc lancé encore plus fort dans la réparation, toujours en restant dans une démarche de récupération.

**Pourquoi viens-tu à Plateforme C ?**

C’est plutôt pour transmettre. S’il y a une panne ou quelque chose qui pose un soucis dans un projet, j’essaie de trouver ce qui ne fonctionne pas. Ce sont des réflexes de réparation que j’ai dans la tête, je pense. Mais ce qui est très important, c’est de rencontrer des gens, de faire plaisir aux autres. C’est souvent ce que je remarque dans les ateliers que je fréquente : les gens ont le sourire quand ils arrivent à passer entre les mailles de cette logique de consommation et de destruction, de gâchis.

**Depuis quand fréquentes-tu l’Atelier Partagé du Breil ?**

J’étais déjà venu à l’atelier du Breil avant qu’il n’ouvre, pour faire un gros tri de tout ce qu’il y avait. Je ne connais pas l’historique du lieu, mais sur le moment il y avait dans l’air un désir de créer un atelier à cet endroit. D’ailleurs quand il y a eu le tri, j’ai récupéré des jouets et d’autres choses pour les démonter proprement et enlever cette tâche à PiNG. Après ce temps de montage, l’atelier a commencé à accueillir du monde. C’est drôle, mais j’aime bien quand on prend des photos à cet endroit car on ne voit que les hauts des crânes (rires).

**Et pour Plateforme C ?**

Pour le fablab, c’était par les gens. J’ai entendu parler d’une soirée où on allait discuter d’obsolescence programmée donc j’y suis allé. Et dans la conversation, j’ai eu écho d’une animation qui allait se dérouler à Bellevue. J’y suis allé en touriste également, toujours avec mes outils, au cas où (rire). D’ailleurs, j’ai sous le coude un projet d’un robot qui s’occuperait de me suivre en portant mes outils. Mais pour en revenir à cet atelier à Bellevue, j’ai rencontré Julien Merlaud qui travaillait à la Ressourcerie de l’île et, de fils en aiguilles, j’en suis arrivé à visiter Plateforme C. Avant, j’étais un solitaire dans mes bricolages, maintenant je suis presque un solidaire ! Je fais les choses pour les autres.

**Est-ce que tu peux nous parler d’un projet que tu as réalisé à l’Atelier Partagé ou au fablab ?**

On peut parler du Tronchoscope. J’ai récupéré des trucs dans l’atelier de mon ancien travail, juste avant la retraite. Quand j’ai commencé à reprendre pied, au moment où j’ai découvert Plateforme C, je plaisantais avec Maëlle (l’animatrice du fablab) en lui disant que c’était dommage que de semaine en semaine les uns oublient les prénoms des autres. Je me suis donc penché sur la fabrication d’une machine qui prendrait des photos des adhérents qui le voudraient bien. J’ai utilisé une imprimante thermique qui n’a pas coûté un sou (vive la récup’), un pied de lampadaire récupéré à l’Atelier Partagé et une caméra qui était une caméra de surveillance à la base. L’imprimante reconnaissait directement la caméra : pas besoin de passer par un PC. J’essaie de faire des effets avec peu de choses. Il suffit donc d’appuyer sur l’imprimante et la photo sort en une minute. On peut également faire des planches de 4 ou 16 photos, tout ça géré par des petits boutons et pas par de l’électronique.

Maëlle m’a demandé comment on allait l’appeler alors j’ai cherché et, finalement, on est arrivé au Tronchoscope. Ça a sûrement servi pendant un moment à faire ça, des photos des adhérents, et puis c’était sympa parce que j’avais amené des magnets et il y avait les cartes d’adhérents avec la photo sur un tableau. On savait qui était qui, comme ça ça facilitait le contact.

**Pour finir, as-tu une anecdote qui te revient en mémoire, que ça soit à Plateforme C ou à l’Atelier Partagé ?**

Je me suis retrouvé, un jour à Plateforme C, avec Rémy, à aller voir l’espace de construction de bateaux en face du fablab. On a visité un peu et je suis tombé devant un bloc machine, je ne sais plus à quoi ça servait. J’ai apprécié la manière dont ça avait été fait et construit et on m’a dit que c’était en vrac à cause d’une chaîne cassée et que ça ne marchait plus. Mais, moi je me suis dis que je connaissais ces chaînes là ! Je suis rentré chez moi et j’ai fouillé un peu pour finalement retrouver dans mon stock des chaînes des mêmes dimensions : pile ce qu’il fallait ! La machine a donc pu repartir après ça. C’est la force de la récupération, tu ne sais pas quand est-ce qu’un élément servira, mais ça servira !
