---
title: 'Documenter c’est partager ses connaissances 3/3'
media_order: IMG_2114-870x550.jpg
type_ressource: text
feature_image: IMG_2114-870x550.jpg
license: cc-by-sa
serie:
    -
        page: /fiches/documenter_partager_ses_connaissances_1
    -
        page: /fiches/documenter_partager_ses_connaissances_2
date: '22-06-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture libre'
        - opensource
        - 'c libre'
        - fablab
        - documentation
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: article
    description: 'Voici le troisième article de la trilogie sur la documentation dans les fablabs.'
    image: IMG_2114-870x550.jpg
metadata:
    description: 'Voici le troisi&egrave;me article de la trilogie sur la documentation dans les fablabs.'
    'og:url': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_3'
    'og:type': article
    'og:title': 'Documenter c&rsquo;est partager ses connaissances 3/3 | PiNG Ressources Num&eacute;riques'
    'og:description': 'Voici le troisi&egrave;me article de la trilogie sur la documentation dans les fablabs.'
    'og:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_3/IMG_2114-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Documenter c&rsquo;est partager ses connaissances 3/3 | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Voici le troisi&egrave;me article de la trilogie sur la documentation dans les fablabs.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_3/IMG_2114-870x550.jpg'
    'article:published_time': '2017-06-22T00:00:00+02:00'
    'article:modified_time': '2020-12-02T09:21:48+01:00'
    'article:author': Ping
---

*Voici le troisième article de la trilogie sur la documentation dans les fablabs. Nous allons dans cet article étudier le lien entre documentation et propriété industrielle ainsi que les distinctions entre propriété littéraire et artistique et propriété industrielle.*

Je vous parle souvent de LA propriété intellectuelle au singulier. Il est vrai que les droits qu’elle contient se ressemblent fortement. Ils sont des droits de propriété ou presque, ils s’intéressent à des créations immatérielles et permettent en général à des créateurs d’être financés pour leur acte de création.
Mais alors pourquoi différencier propriété littéraire et artistique et propriété industrielle ?


La réponse est qu’en dépit des similitudes, ces deux branches de la propriété intellectuelle ne suivent pas vraiment la même logique.
La propriété littéraire et artistique va protéger la création et au travers de la création, elle va aussi protéger le créateur. Elle a indéniablement une vocation culturelle et artistique. En grossissant un peu le trait, elle protège l’art en général.
De son côté, la propriété industrielle va plutôt protéger l’investissement c’est-à-dire les moyens matériels et humains nécessaires à la production de ce qu’on appelle une création industrielle.
Bien entendu, ces différences vont se traduire par l’application de règles propres à chacune des deux branches.
Dans l’article précédent , nous parlions de la propriété littéraire et artistique. Il nous reste donc, l’autre branche de la propriété intellectuelle à explorer : la propriété industrielle.
Que se passe-t-il quand je documente une création industrielle ? Quelle licence libre puis-je choisir ?
Dans un premier temps, il faut là encore, bien faire la différence entre la documentation du projet et le projet lui-même (ici une création industrielle). En effet, la création industrielle (en général une invention ou un design) va être soumise au droit de la propriété industrielle. La documentation de cette création, elle, est une œuvre de l’esprit qui ne sera protégée, que si elle est originale, par le droit de la propriété littéraire et artistique
**Précisons-le dès à présent : au regard de la loi, mettre une licence libre sur la documentation d’une création industrielle ne s’applique qu’à la documentation et pas à la création.**
Par exemple, si je fais une invention, que je la documente, que cette documentation est originale, et que je choisis la licence CC-BY-NC, tout le monde pourra réutiliser cette documentation mais en citant à chaque fois mon nom et sans en faire un usage commercial.
**En revanche, tous pourront très bien réutiliser l’invention, en faire un usage commercial, sans même citer mon nom en tant qu’inventeur.**

Il faut donc bien distinguer la notion de création industrielle de celle d’œuvre de l’esprit car en aucun cas l’une ne peut se substituer à l’autre.

Il existe plusieurs types de créations industrielles. Les inventions, les dessins ou les modèles (comprendre le design industriel), les obtentions végétales (comprendre un monopole économique sur un organisme végétal génétiquement modifié)… Ce sont des créations qui sont le fruit de l’intellect de créateurs. Elles doivent avoir une vocation industrielle, c’est-à-dire qu’elle vont avoir vocation à être reproduites pour être commercialisées.
Elles vont représenter un avantage économique et donc à ce titre vont constituer un marché.

On le voit dès à présent la propriété industrielle a une vocation bien plus économique que la propriété littéraire et artistique. Elle est là essentiellement pour stimuler l’innovation.

Le postulat de départ est que l’innovation est un facteur de bien-être socio-économique (comprendre : l’innovation participe à l’économie de la société et les gens aiment bien avoir des nouveaux objets, des nouvelles technologies). Or, si on ne protège pas l’innovation, les entreprises qui innovent vont rapidement se faire copier et perdre des parts de marché. En effet, schématiquement, le prix du produit pour une entreprise qui fait de l’innovation, correspond, entre autre chose, au coût des matériaux et à celui de la R&D (Recherche et Développement) de ce produit. Si une autre entreprise copie ce produit, le prix de vente sera équivalent au coût des matériaux (le même) et à l’ingénierie inverse (acheter le produit, le démonter et comprendre comment il peut être reproduit).

On note ici que la R&D est bien plus onéreuse qu’une « simple » ingénierie inverse. Le « copieur » pourra vendre le même objet (rigoureusement le même : même qualité, même matériaux …) mais moins cher.
Que va faire le consommateur ? Là, le postulat est qu’il va acheter le moins cher.

C’est en considérant ces deux postulats que les règles de la propriété industrielle ont été construites. Bien entendu, ces deux postulats peuvent être critiqués. D’un côté, l’innovation n’est peut-être pas toujours un facteur de bien-être socio-économique. Peut-être que trop d’innovation tue l’innovation. D’un autre côté, le consommateur peut aussi vouloir privilégier l’entreprise qui est à l’origine du produit car il considère qu’elle doit être valorisée ou qu’en cas de défaut de l’objet, l’entreprise qui a fait la R&D sera plus à même d’en comprendre la cause.
Mais l’objectif de cet article n’est pas de s’étendre sur la critique de ces postulats.

Le droit organise donc la protection de l’innovation par la protection des créations industrielles. Celle-ci prend la forme d’un monopole économique temporaire (20 ans pour les inventions brevetées et 25 ans pour les dessins et modèles déposés).

Attention, il n’est pas question ici de droit moral ou de droit de paternité, le monopole est purement économique.

Par ailleurs, vous vous en doutez, toutes les créations industrielles ne sont pas protégées. Elles ne sont d’ailleurs pas toutes susceptibles de protection : il y a des critères à respecter.

Nous l’avons vu, pour avoir un droit d’auteur sur une œuvre de l’esprit, il faut que celle-ci soit **originale** c’est-à-dire qu’elle doit porter, en elle, l’empreinte de la personnalité de l’auteur. Du côté de la propriété industrielle, le critère qui va différencier une création protégée et une création non protégée, c’est la nouveauté. La nouveauté est un critère objectif (au contraire de l’originalité qui est un critère subjectif) et c’est un critère que l’on peut dire absolu.

Qu’est donc que ce critère de la nouveauté ? Supposons que je fasse une création industrielle à un instant donné. Cette invention sera considérée comme nouvelle si elle n’est pas comprise dans **l’état de la technique**. Qu’est-ce donc que l’état de la technique  ? C’est assez simple. C’est l’ensemble des créations industrielles auxquelles on peut avoir accès, peu importe la manière. C’est le cas des créations déjà protégées (invention brevetée, modèle déposé…) mais aussi des créations qui n’ont pas été déposées, mais qui ont été divulguées au public (sur un site internet, dans une publication scientifique, ou qui tout simplement sont commercialisées). En résumé, une création industrielle sera nouvelle (et donc susceptible de faire l’objet d’un monopole économique) si et seulement si, elle est différente de toutes les autres créations dont on peut avoir connaissance.

De plus, au contraire de la propriété littéraire et artistique, pour avoir un droit sur une création industrielle, il faut effectuer une formalité de dépôt. Cela veut dire qu’il faut aller à l’INPI (institut national de la propriété industrielle) présenter son invention, qui doit remplir les critères de protection, et enfin s’acquitter des frais relatifs au dépôt (compter environ 5 000 euros en moyenne).

Je le répète encore car c’est souvent oublié : en propriété industrielle, le dépôt est **OBLIGATOIRE** pour obtenir un droit. En propriété littéraire et artistique
il n’est **PAS OBLIGATOIRE**.
Du côté de la preuve de ce droit, en propriété industrielle, le titre qui vous est décerné après dépôt vaut preuve du droit de propriété, alors qu’en propriété littéraire et artistique, il est généralement conseillé de recourir à l’enveloppe Soleau.
Notons que cette enveloppe n’est pas un dépôt, c’est simplement une preuve de l’existence de la création, qui doit être **originale** pour bénéficier de la protection du droit d’auteur.

**Là est donc tout le cœur du sujet. Si je ne dépose pas mon invention ou mon design, alors je n’ai pas de droit de propriété intellectuelle sur cette invention ou sur cette forme de produit.** Si je n’ai pas de droit de propriété, cette création ne m’appartient pas, je ne peux donc pas en faire ce que je veux. Je ne peux pas, par exemple, choisir une licence libre. Ce serait alors, nous l’avons déjà dit, comme décider de mettre la voiture de son voisin en location sans son accord.
Pour choisir une licence libre, il faut alors déposer un titre de propriété industrielle (un brevet ou encore un dessin ou modèle) pour pouvoir imposer aux autres, certains comportements (citer le nom, ne pas en faire un usage économique…). C’est en réalité assez simple : sans monopole économique sur une création, je ne peux pas à moi tout seul contrôler l’économie de cette création. Alors il faut payer pour obtenir le droit et ensuite le repartager. Le modèle ne semble pas très rentable. On peut donc supposer que peu de personnes se lanceront dans cette aventure.
Mais ce n’est pas tout. Si je documente (publie) cette invention, sur fablabo.net par exemple, alors elle n’est plus nouvelle. Elle n’est donc plus brevetable. Ni par moi, ni par personne d’autre. C’est une création libre au sens premier du terme. Elle entre dans le domaine public des créations industrielles.
**Cela veut dire que tout le monde peut utiliser cette invention, la reproduire, la commercialiser, la remixer… sans vous en avertir et sans vous devoir quoi que ce soit.** Donc, tout dépend de la volonté de l’inventeur. S’il souhaite avoir un monopole économique sur cette invention, alors il est important qu’il comprenne cette notion de nouveauté. En revanche, une invention ne se résume pas seulement à un monopole économique, parfois, elle est plus que cela. Lorsque Tim Berners-Lee invente le World Wide Web, il a la possibilité de breveter cette invention. Il aurait alors eu un monopole économique de 20 ans sur le www. Là n’a pas été son choix. Il a délibérément décidé d’en faire « cadeau » à l’humanité. C’est également le cas de Didier Pittet, qui est trop peu connu au regard du nombre de vies qu’il sauve encore tous les jours. C’est l’inventeur du gel hydro-alcoolique, ce petit gel d’alcool, qui sert à nous désinfecter les mains. Monsieur Pittet aurait pu viser le monopole économique ! Seulement, [il a préféré en faire don à l’humanité](http://tcrouzet.com/le-geste-qui-sauve/cp_fr/). Dans nos pays occidentaux, ce n’est probablement pas grand-chose mais dans les pays dans lesquels l’accès à l’eau pour se laver les mains est difficile, cela représente beaucoup.
Au regard de ces exemples, on constate que la meilleure solution n’est pas toujours la protection des inventions. Seulement, faire don de ses inventions à l’humanité n’apporte pas nécessairement de pain sur la table de l’inventeur. Pourtant, que ce soit l’une ou l’autre, les inventions que nous avons citées en exemple, représentent toutes deux de gros marchés. Beaucoup de gens vivent grâce à ces inventions et l’inventeur ne touche rien ! N’y a-t-il pas là une incohérence ? C’est ce que nous pensons, mais en l’état actuel du droit nous ne pouvons pas faire grand-chose. Il y a, ça et là, des initiatives dont nous avons parlé et que nous encourageons. Cela étant, nous pensons qu’il faut une réflexion de fond sur le financement des inventeurs et l’accès à leur invention. La seule solution du monopole n’est pas suffisante, il faut penser à de nouvelles économies alternatives.
Documenter une création industrielle dans un fablab, c’est augmenter l’état de la technique et l’appropriation des techniques par le plus grand nombre. C’est faire le choix du domaine public industriel. C’est un très beau geste mais le maker doit en être conscient, car avoir un droit d’auteur sur la documentation ne suffit pas à protéger la création industrielle et c’est selon nous, trop peu connu dans nos labs.
Nous ne savons pas de quoi demain sera fait et quelles évolutions juridiques vont apparaître. En revanche, nous savons qu’il ne faut pas attendre que le droit change. Créer de bonnes lois est un processus lent et donc souvent en retard par rapport aux pratiques. Il faut continuer à expérimenter, à faire, à démontrer qu’il existe d’autres solutions que la propriété privée. Nous en appelons à nos makers pour inventer le monde de demain.
Voilà nous avons fait le tour de la propriété intellectuelle et des liens qu’elle peut avoir avec la documentation dans les fablabs. C’est un sujet bien complexe, on s’en rend compte ! C’est pourquoi nous essayons de sensibiliser les utilisateurs qui parfois ne sont pas contiens de ces enjeux. Souvent, la question de la propriété intellectuelle se pose trop tard et il est difficile de revenir en arrière. Alors, n’hésitez pas à nous contacter ! Nous allons continuer à travailler sur le sujet et à ce titre, toutes les expériences sont bonnes à prendre
On vous souhaite à toutes et tous une bonne documentation, libre !

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.