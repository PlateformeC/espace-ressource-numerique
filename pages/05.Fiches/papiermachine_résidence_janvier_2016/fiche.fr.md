---
title: 'Papier/Machine : résidence janvier 2016'
media_order: papiermachinecouverture.png
type_ressource: text
feature_image: IMG_0902.jpg
license: none
date: '30-01-2016 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'papier machine'
        - Papier/Machine
        - résidence
    author:
        - 'Charlotte Rautureau'
aura:
    pagetype: article
    description: 'Retour sur la résidence artistique Papier/Machine accueillie au fablab Plateforme C du 18 au 23 janvier 2016.'
    image: papiermachinecouverture.png
metadata:
    description: 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 18 au 23 janvier 2016.'
    'og:url': 'https://ressources.pingbase.net/fiches/papiermachine_r%C3%A9sidence_janvier_2016'
    'og:type': article
    'og:title': 'Papier/Machine : r&eacute;sidence janvier 2016 | PiNG Ressources Num&eacute;riques'
    'og:description': 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 18 au 23 janvier 2016.'
    'og:image': 'https://ressources.pingbase.net/fiches/papiermachine_r%C3%A9sidence_janvier_2016/papiermachinecouverture.png'
    'og:image:type': image/png
    'og:image:width': '646'
    'og:image:height': '512'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Papier/Machine : r&eacute;sidence janvier 2016 | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 18 au 23 janvier 2016.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/papiermachine_r%C3%A9sidence_janvier_2016/papiermachinecouverture.png'
    'article:published_time': '2016-01-30T00:00:00+01:00'
    'article:modified_time': '2021-01-12T16:34:46+01:00'
    'article:author': Ping
---

## Découverte et installation

![IMG_0902.jpg](IMG_0902.jpg?forceresize=100%)

Voilà quelques mois que nous la préparions avec Yanaita Araguas de Grante Ègle et le jour J est enfin arrivé. Lundi 18 janvier, 10h, froid de canard au fablab, la résidence Papier/Machine démarre en douceur autour d’un café. En guise d’introduction de la semaine, je présente l’association PiNG, la dynamique fablab ainsi que le projet Papier/Machine et ses objectifs. Nous poursuivons cette session introductive par un tour de l’atelier et de ses machines afin de se familiariser petit à petit avec cet univers somme toute assez nouveau pour les résidents.

Retour dans l’espace détente pour que chacun partage sa vision de la résidence et ses envies d’expérimentation pour la semaine:
- **Yanaita Araguas** aimerait faire du pop up, de l’impression à partir de formes en papier, de la tampographie ;
- **Geoffroy Pithon** voudrait travailler à partir du dessin et de sa réinterprétation via les machines ;
- **Maxime Prou** souhaite changer de matériau de création et travailler le bois ;
- **Julia Wauters** pense investir la découpe et l’impression sur bois, travailler en volume, profiter d’une liberté plus grande sur le format que d’habitude ;
- **Florence Boudet**, souhaite réinventer des outils d’écriture (le dessin lettre est très dépendant de l’outil), créer des matrices d’impression à partir des chutes ;
- **Hugo Marchal**, vient pour explorer son format de travail, l’édition, avec les outils du fablab pour faire ce qu’il a l’habitude de faire: un objet livre.

Les idées ne manquent pas et la phase à venir d’expérimentation s’annonce d’ors-et-déjà riche. En attendant de se lancer dans le grand bain des tests, une étape cruciale attend les résidents: la formation aux machines du fablab. Coachés par Laurent puis Maëlle, respectivement fabmanager et animatrice du lieu, ils vont apprendre à utiliser les machines suivantes: découpeuse laser, découpe vinyle, fraiseuse et routeur. Le choix est fait de ne pas se former à l’imprimante 3D qui, de prime abord, semble présenter un moindre intérêt pour les travaux de la semaine et qui, par ailleurs, demande un temps d’appropriation trop conséquent pour le format de la résidence. Grâce à la pédagogie dont font preuve les animateurs du lieu, les résidents ne ressortent pas complètement effrayés par les machines à la fin de la formation mais plutôt éclairés pour la suite et toujours aussi motivés, voire plus.

En fin de journée, nous instaurons le temps de bilan qui aura lieu chaque jour de la résidence comme un moment nécessaire pour croiser les travaux et réflexions.

## Expérimentations

![IMG_0948.jpg](IMG_0948.jpg?forceresize=100%)

La deuxième phase de la résidence se déroule du mardi matin au samedi matin inclus. Pendant quatre jours et demie, les résidents prennent place dans le fablab. C’est le cas de le dire puisqu’ils seront presque seuls toute cette semaine tant le froid est saisissant. Si cela amoindrit les croisements avec la communauté, cela présente par ailleurs un avantage : les machines seront (presque) toujours accessibles.

Chacun a carte blanche pour utiliser les machines comme il l’entend et tester telle ou telle technique. Le choix de ne pas produire de projet collectif pour cette résidence ouvre le champ des possibles et promet une pluralité de propositions, de tests et d’expérimentations qui vient servir les objectifs du projet Papier/Machine. Exempts de cette contrainte de production collective, les résidents peuvent explorer plus librement et découvrir le travail des uns et des autres.

Plus ou moins timidement, selon les tempéraments mais aussi les process de création des uns et des autres, ils vont faire connaissance avec les machines de l’atelier et se lancer dans les tests et les expérimentations. Certains se lanceront dès le premier jour, d’autres préféreront s’imprégner du lieu et attendre quelques jours. Ces machines “high tech”, parfois très éloignées de leurs pratiques, peuvent, on le comprend aisément, impressionner. Il sera même parfois tentant de ne pas passer par la machine et d’avoir recours à leurs outils habituels. Par ailleurs, et c’est un paramètre à ne pas négliger, toute utilisation de machine, tout recours à un nouveau matériau ou une nouvelle technique induit une phase de test et de calibrage non négligeable. Si cela fait partie du processus d’expérimentation cela peut également se révéler limitant. Ainsi, les résidents feront parfois des choix de technique et machine en lien avec cette contrainte au détriment d’une autre machine ou technique peut être plus pertinente mais plus chronophage dans cette phase test ou dont l’utilisation se révèle plus technique.

Au fil des jours, les résidents perçoivent de mieux en mieux le potentiel et les limites des machines ce qui aide à préciser leurs projets. Découvrez ci-après leur évolution, résident par résident.

![yana_semaine-1024x724.png](yana_semaine-1024x724.png?forceresize=100%)
![geoffroy_semaine-1024x724.png](geoffroy_semaine-1024x724.png?forceresize=100%)
![florence_semaine-1024x724.png](florence_semaine-1024x724.png?forceresize=100%)
![hugo_semaine-1024x724.png](hugo_semaine-1024x724.png?forceresize=100%)
![maxime_semaine.png](maxime_semaine.png?forceresize=100%)
![julia_semaine.png](julia_semaine.png?forceresize=100%)

## Partage

![IMG_0728.jpg](IMG_0728.jpg?forceresize=100%)

L’un des objectifs de « Papier/Machine » est d’amener différentes communautés de pratiques à se croiser au sein du fablab. Quoi de mieux pour ce faire, que de participer à l’OPENatelier le temps de la résidence ? C’est ainsi que le jeudi après-midi, résidents et adhérents ont partagé l’atelier et leurs pratiques le temps de quelques heures. Les croisements se sont faits de façon informelle. Nombreux sont les adhérents qui sont venus poser des questions aux résidents, notamment sur la sérigraphie qui n’est pas une pratique habituelle à Plateforme C. Le déploiement des spirographes a aussi été l’occasion de croiser les pratiques du graphisme et de l’électronique. Certains adhérents se sont prêtés au jeu, surtout attirés par l’aspect robot et l’envie de les booster un peu. À chacun ses motivations. Ce moment a été l’occasion pour les résidents de faire l’expérience de l’importance de l’échange et du transfert de savoirs revendiquée dans les fablabs et qui, jusqu’à présent, s’était incarnée pour eux principalement via le wiki de l’atelier, faute de personnes fréquentant l’atelier gelé. Diversité des pratiques, des approches, des savoirs et des échanges, voilà ce qui les a interpellés.

Enfin, pour ne pas garder la richesse de cette expérience juste pour PiNG et les résidents, nous avons proposé un temps de restitution le samedi après-midi autour d’un goûter. Une quarantaine de personnes, petits et grands, sont venues voir le résultat de cette semaine d’expérimentation graphique au fablab.  De quoi conclure de façon conviviale cette semaine de travail.

## Bilan et perspectives

Nous sortons d’une semaine de forte émulation où des résidents, aux profils et approches artistiques différentes, se sont emparés du fablab pour tester à tout va, s’approprier les machines, utiliser des techniques différentes, échanger. Si l’on se fie à la diversité des travaux menés et à la satisfaction affichée des résidents, on peut conclure que notre intuition de départ était bonne et qu’il faut continuer à la creuser: les fablabs peuvent être des espaces intéressants de création graphique. Mais essayons d’aller plus loin dans ce bilan….

### Un nouvel environnement à découvrir…
Débarquer dans un fablab n’a rien d’anodin. Il s’agit là d’une nouvelle forme d’atelier, d’un nouvel environnement de travail, de nouveaux outils à s’approprier (parfois très techniques), d’une nouvelle culture également. Il faut donc du temps pour prendre ses marques, trouver sa place et comprendre le potentiel de ce lieu pour ses travaux. Fonction des caractères, certains sauteront le pas très vite et testeront rapidement des machines. D’autres prendront plusieurs jours avant de franchir le pas.

### Le rapport aux machines…
La plupart des résidents ont une démarche créative artisanale dans laquelle la machine, l’outil informatique, n’a que peu de place. On peut, dès lors, comprendre aisément qu’ils aient fait preuve d’une certaine timidité face aux machines à commande numérique du fablab et aient mis quelques jours à se lancer. S’ils avaient bien compris comment elles fonctionnaient dans les grandes lignes  et avaient une vision assez claire de leur potentiel, le frein était principalement technique: peur d’une prise en main complexe, blocage sur les outils numériques (utilisation de code…), crainte de résultats décevants, interrogations sur la lenteur du process et donc sur sa pertinence face à leurs outils habituels. Rassurez-vous, ils ont fini par se lancer pour finalement se rendre compte que les machines n’étaient pas si complexes et développer une soif de test exponentielle. Si au début de la semaine ils se sont dit à de nombreuses reprises *“pourquoi je m’embête avec ces machines ? Je vais prendre mon cutter et ma planche de découpe et faire comme d’habitude”*, à la fin de la résidence le process était inversé et leur démarche avec, au point de vouloir massicoter à la découpeuse laser!

### Les machines testées…
La grande gagnante de la semaine, la machine chouchou des résidents, est – roulements de tambour -…… La découpeuse laser! C’est, en effet, une machine dont la prise en main s’avère assez intuitive et qui se prête bien au travail de l’image, du tracé. Elle crée même une sorte de gourmandise chez les résidents: un test en appelle un autre, décuple les possibilités, les chutes d’un projet resservent à un autre… C’est sans fin et les places étaient chers pour utiliser cet outil.

Si peu de personnes ont exploré les potentiels de la fraiseuse, c’est certainement parce que celle-ci nécessite plus de réglages et fait donc davantage peur. D’après les retours de Florence, qui l’a particulièrement expérimentée, la machine n’est pas si complexe et présente un potentiel très fort, peut-être même plus grand que la découpeuse laser car les paramètres de réglages sont plus fins et la palette d’outil plus large. Le champ des possibles est vaste et ouvert et, là encore, la gourmandise se crée.

Par contre, l’engouement n’est plus là quand on évoque la découpe vinyle. Lente, peu performante, avec une découpe relative du cutter, la machine n’a pas séduit. Alors que les résidents pensaient s’en servir beaucoup – de fait, elle semblait moins impressionnante et plus proche d’outils auxquels ils avaient déjà eu affaire comme des imprimantes –  ils l’ont, pour la plupart, finalement vite abandonnée. Selon eux, il est plus rapide et efficace de faire ce qu’elle fait directement avec leurs propres outils. La machine 0, outils artisanaux 1.

### Outil et œuvre…
Le choix de l’outil impacte forcément l’œuvre et les machines du fablab vont donc tendre à la modifier. Pour certains, le fablab permet de créer des objets dans une précision fine quand, dans leurs pratiques habituelles, il y a toujours une part d’imprécision dans le format de l’objet final. Cela crée un résultat précis, assez inhabituel. Pour d’autres, cet aspect n’est pas toujours valable. Malgré la précision que proposent les machines, la définition de leurs paramètres dépend de l’auteur de l’œuvre. Il peut, dès lors, modifier les paramètres, les fausser, mentir à la machine et ainsi ajouter une part d’aléatoire au rendu final dans lequel on retrouvera une empreinte différente de la machine. Par ailleurs, pour un même effet recherché, chaque machine pourra proposer des rendus différents. Enfin, la machine peut parfois échapper au créateur et produire des effets nouveaux, pas forcément prévus ou désirés, sur son œuvre. L’aléa demeure.

### Plateforme C, un atelier finalement assez adapté…
L’intérêt pour les résidents d’évoluer au sein de Plateforme C dépasse le fait d’avoir accès à de nouvelles machines performantes. En effet, ce type d’atelier semble parfait pour accueillir cette communauté créative. Il y a comme une jonction évidente entre les pratiques artistiques de nos résidents et les pratiques de bricolage. Ainsi, l’atelier propose un confort certain avec tout ce qu’il faut sous la main pour créer. Par ailleurs, les résidents disposent ici d’un grand espace pour travailler, ce qui n’est pas le cas dans leurs ateliers respectifs, et peuvent ainsi travailler des formats plus grands qu’habituellement. Ajoutez à cela de nouveaux matériaux à triturer et vous comprenez que le fablab propose un espace propice au travail de nos créateurs. Le fablab demeure pour eux, cependant, davantage un espace pour expérimenter des process, tester de nouveaux outils, plus que pour produire un livre.

### L’hybridation des cultures…
Les résidents se sont retrouvés plongés dans la culture des fablabs cad la culture libre, le partage des connaissances, la documentation, la réappropriation de la technique, ce qui fait l’esprit et les valeurs de ce type d’atelier.

Le transfert des connaissances et la (ré)appropriation de la technique se sont fait en partie via l’accompagnement des animateurs du lieu. Il s'est agi davantage de mettre en autonomie les résidents et de les encourager à prendre en main les machines plutôt que de faire à leur place. L’objectif sous-jacent à cette approche est de faire des usagers du lieu des acteurs et non des consommateurs, de futurs relais de leurs savoirs à la communauté. Ici, chaque individu est porteur d’un savoir qu’il est invité à partager avec les autres. Les résidents ont pu constater la validité de cette approche quand divers adhérents habituels sont venus les éclairer et les accompagner sur l’usage de telle ou telle machine. Cet échange de savoirs au sein de la communauté a particulièrement été prégnant lors de l’OPENatelier du jeudi, temps fort de la vie du fablab où une cinquantaine de personnes bricolaient ensemble le temps d’un après-midi. Le reste de la semaine, le lieu a plutôt été désert du fait du froid saisissant qui régnait dans l’atelier et les croisements rares, par conséquent. Ceci étant, les résidents ont eux-même expérimenté cette dynamique en échangeant entre eux leurs réglages et tuyaux.

Le transfert des savoirs passe aussi par la documentation des projets que les gens font au fablab sur le wiki fablabo.net. On y retrouve des ressources précieuses sur l’usage des machines, les réglages à faire. Cette volonté de partage a, cependant, ses limites. Les tutoriels et autres explications ne sont pas toujours à jour: les machines évoluent, les tutoriels moins. Par ailleurs, si la volonté de partager existe vraiment, l’accès aux connaissances n’est pas toujours évident. Il faut un bagage minimum pour comprendre certains tutos (code, technique….). À chaque pratique son jargon, reconnaîtront les résidents qui ont eux-même leurs techniques et le vocabulaire lié à celles-ci.

Enfin, côté culture libre, l’usage des logiciels libres de création graphique n’a pas été évident pour les résidents. Difficile pour eux de travailler directement avec ceux-ci et beaucoup sont passés dans un premier temps par illustrator pour créer leurs dessins. Le temps de résidence étant compté, nous n’avons pas obligé les résidents à prendre en main de nouveaux logiciels pour leurs créations. Cela souligne cependant qu’il y a des choses à creuser de ce côté là, des formats d’appropriation à inventer certainement. Reste que les résidents ont bien dû utiliser des logiciels libres une fois sur les machines de l’atelier et que ça n’a pas été exempt de surprise, le passage d’un logiciel propriétaire à un libre créant des décalages dans leur fichier.

### Les sentiments à la fin de la résidence…
À la fin de la semaine, les résidents sont à la fois…
…. heureux d’avoir vécu cette expérience collective et appuyer sur pause dans leurs pratiques quotidiennes pour en explorer de nouvelles,
…. frustrés car une semaine c’est court, qu’il fallait du temps pour se plonger dans ce nouvel univers, et que, par conséquent, il ont encore envie de tester plein de nouvelles choses, d’utiliser de nouveaux matériaux maintenant qu’ils ont bien compris les machines. Ils resteraient bien un petit peu plus.
… transformés, pour certains. Une des résidentes fait le parallèle avec la musique: c’est comme si elle avait eu un nouvel instrument avec une prise en main rapide et que celui-ci lui ouvrait des champs nouveaux et multiples. Cette expérience amène certains résidents à penser différemment leurs travaux et les outils qu’ils utilisent comme un bouleversement finalement assez radical.  Certains pensent revenir au fablab, prendre un abonnement. Pour eux, l’aventure ne fait que commencer semble-t-il….

### La suite ?
Après cette semaine, il reste un goût d’inachevé et on aimerait bien poursuivre ce travail de résidence. À court terme, cela semble compliqué au regard des contraintes des uns et des autres mais pourquoi pas plus tard? Au-delà de la résidence, la pertinence du projet Papier/Machine étant confirmée, il reste à PiNG à définir la suite du projet pour les mois à venir. Workshops avec les écoles partenaires du fablab, contenu pédagogique pour des stages en direction des enfants, nouvelles résidences, développement d’outils graphiques DIY pour l’atelier par les adhérents…. Les pistes ne manquent pas. À suivre donc…

## La résidence en vidéo

<iframe width="100%" height="600" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/949acb42-c657-40a8-a407-f44f4b272844" frameborder="0" allowfullscreen></iframe>

---
### Autres ressources

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation technique de la résidence par ici : [www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)

**▸ Les retours d’expérience des résidents**

Retrouvez ci-dessous les différents retours d’expérience publiés par les résidents sur leurs sites, blogs ou pages facebook.

– [Le retour de Florence sur ces 6 jours d’expérience](http://www.flo-flo.fr/blog/papiermachine-journal-de-bord/)
– [Les photos de Yana](https://www.facebook.com/media/set/?set=a.495283624010435.1073741835.171803009691833&type=3)
– [Le lien d’atelier McLane](http://ateliermcclane.com/Culture-Club)
– [L’article de Geoffroy](http://formes-vives.org/blog/index.php?2016/01/28/848-papier-machine-dcoupe-laser)
– [Le post Facebook de l’Atelier Bingo](https://www.facebook.com/atelierbingo/posts/934748169935278)

```
