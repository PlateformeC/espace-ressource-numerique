---
title: 'Gravure sur plexiglass pour éclairage led'
media_order: gravure-plexi-led-web2.jpg
date: '13-12-2020 00:00'
taxonomy:
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'fraiseuse carvey'
    - 'carte postale'
    - 'Expérimentation matériaux'
    - 'workshop'


    author:
        - 'Yanaïta Araguas'
aura:
    pagetype: article
    description: ‘Gravure sur plexiglass éclairée par une led. Une expérimentation dans le cadre d'un workshop à Plateforme C avec l'ESBAN’
    image: gravure-plexi-led-web2.jpg
    category:
        - pratiques

---

##  Gravure sur plexiglass pour éclairage led

![gravure-plexi-led-web1.jpg](gravure-plexi-led-web1.jpg?forceresize=100%)
![gravure-plexi-led-web2.jpg](gravure-plexi-led-web2.jpg?forceresize=100%)
![gravure-plexi-led-web3.jpg](gravure-plexi-led-web3.jpg?forceresize=100%)


Le projet d'exploration Papier/Machine a accueilli, en novembre 19, 22 étudiant·es de l’École des Beaux-Arts Nantes St Nazaire pour expérimenter les potentiels de création graphique du fablab autour de la série et de la carte postale.

Voici l'une des expérimentations réalisées.

L'étudiant a travaillé sur une plaque de plexiglass usinée à la fraiseuse* pour produire une surface gravée, irrégulière, qui puisse accrocher la lumière.
L'idée était de l'éclairer avec des leds afin de rendre quasi « phosphorescent » le dessin.
Nous n'avons pas pu voir le résultat final mais vous retrouvez ci-dessous le principe souhaité (avec un dauphin, désolé!)
(trouvé sur le site https://creation-plexi.com)
![Unknown.jpg](Unknown.jpg?forceresize=100%)

* Fraiseuse : une fraiseuse est une machine-outil utilisée pour usiner tous types de pièces mécaniques, à l'unité ou en série, par enlèvement de matière à partir de blocs ou parfois d'ébauches estampées ou moulées, à l'aide d'un outil coupant nommé fraise. En-dehors de cet outil qui lui a donné son nom, une fraiseuse peut également être équipée de foret, de taraud ou d'alésoir.
Plus d'infos : [wikipedia](https://fr.wikipedia.org/wiki/Fraiseuse)

### Autres ressources
Matériaux utilisés :
Plexiglass

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx
