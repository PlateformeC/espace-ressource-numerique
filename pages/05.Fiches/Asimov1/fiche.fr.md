---
title: 'ASIMOV1 (imprimante 3D)'
media_order: Asimov.JPG
type_ressource: text
feature_image: Asimov.JPG
license: cc-by-nc-sa
date: '01-01-2016 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - 'impression 3D'
    author:
        - PiNG
aura:
    pagetype: article
    description: 'Cette machine à commande numérique sert à l''impression 3D. Elle est située au fablab Plateforme C.'
    image: Asimov.JPG
metadata:
    description: 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'og:url': 'https://ressources.pingbase.net/fiches/asimov1'
    'og:type': article
    'og:title': 'ASIMOV1 (imprimante 3D) | PING Ressources Num&eacute;riques'
    'og:description': 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'og:image': 'https://ressources.pingbase.net/fiches/asimov1/Asimov.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '1161'
    'og:image:height': '861'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'ASIMOV1 (imprimante 3D) | PING Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine &agrave; commande num&eacute;rique sert &agrave; l''impression 3D. Elle est situ&eacute;e au fablab Plateforme C.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/asimov1/Asimov.JPG'
    'article:published_time': '2016-01-01T00:00:00+01:00'
    'article:modified_time': '2020-12-02T09:21:42+01:00'
    'article:author': Ping
---

*Cette machine à commande numérique sert à l'impression 3D. Elle est située au fablab Plateforme C.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Asimov1?classes=btn)
