---
title: '[MAKER À COEUR] Antoine, le designer produit'
media_order: antoine.png
type_ressource: video
feature_image: antoine.png
license: cc-by-nc-sa
date: '01-12-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Nous vous présentons Antoine : designer œuvrant à Plateforme C qui nous parle de son activité professionnelle au sein du fablab !'
    image: antoine.png
metadata:
    description: 'Nous vous pr&eacute;sentons Antoine : designer &oelig;uvrant &agrave; Plateforme C qui nous parle de son activit&eacute; professionnelle au sein du fablab !'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_antoine_le_designer_produit'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Antoine, le designer produit | Espace Ressources Num&eacute;riques'
    'og:description': 'Nous vous pr&eacute;sentons Antoine : designer &oelig;uvrant &agrave; Plateforme C qui nous parle de son activit&eacute; professionnelle au sein du fablab !'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_antoine_le_designer_produit/antoine.png'
    'og:image:type': image/png
    'og:image:width': '956'
    'og:image:height': '537'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Antoine, le designer produit | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Nous vous pr&eacute;sentons Antoine : designer &oelig;uvrant &agrave; Plateforme C qui nous parle de son activit&eacute; professionnelle au sein du fablab !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_antoine_le_designer_produit/antoine.png'
    'article:published_time': '2017-12-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:44:08+01:00'
    'article:author': Ping
---

*Nous vous présentons Antoine : designer œuvrant à Plateforme C qui nous parle de son activité professionnelle au sein du fablab !*

<iframe width="100%" height="450" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/638f7940-4127-48e8-af6f-789775609509" frameborder="0" allowfullscreen></iframe>
