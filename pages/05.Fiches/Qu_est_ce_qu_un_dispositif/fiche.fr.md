---
title: 'Agamben, Qu''est ce qu''un dispositif ?'
media_order: agamben.jpg
type_ressource: text
feature_image: agamben.jpg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - essai
        - dispositif
        - technosciences
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: website
    description: 'Quelle stratégie devons-nous adopter dans le corps à corps quotidien qui nous lie aux dispositifs ?'
    image: agamben.jpg
show_breadcrumbs: true
metadata:
    description: 'Quelle strat&eacute;gie devons-nous adopter dans le corps &agrave; corps quotidien qui nous lie aux dispositifs ?'
    'og:url': 'https://ressources.pingbase.net/fiches/qu_est_ce_qu_un_dispositif'
    'og:type': website
    'og:title': 'Agamben, Qu''est ce qu''un dispositif ? | Espace Ressources Num&eacute;riques'
    'og:description': 'Quelle strat&eacute;gie devons-nous adopter dans le corps &agrave; corps quotidien qui nous lie aux dispositifs ?'
    'og:image': 'https://ressources.pingbase.net/fiches/qu_est_ce_qu_un_dispositif/agamben.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '512'
    'og:image:height': '249'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Agamben, Qu''est ce qu''un dispositif ? | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Quelle strat&eacute;gie devons-nous adopter dans le corps &agrave; corps quotidien qui nous lie aux dispositifs ?'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/qu_est_ce_qu_un_dispositif/agamben.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-24T15:04:19+01:00'
    'article:author': Ping
---

## Résumé :

Face à la prolifération des dispositifs dans nos existences - du téléphone portable à la télévision, de l'ordinateur à l'automobile, précise la quatrième de couverture - Agamben opère un retour à Foucault afin de répondre à la question *"Quelle stratégie devons-nous adopter dans le corps à corps quotidien qui nous lie aux dispositifs ?"*

## Notes de Meven :

Agamben revient donc au concept de *dispositif*. Celui-ci émerge dans les années 1970, alors que les débats philosophiques se concentrent notamment sur la critique des institutions (prison,  hôpital, asile, État, école, sexualité...), et qu'un  certain nombre d'auteurs se focalisent sur la question du *fonctionnement technique* des relations de pouvoir (Deleuze, Lyotard, et particulièrement Foucault qui s'occupe alors de la gouvernementalité, du gouvernement des hommes).

Si Foucault ne donne jamais une définition précise de ce que recouvre le terme de dispositifs, Agamben la repère dans un entretien de 1977 :

*"Ce que j'essaie de repérer sous ce nom c'est [...] un  ensemble résolument hétérogène comportant des discours, des  institutions, des aménagements architecturaux, des décisions  réglementaires, des lois, des mesures administratives, des énoncés  scientifiques, des propositions philosophiques, morales,  philanthropiques ; bref, du dit aussi bien que du non-dit, voilà les  éléments du dispositif. Le dispositif lui-même, c'est le réseau qu'on  établit entre ces éléments […] par dispositif, j'entends une sorte –  disons – de formation qui, à un moment donné, a eu pour fonction majeure de répondre à une urgence. Le dispositif a donc une fonction  stratégique dominante... J'ai dit que le dispositif était de nature  essentiellement stratégique, ce qui suppose qu'il s'agit là d'une  certaine manipulation de rapports de force, d'une intervention  rationnelle et concertée dans ces rapports de force, soit pour les  développer dans telle direction, soit pour les bloquer, ou pour les  stabiliser, les utiliser. Le dispositif est donc toujours inscrit dans  un jeu de pouvoir, mais toujours lié aussi à une ou à des bornes de  savoir, qui en naissent, mais tout autant le conditionnent. C'est ça le  dispositif : des stratégies de rapports de force supportant des types de savoir et supportés par eux."*

Il résume cet extrait en trois points :

*" 1) Il s'agit d'un ensemble hétérogène qui inclut virtuellement chaque  chose, qu'elle soit discursive ou non : discours, institutions,  édifices, lois, mesures de police, propositions philosophiques. Le  dispositif pris en lui même est le réseau qui s'établit entre ces  éléments.
2) Le dispositif a toujours une fonction stratégique concrète et s'inscrit toujours dans une relation de pouvoir.
3) Comme tel, il résulte du croisement des relations de pouvoir et de savoir. "*

Après ce premier temps de définition, Agamben propose de tracer une généalogie du terme de dispositif, d'abord à l'intérieur de l'oeuvre de Foucault, puis dans un contexte historique plus large, remontant jusqu'à l'*oikonomia* théologique (en grec administration de l'*oikos*, de la maison) ayant donnée le *dispositio* latin. Il explique sa fonction dans la théologie chrétienne et dans le dogme d'un gouvernement divin providentiel du monde. En dernière instance, le lien qui rassemble tous les termes est pour Agamben le renvoi à une *économie* soit *"un ensemble de praxis, de savoirs, de mesures, d'institutions, dont le but est de gérer, de gouverner, de contrôler, d'orienter (…) les gestes et les pensées des hommes"*

Agamben propose alors un partage de l'être en deux classes, les êtres vivants et les dispositifs aux seins desquels ils ne cessent d'être saisis et "entre les deux, comme tiers les sujets (…) ce qui résulte de la relation, du corps à corps entre les vivants et les dispositifs". Pour lui en effet, les dispositifs produisent leurs sujets, sont le lieu de processus de subjectivation. Comme un même individus peut être le lieu de plusieurs processus de subjectivation, un même objet, par exemple le téléphone, peut produire de multiples sujets : l'internaute, l'utilisateur de téléphone, etc.

Paraphrasant Marx, Agamben qualifie la phase extrême du capitalisme que nous vivons comme *"une gigantesque accumulation et prolifération de dispositifs"*, qui sont autant de machines de gouvernement. Il s'accorderait avec les auteurs (anonymes) de l'ouvrage [*La Cassure*](https://www.editionsdivergences.com/produit/la-cassure/), pour lesquels *"disposer une vie, de tout temps, c'est avant tout l'insérer dans  un régime d'évidences qui façonne nos regards et nos gestes, et neutralise la puissance de la vie : son pouvoir-être-autre".*

Enfin, il conclue en mettant en évidence le problème qui selon lui nous fait face, la nécessité de la *"profanation des dispositifs"*, devant rendre ingouvernables ceux qui se montreraient capables d'intervenir aussi bien sur les processus de subjectivation que sur les dispositifs.

## Citer l'ouvrage :
Agamben, G. (2014). Qu’est-ce qu’un dispositif ? Rivages.
