---
title: 'Pratiques numériques et éducation populaire'
media_order: jquelo_003.png
type_ressource: text
feature_image: jquelo_003.png
license: cc-by-sa
date: '18-11-2013 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'éducation populaire'
        - 'pratiques numériques'
    author:
        - 'Jocelyne Quélo'
aura:
    pagetype: website
    description: 'Jocelyne Quélo, directrice de l’Espace Jean-Roger Caussimon (Tremblay-en-France), nous parle du lien entre pratiques numériques et éducation populaire.'
    image: jquelo_003.png
metadata:
    description: 'Jocelyne Qu&eacute;lo, directrice de l&rsquo;Espace Jean-Roger Caussimon (Tremblay-en-France), nous parle du lien entre pratiques num&eacute;riques et &eacute;ducation populaire.'
    'og:url': 'https://ressources.pingbase.net/fiches/pratiques_numeriques_et_education_populaire'
    'og:type': website
    'og:title': 'Pratiques num&eacute;riques et &eacute;ducation populaire | Espace Ressources Num&eacute;riques'
    'og:description': 'Jocelyne Qu&eacute;lo, directrice de l&rsquo;Espace Jean-Roger Caussimon (Tremblay-en-France), nous parle du lien entre pratiques num&eacute;riques et &eacute;ducation populaire.'
    'og:image': 'https://ressources.pingbase.net/fiches/pratiques_numeriques_et_education_populaire/jquelo_003.png'
    'og:image:type': image/png
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Pratiques num&eacute;riques et &eacute;ducation populaire | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Jocelyne Qu&eacute;lo, directrice de l&rsquo;Espace Jean-Roger Caussimon (Tremblay-en-France), nous parle du lien entre pratiques num&eacute;riques et &eacute;ducation populaire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/pratiques_numeriques_et_education_populaire/jquelo_003.png'
    'article:published_time': '2013-11-18T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:12:42+01:00'
    'article:author': Ping
---

*Jocelyne Quélo, directrice de l’Espace Jean-Roger Caussimon (Tremblay-en-France), nous parle du lien entre pratiques numériques et éducation populaire.*

Dans l’idée d’apporter un premier point d’ancrage à ma pensée sur la relation entre éducation populaire et pratiques numériques pour débuter ce texte, je me suis penchée sur la notion d’éducation populaire rédigée collectivement sur Wikipédia.

« L'action des mouvements d'éducation populaire se positionne en complément de l'enseignement formel. C'est une éducation qui dit reconnaître à chacun la volonté et la capacité de progresser et de se développer, à tous les âges de la vie. Elle ne se limite pas à la diffusion de la culture académique, elle reconnaît aussi la culture dite populaire (culture ouvrière, des paysans, de la banlieue…). Elle s'intéresse à l'art, aux sciences, aux techniques, aux sports, aux activités ludiques, à la philosophie, à la politique. Cette éducation est perçue comme l'occasion de développer les capacités de chacun à vivre ensemble, à confronter ses idées, à partager une vie de groupe, à s'exprimer en public, à écouter… » (1)

Je notais déjà là les premiers liens entre pratiques numériques et éducation populaire où ce processus prend corps dans les interconnections humaines, culturelles, techniques, qui se voient facilitées et démultipliées par les outils numériques. En effet, pouvant autant emprunter à la sociologie, à la psychologie comportementale, aux sciences appliquées, à l’histoire, aux arts plastiques, à la scène…, les expressions numériques proposent un dialogue entre l’imaginaire humain et le sens de nos réalités. L'enjeu est alors d'étendre, de déployer, d'explorer des possibilités, et ainsi de donner matière à penser, plutôt que de chercher une finalité induisant un objectif fixe préétabli, éminemment réducteur.

Car, « la question de l’accès ne résout pas celle de l’appropriation ; la diffusion massive des objets techniques ne débouche pas sur l’uniformisation des pratiques... Il faut envisager la question des usages dans une ”double dimension, micro-sociologique (pratique et représentation des objets techniques) et macro-sociologique (matrices culturelles et socio-politiques)“ » (2) disait déjà Laurence Hamon en 2002, alors qu’elle était doctorante au laboratoire de Recherche sur le langage (LRL) de l’université Blaise Pascal, Clermont 2.

Dans la démarche de construction de l’identité, les technologies numériques - et notamment l’Internet - offrent une autre opportunité de découvrir par soi-même le monde qui nous entoure, en dehors des systèmes éducatifs classiques. Cette sphère peut être investie comme étant un champ d’expressions et d’expérimentations où chacun est amené, à son propre rythme, vers une connaissance fondée sur l'expérience et l'observation. Partant de la singularité des modes d’apprentissage, il s’agit alors de favoriser les conditions nécessaires au développement de la créativité.

Aussi, en s’appuyant sur les principes actifs « s’inspirer, réfléchir, construire, montrer et confronter », différentes propositions qui croisent plusieurs domaines d’expressions artistiques et culturelles sont souvent proposées, comme autant d’entrées possibles vers une appropriation des outils et du langage de la culture numérique, de la culture artistique, et de la culture en général à différents niveaux. Dans cette perception qui aborde tant les enjeux de l'économie du savoir dans sa globalité environnementale, sociale et économique, la culture s’entend dès lors au sens d’un comportement social. Un comportement « libre », fondé sur le partage des connaissances et la hiérarchie de la contribution, sur la certification par les pairs et la formation permanente...

Mais revenons à la définition de Wikipédia : « L’éducation populaire se différencie aujourd’hui de l’animation socio-culturelle de par sa mission culturelle, qui pourrait être définie autour de deux axes principaux : la diffusion culturelle, qui vise à permettre au plus grand nombre l’accès à la culture générale, et la valorisation de la culture collective, qui consiste à reconnaître la culture comme moyen d’émancipation et de citoyenneté. » (3)

L’ouverture et la réciprocité inscrites au cœur des actions de médiation, des propositions de pratiques collectives et projets transversaux des lieux d’éducation populaire – physiques ou virtuels, nomades ou sédentaires – permettent la transformation des individus en communautés actives d’amateurs. C’est pourquoi l’approche des pratiques numériques s’inscrit ici dans une mise en perspective curieuse et exploratrice, une approche qui vise à donner les outils non seulement pour comprendre les interfaces et les modes de production contemporains mais aussi anticiper ou amorcer ceux en devenir. L’ensemble des actions proposées prend ainsi sens dans une démarche empirique, car il s’agit de permettre à chacun de la faire sienne, par l’extension, la contextualisation et la recherche.

Pour ce faire, trois angles d’approche me paraissent difficilement dissociables : - la sensibilisation et le partage des connaissances autour d’un sujet donné, qui peuvent se matérialiser par des propositions de courte durée dans lesquelles s’inscrivent les publics (ateliers d’expérimentation en groupe ou en famille, présentation de dispositifs interactifs, rencontres entre œuvres, artistes, chercheurs, théoriciens et publics), - le développement de projets créatifs à moyenne durée qui prennent pour essences les publics et les territoires et impliquent les participants dans un processus créatif (résidences de création collective, projets éducatifs en milieu scolaire et péri-scolaire),
- la mutualisation des ressources et des contenus dans une démarche prospective à longue durée, afin d’enrichir, détourner, ouvrir à la re-création et aux partages des expériences et des connaissances.

Je prendrai ici pour exemple un projet mené en 2013 au sein de l’Espace Jean-Roger Caussimon de Tremblay-en-France. « Ayant accueilli le Graffiti Research Lab (fr) en résidence durant une semaine, ce temps de présence a permis à des publics diversifiés de découvrir leur travail à travers exposition et espace de création ouvert, de se questionner sur l’obsolescence des médias, le détournement technologique, les expressions artistiques urbaines... Certains se sont alors inscrits dans un atelier de fabrication de bombes de lumière et d’autres au sein d’un stage croisant hip hop et light painting. Ce dernier croisement a amené artistes, chercheurs et danseurs amateurs en présence à imaginer ensemble une interface de graffitis en temps réel intégrée à l’espace chorégraphique, point de départ à un projet de création collectif. Enfin, à l’occasion de la présentation publique de ce projet, s’est opérée la rencontre improbable entre le hip hop et la percussion africaine, engendrant de nouveaux désirs. Un deuxième temps de création en compagnie d’artistes et de chercheurs a alors vu le jour, permettant dès lors d’allier percussions augmentées fabriquées par les participants, graffitis temps réel et hip hop. » (4) Une création en re-création…

Une complémentarité d’actions qui permet ainsi à chacun de s’inscrire dans une démarche collective, participative et active, engendrant des principes d’ouverture - vers l’autre, vers d’autres savoirs, d’autres espaces de croisements, de frottements… - et d’inscription citoyenne, où l’engagement professionnel rencontre celui de l’amateur, du curieux ou du passionné.

Entre découverte de l’autre, inscription en tant qu’individu actif et projection collective, je terminerai ce petit laïus par cette question : le rôle de l'éducation populaire dans sa relation aux pratiques numériques est-il d'introduire de la cohésion sociale dans le monde tel qu'il est ou bien d’ouvrir à penser conjointement la transformation sociale ?


[Jocelyne Quelo](http://jocelynequelo.fr/)

---
- 1-3 [Éducation populaire, Wikipédia](http://fr.wikipedia.org/wiki/%C3%89ducation_populaire)
- 2 Analyse de Apprentissage des langues et technologies : usages en émergence, Alsic, 2002
- 4 Création numérique & lien social, Magazine des cultures digitales #72, 2013
