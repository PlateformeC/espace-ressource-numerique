---
title: 'Chroniques d’un Atelier Partagé #1'
media_order: IMG_1090-870x550.jpg
type_ressource: text
feature_image: IMG_1090-870x550.jpg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/chroniques_dun_atelier_partage_2
    -
        page: /fiches/chroniques_dun_atelier_partage_3
    -
        page: /fiches/chroniques_dun_atelier_partage_4
    -
        page: /fiches/chroniques_dun_atelier_partage_5
date: '13-06-2018 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'atelier partagé'
        - low-tech
        - réparation
        - chroniques
    author:
        - 'Édouard Bourré-Guilbert'
aura:
    pagetype: article
    description: 'Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé.'
    image: IMG_1090-870x550.jpg
metadata:
    description: 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:url': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_1'
    'og:type': article
    'og:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #1 | PING Ressources Num&eacute;riques'
    'og:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_1/IMG_1090-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #1 | PING Ressources Num&eacute;riques'
    'twitter:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_1/IMG_1090-870x550.jpg'
    'article:published_time': '2018-06-13T00:00:00+02:00'
    'article:modified_time': '2021-01-08T16:47:55+01:00'
    'article:author': Ping
---

*Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. En tant qu’explorateur associé de PiNG, Édouard nous livre son regard sur l’aventure de l’Atelier Partagé du Breil, que l’association anime depuis maintenant 3 ans. Une façon de nous faire découvrir ce qui compose un tel lieu : ses figures, ses temps forts, ses zones de tensions et ses petits miracles.*

À l’Atelier Partagé du Breil, qu’il soit précis ou indécis, ambitieux ou maladroit, on y vient avec un projet.

## ÉPISODE 1 : POURQUOI ON Y VIENT

Si j’avais su.

Ma venue à l’Atelier Partagé du Breil est un coup fomenté depuis une bonne quinzaine d’années. Tout a commencé par l’obtention du brevet des écoles. Persuadé que j’avais vécu l’enfer, j’exige une rétribution à la hauteur de l’effort fournit et obtiens de mes parents l’achat d’une chaîne-HiFi qui accompagnera fidèlement mes premiers émois musicaux. Et puis, l’arrivée de Napster, Emule et autres comparses du dématérialisé me fera bouder le CD au profit du MP3. Malgré cela, cette chaîne va me suivre tout au long des déménagements jusqu’à finir stockée durant une expatriation québécoise. À mon retour, c’est le grand tri, mais l’affect m’empêche de m’en séparer. D’autant que le support CD étant boudé par tout le monde, je chine régulièrement des pépites, sans parler de ce label indépendant que j’accompagne dans son développement et qui m’envoie sa production sur ce même support. Ma chaîne retrouve donc une place centrale dans mon salon. Problème, le lecteur CD « saute » ce qui rend l’écoute chaotique. Je suis un peu démuni face à ce souci pour lequel il est désormais inenvisageable de faire marcher la garantie. Seulement voilà, je n’ai jamais été un grand aventurier quand il s’agit d’ouvrir seul un appareil électronique. La réparation professionnelle étant écartée pour des raisons de coûts disproportionnés, les copains compétents habitant trop loin pour faire plus qu’un diagnostic, me voilà tiraillé par la volonté de trouver une solution locale sans savoir comment m’y prendre.
J’ai bien dans un coin de la tête ce concept de Repair Café, glané au gré d’une lecture sur l’obsolescence programmée. Alors voilà, 1+1 faisant la somme qu’on sait, j’associe mon problème à cette potentielle solution, je range le tout dans ce même coin de tête, déjà bien encombré par d’autres additions du même genre, et je passe à autre chose.

Jusqu’au jour où un ami me parle de son envie de fabriquer une éolienne et m’invite à l’accompagner lors des portes ouvertes du fablab nantais animé par PiNG où il avait engagé des rencontres autour de son projet. A notre arrivée, nous sommes accueillis par Alice, en service civique, qui nous fait faire le tour du lieu. On y découvre des imprimantes 3D en pleines activités mais également une station de découpe laser et bien d’autres machines et outillage à disposition des adhérents. L’ambiance est bonne, je me lance donc naturellement à raconter ma vie et surtout celle de ma chaîne-HiFi. Alice m’aiguille alors sur un autre endroit animé par PiNG, l’Atelier Partagé du Breil. Elle m’invite à m’y rendre le mardi suivant qui sera justement consacré à la réparation d’appareils. Elle me présente aussi un certain Jean-Pierre, adhérent qui saura sûrement m’accompagner et qui se trouve justement là ce soir même. Quelques mots encourageants plus tard avec ledit Jean-Pierre, me voilà prêt à débarquer le mardi suivant dans cet atelier partagé.

Ainsi, tout commence par un projet. Ce sera d’ailleurs la première question qui me sera posée en arrivant à l’Atelier Partagé par Thomas, coordinateur du lieu : « C’est quoi ton projet ? ». Le mien consistera à réparer cette platine CD. Pourquoi ? Probablement pour une raison affective tout autant qu’économique, écologique et sociale.
Consacrer ses mardis après-midi à l’atelier implique un emploi du temps souple. Ça tombe bien, c’est une période légère dans mon activité que j’exploite à faire ce que je repoussais depuis tant de temps, la réparation de cette chaîne étant en haut de la pile. Et ce sera chose faite en l’espace d’une séance de 3 heures, accompagné de plusieurs bonnes volontés. La solution ? Le potentiomètre, caché derrière le lecteur optique, qui prend la forme d’une vis cruciforme. Tourner légèrement le composant permet d’accentuer la puissance du lecteur optique. Après intervention, on aurait pu faire passer un troupeau d’éléphants à côté de cette chaîne que le CD n’aurait pas sauté. Voici donc la piste suivie sous les conseils du fameux Jean-Pierre, après concertation avec Thomas, sous mon regard inquiet de néophyte de l’optique, la main crispée sur le tournevis. Et ça marche toujours, presqu’un an après l‘opération. Tout cela sera d’ailleurs documenté sur un wiki afin d’avoir l’archive à disposition pour un prochain adhérent ou un internaute cherchant des informations sur un problème similaire.

Alors évidemment, réussir une réparation, ça stimule. On revient avec d’autres appareils qu’on aimerait faire revivre de nouveau. Et donc autant de projets. Et puis, entre deux trous percés, deux soudures loupées, on s’intéresse aux projets des autres, habitués ou pas des lieux. Il y a autant de projets que de profils, ce qui donne un air singulier à chaque mardi. On voit passer des grille-pains, des vélos, des fers à repasser, des machines à coudre et autres radios de tout genre. Mais pas que. La réparation est l’un des axes porté par l’atelier. Ce n’est pas le seul. D’autres semaines, d’autres possibilités, comme celle d’un rendez-vous couture, d’un atelier d’installation LINUX, d’une présentation sur la sécurité des données sur Internet ou encore d’une tentative de grainothèque. Un événement n’en chassant pas un autre, l’atelier est ouvert à tous aux mêmes horaires chaque semaine et ce quelque soit la programmation. Il est donc probable que vous franchissiez le seuil de cette porte en vous demandant ce que l’on fabrique tous ici, tellement la première image sera hétéroclite. À l’Atelier Partagé du Breil, on n’a pas peur du mélange des genres, on l’encourage.
Vous avez un projet ? Venez nous en parler mardi après-midi prochain ! Il y aura même du café.
