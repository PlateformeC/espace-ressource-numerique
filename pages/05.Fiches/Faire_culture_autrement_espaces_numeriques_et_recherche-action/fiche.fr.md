---
title: 'Faire culture autrement, espaces numériques et recherche-action'
media_order: 'img_4098.jpg,tierslieux2-1130x650.jpg'
type_ressource: text
feature_image: tierslieux2-1130x650.jpg
license: cc-by-sa
date: '18-12-2013 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture numérique'
        - recherche-action
        - 'espaces numériques'
    author:
        - 'Hugues Bazin'
aura:
    pagetype: website
    description: 'Par Hugues Bazin, chercheur en sciences sociales au Laboratoire d''Innovation Sociale par la Recherche-Action.'
    image: tierslieux2-1130x650.jpg
metadata:
    description: 'Par Hugues Bazin, chercheur en sciences sociales au Laboratoire d''Innovation Sociale par la Recherche-Action.'
    'og:url': 'https://ressources.pingbase.net/fiches/faire_culture_autrement_espaces_numeriques_et_recherche-action'
    'og:type': website
    'og:title': 'Faire culture autrement, espaces num&eacute;riques et recherche-action | Espace Ressources Num&eacute;riques'
    'og:description': 'Par Hugues Bazin, chercheur en sciences sociales au Laboratoire d''Innovation Sociale par la Recherche-Action.'
    'og:image': 'https://ressources.pingbase.net/fiches/faire_culture_autrement_espaces_numeriques_et_recherche-action/tierslieux2-1130x650.jpg'
    'og:image:type': image/webp
    'og:image:width': '1130'
    'og:image:height': '650'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Faire culture autrement, espaces num&eacute;riques et recherche-action | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Par Hugues Bazin, chercheur en sciences sociales au Laboratoire d''Innovation Sociale par la Recherche-Action.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/faire_culture_autrement_espaces_numeriques_et_recherche-action/tierslieux2-1130x650.jpg'
    'article:published_time': '2013-12-18T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:02:46+01:00'
    'article:author': Ping
---

*Par Hugues Bazin, chercheur en sciences sociales au Laboratoire d'Innovation Sociale par la Recherche-Action.*

## Culture ou technique, l’innovation sociale à quel service ?
Il y aurait une façon de considérer la culture numérique comme une forme culturelle classique dématérialisée par des supports techniques. Le problème c’est que la sphère politique ou décisionnelle ne semble voir que cet aspect technique et avant tout communicationnel qu’il suffirait de développer ou de contrôler à l’exemple des réseaux sociaux. D’où une fracture culturelle qui se caractérise par de nombreux malentendus. Ainsi, la culture hacker peut être comprise comme une recomposition créative d’éléments existants dans une logique collaborative alors que d’autres imagineront sous l’angle du piratage une zone grise du non-droit.

Si nous considérons autrement le flux numérique comme une **boucle processuelle entre dématérialisation et rematérialisation**, nous pouvons imaginer de nouveaux rapports entre dimensions individuelle et collective, privée et publique. Le caractère individualiste de la pratique numérique dématérialisée dans le rapport à l’informatique se rematérialise dans des formes d’investissements collectifs occupant de nouveaux espaces comme les tiers lieux. Une « communauté de solitudes » et une « culture de la multitude » ne sont alors finalement que les deux versants d’une nouvelle grammaire culturelle.

Comprenons que ce n'est pas la technique qui crée la culture, **c'est la culture qui s'empare de la technique pour répondre aux défis contemporains**. Les espaces publics numériques sont-ils des lieux techniques d’accès à Internet ou les lieux d’émergence d’une nouvelle culture ? Dans le premier cas nous ferions que reproduire les schémas de pensée d’action classique, dans le second nous serions dans une appréhension de la complexité des « tiers espaces ».

L’humain peut très bien vivre sans technologie, il ne peut pas vivre sans culture. Dire que l’être humain est avant tout un **« être culturel »** est inséparable de l’histoire de l’humanité. L’agriculture, les villes, les civilisations sont le fruit d’un « travail de la culture » qui n’a cessé de répondre aux besoins humains puisque ce qui caractérise avant tout notre condition est notre inaptitude à s’insérer dans un environnement, nous avons toujours besoin de « béquilles ». En cela, **une innovation n'a d'intérêt que si elle est socialisée**, c’est-à-dire réapproprier dans un contexte social, au service d'une utilité sociale. La technique est profondément un acte culturel qui vient pallier notre incomplétude.

Cela signifie également qu’il n’y a pas d’**« idiot culturel »**. Nous disposons tous d’un **capital social**, d’une **intelligence des situations**, une capacité de mobiliser des **compétences** et de développer une **auto expertise**. La culture est la résultante de ce processus qui permet de vivre en société et de la transformer, de se construire et s’émanciper en tant qu’individus et groupes.

## Un changement de paradigme sociétal

La logique d’auto-fabrication propre à la culture do-it-yourself où chacun construit chez soi sa propre culture « à la carte » n’est pas le propre des nouvelles technologies. Elles n’ont fait qu’accentuer le mouvement correspondant au besoin d’investir d’autres **circuits de développement** n’empruntant plus au modèle industriel qui avait façonné le siècle précèdent.   

Déjà dans les années 70 apparaissaient les premières fissures dans le **paradigme sociétal du « progrès perpétuel »** colporté par le modèle économique libéral. Les cultures punk et alternatives portaient les signes annonciateurs sur le terreau en décomposition des grands bassins d’emplois alors que l’on appelait encore « crise » temporaire (toujours aujourd’hui ?) l’amorce d’un changement profond de société.

Cependant, il y a une difficulté particulièrement française de prendre en compte ces mouvements qui viennent du « bas ». La culture numérique s’inscrit dans des **situations écosystémiques** dans le sens où les réponses ne peuvent se comprendre qu’en situation, dans un jeu de relations d'interdépendance qui suscite ses propres formes de régulations. C'est à l'image d'une **« apiculture politique »** basée sur une intelligence sociale des intérêts réciproques qui instaurent des liens inédits entre les individus et leurs compétences indépendamment de leur appartenance.

La plupart des institutions fonctionnent toujours selon le modèle vertical républicain d’ascension s’appuyant sur une logique d’**accompagnement social**, celle de l’éducation populaire d’après-guerre. D’où la création d’une pléiade de métier cherchant à combler les « trous » par la professionnalisation d’un processus du travail de la culture, de solidarité et de développement local. Cela ne peut que favoriser le corporatisme de ces nouveaux corps de métier, mais ne valide en rien l’émergence de nouveaux champs de compétences et la capacité de comprendre ces situations comme des alternatives politiques.

## Le surgissement d’une culture du bricolage

Or, c’est justement dans ces « trous » que se logent les espaces interstitiels d’une recomposition sociale. La culture numérique est de l’ordre du **surgissement**, non de l’accompagnement. C’est une culture propre à la **forme du bricolage**. Elle est liée aux contingences des situations : l’indétermination, l’incertitude, l’aléatoire, le hasard sont des éléments constitutifs. Il n'y a pas d'intentionnalité initiale orientée par une méthode ni d’attente particulière en termes de productivité. Le bricolage ne cherche pas à diriger d’autres personnes et imposer une finalité. Le bricolage s’instaure rarement dans les lieux officiels, mais plutôt dans des espaces interstitiels. C’est une **approche qui se structure à partir des situations humaines** dans leur hétérogénéité et leur incomplétude. Elle permet paradoxalement au mieux de répondre aux situations complexes, car elle n'est pas tributaire des logiques disciplinaires. Effectivement, nous sommes généralement formés par nos champs d'appartenance socioprofessionnelle qui ne permettent plus de penser la réalité autrement.

La forme du bricolage appartient à une démarche que les logiques méthodologiques institutionnelles sont incapables de reconnaître. Au lieu de partir d'un cadre pré-établi, ce sont les matériaux qui nous guident dans le sens où il n’y a pas de formes prédéterminées. C’est le **dialogue avec les matériaux** qui donnent une forme et celle-ci prend sens une fois visible dans la relation entre un contenu et un contenant à l’instar de la forme des tiers lieux où des fab-labs.  La  réintroduction au centre d’une **« culture du geste »** participe d’une compréhension empirique et inductive des situations.

Cette **maîtrise d’usage** partant des espaces se construit transversalement à partir de la capacité auto réflexive d’autoformation et d’auto expertise des praticiens. Elle s’oppose à une ingénierie de projet basé sur les pouvoirs verticaux du technicien. Cela bouleverse les postures socioprofessionnelles dans la relation entre **« agent-auteur-acteur »**. Par exemple, le processus collaboratif de la culture numérique bouscule le statut d’« auteur » dans sa capacité à créer de nouveaux référentiels partagés. Il interroge également la posture des « agents » missionnés dans leur capacité à réformer les institutions de l’intérieur plutôt que simplement reproduire la commande institutionnelle. L’« acteur » n’est alors plus celui qu’on sollicite comme caution sous une forme participative, il devient réellement **coproducteur d’un processus d’auto fabrication** dont il maîtrise le sens de toutes les étapes jusqu’à la production finale.

Nous sommes dans un processus éminemment **démocratique**. Le caractère non programmé propre à ces formes de surgissement échappe aux logiques de surveillance généralisée. C’est donc un mode opératoire particulièrement efficace dans les pays autoritaires lorsqu’il s’agit d’occuper l’espace public à travers la propagation « virale » de nouvelles pratiques. Partout, le surgissement interroge tout autant les formes de gouvernance en panne ou obsolètes en créant de nouvelles **interfaces** entre des processus **instituant** et des fonctionnements **institués**.

## Les archipels de l’expérience humaine, une éthique de la reliance

Bien souvent, les formes **d’expérimentation populaire** n’accèdent pas à une visibilité et une intelligibilité, car elle ne trouve pas leur traduction dans le champ disciplinaire institutionnel et les modes de décision politique.

La conséquence est qu’il existe des pans entiers de l’expérience humaine, des **« tiers espaces »** de l’expérience en somme, qui restent dans l’angle mort de la connaissance. On peut définir le tiers espace comme un lieu non attribué, non fonctionnalisé et donc particulièrement ouvert pour accueillir une diversité humaine et d’expériences. C’est un espace qui se construit à partir de la maîtrise d’usage, dans la manière de répondre aux situations. C’est-à-dire qu’il **se définit en son centre, par un fonctionnement écosystémique** et non par les bords d’une intervention professionnelle. C’est en cela un espace qui « pousse du milieu ». Ces centralités multiples entre lesquels nous nous déplaçons n’apparaissent dans aucune cartographie territoriale et font pourtant partie de notre territoire de l’expérience. Elles dessinent de **nouvelles mobilités spatiales, mentales et sociales**. Les parties visibles comme les tiers-lieux finissent par **former un archipel** laissant deviner un réseau immergé qui les relie. Nous touchons un enjeu politique majeur qui est de savoir où se forment aujourd’hui les **espaces du commun** qui ne sont pas les lieux affinitaires occupés par des « tribus » ou les espaces marchands. Dans la pratique, seuls de nouveaux espaces du commun seraient capables de dépasser l’ethnicisation qui caractérise actuellement nos rapports sociaux en dépassant l’ « identité racine » pour une **« identité relation »**.

Cette **« reliance »** est une autre manière d’évoquer une **pensée de l’archipel**. Une éthique de la reliance impose le dialogue avec les matériaux, les usagers, les contextes et s’harmonise donc bien avec les **pratiques résilientes** (auto-réparatrices) du bricolage. Elle s’inscrit dans une démarche transdisciplinaire susceptible de prendre en compte cette complexité et de penser la culture numérique comme un fait social total, une forme complète, non comme une addition de pratiques disciplinaires dans des lieux séparés. Cela implique de construire des espaces à partir des usages du numérique, être guidé par le surgissement au lieu de partir des projets des lieux existants pour y inclure du numérique en termes d’accompagnement. Cette forme « a-disciplinaire », voire « indisciplinée » correspond à une **architecture fluide**, un mode de structuration qui touche l’ensemble des domaines de la vie et pourrait également contribuer à de bâtir une autre ville.

Ces « objets culturels non identifiés » ne sont pas destinés à devenir des « labels », peut-être est-ce bon de ne pas trop les cerner.  C'est une pensée politique de la culture qu’il s’agit plutôt de restaurer pour comprendre comment ces formes émergentes donnent un sens aux enjeux contemporains. Sans nécessairement inventer de nouveaux mots, il nous faudrait instaurer cette **grammaire de la pensée complexe**.

C'est la prise en compte systémique de la culture dans ces trois fonctions : **culture vivante** (comment vivre ensemble, instaurer des espaces du commun), la **culture transmise** (comme faire correspondre et transférer les compétences et les expériences sans passer obligatoirement par les schémas académiques de transmission) et la **culture symbolique** (comment construire une parole et des enjeux, c'est-à-dire de nouveaux référentiels qui servent de points de repère pour tous, accessibles et réappropriables par tous).

## Les laboratoires communautaires de la recherche-action

La recherche-action appartient à un tiers espace scientifique. Elle est donc la plus à même de travailler sur le tiers-espace de la culture numérique. Comme la culture numérique, c’est avant toute une pratique avant d’être un discours. C’est une **science de la pratique appliquée et impliquée**.

Cela permet de désacraliser la démarche de recherche qui n’est pas la propriété de la culture savante. La recherche-action part du postulat que la culture profane possède en elle-même les ressources pour produire une connaissance avec son propre langage et ses propres dispositifs. Le laboratoire social et la formation-action font partie de ces dispositifs.

On peut imaginer les **laboratoires sociaux** comme des fab-labs de la recherche capables de faire émerger des **« chercheurs collectifs »**. Ces groupes qui dépassent l’addition des compétences individuelles pour rejoindre une intelligence collective peuvent ainsi contractualiser un investissement dans des **ateliers coopératifs** articulés à une formation-action. La **formation-action** vise à mettre les acteurs en posture réflexive et tirer une connaissance des pratiques pour la réinvestir dans un processus de transformation répondant aux besoins actuels. C'est intégrer le principe de l'expérimentation issue du modèle scientifique dans un investissement en situation.

Le laboratoire social n’impose pas un point de vue surplombant sur la réalité, il s’appuie sur la réalité des tiers espaces. Il ne fait que créer les conditions d’une mise en mouvement des acteurs-chercheurs et d’une mise en visibilité d’une production de connaissance. Ce sont au départ des micro-réalisations, des formes d'organisation interstitielle. Mais un processus de changement n’a pas d’impact s’il n’y a pas dans le même mouvement des mots pour le penser. Il y a nécessité de **changer de paradigme pour penser l’action et les situations sociales** comme nous l’avons souligné en passant de la logique d'ingénierie vers celle du bricolage, de la logique de projet vers celle de maîtrise d'usage. C’est une manière de réintroduire dans le processus d'innovation l'erreur, le désordre, l’errance, l’incertitude. .. C’est donc se doter de **nouveaux outils d’évaluation** non en termes de finalité, mais par **la capacité à mettre en lien et en extraire une expérience partageable.**

L'important aujourd'hui est donc d'établir de **nouvelles correspondances**, de mettre en raisonnante une question soulevée dans un champ disciplinaire ou socioprofessionnel pour répondre à un problème dans un autre champ d’activité à l’exemple du principe open source de la culture hacker qui inspire de nouveaux modèles économiques. Il s’agit ainsi de **transférer des compétences** d’un domaine à l’autre en questionnant les corps de métiers existants. Il s’agit enfin de valoriser l’implication des **« passeurs de frontières »**, tous ces **« marginaux sécants »** sans lesquels les **minorités actives** perdraient leurs capacités d’influence.

La recherche-action permet donc à ces acteurs de s’automissionner et de légitimer leurs paroles en tant qu’**« acteurs-chercheurs »** pour qu’elle devienne audible dans l’espace public et interroge les modèles de gouvernance, de production et de consommation.
