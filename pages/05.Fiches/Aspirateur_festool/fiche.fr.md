---
title: 'Aspirateur Festool'
media_order: 759px-Aspi.jpg
type_ressource: text
feature_image: 759px-Aspi.jpg
license: cc-by-nc-sa
date: '01-01-2018 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: article
    description: 'Outil servant à aspirer les poussières de bois'
    image: 759px-Aspi.jpg
metadata:
    description: 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'og:url': 'https://ressources.pingbase.net/fiches/aspirateur_festool'
    'og:type': article
    'og:title': 'Aspirateur Festool | PING Ressources Num&eacute;riques'
    'og:description': 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'og:image': 'https://ressources.pingbase.net/fiches/aspirateur_festool/759px-Aspi.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '759'
    'og:image:height': '600'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Aspirateur Festool | PING Ressources Num&eacute;riques'
    'twitter:description': 'Outil servant &agrave; aspirer les poussi&egrave;res de bois'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/aspirateur_festool/759px-Aspi.jpg'
    'article:published_time': '2018-01-01T00:00:00+01:00'
    'article:modified_time': '2021-01-05T14:33:57+01:00'
    'article:author': Ping
---

*Cet outil sert à aspirer le bois et **uniquement** le bois ! Pour les autres déchets, n'utiliser que l'autre aspirateur (KARCHER), et pour les poussières de bois, n'utiliser que celui là (FESTOOL).*

Il est possible, et de ce fait, obligatoire, de le brancher sur les appareils électroportatifs (défonceuse, scie sauteuse, scie circulaire,...) pendant leur fonctionnement.

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Aspirateur_Festool?classes=btn)
