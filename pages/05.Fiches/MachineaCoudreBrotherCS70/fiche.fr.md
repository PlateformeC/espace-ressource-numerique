---
title: 'Machine à coudre BROTHERCS70'
media_order: Brother-cs70.jpg
type_ressource: text
license: none
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
        - couture
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cet outil sert à la couture.'
    image: Brother-cs70.jpg
metadata:
    description: 'Cet outil sert &agrave; la couture.'
    'og:url': 'https://ressources.pingbase.net/fiches/machineacoudrebrothercs70'
    'og:type': website
    'og:title': 'Machine &agrave; coudre BROTHERCS70 | Espace Ressources Num&eacute;riques'
    'og:description': 'Cet outil sert &agrave; la couture.'
    'og:image': 'https://ressources.pingbase.net/fiches/machineacoudrebrothercs70/Brother-cs70.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '800'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Machine &agrave; coudre BROTHERCS70 | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cet outil sert &agrave; la couture.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/machineacoudrebrothercs70/Brother-cs70.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:06:54+01:00'
    'article:author': Ping
---

*Cet outil sert à la couture.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/MachineaCoudreBROTHERCS70?classes=btn)
