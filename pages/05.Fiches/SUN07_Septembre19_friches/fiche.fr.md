---
title: 'Les doigts dans la prise #7 : les friches numériques'
type_ressource: audio
feature_image: friches_SUN_radio_PiNG.png
license: cc-by-nc-sa
published: true
date: '25-09-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Julien Bellanger'
aura:
    pagetype: article
    description: '*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: friches_SUN_radio_PiNG.png
metadata:
    description: '*Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://ressources.pingbase.net/fiches/sun07_septembre19_friches'
    'og:type': article
    'og:title': 'Les doigts dans la prise #7 : les friches num&eacute;riques | Espace Ressources Num&eacute;riques'
    'og:description': '*Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://ressources.pingbase.net/fiches/sun07_septembre19_friches/friches_SUN_radio_PiNG.png'
    'og:image:type': image/webp
    'og:image:width': '800'
    'og:image:height': '534'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #7 : les friches num&eacute;riques | Espace Ressources Num&eacute;riques'
    'twitter:description': '*Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun07_septembre19_friches/friches_SUN_radio_PiNG.png'
    'article:published_time': '2019-09-25T16:31:00+02:00'
    'article:modified_time': '2020-12-09T13:51:24+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-les-friches-numeriques) !

![Les doigts dans la prise - les friches numériques](7%20-%20Les%20doigts%20dans%20la%20prise%20-%20les%20friches%20numeriques.mp3)

Nous vivons dans ce qui est annoncé comme une époque en transition(s) (numérique, écologique, démocratique) au sein de laquelle les organisations collectives sont largement modifiées par un mouvement de mondialisation englobant. Cette mondialisation a provoqué et provoque encore différents exodes, reconfigurant l’urbanisation des villes.
De nouveaux concepts comme les « Smart City », centrés sur l’usage des technologies puis du « smart citizen » basé sur l’habitant-usager, ont fleuri ces dernières années. Cela se traduit depuis une dizaine d’années par des théories et pratiques très vite homogénéisées et étudiées, non sans que soit pointées du doigt les menaces sur la démocratie, l’arrivée de villes intelligentes sans flâneur, …
D’autre part, les services et activités du secteur culturel ont dans une certaine mesure été dirigés comme solutions de cohésion, de diversité, voire de mixité sociale. Parallèlement, la « culture » est devenue un facteur de développement, d’attractivité pour les villes. Un consensus s’est dégagé reconnaissant ‘la culture comme valorisation des territoires dans la concurrence urbaine’, et les politiques publiques s’activent depuis plus de trente ans dans ce sens. L’arrivée depuis peu du concept de ‘tiers lieux’ culturels apporte une nouvelle démonstration de l’usage des actions culturelles (jouant avec le trio : restauration rapide, bière locale et yoga, comme concept novateur) au profit d’une logique économique d’aménagement et d’une gentrification assumée.
On peut considérer que les lieux et institutions culturelles assimilent les émergences urbaines et proposent des espaces de médiation et d’action culturelle dont la diversité et l’autonomie serait à questionner. Je ferais l’analyse des activités décrites par la suite dans un contexte sociologique analysant le rapport aux publics, le lien avec les pouvoirs publiques et les enjeux de développement économique et social d’un territoire. La « culture » n’est pas uniquement celle des musées, des cinémas et opéras, mais aussi celle de lieux culturels pluridisciplinaires à la croisée des activités socio-culturelles, artistiques et sociales. De la structuration de l’offre des salles de ‘musiques actuelles’ aux friches culturelles, en passant par les squats artistiques, l’émergence de tels lieux a ouvert ce qui a été nommé une ‘nouvelle époque d’action culturelle’_1 .

Comment ré-ouvrir des interrogations déjà formulées par les travaux et études comme celui de Michel Dufour sur les friches culturelles. Comment le public s’approprie-t-il un lieu ? Sur quelle base la programmation s’effectue t-elle ? Comment associer adhérent, usager et public d’une activité associative ? Ces lieux ont-ils une réelle influence sur le quartier ou la ville dans lequels ils sont implantés ? Quelles professions et compétences entrent en action pour les animer ? Comment soutenir l’émergence de nouveaux espaces ou comment ré-interroger ceux existant ? Peut-on développer des politiques culturelles affranchies de toute logique marchande_2 ?…

SOURCES :
(1) : Friches, laboratoires, fabriques, squats, projets pluridisciplinaires : Rapport à M Michel Dufour, Secrétaire d’État au patrimoine et à la décentralisation culturelle par F. Lextrait et G. Groussard
(2) : M. Correia, « L’envers des friches culturelles », Revue Le Crieur, 2018. *

*Texte grafouillé par Julien Bellanger*
