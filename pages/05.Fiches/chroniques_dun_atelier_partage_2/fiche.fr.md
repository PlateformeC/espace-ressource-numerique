---
title: 'Chroniques d’un Atelier Partagé #2'
media_order: IMG_9635-1.jpg
type_ressource: text
feature_image: IMG_9635-1.jpg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/chroniques_dun_atelier_partage_1
    -
        page: /fiches/chroniques_dun_atelier_partage_3
    -
        page: /fiches/chroniques_dun_atelier_partage_4
    -
        page: /fiches/chroniques_dun_atelier_partage_5
date: '09-07-2018 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'atelier partagé'
        - low-tech
        - réparation
        - chroniques
    author:
        - 'Édouard Bourré-Guilbert'
aura:
    pagetype: article
    description: 'Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé.'
    image: IMG_9635-1.jpg
metadata:
    description: 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:url': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_2'
    'og:type': article
    'og:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #2 | PING Ressources Num&eacute;riques'
    'og:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'og:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_2/IMG_9635-1.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #2 | PING Ressources Num&eacute;riques'
    'twitter:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_2/IMG_9635-1.jpg'
    'article:published_time': '2018-07-09T00:00:00+02:00'
    'article:modified_time': '2021-01-08T16:56:12+01:00'
    'article:author': Ping
---

*Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. En tant qu’explorateur associé de PiNG, Édouard nous livre son regard sur l’aventure de l’Atelier Partagé du Breil, que l’association anime depuis maintenant 3 ans. Une façon de nous faire découvrir ce qui compose un tel lieu : ses figures, ses temps forts, ses zones de tensions et ses petits miracles.*

À l’Atelier Partagé du Breil, on se prépare à l’ère post-industrielle sans le savoir.

## ÉPISODE 2 : LE LIEU

Il est 14h tout pile aux abords du numéro 38 de la rue du Breil, à Nantes, comme probablement partout ailleurs où ce fuseau horaire fait la loi ! J’enclenche fermement mon frein à main et sors de mon véhicule, un grand carton dans les bras. Je longe une barre d’immeuble, une de celles que l’on a faites le siècle dernier en beige ou en saumon puis que l’on a rafraîchie récemment, lors d’un vaste programme d’isolation par l’extérieur, en y ajoutant des touches de couleurs vives. L’association PiNG n’est pas la seule à partager cet ensemble urbain. Au fur et à mesure de l’ascension de la cage d’escalier, des bruits m’indiquent une séance de gymnastique tonique sur ma gauche, probablement une répétition théâtrale sur ma droite. Arrivé au second étage, un panneau au milieu du couloir signale la fin de mon chemin ou le début de l’aventure, c’est selon. J’y suis presque et ça tombe bien car vous aussi.

Dès le couloir, il y avait un je-ne-sais-quoi d’années studieuses dans l’air. Sur le seuil de la porte, ça se confirme. Je découvre une pièce dont l’espace rappelle, par certains aspects, une salle de classe. Quatre rangées de tables parallèles les unes aux autres, complétées par un établi. Des étagères aux murs ou servant à délimiter des espaces. Un bric à brac d’objets, d’outils et de composants rangés à leurs places. Et au milieu de tout ça, trois personnes, affairées à leurs projets, avec en fond sonore la radio branchée sur les ondes de FIP.
Soudainement, d’un même pas, les trois se mettent à danser et à lancer des fleurs aux pétales délicats, telles des vahinés du Pacifique accueillant un invité de marque, et… puis je reprends mes esprits. L’un des trois lève la tête et m’accueille avec le sourire. C’est Thomas, coordinateur du lieu. Je suis également reconnu par Alice, qui m’avait aiguillé ici peu de temps avant, et Jean-Pierre, avec qui j’avais échangé sur mon problème à Plateforme C, le Fablab également animé par l’association et situé plus loin, sur l’île de Nantes. Après s’être présenté, Thomas me fait faire le tour de la pièce, me montre l’outillage à disposition en me racontant l’idée et l’actualité du lieu.

L’Atelier Partagé du Breil est lancé en janvier 2016. Avant ça, ce grand corps de bâtiment est une école. Une partie continuera d’ailleurs d’occuper cette fonction. Le reste sera réhabilité en pôle associatif, inauguré en 2008, dont PiNG deviendra l’un des occupants. A ce moment là, l’association axe son travail sur la démocratisation du multimédia et d’Internet. Et puis des « open-ateliers » ont déjà lieu ici et là, des temps de bidouillage qui vont muer en un Fablab géré par l’association de 2010 à 2013. La place finissant par manquer, le Fablab déménagera dans un hangar sans banane, laissant la pièce de l’actuel atelier en jachère. Et comme tout bon terrain laissé au repos, une herbe folle va en émerger. Alimentée par ces années d’expérimentations, l’association est animée par l’idée de développer un environnement propice au « low-tech ». Partant du constat que le Fablab attire et retient des personnes déjà à l’aise avec les technologies, elle envisage de reconvertir cette pièce devenue débarras en atelier commun pour les habitants du quartier. Et puis chemin faisant, le concept se concrétise et devient l’Atelier Partagé du Breil.

Cet atelier, dont le nom et l’usage s’inspirent des jardins partagés, a donc pour origine le concept de « low-tech ». Néologisme au sens assez large, il prend ses racines dans un mode de production pré-industrielle. Partant du principe que l’industrialisation de notre quotidien nous éloigne des technologies, le « low-tech » vise à reprendre la main sur ces dernières en invitant tout à chacun à produire au maximum ce dont il a l’usage, revenant à l’ingéniosité et à la sobriété de nos aïeux qui fabriquaient ce dont ils avaient besoin pour subsister. Cette idée, développée depuis le siècle dernier par les théoriciens d’une impasse industrielle tel que Ivan Illich, Ingmar Granstedt ou plus récemment Philippe Bihouix, invite à penser une ère post-industrielle ainsi qu’à interroger notre dépendance vis à vis d’une technologie « high-tech » présente dans tous les coins de notre quotidien.

Et dans les faits ? L’Atelier Partagé du Breil est un mille-feuille. Lieu de rencontre, il est vecteur du lien social. Lieu de programmation, il sensibilise aux questions du numérique. Lieu de réparation et de production, il permet une émancipation à l’égard des technologies. Derrière chacune de ces couches, des sujets émergent ou se confirment. Ainsi, quoi de plus naturel que d’interroger son prochain achat de grille-pain quand, après y avoir passé une après-midi, on constate qu’il n’est pas réparable. C’est alors l’éco-conception qui s’immisce dans notre réflexion de gourmand du pain grillé. Ou bien encore, quoi de plus normal que d’interroger le renouvellement de son téléphone quand les mises à jours ne se font plus. C’est la rencontre avec l’obsolescence programmée. Lorsque nous fabriquons un nouvel objet au sein de l’atelier, il apparaît de plus en plus naturel d’interroger son usage dans le temps. C’est ainsi que j’ai pu être le témoin d’un cas de ré-emploi insolite, celui d’un clitoris en résine réalisé avec l’imprimante 3D de l’atelier pour illustrer un cours de biologie, et qui trouva une seconde vie en tant que composant de boucle d’oreille !

L’Atelier Partagé du Breil illustre la prise en main d’une production locale et autonome, s’alimentant par les projets et les compétences de chacun, rendant in fine l’appropriation des technologies plus désirable, moins subie. Par effet ricochet, l’atelier invite à une certaine forme de sobriété en ressources car il interpelle chacun sur ses besoins et sa capacité à y répondre. Dans ce lieu de tous les possibles, l’efficacité n’est pas de mise, la démarche prévaut. Venez nous rejoindre mardi prochain, prenez votre après-midi ou débarquez après le boulot, on ferme à 21h.
