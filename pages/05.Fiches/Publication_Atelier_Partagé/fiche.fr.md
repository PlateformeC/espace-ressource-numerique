---
title: 'L''ATELIER PARTAGÉ DU BREIL, BRICOLAGE CONVIVIAL'
media_order: atelierpartage.jpeg
type_ressource: fichier
feature_image: atelierpartage.jpeg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/publication_pfc
    -
        page: /fiches/publication_slowtech
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
        - pratiques
        - recits
    tag:
        - fablab
        - réparation
        - tiers-lieux
    author:
        - 'Thomas Bernardi'
        - 'Charlotte Rautureau'
        - 'Yanaita Araguas'
aura:
    pagetype: article
    description: 'Le deuxième opus de la collection « Ateliers Ouverts » est consacré à l’expérience de l’Atelier Partagé du Breil, atelier de bricolage convivial en quartier.'
    image: atelierpartage.jpeg
show_breadcrumbs: true
metadata:
    description: 'Le deuxi&egrave;me opus de la collection &laquo; Ateliers Ouverts &raquo; est consacr&eacute; &agrave; l&rsquo;exp&eacute;rience de l&rsquo;Atelier Partag&eacute; du Breil, atelier de bricolage convivial en quartier.'
    'og:url': 'https://ressources.pingbase.net/fiches/publication_atelier_partag%C3%A9'
    'og:type': article
    'og:title': 'L''ATELIER PARTAG&Eacute; DU BREIL, BRICOLAGE CONVIVIAL | PiNG Ressources Num&eacute;riques'
    'og:description': 'Le deuxi&egrave;me opus de la collection &laquo; Ateliers Ouverts &raquo; est consacr&eacute; &agrave; l&rsquo;exp&eacute;rience de l&rsquo;Atelier Partag&eacute; du Breil, atelier de bricolage convivial en quartier.'
    'og:image': 'https://ressources.pingbase.net/fiches/publication_atelier_partag%C3%A9/atelierpartage.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1024'
    'og:image:height': '682'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'L''ATELIER PARTAG&Eacute; DU BREIL, BRICOLAGE CONVIVIAL | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Le deuxi&egrave;me opus de la collection &laquo; Ateliers Ouverts &raquo; est consacr&eacute; &agrave; l&rsquo;exp&eacute;rience de l&rsquo;Atelier Partag&eacute; du Breil, atelier de bricolage convivial en quartier.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/publication_atelier_partag%C3%A9/atelierpartage.jpeg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2021-01-12T12:11:48+01:00'
    'article:author': Ping
---

## Mode d’emploi

Le deuxième opus de la collection « Ateliers Ouverts » est consacré à l’expérience de l’Atelier Partagé du Breil, atelier de bricolage convivial en quartier. Il revient sur la première phase d’expérimentation du projet en nous en présentant les contours et les courants intellectuels qui ont inspiré cette initiative, de Grandstedt à Illich. Si le temps est l’allié de ce type d’expérience, nous espérons que ce livret en soi aussi un levier.

[TÉLÉCHARGER](https://station.pingbase.net/index.php/s/3MZSfx3QnjZKiHq?classes=btn-down)

[ACHETER](https://ping.odoo.com/shop/product/livre-bricolage-convivial-mode-d-emploi-8?classes=btn)

---
- Textes : Charlotte Rautureau & Thomas Bernardi
- Mise en page et illustration : Yanaita Araguas
