---
title: 'Documenter c’est partager ses connaissances 2/3'
media_order: IMG_9689.jpg
type_ressource: text
feature_image: IMG_9689.jpg
license: cc-by-sa
serie:
    -
        page: /fiches/documenter_partager_ses_connaissances_1
    -
        page: /fiches/documenter_partager_ses_connaissances_3
date: '12-06-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture libre'
        - opensource
        - 'c libre'
        - fablab
        - documentation
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: article
    description: 'Voici le deuxième article de la trilogie sur la documentation dans les fablabs. '
    image: IMG_9689.jpg
metadata:
    description: 'Voici le deuxi&egrave;me article de la trilogie sur la documentation dans les fablabs. '
    'og:url': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_2'
    'og:type': article
    'og:title': 'Documenter c&rsquo;est partager ses connaissances 2/3 | PiNG Ressources Num&eacute;riques'
    'og:description': 'Voici le deuxi&egrave;me article de la trilogie sur la documentation dans les fablabs. '
    'og:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_2/IMG_9689.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Documenter c&rsquo;est partager ses connaissances 2/3 | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Voici le deuxi&egrave;me article de la trilogie sur la documentation dans les fablabs. '
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_2/IMG_9689.jpg'
    'article:published_time': '2017-06-12T00:00:00+02:00'
    'article:modified_time': '2020-12-02T09:21:48+01:00'
    'article:author': Ping
---

*Voici le deuxième article de la trilogie sur la documentation dans les fablabs. Nous allons dans cet article, et le suivant, nous concentrer sur le lien entre documentation et propriété intellectuelle, et plus précisément ici, le lien avec la propriété littéraire et artistique.*

De manière générale, il est assez compliqué de comprendre les enjeux de la propriété intellectuelle lorsqu’on ne connaît pas les grandes lignes de ce domaine. Or, c’est un domaine assez complet et parfois complexe. Afin d’éviter de faire un cours de droit préalable, je vous propose d’attaquer  directement  le sujet et lorsque cela demandera des connaissances juridiques, je vous en ferai part au fur et à mesure (ce seront les paragraphes en italiques alignés à droite). Ce préalable posé, venons-en à notre sujet du jour.

Nous l’avons précisé, il y a à la fois l’ensemble de ce qui est documenté qui forme un tout (pour Plateforme C il s’agit du site [fablabo.net](http://www.fablabo.net/)) et il y a ce qui est documenté. Généralement ce qui est documenté ce sont :
– des projets
– des façons d’utiliser/réparer une machine
– et éventuellement des façons d’animer un lieu, un atelier…

Chacun de ces types de documentation intègre de la propriété littéraire et artistique et/ou de la propriété industrielle (objet de l’article suivant).

À tout seigneur, tout honneur, commençons par le plus général et aussi le plus facile. La majorité des fablabs se sont dotés d’un espace de documentation. Il s’agit en général d’un wiki qui, aux yeux du droit, constitue une base de données.

La définition juridique d’une base de donnée est la suivante : [*« On entend par base de données un recueil d’œuvres, de données ou d’autres éléments indépendants, disposés de manière systématique ou méthodique, et individuellement accessibles par des moyens électroniques ou par tout autre moyen. »*](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=D4BD226D30BDEE9DD21EEB8CA55C2EFB.tpdila15v_3?idArticle=LEGIARTI000006278879&cidTexte=LEGITEXT000006069414&dateTexte=20170605)

Or, il faut savoir que les bases de données font l’objet d’un droit de propriété intellectuelle. En effet :

[« *Le producteur d’une base de données, entendu comme la personne qui prend l’initiative et le risque des investissements correspondants, **bénéficie** d’une protection du contenu de la base lorsque la constitution, la vérification ou la présentation de celui-ci atteste d’un investissement financier, matériel ou humain substantiel.* »](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=D4BD226D30BDEE9DD21EEB8CA55C2EFB.tpdila15v_3?idArticle=LEGIARTI000006279245&cidTexte=LEGITEXT000006069414&dateTexte=20170605)

Avec ce « droit des producteurs de base de données », l’extraction de tout ou partie substantielle de la base est par défaut interdite. Ainsi, les gestionnaires du fablab doivent s’interroger sur ce qu’ils feront de ce droit. Comment vont-ils le gérer ?
Si les producteurs de ces bases ne disent rien, alors par défaut on ne peut pas extraire de données. Dans ce cas, le partage des connaissances est relativement limité. Mais, ils peuvent opter pour une licence libre de leur choix (d’ailleurs les Creatives Commons sont tout à fait compatibles avec les bases de données). Si tel est leur choix, alors en fonction de la licence choisie, un certain nombre d’usages vont être autorisés.
Le meilleur exemple pour comprendre cela est Wikipédia. Les articles présents sur Wikipédia sont des œuvres écrites par des auteurs. Ces œuvres sont *« disposées de manière systématique ou méthodique, et individuellement accessibles par des moyens électroniques »*. Nous sommes donc bien en présence d’une base de données.
En optant pour la licence Creative Commons Attribution (CC-BY), les producteurs de la base de données Wikipedia autorisent tout un chacun à réutiliser tout ou partie de cette base de quelque manière que ce soit, à la seule condition de préciser la source et le nom de l’auteur.
Ainsi, utiliser une licence libre pour ces bases permet de s’inscrire dans une démarche de partage des connaissances, ce qui est, on le rappelle, un des piliers de la dynamique des fablabs et de la culture libre.

Cette question est loin d’être marginale. Imaginons, par exemple, qu’un éditeur souhaite publier un recueil des plus belles trouvailles documentées sur fablabo. Comment collectivement réagirions-nous ? Peut-être que le plus simple est encore, avant que cela n’arrive, de choisir la licence qui conviendrait le mieux à tous. Il me semble qu’il y a là une piste à creuser.

Ensuite, regardons les différentes entrées de cette base. Du point de vue de la propriété littéraire et artistique, il faut s’intéresser à plusieurs choses :
– aux créations intellectuelles qui sont documentées (les projets qui vont avoir vocation à être protégés par la propriété littéraire et artistique et/ou par la propriété industrielle).
– aux techniques documentées (utilisation des machines, tutos de réparations, façon d’animer un lieu ou un atelier…)
– à la documentation de ces créations, c’est-à-dire la façon dont sont expliqués soit les projets, soit les techniques.

Pour avoir un droit d’auteur sur sa création littéraire et artistique, il faut remplir certains critères. Je les rappelle ici pour information mais l’objectif du jour n’est pas de les détailler. Il faut que la création résulte d’une activité humaine consciente (une œuvre de l’esprit ne peut pas être créée par un logiciel tout seul, elle ne peut pas non plus résulter du hasard…), elle doit être perceptible par les sens, à l’exception du goût et de l’odorat ([pour le moment](https://scinfolex.com/2017/05/25/la-cuisine-va-t-elle-cesser-detre-open-source/)). Mais surtout elle doit être originale. Cela veut dire qu’elle doit être personnelle à l’auteur, elle doit porter en elle la façon de créer de l’auteur. On dit qu’elle est empreinte de la personnalité de l’auteur.

Le droit de la propriété littéraire et artistique précise également que l’obtention de ce droit est automatique. Il n’y a pas de formalités à remplir pour avoir un droit d’auteur, il suffit de faire une œuvre de l’esprit originale et d’être capable de le prouver.

Par ailleurs, comme j’ai déjà eu l’occasion de le dire ici ou là, la propriété intellectuelle est un peu différente de la propriété à laquelle on est généralement confronté. En effet, il ne va pas s’agir de la propriété de l’objet physique, de l’objet matériel. Il va s’agir d’un type de propriété qui concerne une création intellectuelle, immatérielle.

C’est probablement plus parlant avec des exemples. Prenons le cas d’un livre, la propriété littéraire et artistique ne va pas (ou peu) s’intéresser à la propriété du support (le papier, l’encre, la colle …). Elle va concerner la propriété de l’histoire racontée dans le livre. Lorsque l’on achète un livre, on peut se servir de l’objet, mais on ne peut pas faire n’importe quoi avec l’histoire (l’adapter en pièce de théâtre, en faire un film …) sans demander l’autorisation de l’auteur. Il en va de même pour un logiciel, la propriété intellectuelle ne va pas s’intéresser à son support de stockage sur lequel il est, mais au code source. Acheter un logiciel « propriétaire » me donne le droit de l’utiliser mais pas de le modifier ou d’en commercialiser une version modifiée.

Quand on parle de cet objet immatériel, on parle souvent de création intellectuelle. C’est le terme générique pour parler des créations qui sont soumises à la propriété intellectuelle. Il en existe deux types qui suivent les deux branches de la propriété intellectuelle (voir schéma). Il existe les créations littéraires et artistiques aussi appelées plus souvent les œuvres de l’esprit. Et il existe les créations industrielles dont nous parlerons dans le prochain article.
Pour l’heure, nous nous intéressons aux œuvres de l’esprit.

Il n’y a pas vraiment de définition de l’œuvre de l’esprit. En revanche, [la loi](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=936FE5561AAF54747264D259307A4EB0.tpdila15v_3?idArticle=LEGIARTI000006278875&cidTexte=LEGITEXT000006069414&dateTexte=20170605) donne des exemples. L’avantage c’est que la liste n’est pas limitative. Presque tout peut-être une œuvre de l’esprit. Le vrai critère sera donc l’originalité.  

**À ce stade, il est donc important de bien faire la différence entre la création et la documentation de la création. Si les deux peuvent être protégées par le droit d’auteur car elles remplissent toutes deux les critères nécessaires à la protection (œuvre de l’esprit + originalité) alors il y aura deux droits d’auteur différents.** Prenons un exemple. Si je fais une sculpture, cette œuvre (si elle en remplit les conditions) sera protégée par le droit d’auteur. En tant qu’auteur (sculpteur) je vais donc avoir un droit de propriété (je peux donc si je le souhaite opter pour une licence libre). Maintenant, je documente cette œuvre, c’est-à-dire, j’écris un texte qui va expliquer : pourquoi j’ai  fait cette sculpture, comment je l’ai faite, etc. Ce texte (s’il remplit les conditions) sera également protégé par le droit d’auteur. En tant qu’auteur (écrivain) je vais également avoir un droit de propriété (un autre) que je pourrai là encore placer sous licence libre. Ce sont deux œuvres différentes (bien qu’elles soient en rapport  avec la même création).

Maintenant que nous avons ces connaissances, déroulons le raisonnement.

Je suis un maker et j’ai fait un projet. Ce projet est une œuvre de l’esprit (sculpture, peinture, code, musique, …). Si elle n’est pas originale (ne porte pas l’empreinte de la personnalité de l’auteur) alors, elle n’est pas protégée par le droit d’auteur. Si je n’ai pas ce droit, alors je ne peux pas « selon la loi » choisir une licence libre. Ce serait un peu comme essayer de louer la voiture de son voisin. Pour faire un contrat sur une chose, il faut posséder cette chose. Dans le cas d’une œuvre de l’esprit, posséder cette chose, c’est avoir un droit d’auteur.
Donc si l’œuvre n’est pas originale, il n’y a pas de droit d’auteur et donc pas de questions à se poser, cette création fait partie des créations libres, chacun est libre de s’en inspirer.
En revanche, si elle est originale, alors je suis auteur et comme pour le droit des producteurs de base de données, si je ne dis rien, un certain nombre d’usages sont interdits. Cela dit, je peux les autoriser en choisissant une licence libre. Et là, il faut préciser certaines choses. Documenter son œuvre ne veut pas dire abandonner son droit. Ce n’est pas parce que j’ai parlé de mon œuvre que je n’en suis plus l’auteur. De même, choisir une licence libre ne veut pas dire non plus dire abandonner son droit. Simplement, en faisant le choix d’une licence libre, je vais autoriser un certain nombre d’usages qui ne seraient pas autorisés en l’absence de cette licence.

Là, il est important  d’ajouter que le choix d’une licence libre demande de comprendre quelques petites notions. Tout d’abord, il existe beaucoup de licences libres différentes (Creative Commons, GNU/GPL, Art Libre, Licence Ouverte…). Chacune de ces licences s’appliquent à un certain type d’œuvre. La GNU/GPL (ou encore la BSD) s’applique aux logiciels, les Creative Commons (ou encore la licence Art Libre) s’appliquent à toutes les œuvres de l’esprit (et moins aux logiciels car ils sont un peu à part).

Il importe donc de bien choisir sa licence. Par analogie, il ne nous viendrait pas à l’esprit de vendre une voiture avec un contrat de vente de maison. Les règles ne sont pas les mêmes,  aussi faut-il bien choisir son contrat.

Ensuite, une fois que la bonne catégorie de licence est choisie, il faut choisir celle qui convient le mieux aux usages que l’auteur souhaite autoriser. Et là, il faut le dire, les Creatives Commons ont fait un remarquable travail de pédagogie et de simplicité. Il existe moult vidéos et textes expliquant clairement [ces options](https://framatube.org/media/presentation-des-licences-creative-commons-avec-cc).

Et enfin, comme l’œuvre « documentation » est différente de l’œuvre « projet », il faut choisir les licences libres en adéquation avec ce que je veux pour ces différentes œuvres. Je peux vouloir que la documentation de l’œuvre (qui est elle-même une œuvre) soit très partagée  et que l’œuvre principale le soit moins. Il faut simplement le préciser et s’intéresser au bon choix de la licence.

Par ailleurs, un petit point technique. L’ensemble des licences libres demande qu’à chaque usage de l’œuvre soit associé le nom de l’auteur. Il est donc très important que l’auteur accole son nom à son œuvre car s’il ne le fait pas, il n’est pas possible d’associer l’auteur à l’œuvre.

Un dernier point très « important », tout n’est pas protégé par le droit d’auteur, loin s’en faut.

Le droit de la propriété littéraire et artistique ne protège pas les simples découvertes, il ne protège pas les idées, pas plus que le simple savoir-faire.

Parlons ici des fiches techniques d’utilisation d’une machine ou de réparation d’un objet. Les connaissances qui sont présentes dans ces fiches ne sont que du simple savoir-faire. Celui-ci ne peut pas être protégé par le droit d’auteur. Il fait partie des connaissances libres que chacun peut réutiliser. En revanche, la documentation, c’est-à-dire la façon de présenter ces connaissances, si elle est originale, pourra, quant à elle, faire l’objet d’un droit d’auteur, l’auteur aura donc le droit de s’opposer à ce que cette fiche soit réutilisée sans son accord. Il peut bien entendu opter également pour une licence libre. En revanche, les connaissances, les savoir-faire sont et resteront commun à tous.

Lorsqu’il s’agit enfin de la documentation relative à l’animation d’un atelier ou d’un lieu, c’est un peu plus complexe. Soit c’est un simple savoir-faire et alors cela fait partie du domaine public, ce qui implique que ce savoir-faire peut être réutilisé par tous ou que tous peuvent s’en inspirer. Soit la façon d’animer un atelier ou un lieu est suffisamment travaillée pour ne pas être un simple savoir-faire et pour être empreinte de la personnalité de l’auteur, alors elle pourra prétendre à la protection par le droit d’auteur. Dans ce cas, le créateur de cette façon d’animer sera l’auteur et à ce titre, bénéficiera d’un droit d’auteur. Cela lui permettra d’interdire la réutilisation de cette « œuvre de l’esprit » tout comme cela lui permettra de choisir une licence libre. Et là encore, la documentation relative à cette méthode d’animation peut-être ou non originale et alors en découlent les mêmes conséquences. Cela est également vrai s’il y a plusieurs auteurs simplement tous les auteurs devront se mettre d’accord.

En résumé, lorsqu’on parle de propriété littéraire et artistique et de la documentation dans les fablabs, il faut distinguer :
– la base de données (comme le wiki [fablabo.net](http://www.fablabo.net/), …) et la licence libre qui y est associée
– les données de cette base qui sont :
° les connaissances techniques, savoir-faire … qui sont dans le domaine public.
° les œuvres de l’esprit (projets en tout genre) qui, si elles sont originales font l’objet d’un droit d’auteur. Alors il faut respecter la licence libre choisie par le ou les auteurs.
° les documentations de ces données qui sont elles-mêmes des œuvres de l’esprit qui, si elles sont originales, font l’objet d’un droit d’auteur différent. Et, là encore, il faudra respecter la licence libre relative à la documentation.

Enfin, pour viser l’exhaustivité, il arrive souvent que des projets soient composés de plusieurs œuvres. Par exemple une partie histoire et une partie logiciel. Dans ce cas, il faut séparer chacune des œuvres et y associer les licences libres qui conviennent et que l’auteur a choisi (ou l’ensemble des auteurs s’ils sont plusieurs).

Voilà ce qu’il en est de la documentation et de la propriété littéraire et artistique. La semaine prochaine, nous verrons ce qu’il en est des créations industrielles.

Bonne semaine !

**P.S :** L’honnêteté intellectuelle me pousse à préciser ici que ce dont je vous parle là, ce sont les grandes lignes. Je me suis efforcé de vous donner le droit tel que la loi le prononce. Or, j’aime à dire que le droit est un outil. Un peu comme un tournevis qui ne sert pas toujours à visser mais parfois à ouvrir des pots de peintures, le droit est parfois détourné de son objectif principal. En effet, nous voyons aujourd’hui des créations qui sont protégées par le droit d’auteur de manière « un peu » abusives. Cela contribue grandement à une vision critique de cette matière. Mais comme tout outil, il n’appartient qu’à nous de l’utiliser de la manière qui nous semble la plus juste. Nous avons aujourd’hui bien des façons de redonner au terme « œuvre de l’esprit » toutes ses lettres de noblesses.

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.
