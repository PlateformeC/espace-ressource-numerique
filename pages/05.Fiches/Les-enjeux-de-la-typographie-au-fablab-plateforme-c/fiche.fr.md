---
title: 'Les enjeux de la typographie au fablab Plateforme C'
media_order: typo-cp-laser.jpg.jpg
type_ressource: text
feature_image: typo-fraiseuse-cp.jpg
license: cc-by-nc-sa
date: '13-12-2020 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - Papier/Machine
        - 'fraiseuse carvey'
        - 'carte postale'
        - 'Expérimentation matériaux'
        - workshop
        - 'decoupeuse vinyle'
        - 'decoupe vinyle'
        - plotter
        - papier
        - typographie
        - 'decoupeuse laser'
        - 'apier machine'
    author:
        - 'Yanaïta Araguas'
aura:
    pagetype: article
    description: 'Quelques ressources sur les enjeux de la typographie au fablab Plateforme C'
    image: typo-fraiseuse-cp.jpg
metadata:
    description: 'Quelques ressources sur les enjeux de la typographie au fablab Plateforme C'
    'og:url': 'https://ressources.pingbase.net/fiches/les-enjeux-de-la-typographie-au-fablab-plateforme-c'
    'og:type': article
    'og:title': 'Les enjeux de la typographie au fablab Plateforme C | PiNG Ressources Num&eacute;riques'
    'og:description': 'Quelques ressources sur les enjeux de la typographie au fablab Plateforme C'
    'og:image': 'https://ressources.pingbase.net/fiches/les-enjeux-de-la-typographie-au-fablab-plateforme-c/typo-fraiseuse-cp.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '984'
    'og:image:height': '654'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les enjeux de la typographie au fablab Plateforme C | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Quelques ressources sur les enjeux de la typographie au fablab Plateforme C'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/les-enjeux-de-la-typographie-au-fablab-plateforme-c/typo-fraiseuse-cp.jpg'
    'article:published_time': '2020-12-13T00:00:00+01:00'
    'article:modified_time': '2021-01-12T12:50:02+01:00'
    'article:author': Ping
---

## Les enjeux de la typographie au fablab Plateforme C

Au fablab Plateforme C, nous avons une contrainte importante sur un projet comme Papier/Machine : pas de photocopieur ou de dispositif dédiés à la reproduction de texte (pour l'instant... c'est en cours de changement!).
La typographie*, c'est donc le nerf de la guerre !
Il nous faut trouver d'autres solutions avec les outils disponibles et, comme toujours, celà nous permet d'essayer de nouvelles choses.
Voici quelques-unes des expériences produites :

### À la découpeuse laser :
-Découpe sur contreplaqué de peuplier pour pochoirs :
![typo-cp-laser.jpg](typo-cp-laser.jpg?forceresize=100%)

-Raster* sur de la cire :
![experimentation-cire-web-1.jpg](experimentation-cire-web-1.jpg?forceresize=100%)
![experimentation-cire-web-2.jpg](experimentation-cire-web-2.jpg?forceresize=100%)
![experimentation-cire-web-3.jpg](experimentation-cire-web-3.jpg?forceresize=100%)
![experimentation-cire-web-4.jpg](experimentation-cire-web-4.jpg?forceresize=100%)
![experimentation-cire-web-5.jpg](experimentation-cire-web-5.jpg?forceresize=100%)
![experimentation-cire-web-6.jpg](experimentation-cire-web-6.jpg?forceresize=100%)


### Usinage à la fraiseuse:
- Sur contreplaqué de peuplier
![typo-fraiseuse-cp.jpg](typo-fraiseuse-cp.jpg?forceresize=100%)
![typo-fraiseuse-cp2.jpg](typo-fraiseuse-cp2.jpg?forceresize=100%)

-Sur plexiglass :
![typo-fraiseuse-plexiglass1.jpg](typo-fraiseuse-plexiglass1.jpg?forceresize=100%)

-Sur papier :
![typo-fraiseuse-papier1.jpg](typo-fraiseuse-papier1.jpg?forceresize=100%)

### Détournement de la découpeuse vinyle

Nous avions déjà pu voir la découpeuse vinyle utilisée comme machine à dessiner en remplaçant la petite lame (plotter* de découpe) par un stylo bille par exemple, produisant de magnifiques volutes parfaitement géométriques, faisant concurrence aux meilleurs spyrographes.
On a aussi pu trouver des ressources ici sur le dessin avec une machine de ce type.
[Fablab descartes - dessiner avec une découpeuse vinyle](https://www.fablab-descartes.com/projects/comment-dessiner-avec-la-decoupe-vinyle/)

Lors du workshop avec l'ESBA Nantes St Nazaire, des étudiant·es ont utilisé la découpeuse vinyle pour la transformer en machine à dessiner en remplaçant le plotter de découpe par un stylo à bille usagé afin de ne pas laisser de trace colorée et d'éviter de «blesser» le papier.
Ils ont produit ainsi une forme d'embossage* qui leur a permis de produire du texte sur différents supports sur lesquels il est intéressant de se pencher.
Le papier aluminium réagit très bien.
![ecrire-a-la-decoupe-vynil-12.jpg](ecrire-a-la-decoupe-vynil-12.jpg?forceresize=100%)
Avec le plotter de découpe, l'utilisation de papiers épais légèrement plastifiés et scintillants est également intéressante, amenant une typographie subtilement identifiable.
![ecrire-a-la-decoupe-vynil-1.jpg](ecrire-a-la-decoupe-vynil-1.jpg?forceresize=100%)

* Le raster est un mode d'action que propose la découpeuse laser. On fournit à la machine un fichier image en niveaux de gris (noir et blanc). Le laser va balayer la surface de la zone à usiner (correspondant à la taille de l'image fournie) et sa puissance varie en fonction de la valeur des noirs et des blancs de l'image traitée, produisant ainsi des nuances sur le matériau (plus ou moins 'cramé')

* Plotter : à la base, un plotter est une machine qui trace des lignes ("to plot" en anglais veut dire tracer), à la différence d'une imprimante, dont le travail va être de couvrir toute une surface avec de l'encre. À ce titre, les plotters sont typiquement utilisés pour imprimer les patrons de couture, les schémas techniques en ingénierie ou en architecture. 
Un plotter fonctionne avec une tête qui vient se promener sur le matériau, tête qui dispensera de l'encre, mais qui pourra également être dotée d'une lame afin de découper le matériau choisi.
[Plotter de découpe - markerist](https://www.makerist.fr/topics/Tout-savoir-sur-le-plotter-de-decoupe)

*La typographie (souvent abrégée en « typo ») désigne les différents procédés de composition et d’impression utilisant des caractères et des formes en relief, ainsi que l’art d’utiliser les différents types de caractères dans un but esthétique et pratique.
Le terme comporte différentes acceptions : « impression typographique » pour la technique d’impression, « dessin de caractères » ou « création de caractères » pour la création de polices d’écriture, « lettrage » pour le dessin manuel de caractères.

* L’embossage est une technique qui a pour objectif de créer des formes en relief dans du papier ou un autre matériau déformable. L’embossage est la version artisanale de l’emboutissage, qui s’applique le plus souvent à de la tôle.
[Embossage - wikipedia](https:/fr.wikipedia.org/wiki/Embossage)

![ecrire-a-la-decoupe-vynil-2.jpg](ecrire-a-la-decoupe-vynil-2.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-6.jpg](ecrire-a-la-decoupe-vynil-6.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-7.jpg](ecrire-a-la-decoupe-vynil-7.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-8.jpg](ecrire-a-la-decoupe-vynil-8.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-9.jpg](ecrire-a-la-decoupe-vynil-9.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-11.jpg](ecrire-a-la-decoupe-vynil-11.jpg?forceresize=100%)
![ecrire-a-la-decoupe-vynil-13.jpg](ecrire-a-la-decoupe-vynil-13.jpg?forceresize=100%)

### Autres ressources
Matériaux utilisés :
Papier couché noir
Papier couché blanc
Papier aluminium
Papier à grain blanc
Cire d'abeille
Contreplaqué de peuplier
Plexiglass

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx
