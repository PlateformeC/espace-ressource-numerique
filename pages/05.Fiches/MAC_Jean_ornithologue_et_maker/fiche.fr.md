---
title: '[MAKER À CŒUR] Jean, ornithologue et maker'
media_order: IMG_9357_paysageretaillee.jpg
type_ressource: text
feature_image: IMG_9357_paysageretaillee.jpg
license: cc-by-nc-sa
date: '01-02-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: ' Jean, ornithologue et maker, nous présentes ses projets au fablab Plateforme C'
    image: IMG_9357_paysageretaillee.jpg
metadata:
    description: ' Jean, ornithologue et maker, nous pr&eacute;sentes ses projets au fablab Plateforme C'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_jean_ornithologue_et_maker'
    'og:type': website
    'og:title': '[MAKER &Agrave; C&OElig;UR] Jean, ornithologue et maker | Espace Ressources Num&eacute;riques'
    'og:description': ' Jean, ornithologue et maker, nous pr&eacute;sentes ses projets au fablab Plateforme C'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_jean_ornithologue_et_maker/IMG_9357_paysageretaillee.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '532'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; C&OElig;UR] Jean, ornithologue et maker | Espace Ressources Num&eacute;riques'
    'twitter:description': ' Jean, ornithologue et maker, nous pr&eacute;sentes ses projets au fablab Plateforme C'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_jean_ornithologue_et_maker/IMG_9357_paysageretaillee.jpg'
    'article:published_time': '2017-02-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:54:13+01:00'
    'article:author': Ping
---

**Bonjour Jean,  peux-tu te présenter ?**

« Je suis Jean Lebouvier, Jean de son prénom, et je suis en retraite depuis un an. Avant j’étais responsable qualité dans une PME d’environ une centaine de personnes. C’est le fait d’être en retraite et d’avoir du temps qui me motive pour faire d’autres choses et puis l’idée de retrouver du monde dans la journée ça me va bien. Les fablabs ce sont des lieux qui m’ont toujours attiré, il s’y passe plein de choses. »

**Justement, comment as-tu découvert Plateforme C ?**

« Les fablabs, ça fait partie des sujets qui sont abordés dans les forums et autres. Celui de Nantes, comment je l’ai découvert ? Par des articles de presse peut-être aussi. »

**Viens-tu ici principalement pour les loisirs ?**

« Oui effectivement. Loisirs mais il y a quand même deux vocations. Le premier intérêt c’était les imprimantes 3D, pour apprendre comment ça marchait. Donc je me suis formé à la conception 3D parce que j’avais des besoins réels. J’avais des petites pièces d’un lecteur de cd qui étaient cassées, donc je pouvais les re-modéliser et les fabriquer. J’avais aussi des pièces de cassées d’un pied photo que j’ai réparé comme ça.  Et puis après j’ai fait 2-3 petits projets plutôt de loisirs, des petites maquettes pour mes petits-enfants par exemple. »

**Tu travailles en ce moment sur un système d’observation de nidification des oiseaux, peux-tu nous parler de ce projet ?**

« Je m’intéressais déjà avant ma retraite à l’ornithologie en termes d’observation de la période de nidification des oiseaux. Ça fait une huitaine d’années que j’ai installé une caméra dans un nichoir de mésanges charbonnières ou mésanges bleues. Ça fonctionne bien, je vois les images, je les enregistre et après je fais des montages que je mets sur un blog. Je pouvais voir les images quand je le voulais. Et la saison dernière, j’ai eu l’occasion de suivre la nidification de balbuzards pêcheurs et là en direct  (et c’est addictif comme sujet, ce sont de très gros oiseaux), sur un site où ils enregistrent et diffusent en streaming les vidéos pendant la nidification des oiseaux ! Et du coup ça m’a donné envie de mettre ça à disposition pour que mes petits-enfants puissent le voir. Moi je fais beaucoup de rando et il y a des gens qui sont intéressés et sont demandeurs d’en voir plus. Et donc j’ai fouillé internet pendant longtemps et puis j’ai vu que [Rasberry Pi](https://fr.wikipedia.org/wiki/Raspberry_Pi#Prototype) ça pouvait être une bonne base pas chère : c’est un nano ordinateur qui coûte 35/40 euros qui permet de faire l’interface entre les images et internet pour mettre en œuvre des moyens légers par rapport à un pc. Aujourd’hui je peux faire du streaming avec mon pc mais c’est un 7 pouces, ça consomme… Donc l’idée c’est de le faire de manière plus légère chez moi.
Le dernier objectif est de pouvoir faire des choses suffisamment compactes et modulables pour proposer ça à des écoles et des centres de loisirs. C’est une autre manière de sensibiliser à la nature, ça a un côté ludique (je le vois à l’attrait de mes petits-enfants). Si c’était diffusé en direct dans l’école ça pourrait faire une animation. »

**En quoi est-ce que Plateforme C t’aide pour ton projet ?**

« Et bien tu vois, tout à l’heure tu m’as vu discuter avec quelqu’un qui a déjà fait la même chose, ça permet de partager les ressources et les capacités. Et puis par exemple, moi la programmation j’ai pas du tout de formation là-dessus. J’ai fait de la programmation de base, c’est-à-dire de gestion de données il y a 30 ans, mais bon.
Et puis ça permet de retrouver du monde parce que chez moi je suis tout seul toute la journée. Mais ici il fait froid (ha!ha!). »

**Et qu’est-ce que ça représente pour toi Plateforme C ?**

« Je suis venu ici pour la première fois en septembre dernier. Ces locaux-là ils me parlent, à l’époque j’étais chef de projet sur la fabrication de tableaux électriques qu’étaient embarqués sur les bateaux, donc j’allais très régulièrement sur les chantiers Dubigeon [chantiers de bateaux de l’autre côté de la Loire, en face de Plateforme C]. C’est un lieu insolite mais historique qui me rappelle des souvenirs.  »


Comme l’a dit Jean à l’apéro|projets de février, si vous êtes intéressés par son projet et que vous avez du temps, de la motivation et des connaissances sur Rasberry Pi, la programmation, la sécurité des accès internet : vous pouvez le contacter sur son adresse mail  lebouvier@free.fr.
Pour en savoir plus à propos de son projet sur les mésanges bleues : [http://randonature.over-blog.com/](http://randonature.over-blog.com/)
