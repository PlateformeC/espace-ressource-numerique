---
title: 'Et le texte flux'
media_order: bookscanner.jpg
type_ressource: text
feature_image: bookscanner.jpg
license: cc-by-sa
date: '07-04-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'écritures numériques'
        - langage
        - code
    author:
        - 'Catherine Lenoble'
aura:
    pagetype: website
    description: 'Édition, écriture numérique, langages. Un article proposé par Catherine Lenoble, auteure et médiatrice autour des cultures numériques, membre de la coopérative Artefacts.*'
    image: bookscanner.jpg
metadata:
    description: '&Eacute;dition, &eacute;criture num&eacute;rique, langages. Un article propos&eacute; par Catherine Lenoble, auteure et m&eacute;diatrice autour des cultures num&eacute;riques, membre de la coop&eacute;rative Artefacts.*'
    'og:url': 'https://ressources.pingbase.net/fiches/et_le_texte_flux'
    'og:type': website
    'og:title': 'Et le texte flux | Espace Ressources Num&eacute;riques'
    'og:description': '&Eacute;dition, &eacute;criture num&eacute;rique, langages. Un article propos&eacute; par Catherine Lenoble, auteure et m&eacute;diatrice autour des cultures num&eacute;riques, membre de la coop&eacute;rative Artefacts.*'
    'og:image': 'https://ressources.pingbase.net/fiches/et_le_texte_flux/bookscanner.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Et le texte flux | Espace Ressources Num&eacute;riques'
    'twitter:description': '&Eacute;dition, &eacute;criture num&eacute;rique, langages. Un article propos&eacute; par Catherine Lenoble, auteure et m&eacute;diatrice autour des cultures num&eacute;riques, membre de la coop&eacute;rative Artefacts.*'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/et_le_texte_flux/bookscanner.jpg'
    'article:published_time': '2014-04-07T00:00:00+02:00'
    'article:modified_time': '2020-11-26T13:54:30+01:00'
    'article:author': Ping
---

*Édition, écriture numérique, langages. Un article proposé par Catherine Lenoble, auteure et médiatrice autour des cultures numériques, membre de la coopérative Artefacts.*

**Édito-** . Si l'on parle souvent d'édition et d'écriture numérique, dans l'entre-deux la notion d'éditorialisation, mérite que l'on y porte attention. La définition proposée par la revue Sens Public [1] en introduction du séminaire [« Écritures numériques & éditorialisation »](http://seminaire.sens-public.org/) me paraît éclairante : « Ensemble des pratiques d’organisation et de structuration de contenus sur le web. Ces pratiques sont les principes de l’actuelle production et circulation du savoir. La différence principale entre le concept d’édition et celui d’éditorialisation est que le dernier met l’accent sur les dispositifs technologiques qui déterminent le contexte d’un contenu et son accessibilité. »
En ce sens, une tablette ne représente pas uniquement un marché ou produit d'innovation technologique, c'est aussi dans un système d'appréhension plus large, un dispositif de lecture/écriture, une interface-écran, un objet connecté qui modifie notre lecture du monde et par extension notre capacité à l'écrire et produire de la connaissance.  De la même façon, un algorithme peut-être producteur de sens en agençant des contenus destinés à être lus et, à ce titre, être examiné en tant que dispositif d'éditorialisation.

**Voir/écrire.** Ayant récemment travaillé à la coordination d'un livre numérique [2] sur les nouvelles pratiques d'édition, de lecture et d'écriture en réseau, j'ai réalisé, au fil de mes lectures préparatoires à quel point les critiques et manières de penser le livre et l'écriture numérique par celles et ceux qui habitent ce territoire peuvent sembler discordantes.
L'occasion m'étant donné de parler ici d'édition et d'écriture numérique, *hyper* vaste sujet, je choisis de me pencher précisément sur ces manières de voir/écrire qui me paraissent contenir une question essentielle : celle de la place du langage et des langages de programmation dans la mutation et la reconfiguration de tout un secteur - l'édition - et des pratiques professionnelles qui en découlent mais aussi et surtout dans ce que nous avons inscrit comme pratiques sociales et culturelles fondamentales : la lecture et l'écriture.


Double lecture. C'est en lisant en parallèle, Katherine Hayles en papier et François Bon sur tablette, que j'ai eu envie d'extraire les deux citations suivantes :

*Print is flat, code is deep* - Katherine Hayles. [3]
*Le livre numérique n'a pas d'épaisseur* - François Bon. [4]

Katherine Hayles, peu connu en France malgré les tentatives de signalement [5]  est une intellectuelle américaine, critique littéraire et enseignante dans le domaine de la littérature postmoderne entre théorie des médias, littérature électronique et humanités numériques. On lui doit de nombreux essais *How We Think* (2012),  *My Mother Was A Computer* (2005), *Writing Machines* (2002), *How We Became Posthuman* (1999) et le concept de technotexts - où comment repenser la matérialité du texte entre l'aspect formel, la physicalité du support et le contenu généré, ré-interrogeant au passage aussi bien la « voix » de l' « auteur » que fonction du lecteur.

François Bon est un écrivain français, auteur de nombreuses publications (romans, essais, biographies) et présent sur la toile de manière protéiforme. Dès les années 2000, il transforme son site personnel en revue littéraire numérique : [remue.net](http://remue.net/) qu'il laisse vivre en collectif pour explorer à partir de 2005 un nouveau territoire d'écriture avec le site [tierslivre.net](http://www.tierslivre.net/). En 2008, il initie une aventure éditoriale singulière sur un modèle coopératif avec publie.net, première maison d’édition exclusivement numérique de littérature contemporaine. Depuis 2013, il enseigne l’écriture créative à l’École nationale supérieure d’arts Paris-Cergy.


**Madeleine du livre.** Alors entre les lignes, en si peu de signes, que peut-on lire, que peut-on dire ? D'un côté, l'épaisseur symbolique du code, de l'enrichissement et de la profondeur qu'il apporte au livre ; de l'autre, la comparaison fétichiste au livre et à la culture imprimée...  mais ce serait là, un maladroit raccourci, Publie.net offre aujourd'hui un généreux catalogue de textes de littératures contemporaines qui ne respire pas la nostalgie.

Le livre numérique ou les *technotexts* identifiés par Katherine Hayles (dont *La maison des feuilles* de Mark Z. Danielewski fait référence, roman à travers lequel le lecteur s'emmêle dans des boucles réflexives de jeux d'écriture intertextuelle inscrites dans la physicalité même de l'objet-livre) ont de beaux jours devant eux. Et non, il ne s'agit pas d'abandonner un support au profit d'un autre mais plutôt d'encourager et accompagner ces expériences de lecture en transition, hybrides et de plus en plus sociales, et favoriser du même élan, l'écriture dans toute sa dimension : expressive, créative, collaborative.

Auteurs, éditeurs, designers, bibliothécaires, animateurs multimédia, médiateurs du livre, lecteurs, internautes, n'ont pas fini de nous étonner... En témoigne d'ailleurs l'exposition en ligne [Erreur d'impression - Publier à l'ère du numérique](http://espacevirtuel.jeudepaume.org/erreur-dimpression-1674/)[6]  tout juste clôturée au Jeu de Paume, le livre du futur  a aussi son petit côté... retro.

Catherine Lenoble - http://litteraturing.net

---

- 1 : [Séminaire Sens Public - cycle 2013/14](http://seminaire.sens-public.org)
- 2 : [lire+écrire] est un livre numérique sur le livre numérique qui articule des contributions d'auteurs ouvrant des perspectives depuis le point de vue du designer, de l'artiste, de l'enseignant-chercheur, du juriste-bibliothécaire, du médiateur du livre et critique littéraire et des « recettes » pratiques  à travers des retours d'expérience et fragments d'atelier d'écritures numériques. Edité chez publie.net en partenariat avec la région Pays de la Loire – disponible depuis le 15/03 [en libre téléchargement sous licence Creative Commons](http://www.publie.net/livre/lireecrire/)
- 3 : Print Is Flat, Code Is Deep : The Importance of Media-Specific Analysis , N.Katherine Hayles Poetics Today 25.1 (2004) 67-90
- 4 : [Après le livre, F.Bon édité chez publie.net (2011)](http://www.publie.net/les-collections/critique-essai/)
- 5 : J'aimerai pouvoir lire Katherine Hayles en français par Emmanuel Guez paru dans le [MCD#66 Machines d'écriture](http://writingmachines.org/2013/06/13/katherine-hayles/).
- 6 : Une des dernières œuvres présentées The Girl Who Was Plugged In est un livre enrichit par des capteurs : l'intrigue se renforce à coup de stimulis délivrés au lecteur au gré du récit et de ses sensations. [Expo en ligne visible jusqu'au 7 avril 2014](http://espacevirtuel.jeudepaume.org/erreur-dimpression-1674/).
