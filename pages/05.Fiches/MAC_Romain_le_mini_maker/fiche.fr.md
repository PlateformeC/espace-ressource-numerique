---
title: '[MAKER À COEUR] Romain, le mini-maker'
media_order: romain.png
type_ressource: video
feature_image: romain.png
license: cc-by-nc-sa
date: '01-05-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Voici Romain, l’un des 14 participants au stage Fablab Junior 2017, qui nous parle de lui et de son expérience à Plateforme C !'
    image: romain.png
metadata:
    description: 'Voici Romain, l&rsquo;un des 14 participants au stage Fablab Junior 2017, qui nous parle de lui et de son exp&eacute;rience &agrave; Plateforme C !'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_romain_le_mini_maker'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Romain, le mini-maker | Espace Ressources Num&eacute;riques'
    'og:description': 'Voici Romain, l&rsquo;un des 14 participants au stage Fablab Junior 2017, qui nous parle de lui et de son exp&eacute;rience &agrave; Plateforme C !'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_romain_le_mini_maker/romain.png'
    'og:image:type': image/png
    'og:image:width': '979'
    'og:image:height': '521'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Romain, le mini-maker | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Voici Romain, l&rsquo;un des 14 participants au stage Fablab Junior 2017, qui nous parle de lui et de son exp&eacute;rience &agrave; Plateforme C !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_romain_le_mini_maker/romain.png'
    'article:published_time': '2017-05-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T15:03:03+01:00'
    'article:author': Ping
---

*Voici Romain, l’un des 14 participants au stage Fablab Junior 2017, qui nous parle de lui et de son expérience à Plateforme C !*

<iframe width="100%" height="450" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/925b9b8e-3175-493a-bf14-c974f5bd2fb4" frameborder="0" allowfullscreen></iframe>
