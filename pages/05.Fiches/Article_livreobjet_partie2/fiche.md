---
title: 'LIVRE-OBJET, OBJET-LIVRE (partie 2)'
media_order: couv.webp
date: '01.09.2019 00:00'
taxonomy:
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'livre-objet'

    author:
    - 'Yanaïta Araguas'
aura:
  pagetype: article
  description:
  image: couv.webp
  category:
    - recherches

---

Chercher à définir la notion de livre-objet nous a amené à prendre en compte la difficulté de faire rentrer cette terminologie dans des cases. Nous sommes face à des objets d’exception, bouleversant perpétuellement les codes du livre.

## Produire autrement – HACKER la chaine graphique

*« La contrainte est toujours positive. Elle semble négative car elle limite le champ des possibles, mais c’est précisément cette limitation qui facilite la création.» Paul Cox*

Lorsqu’on cherche à comprendre quels sont les modes de production et de conception de ces livres hors norme que sont les livres-objets ou les livres d’artistes, on peut dégager trois grands axes :

- L’artiste travaille avec un éditeur qui joue le rôle de conseiller accompagnant; les livres sont imprimés en grand nombre et s’insèrent dans le processus habituel de diffusion. Leur forme, du moins en apparence, se conforme également à l’idée du codex, permettant aux ouvrages une diffusion facilitée (ils peuvent être manipulés, présentés en librairie, envoyés etc.). L’éditeur peut imposer des contraintes ayant pour origine des raisons financières et techniques et qui se répercutent sur l’objet en terme de choix de format, de matériaux, de techniques d’impression etc.

- L’artiste travaille avec un «micro-éditeur»[6]. Les ouvrages sont imprimés en petites séries (micro-impression), souvent numérotés. Les techniques d’impression peuvent être artisanales, permettant un travail pointu de l’objet et une plus grande flexibilité sur la forme. Les contraintes sont souvent liées à des modèles économiques précaires, certaines techniques d’impression ou de façonnage restant inaccessibles de par leurs coûts élevés . La diffusion sera également plus complexe (micro-diffusion) : Il est moins évident de placer les livres en librairie via des distributeurs en raison de la délicatesse des productions et de leur coté hors norme qui les rend difficilement exposables, transportables, manipulables. De plus, chaque marge prise par un prestataire extérieur (distributeur, libraire), vient sérieusement entamer la possibilité de rembourser les frais engagés dans la fabrication lorsque le nombre d’exemplaires est réduit.

- L’artiste travaille en auto-édition[7] et garde ainsi une maîtrise totale de la forme de l’objet-livre qu’il produit lui même. On retrouve souvent les mêmes résultats que pour la collaboration avec une micro-édition, mais ici les choix sont assumés par une seule personne avec pour seules limites celles qu’elle se fixe.

![livreobjetbis_1.jpeg](livreobjetbis_1.jpeg?forceresize=100%)
*Robert Sabuda / The wizard of Oz*

![livreobjetbis_2.jpeg](livreobjetbis_2.jpeg?forceresize=100%)
*Icinori et Jean Lecointre*

On peut discerner en filigrane, derrière les différentes postures présentées ici, des éthiques différentes. On verra ainsi apparaître chez les éditeurs, micro-éditeurs et auto éditeurs, des variations dans la façon d’appréhender la fonction de l’objet, dans leur rapport à l’art et au marché de l’art, ainsi que dans la cible à laquelle ils destinent leurs productions.
Il y a les éditeurs de livres d’artistes qui défendent un accès aux œuvres popularisé, tirant leurs livres à plusieurs milliers d’exemplaires, vendus sur le mode du prix unique du livre.
Il y a les éditeurs de livres d’artistes, ou des artistes qui éditent de petites quantités d’ouvrages signés et numérotés, avec parfois des tirages de tête vendus plus chers, les prix de bases évoluant en fonction de la côte de l’artiste, laissant la porte ouverte à une forme de spéculation financière.
Enfin il y a les éditeurs ou les artistes qui produisent des ouvrages en petite série, souvent signés et numérotés, mais vendus à des prix très abordables, quasiment à perte, avec la volonté de faire découvrir une culture et un univers de pratique en même temps que les livres eux-mêmes.

![livreobjetbis_3.jpeg](livreobjetbis_3.jpeg?forceresize=100%)
*Katjastroph*

Les contraintes de fabrication sont souvent définies ou modelées par les modèles économiques. Ainsi, bien qu’un éditeur puisse imposer à un artiste de se fondre dans le cadre d’une collection avec un format imposé et parfois un papier ou l’usage d’une typographie, parfois, à l’inverse, il se mettra au service du projet de l’artiste pour trouver les moyens de fabriquer au plus prés du projet initial.
Un artiste micro-édité ou auto-édité verra apparaître des limites définies par sa capacité à faire seul mais son champ d’action sera sans doute plus ouvert. Le format pourra être contraint par la taille du photocopieur ou celle de ses écrans de sérigraphie mais on pourra récupérer du papier utilisé pour un autre projet, inventer une reliure pour décupler la taille de l’objet ou découper certaines parties à la main et faire de même des assemblages de précision.
Dans le cas des fanzines[8] graphiques, issus d’un courant plus alternatif, plus underground, le temps humain, celui de la fabrication, n’est souvent pas pris en compte dans le prix de vente de l’objet, le but premier étant de produire un objet dont on pourra être fier et que l’on pourra transmettre à un prix «raisonnable». Ce dernier cas produira des objets parfois incroyablement complexes et sensibles car on sort de la notion de rentabilité pour repousser les limites de ce qui est possible (comme on peut le voir dans le film de Francis Vadillo Undergronde tout particulièrement avec Un [fanzine carré numéro C](unfanzine.com/un-fanzine-carre-numero-c/) – Ed Hécatombe, 2013).

![livreobjetbis_4.jpeg](livreobjetbis_4.jpeg?forceresize=100%)
*UN FANZINE CARRÉ NUMÉRO C, ed. Hecatombe*

Si la conception de l’objet ne va pas s’appuyer sur les même bases, dans tous les cas, il faudra que les concepteurs repoussent les limites de leurs pratiques, imaginent des solutions pour détourner les techniques qu’ils ont à portée de main, adaptent, transforment, et réinventent les moyens de produire un objet sur mesure, un objet dont le sens et la forme se fondent intimement. Les concepteurs, les éditeurs et les imprimeurs, s’ingénient à imaginer comment créer de nouvelles formes, et parfois la magie opère au travers d’une grande simplicité de moyens, comme on peut le voir dans des livres comme Poémotion (dans lequel une feuille de plastique transparent imprimée de bandes noires, vient animer la surface de la feuille en un incroyable jeu optique) ou Oh mon chapeau de Louis Rigaud et Anouk Boisrobert,(un album où la forme du pop-up est réduite à son plus simple appareil pour laisser placer à un graphisme intelligent, pour une efficacité redoutable). A l’inverse, des livres comme ceux de Katsumi Komagata, s’ils peuvent paraître simples de par leur fonctionnement (dans Little tree une pousse se transforme en arbre qui croît puis meurt, A cloud nous donne à voir un nuage découpé dans le papier, qui se déplace de page en page créant un jeu de profondeur entre les pages du livre) sont d’une finesse incroyable et bien plus complexes qu’il n’y parait. Le papier change souvent à l’intérieur d’un même livre, nous offrant une expérience sensorielle complexe et l’impression est toujours d’une qualité parfaite. On peut saluer, en plus du travail de Katsumi Komagata et de sa maison d’édition One stroke, celui des Trois Ourses qui nous permettent d’accéder à ses livres en France (ainsi qu’au travail de nombreux autres artistes, dont le fameux Bruno Munari).

![livreobjetbis_5.jpeg](livreobjetbis_5.jpeg?forceresize=100%)
*A cloud, Katsumi Komagata © photo: Anaïs Beaulieu / Éditions Les Trois Ourses*

![livreobjetbis_6.jpeg](livreobjetbis_6.jpeg?forceresize=100%)
*Little tree / Petit Arbre, Katsumi Komagata © photo: Anaïs Beaulieu / Éditions Les Trois Ourses*

On trouve aussi un exemple édifiant dans l’aventure de l’ABC 3D de Marion Bataille. Dans La revue des livres pour enfant (n°5 Aujourd’hui l’album – publiée par la bnf), Cécile Bourdaire montre le chemin parcouru entre la première édition, parue chez les trois ourses à 30 exemplaires, vendue 845 euros et la ré-édition qui voit le jour chez Albin Michel, transformée en un best-seller mondial, vendu 15,50 euros en France. On imagine bien que l’objet n’a pas été produit dans les mêmes conditions (lieu de fabrication, personnel, choix des matériaux…) et que ces conditions ont dû être pensées en fonction du but poursuivi, de la posture éthique, du modèle économique.

![livreobjetbis_7.jpeg](livreobjetbis_7.jpeg?forceresize=100%)
*ABC3D, Marion Bataille © photo : Les Trois Ourses*

Le livre-objet et le livre d’artiste plient à leur volonté l’ensemble des maillons qui permettent de le produire. Ils obligent ceux qui les conçoivent à hacker la chaîne graphique[9] perpétuellement, de la production de masse à la micro-édition, pour trouver des solutions…
*« Le sens du livre est le livre en son entier, non ce qu’il contient. En ce cas seulement, le livre n’a pas un sens, il est son sens; il n’a pas une forme, il est une forme. En somme, à partir du moment où l’artiste a la complète maîtrise de la production, et il l’aura de plus en plus, le livre devient un médium à part entière.»* Anne Moeglin Delcroix
Lorsque l’on se penche sur l’univers des micro éditeurs et des «fanzineux», on y découvre des trésors d’ingéniosité.
On peut s’en rendre compte en regardant la KraftKollektion, une série de livres produits dans le cadre du festival Kraft à Nantes et d’une résidence d’une semaine durant laquelle chaque binôme d’artistes devait produire un objet avec pour seule contrainte un format et un budget serré, ainsi qu’un minimum de 30 exemplaires.
On y verra un bon exemple de détournements : du lino est attaqué avec un produit pour déboucher les toilettes pour amener de la texture, des reliures sont cousues à la machine, on utilise de vieilles presses typo, des languettes sont collées à la main, de sombres tractations sont passées avec le musée de l’imprimerie de Nantes pour emporte-piècer, utiliser les presses, l’atelier de sérigraphie marche à plein régime, on dresse des Autels de papier, on découpe au scalpel des kilomètres de dentelle, et parfois on écorne le format au passage, tant pis pour les règles …

![livreobjetbis_8.jpeg](livreobjetbis_8.jpeg?forceresize=100%)
*Billy & Fredster*
![livreobjetbis_9.jpeg](livreobjetbis_9.jpeg?forceresize=100%)
*Déluge, Pédro & Manica Jean Louis -Imprimerie Trace*
![livreobjetbis_10.jpeg](livreobjetbis_10.jpeg?forceresize=100%)
*Kazy & L’imprimerie Trace*
![livreobjetbis_11.gif](livreobjetbis_11.gif?forceresize=100%)
*Katja Bot/Yanaïta Araguas*

On se retrouve ici avec une communauté très ancrée dans les pratiques du diy[10] qui explore inlassablement les possibles de la conception et de la fabrication d’objets-livres et de libres-objets : une communauté de Makers[11], issue cependant de toute la tradition artisanale de l’imprimerie et de l’étique punk du fanzinat, qui n’est pas sans évoquer parfois celle de la communauté du libre et des fablabs[12] dans les notions de transmission de pair à pair et de faire ensemble.

## Chemin de traverse

C’est dans ce contexte complexe de la micro-édition qu’évolue en partie le projet Papier/Machine. Initié par Charlotte Rautureau (PiNG -Café crème) en partenariat avec Yanaïta Araguas (L’atelier dans l’ombre – éditions Grante ègle), il propose de questionner les pratiques liées au travail du papier, de l’impression artisanale[13] et du livre au sein d’un espace tel que le fablab Plateforme C[14]. Il a valeur de laboratoire et d’observatoire des hybridations, collisions, mutations qui pourront résulter du mélange de ces deux univers, en proposant aux artistes et artisans d’accéder à de nouvelles formes de production.

![livreobjetbis_12.jpeg](livreobjetbis_12.jpeg?forceresize=100%)
*Résidence Papier/Machine*

Cette année, le projet Papier /Machine s’est concentré sur la question du livre-objet et, dans ce cadre là, furent accueillis à Plateforme C deux temps de création : un workshop avec l’école de design de Nantes et une résidence d’artistes.
Au travers des objets conçus et fabriqués, et des retours d’expériences, la pertinence de cette proposition s’affirme. Avoir la possibilité de travailler de nombreux matériaux ( bois, papier, carton, plexiglass etc.) avec des machines telles qu’une découpeuse laser, une fraiseuse, une brodeuse numérique, une découpe vinyle, c’est accéder à de nouvelles possibilités techniques longtemps réservées aux artistes en lien avec de grosses maison d’édition. Au sein du fablab, appliquer ces techniques à des projets de type «micro-édition», c’est utiliser des techniques industrielles pour des projets sans obligation de rentabilité et les résultats produits sont séduisants en terme de diversité, de sens et d’expérience humaine.

![livreobjetbis_13.webp](livreobjetbis_13.webp?forceresize=100%)
*Résidence Papier/Machine*
![livreobjetbis_14.webp](livreobjetbis_14.webp?forceresize=100%)
*Résidence Papier/Machine*
![livreobjetbis_15.webp](livreobjetbis_15.webp?forceresize=100%)
*Résidence Papier/Machine*

À défaut de l’accompagnement expert de l’éditeur, celui des responsables du fablab et de ses utilisateurs a été précieux dans l’appropriation des machines par les artistes, poursuivant des buts parfois à l’opposé de ceux habituellement recherchés.
Les erreurs, les bugs des machines atteignant leurs limites furent souvent sources de détournement heureux (tirer partie d’une découpeuse laser qui va trop vite et simplifie les tracés, des brûlures qui apparaissent sur les matériaux, se servir de la fraiseuse comme d’un marteau-piqueur, écrire un texte en remplaçant le plotter de la découpe vinyle par un stylo parce qu’il n’y a pas de photocopieur), révélant ainsi une relation poétique homme/machine…
Il y a là sans aucun doute une possibilité de produire autrement l’objet-livre dans la limite de la définition des fablabs (du prototypage et pas de la série). Produire avec sens mais avec d’autres outils. Toucher du doigt les possibilités d’une production industrielle appliquée à une production artisanale et artistique. Ouvrir des portes. Faire de l’unique ambitieux, expérimenter et partager.

[6] La micro-édition désigne des pratiques d’édition spontanées et à toute petite échelle – Wikipédia

[7] L’auto-édition ou autoédition consiste pour un auteur à prendre lui-même en charge l’édition de ses ouvrages, sans passer par l’intermédiaire d’une maison d’édition – Wikipédia

[8] Le mot FANZINE (de fanatic et de magazine) est apparu dans les années 30 aux USA pour désigner les REVUES AMATEURES créées et diffusées par des passionnés de science-fiction (alors boudée par la presse officielle). Actuellement un fanzine peut être un MAGAZINE de fans, aussi bien qu’un MOYEN D’EXPRESSION ARTISTIQUE, LITTÉRAIRE, POLITIQUE, etc. ou qu’un OBJET DE CRÉATION à part entière. Aux contenus et aspects multiples, les fanzines ont une démarche commune : TOUT FAIRE SOI-MÊME de A à Z SANS FINALITÉ LUCRATIVE. Un fanzine est un fanzine à partir du moment où son auteur en a décidé ainsi! Projet collectif ou solo, la publication d’un fanzine est le plus souvent liée à d’autres pratiques (street art, musique, cinéma de genre, organisation de concerts, mail art, etc.). Définiton donnée par La Fanzinothèque de Poitiers

[9] «La chaîne graphique est une expression, communément employée dans l’industrie graphique, pour désigner l’ensemble des professions qui interviennent de la conception à la finalisation d’un produit graphique qui sera produit à de multiples exemplaires par un procédé d’impression» -Wikipédia

[10] Littérallement «Do it Yourself» : faîtes le vous-même au lieu de l’acheter.

[11] En français «ceux qui font ». «La culture maker (de l’anglais make, lit. faiseur) est une culture (ou sous-culture) contemporaine constituant une branche de la culture Do it yourself (DIY) tournée vers la technologie et la création en groupe. La communauté des makers apprécie de prendre part à des projets orientés ingénierie. Les domaines typiques de ces projets sont ainsi l’électronique, la robotique, l’impression 3D et l’usage des machines-outils à commande numérique (CNC), mais également des activités plus traditionnelles telles que la métallurgie, la menuiserie, les arts traditionnels et l’artisanat» – wikipédia

[12] Fab lab contraction de Fabrication et Laboratory désigne un espace destiné à la fabrication rapide de biens : vêtements, outils, etc. Il permet également de réaliser des maquettes d’objets complexes qui pourront ensuite être réalisés à grande échelle (prototypage). Le concept de Fab Lab vient du MIT (Massachusetts Institute of Technology) (…) A moyen terme, le concept de Fab Lab et d’objets libres (modifiables, diffusables) risque de s’opposer au monde de l’industrie des objets propriétaires et des brevets.

[13] Artisanale en opposition aux productions industrielles qui nécessitent souvent l’utilisation de machines à commandes numériques

[14] Plateforme C est un fablab, un atelier collaboratif de fabrication numérique et de prototypage rapide dans lequel il est possible de fabriquer “presque n’importe quoi”. Il regroupe un ensemble de machines à commande numérique (imprimantes 3D, découpe vinyle, découpe laser, fraiseuse numérique, etc.) ainsi que les outillages mécaniques et électroniques standards. Il est géré par l’association PiNG et se situe à Nantes.

## RESSOURCES

**Artistes, livres et maisons d’édition :**
- [Lothar Meggendorfer](https://www.youtube.com/watch?v=wGfgauAP7GY)
- [Bruno Munari](https://lestroisourses.com/librairie/artiste/7-bruno-munari)
- [Katsumi Komagata](https://lestroisourses.com/librairie/artiste/5-katsumi-komagata)
- [Votjech kubasta](https://www.youtube.com/watch?v=fT60FZna4vg&t=3s)
- [L’ombrocinéma: Poémotion – Takahiro Kurashima](https://www.youtube.com/watch?v=JNnu3UDRtHk)
- [Livre minuscule](https://www.bfmtv.com/international/asie/japon/le-plus-petit-livre-du-monde-illisible-a-l-oeil-nu-est-japonais_AN-201303130033.html)
- [“Reconstitution d’un accident qui ne m’est pas encore arrivé et où j’ai trouvé la mort” de Christian Boltanski](https://www.youtube.com/watch?v=K95iYj5fZ-4)
- [Un fanzine carré numéro C](http://unfanzine.com/un-fanzine-carre-numero-c/)
- [Livre anatomique](https://www.youtube.com/watch?v=7p6T2s5GyyM)
- [ABC3D – Marion Bataille](https://www.youtube.com/watch?v=eaImFMwpAm4)
- [Les trois ourses](https://lestroisourses.com/)
- [CNEAI](http://www.cneai.com/)

**Articles et documentation :**
- [Histoire du livre – BNF](http://classes.bnf.fr/livre/arret/histoire-du-livre/premiers-supports/index.htm)
- [Petite géographie du livre d’artiste](https://www.artpress.com/2012/08/27/petite-geographie-du-livre-dartiste/)
- [Undergronde -Un film de Francis Vadillo sur l’univers du fanzine et de la micro-édition](https://filmsdesdeuxrives.wixsite.com/societe/undergronde)
- [La micro-édition](http://www.okzk.fr/la-micro-edition/)
- Anne Moeglin-Delcroix
Esthétique du livre d’artiste (1960-1980), Paris, Jean-Michel Place/Bibliothèque nationale de France
- Revue Hors cadre n°4
Apparition /disparition- 2009
- [Sur les épaules de Darwin – Jean Claude Ameisen – Eclats de Passé : Les livres peints d’Amérique Centrale](https://www.franceinter.fr/emissions/sur-les-epaules-de-darwin/sur-les-epaules-de-darwin-05-mai-2018)

**Initiatives et lieux :**

- Laboratoire Bruno Munari :
https://www.youtube.com/watch?v=113npL7UoeA
https://www.youtube.com/watch?v=kI1t-em1blE

- [La Fanzino](https://www.fanzino.org/)
