---
title: 'Les doigts dans la prise #12 : Un genre de lutte sur Wikipédia'
type_ressource: audio
feature_image: 463px-We_Can_Edit.jpg
license: cc-by-nc-sa
published: true
date: '03-03-2020 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Maëlle Vimont'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: 463px-We_Can_Edit.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://ressources.pingbase.net/fiches/sun12_mars20_genrewikipedia'
    'og:type': article
    'og:title': 'Les doigts dans la prise #12 : Un genre de lutte sur Wikip&eacute;dia | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://ressources.pingbase.net/fiches/sun12_mars20_genrewikipedia/463px-We_Can_Edit.jpg'
    'og:image:type': image/webp
    'og:image:width': '926'
    'og:image:height': '599'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #12 : Un genre de lutte sur Wikip&eacute;dia | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun12_mars20_genrewikipedia/463px-We_Can_Edit.jpg'
    'article:published_time': '2020-03-03T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:58:39+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-un-genre-lutte-sur-wikipedia) !

![Les doigts dans la prise - Un genre de lutte sur Wikipedia](Les_doigts_dans_la_prise_12_Un_genre_de_lutte_sur_wikipedia.mp3)

Bonjour et bienvenue à toutes et à tous pour cette nouvelle chronique Les doigts dans la prise de l’association PiNG.

À quelques jours de la journée internationale de lutte pour les droits des femmes, j’ai mené l’enquête pour vous sur ce qu’on appelle « les biais de genre » sur wikipédia. Wikipédia c’est l’un des sites les plus consultés au monde avec plus de 600 millions de visites quotidiennes1. On compte 160 millions de pages disponibles2 et plus de 85 millions d’éditeurs , dont 300 000 sont considérés comme actifs3. ».

Euuuh Stop. En vous disant « des éditeurs actifs ».., je suis prête à parier que vous imaginez des hommes…comme moi, par réflexe…. Et bien ça… C’est un bel exemple de ce qu’on appelle « un biais de genre ». Ces biais ont la fâcheuse tendance « d’échaper bien souvent à notre vigilance », et « ils peuvent être implicite ou inconscients »….c’est un fait malheureusement très répandu, et vous vous en doutez, cela ne concerne pas seulement wikipédia…ou mes associations d’idées plus ou moins conscientes ou automatiques.

Comme le partage « Just for the record », un projet portant notamment sur la représentation des genres dans les nouveaux médias, « la façon dont on raconte et on enregistre notre histoire en dit beaucoup sur les structures de pouvoir qui existent dans notre société […] En d’autres termes, Wikipédia a un impact énorme sur la façon dont on voit le monde ».

Creusons le sujet.

«  Le biais de genre », appelé aussi « sexisme sur Wikipédia » ou en anglais « gender gap », est un biais lié à la représentation des genres, dans le contenu et parmi les personnes qui contribuent. En fouillant sur internet, j’ai découvert qu’il existe en fait plusieurs types de biais de genre sur le site et parmi la communauté. Et autant vous prévenir, c’est pas très réjouissant.

Il y a d’abord le biais « quantitatif ». Et il est sans appel.

En 2020, sur wikipédia francophone, en ce qui concerne les pages de biographies, on compte seulement 18 % de pages qui concernent des personnalités féminines4. Et les personnes qui contribuent à écrire sur wikipédia ? Selon l’encyclopédie elle-même, 90 % sont des hommes.

Ouch… On est bien loin d’une représentation diversifiée et équilibrée des genres…

Le biais quantitatif n’est malheureusement pas le seul à sévir. Dans le panel, il y a aussi des biais plus difficiles à débusquer, comme le biais « structurel», le biais de visibilité et le biais lexical…

Tout ça n’est pas très réjouissant hein ?… Heureusement, ce n’est pas une fatalité.

Il existe des initiatives et des personnes qui s’engagent au quotidien pour combler le fossé des genres sur wikipédia et ailleurs. Elles méritent ici d’être saluées, remerciées…et mises en lumière.

Parmi elles, citons le projet « les sans pagEs » et l’atelier Femmes et Féminisme organisé par les Affs à Nantes. Iels créent et améliorent des articles Wikipédia portant sur des femmes, sur les féminismes, ou d’autres sujets sous-représentés. Iels enrichissent aussi les illustrations, les données, et s’entraident avec d’autres collectifs qui existent ailleurs dans le monde.

Saluons enfin au passage la collective « Roberte Larousse » et sa projet artistique critique « wikifémia » qui propose de mettre en scène des biographies de femmes remarquables. Les productions de la collective sont écrites à la féminine, car elle s’agit de démasculiniser autant l’histoire que la langue et sa grammaire.

Et parce qu’il la faut bien ; c’est sur cette note féminine et positive que je vais clôturer cette chronique pour aujourd’hui en vous souhaitant de belles journées de lutte pour réduire les fossés et les inégalités…en tous genres !
Notes :

[1] [« Wikistats 2 – Statistics For Wikimedia Projects »](https://stats.wikimedia.org/v2/#/all-projects/reading/total-page-views/normal%7Cbar%7C1-month%7C~total%7Cdaily), sur stats.wikimedia.org

[2] [« Wikistats 2 – Statistics For Wikimedia Projects »](https://stats.wikimedia.org/v2/#/all-wikipedia-projects), sur stats.wikimedia.org

[3] [« List of Wikipedias », dans Wikipedia](https://en.wikipedia.org/w/index.php?title=List_of_Wikipedias&oldid=926748740), 18 novembre 2019

[4] https://www.lesaffs.fr/: « Savez-vous que ? Sur Wikipédia francophone, en 2020 18% des biographies concernent des femmes (15% en 2016). » consulté le 26/02/2020. »

[Ressources pour aller plus loin en lien avec la thématique](https://info.pingbase.net/technologies-minorites-3/?classes=btn)
