---
title: 'Le jeu de lois'
media_order: Jeudelois1.jpg
type_ressource: fichier
feature_image: Jeudelois1.jpg
license: cc-by
published: true
date: '05-09-2018 11:10'
taxonomy:
    category:
        - pratiques
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
    author:
        - 'Charlotte Rautureau'
        - 'Vladimir Ritz'
        - 'Adrien Martinière'
        - 'Thomas Bernardi'
aura:
    pagetype: article
    description: 'Un jeu de plateau pour se familiariser avec la propriété intellectuelle et la culture libre.'
    image: Jeudelois1.jpg
show_breadcrumbs: true
metadata:
    description: 'Un jeu de plateau pour se familiariser avec la propri&eacute;t&eacute; intellectuelle et la culture libre.'
    'og:url': 'https://ressources.pingbase.net/fiches/le_jeu_de_lois'
    'og:type': article
    'og:title': 'Le jeu de lois | PiNG Ressources Num&eacute;riques'
    'og:description': 'Un jeu de plateau pour se familiariser avec la propri&eacute;t&eacute; intellectuelle et la culture libre.'
    'og:image': 'https://ressources.pingbase.net/fiches/le_jeu_de_lois/Jeudelois1.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '2592'
    'og:image:height': '1728'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Le jeu de lois | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Un jeu de plateau pour se familiariser avec la propri&eacute;t&eacute; intellectuelle et la culture libre.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/le_jeu_de_lois/Jeudelois1.jpg'
    'article:published_time': '2018-09-05T11:10:00+02:00'
    'article:modified_time': '2021-01-13T11:06:49+01:00'
    'article:author': Ping
---

*Un jeu de plateau pour se familiariser avec la propriété intellectuelle et la culture libre. À travers histoires vraies, personnages et dates clés, seuls ou équipe, les joueurs tentent de réaliser un projet tiré au sort avec l’aide d’un maître du jeu.*

[Télécharger les éléments du jeu (cartes et livret)](http://station.pingbase.net/index.php/s/GrTARrYfz4PZP2Q?classes=btn-down)

[Documentation technique (plateau)](http://fablabo.net/wiki/Jeu_de_Lois?classes=btn)
