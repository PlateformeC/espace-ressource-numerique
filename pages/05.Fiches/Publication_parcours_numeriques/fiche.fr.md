---
title: "Parcours Numériques\_: cultures, pratiques & médiations partagées"
media_order: PARCOURS-NUMERIQUE.png
type_ressource: fichier
license: cc-by-sa
date: '01-01-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'médiation numérique'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Afin de partager avec le plus grand nombre la démarche mise en oeuvre dans le cadre de Parcours numériques, une publication, sous la forme d''un livre et d''une affiche, a été publiée en octobre 2014.'
    image: PARCOURS-NUMERIQUE.png
metadata:
    description: 'Afin de partager avec le plus grand nombre la d&eacute;marche mise en oeuvre dans le cadre de Parcours num&eacute;riques, une publication, sous la forme d''un livre et d''une affiche, a &eacute;t&eacute; publi&eacute;e en octobre 2014.'
    'og:url': 'https://ressources.pingbase.net/fiches/publication_parcours_numeriques'
    'og:type': website
    'og:title': 'Parcours Num&eacute;riques&nbsp;: cultures, pratiques &amp; m&eacute;diations partag&eacute;es | Espace Ressources Num&eacute;riques'
    'og:description': 'Afin de partager avec le plus grand nombre la d&eacute;marche mise en oeuvre dans le cadre de Parcours num&eacute;riques, une publication, sous la forme d''un livre et d''une affiche, a &eacute;t&eacute; publi&eacute;e en octobre 2014.'
    'og:image': 'https://ressources.pingbase.net/fiches/publication_parcours_numeriques/PARCOURS-NUMERIQUE.png'
    'og:image:type': image/png
    'og:image:width': '874'
    'og:image:height': '473'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Parcours Num&eacute;riques&nbsp;: cultures, pratiques &amp; m&eacute;diations partag&eacute;es | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Afin de partager avec le plus grand nombre la d&eacute;marche mise en oeuvre dans le cadre de Parcours num&eacute;riques, une publication, sous la forme d''un livre et d''une affiche, a &eacute;t&eacute; publi&eacute;e en octobre 2014.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/publication_parcours_numeriques/PARCOURS-NUMERIQUE.png'
    'article:published_time': '2014-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:17:08+01:00'
    'article:author': Ping
---

*Afin de partager avec le plus grand nombre la démarche mise en oeuvre dans le cadre de Parcours numériques, une publication, sous la forme d'un livre et d'une affiche, a été publiée en octobre 2014.*

Cet ouvrage, coordonné par l'association PiNG, propose un retour en textes et images sur un parcours numérique effectué en région Pays de la Loire, de 2011 à 2014 : à la rencontre d’un réseau d’espaces de médiation numérique, entre échanges et pratiques.
Des articles de fond sur des sujets liés à la culture numérique, ainsi qu’une partie pratique, complètent cette expédition en apportant des clefs de compréhension et de la matière à expérimenter collectivement.
Outil de travail, manuel pratique ou initiateur de rencontres  nous espérons ; que cet ouvrage vous guidera dans vos explorations numériques, à vous d’inventer votre propre parcours et de le partager.

Une affiche exposant les enjeux de la culture numérique complète cet ouvrage.

[TÉLÉCHARGER](https://station.pingbase.net/index.php/s/Q6Pn23jGkpXdo7o?classes=btn)

---
Conception graphique & mise en page : Yanaïta Araguas.
