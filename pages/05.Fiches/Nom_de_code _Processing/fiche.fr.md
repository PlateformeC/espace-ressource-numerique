---
title: 'Nom de code : Processing'
media_order: photo.jpg
type_ressource: text
feature_image: photo.jpg
license: cc-by-nc-sa
date: '06-11-2013 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - langage
        - code
        - processing
        - programmation
    author:
        - 'Thomas Bernardi'
aura:
    pagetype: website
    description: 'Le code comme un outil plastique'
    image: photo.jpg
metadata:
    description: 'Le code comme un outil plastique'
    'og:url': 'https://ressources.pingbase.net/fiches/nom_de_code%20_processing'
    'og:type': website
    'og:title': 'Nom de code : Processing | Espace Ressources Num&eacute;riques'
    'og:description': 'Le code comme un outil plastique'
    'og:image': 'https://ressources.pingbase.net/fiches/nom_de_code%20_processing/photo.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '640'
    'og:image:height': '360'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Nom de code : Processing | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Le code comme un outil plastique'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/nom_de_code%20_processing/photo.jpg'
    'article:published_time': '2013-11-06T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:57:37+01:00'
    'article:author': Ping
---

## De l'intérêt de s'approprier la technologie

Cette intuition, en forme de constat, vient consolider le projet d'appropriation des technologies de PiNG: « nous vivons dans les mots d'un écrivain de science fiction »1. L'innovation galopante nous garantie une abondance exponentielle de machines tout autour de nous. Une armada de dispositifs apparus pour nous rendre de précieux … ou nouveaux … services. Un environnement numérique protecteur que l'on voudrait bienveillant et dépourvu de tout parti pris idéologique.

Nous réalisons aujourd'hui que la neutralité présupposée de ces machines n'est pas avérée : l'actualité nous le confirme avec les affaires d'espionnage et de filtrage de nos données personnelles, les grands débats idéologiques autour de l'utilisation du copier/coller dans les pratiques culturelles ou encore l'obsolescence incontestable de nos gadgets électroniques.
Nous sommes la plupart du temps, spectateur de ces pérégrinations numérico-économiques et notre degré de compréhension de tout processus de captation de données, filtrage, construction, production etc. est proche du zéro.

![photo.jpg](photo.jpg)

Image réalisée à partir d'une application développée par Laurent Malys


En somme, nous ne comprenons absolument pas comment fonctionnent les machines qui nous entourent, et par extension – si celles ci sont des éléments constitutifs de notre monde contemporain – nous avons beaucoup de difficultés à appréhender le monde numérique qui est en train de se constituer autour de nous.
Dans un certain sens nous subissons les transformations de notre environnement, induites par les machines et le numérique, sans pouvoir agir sur ces dispositifs.
La démocratie [la puissance (cratos) du peuple (demos)], modèle de nos sociétés occidentales repose sur cet équilibre constitué par les capacités d'agir des personnes sur le monde et la société. Cette démocratie nécessite donc que chacun d'entre nous ai la possibilité de comprendre les technologies qui composent notre monde contemporain (smartphone, réseaux, carte à puce, informatique embarquée, domotique, robotique …) afin de pouvoir l'intégrer au mieux à un processus de construction sociétal.
S'approprier la technologie est donc une nécessité pour garantir l'émancipation des personnes et leur contribution à la construction d'un monde commun.

## Langages informatiques : langages vivants

Au delà de l'aspect matériel des technologies qui nous entourent et qui mériterait à lui seul un vaste chantier d'appropriation, un des fondements de l'informatique est bien le code, essence de la partie logicielle.
Le code informatique, cette suite d'instructions binaires qui permet la mise en branle de nos divers gadgets. Comprendre les machines qui nous entourent signifie bien comprendre ces langages.
Évidemment, la plupart des machines possèdent leur propre idiome et il semble improbable de tous les comprendre et les maîtriser, mais il est un paramètre que beaucoup de personnes ignorent : la logique de programmation est quasiment universelle (variables, fonctions … ) et les paradigmes régissant les différents langages sont peu nombreux et souvent complémentaires. Que l'on parle de Python, de Java, de C++, de PHP, de Lisp, de Prolog … un langage de programmation reste un dialecte qui permet à l'humain de contrôler la machine, de comprendre son fonctionnement, de créer aussi. Une fois un langage appris, il devient beaucoup plus simple d'en apprendre un second etc.

![2_0.png](2_0.png)

La programmation informatique est actuellement une pratique plutôt stigmatisée, du moins en France. Elle serait une activité réservée aux ingénieurs informaticiens et aux geeks avec le lot de clichés qu'elle véhicule : régime pizza, autisme notoire & désert affectif. Le code informatique est vu comme le langage de ceux qui préfèrent converser avec la machine plutôt qu'avec leurs semblables.
Avec cette mythologie peu enviable liée à la pratique du code informatique, difficile de convaincre les foules de s'y mettre.
Enseigner la programmation relativement tôt à l'école serait pourtant un moyen efficace de garantir à tout un chacun une plus grande autonomie dans un environnement de plus en plus informatisé.
« Jamais il n’y a eu autant de jeunes manipulant des ordis, des tablettes ou des smartphones, et si peu d’entre eux comprennent quelque chose à leur fonctionnement intrinsèque. La génération des bidouilleurs a laissé place à celle des consommateurs, qui achètent des produits dont on ne voit pas les vis/vices. » 2

 ```javascript
void setup() {
  size(640, 480);
  background(#B92037);
  String message = "Bonjour monde numérique";
  textSize(30);
  float x = textAscent() + textDescent();
  for (int i = 15; i < height + x; i += x) {
    text(message, 19, i);
  }
  saveFrame("BMN.png");
}
```

## Le code comme un outil plastique

De tout temps, les artistes, créateurs et autres démiurges, ont cherché à s’accaparer les techniques de leur époque. Ils les défrichent, les détournent de leurs usages initiaux, repoussent leurs limites … depuis les ciseaux à bois jusqu'aux platines vinyles, toutes les techniques tombent aux mains des artistes à un moment donné. Avec l'informatique, des générations d'étudiants en art, en design, en architecture … ont utilisé et continuent d'utiliser des logiciels pré-conçus par des ingénieurs pour leur permettre de se servir des ordinateurs dans les conditions les plus confortables possibles. La plupart du temps, ces logiciels disposent d'interfaces graphiques utilisateur (GUI) qui conditionnent leurs usages au travers de menus, options, ergonomies déjà préconçus et favorisent un nivellement des pratiques. La plupart de ces logiciels de renommée sont légion dans les écoles. Ils appartiennent à de grands groupes, ont un prix élevé, et sont propriétaires : impossible d'accéder à leur recette de fabrication. Les écoles d'art et autres, qui utilisent exclusivement ces outils enferment leurs étudiants dans des pratiques dépendantes et éloignées du potentiel réel d'appropriation de l'informatique. Il est toujours possible de s'échapper du carcan imposé par ces logiciels créatifs en les détournant, mais cette pratique se confronte à des limites lorsqu'il s'agit de pousser l'appropriation de l'outil « détourné ». Une personne créative doit pouvoir tirer profit de façon optimale de l'outil qu'elle utilise, et en ces termes, en informatique, il paraît évident qu'elle puisse, si elle le souhaite, utiliser le langage propre à l'outil informatique: le code, Le code comme un outil plastique. Face à la machine, le plasticien, le designer, l'architecte, doivent pouvoir modéliser leurs intentions à partir d'une page blanche afin de se constituer leurs propres outils. Une compréhension du code est donc essentielle et libératrice dans le processus créatif utilisant l'informatique.

![3_0.png](3_0.png)

Image réalisée à partir d'une application développée par Laurent Malys


Parmi les différentes solutions existantes pour introduire la programmation dans un cursus pédagogique on peut citer Processing. Ce langage de programmation soutenu par une large communauté d'utilisateurs vise à vulgariser la programmation auprès des designers, artistes et créateurs en tout genre. Son utilisation permet de se familiariser avec les rudiments du code dans un cadre dédié à la création numérique (graphisme génératif, visualisation de données, simulation, 3D etc.).
En somme Processing est une bonne porte d'entrée vers le monde de la programmation car il permet d'appréhender les bases de la programmation orientée objet, elle même utilisée dans des langages tel que le PHP, le C++, Python, Java etc.
Processing est également très proche d'Arduino en termes de syntaxe, et il permet le développement d'applications en local, directement en Java, sur Internet en Javascript et pour les mobiles en Android. Toutes ces possibilités font de Processing un outil très polyvalent et pédagogique, capable de faciliter l'accès du plus grand nombre au code informatique.

## Retour aux sources

Ce n'est pas tout de comprendre le code informatique, il faut pouvoir y avoir accès. Là est l'enjeu des logiciels libres : rendre possible l'accès aux recettes de cuisine des dispositifs informatiques qui nous entourent afin de pouvoir les comprendre, les modifier, les améliorer etc.
Actuellement, la plupart d'entre nous pensons que c'est un enjeu qui s'adresse aux informaticiens, seuls capables de déchiffrer du code informatique.
Or, nous venons de voir que nous sommes tous concernés par la problématique de l'accès aux codes sources dans un environnement fortement numérique.

![sdsds.png](sdsds.png)

Image réalisée à partir d'une application développée par Laurent Malys

Avoir accès au code source est donc primordial si l'on veut promouvoir une société de personnes émancipées dans un contexte numérique prégnant et non une masse assujettie à une matrice numérique dont elle n'a qu'une vague compréhension.
Processing est un langage de programmation et un EDI (environnement de développement informatique) sous licence libre, ce qui signifie que vous avez accès à ses recettes de fabrication ainsi qu'à celles des programmes réalisés par d'autres avec Processing. De cette façon vous pouvez comprendre, si vous le souhaitez, le programme de Processing, celui des applications réalisées avec Processing, Processing Android, Processing Javascript, Arduino etc.

---

- 1. Ariel Kyrou in « ABC Dick »
- 2. Douglas Rushkoff in « Program or Be Programmed: Ten Commands for a Digital Age  » – 2011  
