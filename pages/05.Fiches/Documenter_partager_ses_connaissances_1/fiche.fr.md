---
title: 'Documenter c’est partager ses connaissances 1/3'
media_order: DSC05673_modifie.png
type_ressource: text
feature_image: DSC05673_modifie.png
license: cc-by-sa
serie:
    -
        page: /fiches/documenter_partager_ses_connaissances_2
    -
        page: /fiches/documenter_partager_ses_connaissances_3
date: '11-06-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture libre'
        - opensource
        - 'c libre'
        - fablab
        - documentation
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: article
    description: 'La documentation est un des piliers de la culture libre et du partage des connaissances.'
    image: DSC05673_modifie.png
metadata:
    description: 'La documentation est un des piliers de la culture libre et du partage des connaissances.'
    'og:url': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_1'
    'og:type': article
    'og:title': 'Documenter c&rsquo;est partager ses connaissances 1/3 | PiNG Ressources Num&eacute;riques'
    'og:description': 'La documentation est un des piliers de la culture libre et du partage des connaissances.'
    'og:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_1/DSC05673_modifie.png'
    'og:image:type': image/png
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Documenter c&rsquo;est partager ses connaissances 1/3 | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'La documentation est un des piliers de la culture libre et du partage des connaissances.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/documenter_partager_ses_connaissances_1/DSC05673_modifie.png'
    'article:published_time': '2017-06-11T00:00:00+02:00'
    'article:modified_time': '2020-12-02T09:21:48+01:00'
    'article:author': Ping
---

*La documentation est un des piliers de la culture libre et du partage des connaissances. Seulement, il n’est pas toujours aisé de s’y retrouver, de savoir quoi documenter et pourquoi. À travers une trilogie d’articles, nous vous proposons de découvrir les aspects juridiques de la documentation dans les fablabs.*

La documentation est une partie importante de la dynamique de la culture libre, des fablabs et du DIY. D’ailleurs, avec le groupe de travail C_libre, il est arrivé à plusieurs reprises que l’on s’interroge sur les implications et les conséquences de la documentation dans un fablab. On a donc décidé de vous parler des points importants de friction et de porosité entre la culture libre et la documentation. Gageons que tout bon Maker a, au moins une fois, vu et/ou lu la charte des Fablabs. Un des articles de cette charte précise :

*What are your responsibilities ?*

- Safety : Not hurting people or machines
- Operations : Assisting with cleaning, maintaining, and improving the lab
- **Knowledge : Contributing to documentation and instruction**

*Quelles sont vos responsabilités ?*

- Sécurité : Ne blesser personne et ne pas endommager l’équipement.
- Fonctionnement : Aider à nettoyer, maintenir et améliorer le Lab.
- **Connaissances : Contribuer à la documentation et aux connaissances des autres.**

Il est donc précisé qu’il faut dans un fablab contribuer à la documentation ! Alors à vos claviers. Oui mais, pourquoi documenter ? Pour qui documenter ? Documenter, oui mais quoi ? Et surtout : que se passe-t-il lorsque je documente ? Ces questions sont récurrentes et il n’est pas toujours évident de percevoir l’intérêt de la documentation ni ce qu’elle implique. Pour y voir plus clair, nous vous proposons une suite d’articles sur le sujet de la documentation dans les fablabs.

Du point de vue du droit, nos amis makers font deux types de créations : ce sont soit des œuvres de l’esprit (entendre par là des créations plutôt artistiques ou culturelles au sens large) soit des créations industrielles (entendre par là des créations qui ont vocation à être reproduites en série pour être vendues et qui procurent un avantage concurrentiel à son créateur). Ainsi, les deux prochains articles suivront cette compartimentation de la propriété intellectuelle.

Mais avant d’entrer dans le vif du sujet, commençons par poser quelques généralités sur la documentation.

Le terme « documentation » est polysémique. C’est à la fois l’ « Action d’appuyer ce que l’on avance (par écrit ou à l’oral) sur des documents » 1. On dira par exemple : « votre propos est bien documenté » lorsque les sources du travail seront accessibles. Ce sens donné au mot documentation doit intéresser le maker car il est fortement conseillé de citer ses sources et de les rendre accessibles (ne dit-on pas d’ailleurs Open Source ?). De plus, un travail bien documenté est souvent un gage de qualité et permettra à l’utilisateur d’aller poursuivre ses recherches. C’est aussi un « Travail spécialisé consistant à rechercher, sélectionner, classer, diffuser et conserver tout document portant sur un ensemble de sujets ou sur un sujet particulier »2. On s’approche plus dans ce sens, du travail du documentaliste qui va conserver et classer un ensemble de documents. On pense bien entendu aux bibliothèques mais il est intéressant de constater que les sites de documentation des fablabs sont des bases de données grandissantes, des sortes de bibliothèques numériques. Concernant le Fablab Plateforme C, cela se passe sur le wiki [Fablabo](http://www.fablabo.net/). Le travail consistant à classer et organiser les éléments qui s’y trouvent est donc un travail de documentation.

![Fablabo Pourquoi Documenter](Fablabo_pourquoi_documenter.png)

Mais à vrai dire, la documentation dans un fablab c’est un peu ça mais pas tout à fait ça non plus. Lorsque l’on parle de documentation dans un fablab on pense surtout à l’ensemble des documents faits par les makers qui visent à présenter et expliquer ce qui a été fait, est fait ou peut être fait dans un fablab. Cette définition est très large, certes. Mais elle permet d’englober la variété de ce qui est documenté dans un fablab. En effet, il peut s’agir du projet d’un maker (c’est souvent le cas), de la méthodologie de l’emploi d’une machine (ça arrive), ou encore de techniques d’animation de lieux ou d’ateliers (c’est plus rare) etc. Le mot documentation correspond alors à l’action de produire de la documentation. Nous sommes alors, les écrivains de ces bibliothèques numériques.

Maintenant que l’on sait de quoi on parle (tout du moins à peu près), un peu de contexte ne fera pas de mal. Il existe beaucoup de littérature sur la documentation car un fablab passera nécessairement par une phase d’interrogation sur le sujet. Il y a donc pléthore de textes, de conférences, de compte-rendus d’ateliers ou de workshops que l’on peut trouver çà et là sur internet et IRL. Concernant Plateforme C, deux jours ont été consacrés à la documentation en [2014](http://fablabo.net/wiki/WorkshopDocumentation) : cela a donné lieu à cette page sous forme de tutoriel qui aide à la documentation.

**Documenter c’est partager ses connaissances. Alors, comment les partager ?**

Avant internet et les fablabs, on répondait classiquement à cette question en précisant qu’il y a deux manières de faire : – soit la manière « orale » technique de partage des connaissances et des savoirs, vieille comme le langage articulé. – soit de manière « écrite », technique qui remonte bien entendu à l’émergence de l’écriture mais qui prend un réel envol dès lors que les écrits vont circuler plus facilement (environ 100 ans après l’invention de l’imprimerie).

Chacune de ces techniques présente des avantages et des inconvénients. L’oral est plus simple et plus direct. Il ne nécessite pas de savoir lire, seulement de parler la même langue. C’est d’ailleurs parfois problématique car le réseau des fablabs est largement mondialisé et il n’est pas toujours évident d’avoir de vrais échanges en raison de la barrière de la langue. On aura d’ailleurs l’occasion d’y revenir car le droit d’auteur n’est pas partout le même. Il varie en fonction des cultures et adopte donc lui aussi son propre langage ce qui n’est pas sans poser certaines difficultés. La transmission orale se fait en présentiel et est souvent plus interactive : celui qui apprend peut en général dialoguer avec celui qui sait. En revanche, en l’absence de cet érudit il n’y a pas de transmission, pas d’échange, pas de partage. La manière orale est moins pérenne que la manière écrite.

L’écrit lui présente l’avantage de toucher un public plus large à travers l’espace et le temps. L’érudit n’a plus nécessairement besoin d’être présent pour que son savoir soit partagé, cela se fait au travers de ses écrits. C’est le reflet d’un instant « t » de la connaissance d’une personne ou d’un collectif de personnes. C’est en quelque sorte une mémoire, un instantané d’une époque. Coté inconvénients, il faut parler la même langue (même problème que précédemment), savoir lire et de manière générale, l’écrit est d’évolution plus lente puisqu’il est moins interactif.

Lorsqu’on parle de documentation bien entendu on se réfère aux documents et il fut un temps pendant lequel les documents n’étaient que ces écrits (mots et dessins). Mais l’ensemble des révolutions technologiques sont passées par là. Grâce à internet, encore une fois, tous les codes et les barrières se sont fragilisés. Il est désormais possible de produire et de diffuser toute sorte de documents, écrits, vidéos, podcasts, logiciels etc. Alors, pourquoi s’en priver ? La documentation dans les fablabs, c’est surtout ça ! C’est vouloir partager un ensemble de choses et le faire.

![Documentation fablab](Documentationfablab-depliant-recto.jpeg)

Et alors qu’est-ce qu’on documente ? Connaissances, savoirs, projets… qu’est-ce que c’est au fond ? Pour beaucoup ce sont simplement des textes, des images (animées ou non) des descriptions… Le juriste y voit plus que ça ! Il y voit souvent des œuvres, des inventions, des dessins et modèles. Il y voit également parfois des contrats, ces fameuses licences libres utilisées plus ou moins correctement. Alors, comment se repérer dans ce méandre ? Comment bien adapter la bonne licence au bon projet et surtout comment bien la choisir pour qu’elle corresponde aux envies de partage du créateur ? Pour répondre à ces questions il va falloir parler de propriété intellectuelle qui se sépare en deux branches : la propriété littéraire et artistique et la propriété industrielle (séparation dont on a parlé plus haut). Ces deux branches n’obéissent pas aux mêmes règles et n’ont pas la même logique, il faudra donc comprendre ces logiques. De plus, chaque situation est spéciale et mérite une réponse personnalisée. On se tient à votre écoute pour cette personnalisation mais pour l’heure, il est temps de vous donner les grandes lignes pour que vous puissiez vous forger votre propre raisonnement et que vous puissiez gagner en autonomie.

Bonne lecture.

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.

1 TLFI (Trésor de la langue française informatisé) : Documentation http://www.cnrtl.fr/definition/documentation
2 Idem
