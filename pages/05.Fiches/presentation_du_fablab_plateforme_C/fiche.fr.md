---
title: 'Présentation du fablab Plateforme C'
media_order: fablab.png
type_ressource: video
feature_image: fablab.png
license: cc-by-nc-sa
date: '01-01-2015 00:00'
taxonomy:
    category:
        - recits
    tag:
        - fablab
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Voici en vidéo la présentation du fablab Plateforme C !'
    image: fablab.png
anchors:
    active: false
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_breadcrumbs: true
content:
    items:
        - '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    description: 'Voici en vid&eacute;o la pr&eacute;sentation du fablab Plateforme C !'
    'og:url': 'https://ressources.pingbase.net/fiches/presentation_du_fablab_plateforme_c'
    'og:type': website
    'og:title': 'Pr&eacute;sentation du fablab Plateforme C | Espace Ressources Num&eacute;riques'
    'og:description': 'Voici en vid&eacute;o la pr&eacute;sentation du fablab Plateforme C !'
    'og:image': 'https://ressources.pingbase.net/fiches/presentation_du_fablab_plateforme_c/fablab.png'
    'og:image:type': image/png
    'og:image:width': '978'
    'og:image:height': '511'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Pr&eacute;sentation du fablab Plateforme C | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Voici en vid&eacute;o la pr&eacute;sentation du fablab Plateforme C !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/presentation_du_fablab_plateforme_c/fablab.png'
    'article:published_time': '2015-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-24T15:04:14+01:00'
    'article:author': Ping
---

*Voici en vidéo la présentation du fablab Plateforme C !*

<iframe width="100%" height="600" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/db40c3de-f352-4491-bc83-a9bba97e1b04" frameborder="0" allowfullscreen></iframe>
