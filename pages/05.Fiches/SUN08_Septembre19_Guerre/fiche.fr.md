---
title: 'Les doigts dans la prise #8 : Dataggedon'
type_ressource: audio
feature_image: dataggedon_guerre_SUN_PiNG-768x1024.jpeg
license: cc-by-nc-sa
published: true
date: '12-11-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Tom Niderprim'
aura:
    pagetype: website
    description: 'La guerre pousse au progrès numérique. Pour être à la pointe, les armées développent des outils toujours plus performants. Tom Niderprim nous en dit plus dans sa chronique.'
    image: dataggedon_guerre_SUN_PiNG-768x1024.jpeg
metadata:
    description: 'La guerre pousse au progr&egrave;s num&eacute;rique. Pour &ecirc;tre &agrave; la pointe, les arm&eacute;es d&eacute;veloppent des outils toujours plus performants. Tom Niderprim nous en dit plus dans sa chronique.'
    'og:url': 'https://ressources.pingbase.net/fiches/sun08_septembre19_guerre'
    'og:type': website
    'og:title': 'Les doigts dans la prise #8 : Dataggedon | Espace Ressources Num&eacute;riques'
    'og:description': 'La guerre pousse au progr&egrave;s num&eacute;rique. Pour &ecirc;tre &agrave; la pointe, les arm&eacute;es d&eacute;veloppent des outils toujours plus performants. Tom Niderprim nous en dit plus dans sa chronique.'
    'og:image': 'https://ressources.pingbase.net/fiches/sun08_septembre19_guerre/dataggedon_guerre_SUN_PiNG-768x1024.jpeg'
    'og:image:type': image/webp
    'og:image:width': '768'
    'og:image:height': '1024'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #8 : Dataggedon | Espace Ressources Num&eacute;riques'
    'twitter:description': 'La guerre pousse au progr&egrave;s num&eacute;rique. Pour &ecirc;tre &agrave; la pointe, les arm&eacute;es d&eacute;veloppent des outils toujours plus performants. Tom Niderprim nous en dit plus dans sa chronique.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun08_septembre19_guerre/dataggedon_guerre_SUN_PiNG-768x1024.jpeg'
    'article:published_time': '2019-11-12T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:52:01+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

La guerre pousse au progrès numérique. Pour être à la pointe, les armées développent des outils toujours plus performants. Tom Niderprim nous en dit plus dans sa chronique.

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-guerre-numerique) !

![Les doigts dans la prise - Dataggedon](Les%20doigts%20dans%20la%20prise%20-%20guerre%20et%20numerique.mp3)

Le numérique, ça permet de faire plein de trucs. Par exemple, il est tout à fait possible de se poser devant le stream d’une série de science-fiction horrifique version années 80, tout en sélectionnant soigneusement son nouveau crush amoureux par un défilement compulsif de visages inconnus. Cela peut éventuellement être agrémenté d’envoi de messages constitués principalement de privates jokes à quelques copains. Et on peut parfaire le tout avec une tâche de fond, prête à être dégainée en cas d’ennui, qui nous demandera d’aligner différentes sortes de bonbons.

Cependant, force est de constater que le numérique sert à faire d’autres choses moins légères . Par exemple, faire la guerre. En réalité, si on en est techniquement là aujourd’hui, c’est principalement pour des motifs militaires.

Revenons un peu en arrière :  Si l’on tape sur Google « histoire d’internet » et que l’on clique sur le lien wikipédia, le tableau qui indique les évènements importants commence par : 1952 - création du SAGE, nom donné à un système d'armes mettant en réseau 40 ordinateurs à autant de radars. Mesurons un instant l’ironie militaire. Un peu plus loin : 1962 - Début de la recherche par la DARPA, une agence du département de la Défense américaine. Visiblement, l’armée est au centre du développement de l’informatique et d’internet et pour cause : la guerre du Vietnam sera un véritable terrain d’expérimentation pour l’informatisation de la guerre.

Dans son livre The Closed World, l’historien de l’informatique Paul Edwards décrit le projet Igloo White, du nom du centre de commande de l’armée de l’air américaine durant le conflit en Asie du Sud-est. Igloo White, c’est la concrétisation du « champ de bataille électronique ». Des capteurs installés sur la piste Ho Chi Minh au Laos détectent les passages de convois ; les données sont transmises directement aux ordinateurs qui calculent leur trajectoire et leur vitesse ; puis les coordonnées sont envoyées aux pilotes d’avion chargés de larguer des bombes aux endroits indiqués. Le processus ne prend pas 5 minutes et pourtant il est décisif dans la manière de guerroyer à l’époque moderne : plus besoin d’être au contact de l’ennemi, le calcul binaire s’occupe de vous dire où et à quel moment il faut frapper. Bien sur, ça lui arrivait de se tromper, et la guérilla viet-cong savait comment détourner le système : un radio-cassette avec un bruit de camion suffisait à ce que quelques bombes soient lâchées n’importe où. Des failles estimées somme toutes dérisoires qui n’ont pas empêché le développement du numérique martial.

Aujourd’hui, où en sommes-nous?  
L’article écrit par Oliver Koch publié dans le monde diplomatique de mars dernier donne un aperçu des pratiques militaires contemporaines. Il faut imaginer un soldat américain confortablement installé derrière un écran à Washington. De l’autre côté de l’atlantique, un drone survole une zone en Afrique ou au Moyen-Orient, dans le cadre de la lutte contre Boko-Haram ou Al-Quaida. Celui-ci surveille des individus aux comportements suspects et compare les informations récoltées à une base de donnée alimentée par le renseignement militaire, les médias et les réseaux sociaux. En s’appuyant sur une analyse des fréquentations et des déplacements des individus, les ingénieurs déduisent si la personne a, je cite, un « profil insurrectionnel » . Ainsi, lorsque qu’un  insurgé est pris pour cible, ce n’est pas tant pour ce qu’il a fait que pour évaluer si son parcours constitue un danger. Selon l’avis des experts du Pentagone, sans autre forme de procès, la personne peut être tuée sur le champ et, souvent, l’on n’apprend son identité qu’après sa mise à mort.  

Nous sommes donc entrés dans l’ère du terrorisme potentiel, où les gouvernements mènent une bataille mondiale, permanente mais silencieuse contre des stéréotypes comportementaux. Plus de frontières, plus d’identités, presque plus d’idéologie même, mais seulement des comportements déviants à surveiller, contrôler et effacer. La rationalité froide de la technologie vient se frotter à un vieux fantasme du contrôle social: celui d’anticiper les actes avant qu’ils ne se produisent. Il semblerait que Minority Report se soit transformé en documentaire géopolitique, à la différence que l’origine de ces douteuses prédictions ne provient pas de trois étranges personnages installés dans une piscine mais plutôt du Big Data.
