---
title: 'Chroniques d’un Atelier Partagé #4'
media_order: IMG_3111.jpg
type_ressource: text
feature_image: IMG_3111.jpg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/chroniques_dun_atelier_partage_1
    -
        page: /fiches/chroniques_dun_atelier_partage_2
    -
        page: /fiches/chroniques_dun_atelier_partage_3
    -
        page: /fiches/chroniques_dun_atelier_partage_5
date: '24-01-2019 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'atelier partagé'
        - low-tech
        - réparation
        - chroniques
    author:
        - 'Édouard Bourré-Guilbert'
aura:
    pagetype: article
    description: 'Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. '
    image: IMG_3111.jpg
metadata:
    description: 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;. '
    'og:url': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_4'
    'og:type': article
    'og:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #4 | PING Ressources Num&eacute;riques'
    'og:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;. '
    'og:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_4/IMG_3111.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '5184'
    'og:image:height': '3456'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Chroniques d&rsquo;un Atelier Partag&eacute; #4 | PING Ressources Num&eacute;riques'
    'twitter:description': 'Une pi&egrave;ce de vie pas comme les autres racont&eacute;e en cinq temps par &Eacute;douard Bourr&eacute;-Guilbert, &agrave; lire comme un carnet de bord pour s&rsquo;embarquer au c&oelig;ur de l&rsquo;Atelier Partag&eacute;. '
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/chroniques_dun_atelier_partage_4/IMG_3111.jpg'
    'article:published_time': '2019-01-24T00:00:00+01:00'
    'article:modified_time': '2021-01-08T17:00:10+01:00'
    'article:author': Ping
---

*Une pièce de vie pas comme les autres racontée en cinq temps par Édouard Bourré-Guilbert, à lire comme un carnet de bord pour s’embarquer au cœur de l’Atelier Partagé. En tant qu’explorateur associé de PiNG, Édouard nous livre son regard sur l’aventure de l’Atelier Partagé du Breil, que l’association anime depuis maintenant 3 ans. Une façon de nous faire découvrir ce qui compose un tel lieu : ses figures, ses temps forts, ses zones de tensions et ses petits miracles.*

Pour les aficionados du bouton Pause, les gourmands du panoramique, les adeptes du Carpe diem, et tous les autres qui ne font pas le lien dans ce qui vient d’être écrit, cet instant vous est dédié.

## ÉPISODE 4 : L’INSTANTANÉ

C’est le début d’une nouvelle année, il est presque 18h03 et, comme le dérèglement climatique n’a pas encore eu son mot à dire là-dessus, la nuit a déjà pris toute sa place dehors. Au second étage du numéro 38 de la rue du Breil, j’avance en direction de l’atelier, dans ce couloir que je connais bien. Et pourtant, je découvre que l’entrée a changé. Cela m’interpelle l’espace d’une demi-seconde. Comme toute bonne personne ayant pris ses habitudes, je suis facilement perturbé lorsque ces dernières changent. À l’instar de ces charentaises qui ne sont pas là où elles devraient être. Les charentaises… l’angoisse.
Soudain, un cri victorieux sort de la salle. Il résonne dans le couloir jusqu’à mes oreilles, chassant au passage les charentaises de ma tête. J’arrive au seuil de la porte, les lumières s’éteignent soudainement. Roh non, je suis pourtant quasiment certain que ce n’est pas mon anniversaire. Je souris tandis que la lumière se rallume. Face à moi, ça fourmille dans un brouhaha ambiant, des éclats de rires, mais personne pour engager d’un anglais non-affirmé le traditionnel Happy Birthday. En réalité, les plombs ont sauté. À cause d’un grille-pain à peine réparé. Un classique ici.
Bon, ce n’est pas tout ça mais qu’est-ce que j’étais venu faire déjà ? Ah oui, se dire bien des choses pour cette nouvelle année, prendre des nouvelles de l’atelier, des gens, probablement attraper un bout de galette royale et un verre de cidre, annoncer des nouvelles et, accessoirement, préparer le terrain pour la réparation de mon téléphone portable. J’ai bien l’autoradio qui est bloqué sur Nostalgie depuis des lustres, mais ma capacité de résilience est plus mise à mal par un téléphone qui me lâche en permanence que par Julien Clerc.

Je rentre dans l’atelier par cette nouvelle pièce, attenante à celle que je connaissais, et démarre ma tournée des têtes familières. J’en profite également pour rencontrer celles que je ne connais pas. Elles sont de plus en plus nombreuses les semaines passant. Ces dernières sont toutes penchées sur un truc, un bidule, avec un machin souvent chouette dans les mains. Aujourd’hui, c’est le premier mardi du mois et, comme tout ceux-là, il est spécialement consacré à la réparation. C’est aussi le premier mardi de l’année et donc le bon moment pour renouveler son adhésion à l’association. De toute manière, c’est mardi soir, et comme vous le savez, « PiNG is the place to be on tuesday night, period!* »

J’arpente donc les deux pièces composant désormais l’atelier, passant de l’une à l’autre et vice versa, par souci d’équité. Je retrouve mes repères dans la pièce historique via les tables, étagères et établis remplis de composants, de matériaux bien identifiés, d’outils à dimensions et fonctions variables, et de projets en cours. Sauf que tout cela a été réaménagé.
Depuis quelques temps, l’atelier accueille l’autre projet animé par l’Association, le fablab Plateforme C (contraction de l’anglais « fabrication laboratory ») et ses machines particulières. Ainsi, une découpeuse laser a pris la place de l’ancien coin bibliothèque qui a migré dans la pièce attenante, en profitant par la même occasion pour s’affirmer comme un centre de ressources riche d’une centaine d’ouvrages, de revues spécialisées, d’une grainothèque et de café toujours bien frais. Dans la famille des machines qui tranche, on passe aussi à côté d’une découpeuse vinyle « pimpée » comme il se doit afin de dessiner en plus de découper, ou encore d’une brodeuse à commande numérique permettant de tisser en vrai ce qui n’aurait probablement jamais dû sortir de la tête de certains ! Sans parler des imprimantes 3D à l’autonomie et à la carlingue variables qui semblent s’affairer toute seule. Aux côtés de toutes ces machines, on retrouve leurs manipulateurs préférés. L’équipe de coordination et les adhérents du fablab viennent se mélanger à celles et ceux de l’atelier, créant des dynamiques nouvelles, renouvelant des rencontres lointaines, déterrant des projets ancestraux ou débattant simplement sur la matière des fèves de la galette des rois.

Je passe au travers d’un petit groupe d’irréductibles qui s’est formé à la frontière des deux salles. En 30 secondes, le conciliabule s’est réuni spontanément autour d’une imprimante multifonction qui vient d’être testée et certifiée valide par l’un des réparateurs assidus. Ça cause de cartouches d’encres plus chères que l’appareil, des nettoyages automatiques qui consomment la moitié de l’encre et créent une mélasse tout au fond, de l’obsolescence programmée, des techniques de chacun pour avoir le sentiment de moins subir tout ça. À deux pas, on baby-sit le manioc, on fabrique des porte-chèques pour maintenir ses économies au chaud, on coud des sacoches de vélo pour promener ses précieux poireaux, ou encore on informe sa famille que le pronostic vital de leur écran plat est engagé. Et dans ce grand ensemble, deux volontaires en service civique semblent évoluer comme des poissons dans l’eau, accueillant le flux hétérogène de visiteurs, répertoriant ce qu’il se passe dans le journal de bord, prenant les (ré)adhésions, etc.

Vous l’aurez probablement compris en arrivant à ces lignes, l’équipement est le décor de cet instantané mais la star, ici, ce n’est pas l’imprimante 3D, ce sont les utilisateurs. Et à travers eux, les valeurs défendues de mixité sociale, d’une éducation populaire. Toutes ces têtes penchées sur ces objets en attente de réanimation en sont l’illustration. Il y a quelque chose de l’ordre du « Gepettesque » quand on voit un objet reprendre vie. Pourtant, c’est l’interaction et la stimulation entre les personnes qui sont les réels bénéfices tirés de ce moment partagé. Le succès d’une telle entreprise se mesure à la qualité des relations entre individus, à leur capacité à faire ensemble. Le projet de l’individu est le prétexte, l’apprentissage le chemin, l’interaction la finalité. Ouf ! Finissons notre verre de jus de pomme et allons méditer cela.

Pour venir goûter à la frangipane garantie sans Bisphénol-A, il n’y a qu’une seule bonne façon de faire, nous rejoindre chaque mardi. La dégustation débute toujours à la même heure, 16h, et nous acceptons les retardataires ! À la semaine prochaine.

Edouard Bourré-Guilbert

*« Le mardi soir, c’est chez PiNG que ça se passe, un point c’est tout. »
