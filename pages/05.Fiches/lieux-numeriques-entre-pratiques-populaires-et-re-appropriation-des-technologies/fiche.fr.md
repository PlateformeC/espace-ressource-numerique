---
title: 'Lieux numériques, entre pratiques populaires et ré-appropriation des technologies ?'
media_order: u6v7p-768x576.jpeg
type_ressource: text
feature_image: u6v7p-768x576.jpeg
license: cc-by-nc-sa
published: true
date: '22-11-2018 12:25'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture libre'
        - fablab
        - 'éducation populaire'
        - 'culture numérique'
        - recherche-action
    author:
        - 'Julien Bellanger'
menu: 'Lieux, pratiques populaires et ré-appropriation des technologies'
aura:
    pagetype: article
    description: 'Bricoleur culturel et autodidacte en montage de projets artistiques et associatifs, je participe au sein de la structure associative PiNG à l’exploration des sentiers «numériques» en ouvrant des «lieux» de pratique et de ré-appropriation des technologies'
    image: u6v7p-768x576.jpeg
metadata:
    description: 'Bricoleur culturel et autodidacte en montage de projets artistiques et associatifs, je participe au sein de la structure associative PiNG &agrave; l&rsquo;exploration des sentiers &laquo;num&eacute;riques&raquo; en ouvrant des &laquo;lieux&raquo; de pratique et de r&eacute;-appropriation des technologies'
    'og:url': 'https://ressources.pingbase.net/fiches/lieux-numeriques-entre-pratiques-populaires-et-re-appropriation-des-technologies'
    'og:type': article
    'og:title': 'Lieux num&eacute;riques, entre pratiques populaires et r&eacute;-appropriation des technologies ? | PING Ressources Num&eacute;riques'
    'og:description': 'Bricoleur culturel et autodidacte en montage de projets artistiques et associatifs, je participe au sein de la structure associative PiNG &agrave; l&rsquo;exploration des sentiers &laquo;num&eacute;riques&raquo; en ouvrant des &laquo;lieux&raquo; de pratique et de r&eacute;-appropriation des technologies'
    'og:image': 'https://ressources.pingbase.net/fiches/lieux-numeriques-entre-pratiques-populaires-et-re-appropriation-des-technologies/u6v7p-768x576.jpeg'
    'og:image:type': image/webp
    'og:image:width': '768'
    'og:image:height': '576'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Lieux num&eacute;riques, entre pratiques populaires et r&eacute;-appropriation des technologies ? | PING Ressources Num&eacute;riques'
    'twitter:description': 'Bricoleur culturel et autodidacte en montage de projets artistiques et associatifs, je participe au sein de la structure associative PiNG &agrave; l&rsquo;exploration des sentiers &laquo;num&eacute;riques&raquo; en ouvrant des &laquo;lieux&raquo; de pratique et de r&eacute;-appropriation des technologies'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/lieux-numeriques-entre-pratiques-populaires-et-re-appropriation-des-technologies/u6v7p-768x576.jpeg'
    'article:published_time': '2018-11-22T12:25:00+01:00'
    'article:modified_time': '2021-01-04T17:36:57+01:00'
    'article:author': Ping
---

*Cet article a été publié dans le numéro 51-52 des Cahiers de l’INJEP autour du thème “[Recherche-action et écriture réflexive : la pratique innovante des espaces comme levier de transformation sociale](https://www.cairn.info/revue-cahiers-de-l-action-2018-2.htm)”*

## Introduction

Bricoleur culturel et autodidacte en montage de projets artistiques et associatifs, je participe au sein de la structure associative PiNG à l’exploration des sentiers «numériques» en ouvrant des «lieux» de pratique et de ré-appropriation des technologies où les questions “d’émancipation collective et de transformation sociale” traversent nos espaces.

Cet article, point d’étape d’une réflexion continue et partagée, propose de poser les bases d’un écrit collectif à partir de **l’hypothèse suivante** : plus il y a de la technologie, plus nous avons besoin de lieux physiques favorisant une réelle appropriation sociale de ces technologies. Mais est-ce suffisant ? Quels sont les retours d’expérience dans ces lieux ? Comment nos explorations numériques viennent-elles interroger l’éducation populaire ? Comment partager ces questions avec les acteurs de l’intérêt général, de l’éducation, de la culture, des mouvements sociaux ?

Partant d’échanges avec différents collaborateurs et amis, nourris par les recherches des uns et des autres, je présente ici une problématique sur les conditions d’appropriation des technologies en la complétant par un lexique en (dé)construction. Cette méthode « contributive » sera utilisée tout au long de l’année pour augmenter ce texte initial _via_ une publication en ligne et sur papier.

Il ne s’agit donc pas d’un constat définitif, figé et dogmatique, mais d’un pas de côté brut, d’une méthode réflexive et d’un espace de croisement pour « se réapproprier les espaces de travail de la culture », pour cultiver de bonnes « formes d’intervention ». Citons Benjamin Cadon de [Labomédia](http://www.labomedia.org/), Jeff Rolez de [Snalis44](https://snalis.org/), Alain Giffard et Xavier F. de [Bureau d’Études](http://bureaudetudes.org/) qui ont participé à ce laboratoire mix-d’idées, ainsi que d’autres contributeurs anonymes pour l’instant.

## Problématique

À la question initiale de savoir si nos espaces de pratiques participaient à une meilleure appropriation sociale des sciences, des techniques et de la technologie numérique, nous avons répondu « oui, mais… ». À partir de ce constat sont apparues quatre interrogations :

### Pourquoi est-ce nécessaire de s’approprier les technologies ?

Une vision du monde qui passe de plus en plus par le prisme du « numérique », notre monde se transforme petit à petit en données binaires avec lesquelles nous sommes invités à interagir.
[La théorie de l’information de Claude Shannon](http://centenaire-shannon.cnrs.fr/) met en avant la discrétisation du vivant, une forme de simplification par la transformation de l’analogique en tranches de 0 et de 1 qui induit une transformation de notre paysage intellectuel et imaginaire.
La suprématie d’une [vision scientiste](https://fr.wikipedia.org/wiki/Scientisme), la représentation du monde à travers la science et la technique, l’efficacité de la preuve par l’expérience, la technique (en) « marche » et s’impose comme vision du monde au détriment d’une approche sensible et plus proche du fonctionnement de la nature.
D’un point de vue logique, la question de la possibilité de s’approprier les techniques est première par rapport à celle de la nécessité. Il faut de la médiation.
Ainsi, la technique sans médiation n’est qu’un aspect du [grand « bluff technologique »](http://www.fayard.fr/le-bluff-technologique-9782818502273), une sorte de culture technique industrielle-consumériste-marketing, qui formate les usages et peut (doit) être combattue par une culture technique critique.
S’il est possible de s’approprier la technique donc, il est nécessaire de le faire parce que la technique tend à se greffer sur la totalité des relations humaines, et à être elle-même la relation de référence, structurante et centrale. Il faut donc délaisser la notion de technique-moyen pour celle d’une [technique-relation humaine](http://maisouvaleweb.fr/innover-avec-gilbert-simondon-ou-comment-reapprendre-a-faire-aimer-les-machines/).
S’approprier la technique ce n’est pas adopter un moyen pour une finalité qui nous est propre ; c’est définir un sens à la relation, entre hommes et techniques.
L’homme ne devant jamais être considéré comme un moyen par l’homme.

### À qui profitent les lieux de… « médiation » ?

Dans quelle mesure, à travers les lieux de médiation, sommes-nous des agents de promotion de ces objets techniques et méthodes ? De façon presque involontaire, nous sommes des facteurs de validation de ces progrès techniques, et ce malgré une posture critique. À travers les arts numériques notamment, nous sommes amenés à utiliser les « dernières technologies » et à en faire ainsi la promotion.
Nous sommes également parfois, contre notre volonté, complètement parties prenantes dans « l’écosystème » créatif et innovant : les labs (fablab, medialab, hacklab, etc.) comme avant-garde de l’innovation (avec, par exemple, la récupération des [hackatons](https://www.cairn.info/revue-cahiers-de-l-action-2018-2-page-87.htm#no5) par les démarches entrepreneuriales), nous sommes parfois défricheurs de futurs terrains fertiles mais dont les légumes et les fruits seront récoltés par des start-up à la pointe de l’intégration capitaliste de ces dynamiques créatives et de partage.

Les lieux de médiation sont donc des lieux de tension, de conflit entre des injonctions à l’innovation industrielle et des appels à un mouvement d’une culture critique. Ces lieux ne peuvent éviter (même placés sous le signe de la **culture libre**) d’être intégrés, à un degré ou à un autre, à [l’économie de l’attention](http://www.seuil.com/ouvrage/pour-une-ecologie-de-l-attention-yves-citton/9782021181425). Dans le modèle du _double-sided market_ (ou marché à double face, façon Google), ils figurent sur le premier côté, parmi toutes sortes de têtes de gondole. Le **conflit des attentions** croise et renforce le **conflit des cultures techniques**.
Cela ne signifie pas que les lieux de médiation soient condamnés à être instrumentalisés. Le seul fait d’ouvrir la question de la culture technique constitue un début de résistance (voire de sagesse).

### Comment et où produire des éléments de médiation vecteurs de transformation sociale ?

Tout en prenant en compte les éléments évoqués précédemment, il convient de faire œuvre de médiation pour aussi tenter de développer un esprit critique et distancié face à ces évolutions sociétales technologiques.
Si l’on ne veut pas connaître le même échec que la décentralisation culturelle (fonds régionaux d’art contemporain [FRAC], scènes nationales dont le public s’est finalement cantonné à quelques catégories socioprofessionnelles, etc.), il convient de renouveler, de réinventer nos modes d’intervention, d’animation et de médiation.
Pour cela, il est nécessaire d’appréhender au plus près les évolutions des pratiques, notamment chez les jeunes, afin de situer un point de départ pertinent pour cet échange de connaissances, de savoirs, de savoir-faire et de savoir penser. Le principe du « lieu de médiation » pose question : le « lieu » constitue une base arrière, socle au développement structurel d’un projet afin, notamment, de développer des formes d’intervention salariées ou bénévoles et d’assurer ainsi une certaine stabilité au projet. Néanmoins, il constitue également un facteur de conservatisme : une certaine inertie face à de potentielles évolutions dans les modes d’intervention, au cœur des [« formes de vie »](https://www.babelio.com/livres/Agamben-Moyens-sans-Fins/105221).

Il pourrait s’agir d’articuler des modes d’intervention « hors les murs » et « dans les appareils » des gens à partir de cette base, d’un lieu physique, et donc de penser cette action de médiation pour développer le sens critique, le libre arbitre, l’autonomie face aux technologies, au plus près des usagers. Il pourrait s’agir de **« s’intercaler » dans la vie numérique** des gens afin de se donner plus de sens et de distance : on peut ainsi imaginer des moyens d’intervention mobiles qui se déplacent sur un territoire au gré des besoins. Il pourrait également s’agir de développer des applications qui contribuent à ces souhaits et qui s’intercalent dans le processus informationnel quotidien afin de mieux le gérer, voire le contrôler.

En considérant le stade d’avancée de [« l’économie de l’attention »](http://www.seuil.com/ouvrage/pour-une-ecologie-de-l-attention-yves-citton/9782021181425), il faut réussir à détourner, à capter une partie de cette attention pour créer des zones d’échange et de médiation. Pour ce faire, des démarches ludiques peuvent être déployées tout en tentant d’esquiver les travers de la « gamification » de nos existences, c’est-à-dire l’usage excessif de dispositifs propres aux jeux vidéo pour jouer avec nos quotidiens. Le hack, le canular, l’humour peuvent également être des leviers pour grignoter des bribes d’attention et opérer parfois des changements d’échelle.
La palette des outils au service de la médiation critique vis-à-vis du numérique peut et doit donc s’étendre et se diversifier pour atteindre ses objectifs dans un monde qui glisse pour l’instant de façon inexorable vers une emprise hégémonique de ces entités numériques sur nos quotidiens.

### Comment objectiver nos limites ?

Je préfère pour l’instant lister sous forme d’items les points à mettre en relation.

*  **dehors/dedans :** poser comme point de vigilance l’écart existant entre le discours produit par nos soins et la façon dont on est perçu de l’extérieur ;
*  **prévisible/désiré :** nous produisons des formats croisant innovation sociale et participation citoyenne qui se situent au sein de la ville et produisent certainement des artefacts ou des conséquences qu’on ne défend sans doute pas par ailleurs. Dès lors, comment penser ou pensons-nous les dispositifs que nous mettons en place ? À quelle échelle pouvons-nous intervenir, quelles formes d’émergences se dissimulent dans nos activités ? Pouvons-nous garder un regard objectif sur ce que produisent les langages définissant nos actions, étant portés par ou portant d’autres types de langage, eux-mêmes pris dans d’autres logiques ? ;
*     **transparence/alternative :** bien comprendre que l’open source n’est plus forcément alternatif, mais que réside dans la fabrication de la valeur et la transparence un terrain plus fertile ;
*     **économie/emploi :** nous parlons ou partons d’une dimension culturelle pour construire nos actions. Ne sont-elles pas aussi attendues du point de vue de l’économie ? Mais notre champ d’action n’est pas celui des technosciences, notre terrain en négociation est celui de l’économie territoriale, c’est-à-dire celle des voisins, de la proximité ;
*     **éducation/populaire :** dans un monde non enchanté, notre terrain est sans doute davantage une tentative d’application de l’appropriation sociale des technologies, la poursuite des techniques populaires commune plutôt qu’une éducation populaire qui connaît des limites ! Quelles sont, aussi, nos limites ? ;
*     **transmission/savoirs :** nous proposons une participation citoyenne à la société technicienne et scientifique : il y a sans doute là un nouveau socle sensible. L’usage volontaire de sujets comme les communautés en ligne, l’artisanat et le néo-artisanat (ou artisanat avec des outils numériques) propose de mettre en exergue de nouvelles compétences : celles favorisant une coopération en réseaux ou celles de savoirs locaux, non universels et donc situés.
*     **institution/autonomie :** ces éléments interrogatifs constituent le projet spécifique d’un groupe spécifique, une association par exemple. Nos projets sont définis de manière **autonome** par rapport aux procédures institutionnelles (du type label, fédération, etc.). Il s’agit de « s’auto-instituer » (_Castoriadis C., 2003, « L’imaginaire radical », Revue du MAUSS, n° 21, p. 383-404_). Il y a donc nécessairement non seulement deux discours, mais deux régimes de pratique et des passerelles diplomatiques à construire. Les strates de notre tactique seraient doubles : d’une stratégie publique en surface à une autre démarche critique dans une **logique scindée**, traversée par des pratiques de design social pour passer du manifeste à l’implémentation. Un empilement salutaire et tactique ?
*     **subjectif/objectif :** la stratégie d’une « logique scindée » ne peut dériver d’une analyse objective du théâtre des opérations, comme si, une fois que les choses avaient été correctement analysées, on avait la liste des tâches, des points d’investissement, un descriptif des priorités. Non ! Le plus important est de savoir quelle logique pilote. C’est certainement le côté **subjectif**, les « valeurs partagées », l’éthique, l’esthétique, la politique, les goûts, les désirs de nos actions ;
*     **limites/pluralité :** si tout a lieu, il y a un point où nos démarches s’arrêtent. Nos limites sont, ici, plurielles : limite de territoire, limite d’échelle, limite d’action. Quelle alliance privilégier pour dépasser une fonction d’éclaireur ? Profiter d’une forme d’organisation prônant une pluralité radicale.

Les formes d’organisation et d’intervention sont ouvertes en ce moment : surtout ne pas les fermer.

## LEXIQUE

Revenir sur les termes qui nous définissent, nous encadrent et nous délimitent est peut-être un bon moyen de tisser des liens et des communs avec d’autres secteurs d’activités et acteurs.
La sémantique décrivant les activités liées au numérique est en mouvement. Elle est souvent déterminée par les financeurs (pouvoir public, marché), mais aussi par ceux qui les activent (citoyens,acteurs) ou ceux qui les commentent (médias,réseaux sociaux) :

### Culture ~~numérique~~ commune

Dans un texte [coécrit avec Alain Giffard en 2014](/fiches/manifeste_pour_la_culture_numerique), nous nous interrogions sur « les lieux » où nous apprenons à comprendre ces technologies numériques, à les anticiper, à les détourner, à nous les approprier. Cette question des conditions nécessaires à cette appropriation n’est rien d’autre que la question de ce que nous avions nommé, pour définir un espace commun : **la culture numérique**.

En partant du postulat que la culture numérique est en mouvement – dans le sens où elle est en formation –, elle ne préexiste pas à sa transmission, en insistant sur la dimension « pratique, atelier ». C’est-à-dire que les usages du public ne sont pas strictement déterminés par l’institution ou le marché. C’est cet écart entre une position de cible et une position active de sujet qui révèle le projet d’appropriation culturelle.

Le point central de cette approche fut l’abandon de l’idée que la technologie pouvait, en se banalisant, diffuser par son mouvement propre les savoirs et savoir-faire nécessaires. Nous avons ainsi proposé des pistes de réflexion sous forme de **manifeste** :

*     Nous prenons parti pour une culture numérique critique. Sans approche critique, pas de véritable formation à la culture numérique qui se réduit alors à un discours d’accompagnement du marketing et à la préparation des consommateurs.
*     Nous pensons que le développement de la culture numérique doit s’inscrire dans la perspective du renforcement des capacités des personnes et des collectifs, c’est-à-dire dans la perspective de la culture de soi.
*     La culture numérique doit être réellement et largement démocratisée. Si nous récusons l’approche par le « rattrapage » et le seul « accès » aux technologies, nous restons fidèles à notre engagement initial de combattre les inégalités dans le domaine numérique et autres.
*     En démocratie, la souveraineté du peuple devient une simple fiction si, face à un environnement qu’il ne comprend pas, qui le « dépasse », il ne peut acquérir l’autonomie suffisante pour comprendre les enjeux, identifier les problématiques et, en fin de compte, s’étant approprié cet environnement, désirer exercer réellement son pouvoir. L’assujettissement du peuple à la technologie est une menace sur la démocratie.
*     Nous préconisons d’associer culture numérique et [culture libre](http://ressource.pingbase.net/clibre/), de construire la culture numérique comme un bien commun.
*     La construction et la transmission de la culture numérique nécessitent la mise en place d’une formation dans les cursus généraux de l’enseignement comme dans l’éducation populaire. Cet enseignement relève de la culture générale et ne peut être cantonné aux cursus scientifiques au sens étroit.

Il faut également aménager des temps de débat sur la culture numérique afin d’activer l’appropriation sociale des technologies. Autrement dit, il faut faciliter l’appropriation de la culture numérique comme « contenu » et comme « problème ».

### Algorithme

« Le modèle économique des GAFAM (Google, Amazon, Facebook, Apple, Microsoft) va obliger à repenser l’articulation du monde entre une forme clivante et extrême de capitalisme et une forme renouvelée de marxisme à l’heure du digital labor, des intelligences artificielles, de la singularité, du transhumanisme, de l’automatisation et des biotechs. »
Ertzscheid O., 2017, [L’appétit des géants. Pouvoir des algorithmes, ambitions des plateformes](https://cfeditions.com/geants/), C & F éditions, Paris

### Technologie

De quoi parle la technologie : « On a recours à technologie, parce-qu’elle a plus de dignité que technique (…)  technologie, est le nom de la technique dépossédée. Elle se fait hors de nous, sans nous » Séris J.-P., 2013, [La technique](https://fr.wikipedia.org/wiki/Jean-Pierre_S%C3%A9ris), PUF, coll. « Quadrige », Paris
La société des hommes est « médiatisée » ? par la technique. La technique n’est pas un simple régime de moyens, il peut être intéressant de se poser la question des techniques, des technologies, des sciences, … Comment une appropriation est-elle encore possible ? Qui programme, qui pilote ?

« Le terme de technique, dans son acception la plus générale, désigne tout procédé permettant de mettre en œuvre des moyens en vue d’une fin. L’ouverture d’une bouteille à l’aide d’un tire-bouchon est une opération technique, de même que la vidange des cuves d’un pétrolier géant, le passage des vitesses d’une automobile ou la résolution d’une équation du troisième degré. […] la technologie désignait au départ la discipline ayant pour objet l’étude de la technique. Mais elle en est venue à désigner ce que l’on nomme également la technoscience, c’est-à-dire un stade du développement de la technique où celle-ci finit par se confondre avec la science. […] ce qui existe, ce sont des programmes aux orientations divergentes et parfois conflictuelles. Nous pouvons résumer cela par une formule : en matière de technologie, tout ce qui est programmé n’aboutit pas, mais tout ce qui aboutit a été programmé. »
[Mandosio J.-M., « La technique, la technologie et la machine », Le partage. Critique socioécologique radicale](http://partage-le.com/2017/02/la-technique-la-technologie-et-la-machine-par-jean-marc-mandosio/)

Portés par la culture du logiciel libre, nous développons, au sein de l’association PiNG, une approche croisant pratique et une approche critique en développant des espaces où l’on tente d’ouvrir les technologies proposées pour construire de nouveaux savoirs et savoir-faire.

### Fablabs

Né aux États-Unis, ce concept réunit sous un label très simple un ensemble de points à respecter pour définir son atelier de pratique numérique comme étant un lieu où l’on peut, dans la mesure du possible, fabriquer, réparer et concevoir tout type de projet : un listing de machines, de logiciels et d’outils techniques est mis à disposition. Depuis une dizaine d’années, les fablabs cristallisent espérances et convoitises pour un renouveau d’un modèle industriel [à cours de batteries de lithium](http://atelier-solidaire-saint-ouen.org/reparation-dune-batterie-lithium-ion/). Ce concept réactive la notion d’ateliers de pratiques, de production en petite série et locale. Comme si les clubs des bricoleurs des années 1970, popularisés par le magazine Système D, [étaient équipés d’Internet](http://fablabo.net/wiki/AteliersystemD).

On peut noter que ces pratiques proposent un retour à la matière, au tangible, au manipulable, au moment où les technologies semblent de plus en plus invisibles. Ces ateliers sont des lieux permettant de transformer la matière (produire, créer) où, pour cela, il y a transmissions de savoirs et de pratiques, ils sont par essence pluridisciplinaires et croisent les domaines de l’agriculture, de la cuisine, des transports, de l’énergie, de l’habitat, des arts et de l’artisanat [formant une société d’ateliers en réseau](http://reso-nance.org/wiki/projets/sda/accueil).

Le phénomène des « makers », actuellement étudié par les sociologues, tend à faire passer les rapprochements prometteurs vers les ateliers d’antan au second plan au profit de modèles d’innovation économique et sociale libertaires. « Malheureusement, la principale compétence dans la culture maker ces temps-ci semble consister à tenir une feuille de calcul sur Google Drive avec un business plan et une stratégie cohérente de relations publiques pour les médias sociaux. » [Fonseca F.  2015, « Gambiarra: repair culture by Felipe Fonseca », Makery, médias pour les laboratoires](http://www.makery.info/en/2015/03/31/gambiarra-la-culture-de-la-reparation/)

### Souveraineté

« La souveraineté technologique nous renvoie à la contribution que chacun et chacune de nous apporte au développement de technologies en sauvant nos imaginaires radicaux, en récupérant notre histoire et nos mémoires collectives, en nous resituant pour pouvoir rêver et souhaiter, ensemble, la construction ici et maintenant de nos infrastructures propres liées à l’information, la communication et l’expression . » Ces enjeux dépassent largement celui des frontières et pourraient constituer le socle d’une appropriation collective. _La souveraineté technologique – Chronique radio La Tête dans le Flux – PiNG_

### Émancipation

Portée par cet espoir d’expression individuelle ou collective, où en sommes-nous de cette utopie en réseaux ? « […] ces dynamiques se sont cristallisées d’une manière tout particulièrement visible à partir de 1995 quand se créent, à la faveur de l’organisation des contre-sommets altermondialistes, les premiers « medialabs », qui connaîtront une période de grande activité durant une dizaine d’années, mais aussi le réseau libertaire Indymedia dont le mot d’ordre “Don’t hate the media, be the media” résonne fortement avec la conviction du « hackerisme » libertaire selon laquelle le Net est le lieu d’une potentielle émancipation des individus dans un esprit d’horizontalité et d’égalité. »
[Auray N., Ouardi S., 2015, « Numérique et émancipation de la politique du code au renouvellement des élites », Mouvement des idées et des luttes, n° 94](http://mouvements.info/numerique-et-emancipation-de-la-politique-du-code-au-renouvellement-des-elites/)

### Infrastructures

Le numérique est partout : nous travaillons avec le numérique, communiquons avec le numérique, apprenons avec le numérique ; avec le numérique, nous faisons la guerre, des rencontres ou des affaires… La liste n’est pas près d’être close ni la ferveur avec laquelle nous soumettons nos activités, nos identités et nos vies à l’emprise du numérique. Il serait temps de prendre conscience que nos organisations, territoires et collectifs se confrontent à cet empilement stratégique : « Du monde plat à l’empilement du Stack, le deuxième phénomène tient à la re-verticalisation solidaire de cette “re-centralisation” ». Aux échanges horizontaux qui portaient les promesses émancipatrices de l’Internet se superpose désormais tout un « empilement » de structures de plus en plus fortement intégrées et hiérarchisées, sous le pouvoir dominant des plateformes, où les GAFA (Google, Amazon, Facebook, Apple) et autres NATU (Netflix, Airbnb, Tesla, Uber) jouent un rôle proprement central. Un livre récent de Benjamin Bratton, The Stack, cartographie cet empilement (stack) en y distinguant six couches superposées. Sur la fondation géologique et les ressources physiques que lui fournit « la terre », « le Cloud » apparaît comme un archipel de « serveurs, de bases de données, de sources d’énergie, de fibres optiques, d’appareils de transmission sans fil et d’applications distribuées ».
[Citon Y., 2016, « Notre inconscient numérique. Comment le numérique court-circuite nos consciences », Revue du crieur, n° 4, p. 144-158](http://www.yvescitton.net/wp-content/uploads/2013/10/Citton-NotreInconscientNumerique-RevueCrieur-juin2016.pdf)

## COMME UNE CONCLUSION

Ces lieux émergents abordent ou chatouillent malicieusement les enjeux actuels du « bien vivre » et de l’« agir ensemble », et c’est sans doute cela leur « fonction » inavouée finale : créer des espaces physiques d’appropriation sociale et de partage de savoirs, d’échange et de réflexion. Le statut associatif permet facilement de concevoir ces dispositifs et situations locales. On pourra se référer aux nombreux écrits sur « l’associationnisme » de Jean-Louis Laville qui décrit parfaitement cette irruption asynchrone, ces formes d’économie et de pratiques populaires.

Cette recherche inductive, qui dessine des réponses à partir d’expériences vécues comme un pendule entre émancipation et transformation sociale, produit des connaissances situées, durables et soutenables, imagine une citoyenneté sociale dans un environnement numérique. Puisque nous savons que nos traces numériques et nos objets technologiques ne disent pas qui nous sommes, mais ce dont nous sommes capables, faut-il imaginer des technologies non pour tous, mais utilisées par tous ?

Par Julien Bellanger, chargé de projet à l’association PiNG