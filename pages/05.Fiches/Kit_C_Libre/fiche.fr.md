---
title: 'Kit C Libre'
type_ressource: fichier
feature_image: kit-c-libre.png
license: cc-by-nc-sa
published: true
date: '30-11-2015 11:13'
publish_date: '30-11-2015 11:13'
taxonomy:
    category:
        - pratiques
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: article
    description: 'Le kit C LiBRE est un kit de médiation autour des questions de propriété intellectuelle et de culture libre.'
    image: kit-c-libre.png
show_breadcrumbs: true
metadata:
    description: 'Le kit C LiBRE est un kit de m&eacute;diation autour des questions de propri&eacute;t&eacute; intellectuelle et de culture libre.'
    'og:url': 'https://ressources.pingbase.net/fiches/kit_c_libre'
    'og:type': article
    'og:title': 'Kit C Libre | PiNG Ressources Num&eacute;riques'
    'og:description': 'Le kit C LiBRE est un kit de m&eacute;diation autour des questions de propri&eacute;t&eacute; intellectuelle et de culture libre.'
    'og:image': 'https://ressources.pingbase.net/fiches/kit_c_libre/kit-c-libre.png'
    'og:image:type': image/png
    'og:image:width': '1089'
    'og:image:height': '733'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Kit C Libre | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Le kit C LiBRE est un kit de m&eacute;diation autour des questions de propri&eacute;t&eacute; intellectuelle et de culture libre.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/kit_c_libre/kit-c-libre.png'
    'article:published_time': '2015-11-30T11:13:00+01:00'
    'article:modified_time': '2021-01-13T11:25:26+01:00'
    'article:author': Ping
---

Le kit C LiBRE est un kit de médiation autour des questions de propriété intellectuelle et de culture libre. Il permet d’expliquer les tenants et les aboutissants de la propriété intellectuelle aux usagers des fablabs ou des espaces numériques.

<iframe width="100%" height="600" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/d8de8b66-9c92-44ea-bbb2-427d2b7c9fce" frameborder="0" allowfullscreen></iframe>

[TÉLÉCHARGER](https://station.pingbase.net/index.php/s/854iZ5tmW9g58nb?classes=btn-down)
