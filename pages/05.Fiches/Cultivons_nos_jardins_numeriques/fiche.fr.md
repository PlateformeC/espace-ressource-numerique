---
title: 'Cultivons nos jardins numériques'
media_order: 5bis.png
type_ressource: text
feature_image: 5bis.png
license: cc-by-sa
date: '28-04-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - fablab
        - communs
        - 'espaces publics numériques'
        - 'culture numérique'
    author:
        - 'Benjamin Cadon'
aura:
    pagetype: article
    description: 'Pour produire des biens communs et réduire la fracture culturelle. Un article de Benjamin Cadon, directeur de Labomedia.'
    image: 1.jpg
metadata:
    description: 'Pour produire des biens communs et r&eacute;duire la fracture culturelle. Un article de Benjamin Cadon, directeur de Labomedia.'
    'og:url': 'https://ressources.pingbase.net/fiches/cultivons_nos_jardins_numeriques'
    'og:type': article
    'og:title': 'Cultivons nos jardins num&eacute;riques | PING Ressources Num&eacute;riques'
    'og:description': 'Pour produire des biens communs et r&eacute;duire la fracture culturelle. Un article de Benjamin Cadon, directeur de Labomedia.'
    'og:image': 'https://ressources.pingbase.net/fiches/cultivons_nos_jardins_numeriques/1.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '882'
    'og:image:height': '1200'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Cultivons nos jardins num&eacute;riques | PING Ressources Num&eacute;riques'
    'twitter:description': 'Pour produire des biens communs et r&eacute;duire la fracture culturelle. Un article de Benjamin Cadon, directeur de Labomedia.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/cultivons_nos_jardins_numeriques/1.jpg'
    'article:published_time': '2014-04-28T00:00:00+02:00'
    'article:modified_time': '2020-12-02T09:21:47+01:00'
    'article:author': Ping
---

*Pour produire des biens communs et réduire la fracture culturelle. Un article de Benjamin Cadon, directeur de Labomedia.*

Culture numérique <=> jardin numérique, vous trouverez peut être l'analogie facile, mais elle est fertile ! On retrouve en effet de plus en plus le vocabulaire propre à l'écologie scientifique dans les discours de « l'innovation numérique », révélant peut-être au passage le caractère « organique » de ces [évolutions](https://fr.wikipedia.org/wiki/Joseph_Schumpeter#L.27entrepreneur_innovateur).

C'est peut-être ce qui rend si difficile le fait de définir ce que l'on entend par « culture numérique ». On peut au moins estimer que ces cultures (car il faut plutôt l'accorder au pluriel), englobent à la fois les productions culturelles traditionnelles (musique, films, livres), dont la « consommation » a grandement évolué avec le développement d'Internet et du haut débit, mais aussi comprennent des créations artistiques qui existent par l'utilisation d'outils numériques dans le processus de création ou de diffusion (le presque antique « NetArt », « L'art numérique », les performances en réseau, …). Partant de cette entrée, il faut élargir le périmètre de notre jardin de cultures numériques à tout ce qui nourrit notre cerveau en passant notamment par le tuyau Internet et le jeux vidéo, transformant ainsi notre paysage imaginaire, nos interactions sociales, notre appréhension de l'espace et du temps, les blagues que l'on peut se raconter le lendemain matin ([voir par exemple la guerre des mèmes entre 4chan et Tumblr](http://owni.fr/2010/11/15/4chan-vs-tumblr-la-guerre-a-bien-eu-lieu/)).

![1](1.jpg)
Installation « noradio » - crédit photo Xul

On peut donc mettre plein de choses dans notre jardin numérique, on s'aperçoit pourtant que chez un certain nombre de nos concitoyens, ce jardin numérique est particulièrement pauvre et peu diverse, remarque que l'on peut également formuler à l'encontre de cette [génération née de l'Internet](https://nicolaslegland.wordpress.com/2013/08/15/les-enfants-ne-savent-pas-se-servir-dun-ordinateur/) (les « digital natives ») qui pour bon nombre, se cantonnent à la fréquentation des grandes surfaces américaines (facebook et youtube en tête).

La culture numérique, c'est aussi s'intéresser aux dynamiques culturelles et mécanismes techniques pour mieux appréhender les enjeux et problématiques d'une numérisation massive de notre quotidien. Des révélations d'Edward Snowden au mythique « ça imprime pas ! », on s'aperçoit que ce quotidien est peuplé d'interactions avec des ordinateurs qui reposent sur une complexité technologique qui ne peut plus tenir dans le cerveau d'un seul homme, il semble alors pertinent que le plus grand nombre s’intéresse au sens de ces évolutions technologiques et en maîtrise mieux les modalités techniques pour ne pas devenir « esclave » de la machine mais garder au contraire une forme d'autonomie, de distance critique éclairée.

![2_1.png](2_1.png)
Installation vidéo« Terminator mon amour » Yves Duranthon et Sébastien Hoeltzener 2013

Je vous propose donc de partir cultiver notre jardin numérique et de considérer au passage la place des espaces publiques numériques dans cet « écosystème », en commençant par un petit détour par celui de Labomedia, association culturelle basée à Orléans.

## Labomedia, clignote depuis 1999

Labomedia évolue depuis 1999 dans ce champ flou des arts et cultures numériques, avec pour ambition de départ de croiser pédagogie et artistique, de constituer un lieu ressource. Partant des premières problématiques d'appréhension du web et des outils multimedia, le projet s’enrichit de résidences d'artistes, de participation à des projets européens autour de l'éducation au multimedia, de travaux de recherche et développement et plus récemment d'un fablab, [« l'atelier du c01n »](http://atelierduc01n.labomedia.org/) orienté vers la création artistique et artisanale, l'échange de savoirs et la (ré)appropriation technologique …

![3_1.png](3_1.png)
Atelier du c01n - crédit photo Xul

Au gré des projets, à l'image de l'usage désormais transversal aux disciplines artistiques du « numérique », des collaborations se nouent, tantôt sur une scène pour illustrer visuellement un concert rendu accessible à des personnes malentendantes, dans l'espace public autour d'un dispositif de projection impliquant les passants, ou sur le web pour outiller un réseau de cartographes culturels au sein de médiathèques. En parallèle simultané, il s'agissait d'expérimenter différents contextes pour partager expériences et savoirs-faires, connaissances et émotions, via la participation à des événementiels « grand public » ou a contrario l'instigation de moments collectifs impromptus, ou encore en proposant chaque semaine un [open atelier](http://openatelier.labomedia.org/) de découverte et de pratique (ponctué par le « bit de dieu » à 19h09 où chacun est invité à présenter un projet, une réalisation, une envie), des sessions à l'atelier du c01n de créations en volume, de circuits électroniques artisanaux, de matériels réformés  « upcyclés », ou encore d'objets ludiques et pédagogiques.

![4.jpg](4.jpg)
Atelier du c01n - crédit photo Xul

Tout cela est « fertilisé » par des rencontres, des croisements de réseaux, un principe d'échange selon la recette des logiciels libres, l'implication d'humains et de machines si possible selon des modalités sympathiques bien que portées par une économie périlleuse. Du fait de l'implantation sur un territoire, chaque projet est par nature singulier et donc non transposable, on peut tout de même essayer de trouver des points communs et futurs potentiels à ces cultivateurs de l'espace publique numérique.


**Les défricheurs**

Parmi ces cultivateurs, on peut tout d'abord considérer les vaillants défricheurs de terrains publics numériques, aux avant-postes des enjeux et problématiques, ils constituent une passerelle entre l'obscurantisme technologique et le monde des vivants, anticipant ce que l'on devrait déjà considérer comme un bien commun. Ils se sentent parfois seuls mais on la capacité à se fédérer efficacement via Internet et interviennent sur des parcelles législatives, artistiques, sociales pour tenter de pacifier un terrain parfois miné.

![5bis.png](5bis.png)
 Installation « Pêter les plombs » - crédit photo Xul


**Les petits producteurs**

Il existe toute une kyrielle d’initiatives valeureuses autour de la « culture numérique », qu'elles soient portées par des espaces publics numériques, medialabs, médiathèques, hackerspaces, cybersquat, fablabs, centres d'art et centres sociaux, collectifs de citoyens et d'artistes, … .

![5.png](5.png)
Ibniz / Viznut

dFFrxFr.F8FF&/qs
dr*/8rsM1{7rsss.3^}2{s}3{d1e^ddd}5Fr/4X11r1Vs3Vs1Vs3VL
dFFrxFr00FF&/qsM1{d5rx2r3&/q}2{dxDrF&/q}3{dFrF&/s}1V.A/2V.3*3V


L'idée selon laquelle le haut débit était désormais dans tous les foyers restant à relativiser, tout comme l'impression qu'avec la simplification technique liée au web 2,0 chacun était à même d'appréhender la complexité du monde auquel cela donnait accès, ces petits et moyens producteurs occupent une place essentielle pour tenter de palier les inégalités culturelles et d'accès aux contenus numériques. L'usage abondant des « téléphones intelligents », l'omniprésence annoncée des objets connectés à Internet, la possibilité de (presque) tout  fabriquer et programmer, toutes ces évolutions sont porteuses de nuages tout comme de potentiels rayons de soleils, d'autant plus s'il existe une multiplicité de possibilités pour accompagner de façon désintéressée les usages et usagers dans la direction la plus clairvoyante.

La multiplication des *labs (fablab, medialab, hacklab, livinglab, …) révèlent l'envie et le besoin de se réunir dans un lieu physique pour apprendre et créer ensemble autour de ces *numériques. Favoriser l'émergence de ce type de lieux et contextes ainsi que leur mise en réseau pour aider partages et échanges est certainement de nature à susciter de la diversité, du partage de ressources qui peuvent par la suite, replantées, donner naissance à des fleurs permettant de remonter le bien être de l'humanité de 10 points (par fleur), tout comme à des légumes sapides générateurs de nouvelles formes économiques.


**La coopérative mondiale**

Il existe dans ce monde sympathique des cultivateurs du bien commun numérique des organisations non gouvernementales qui développent des dynamiques de coopération et d'intelligence collective parfois à l'échelle internationale, un des premiers exemples qui vient en tête est celui de Wikipedia bien sur, mais la production et le partage de ressource existe aussi à plus petite échelle, parfois dans une logique de personne à personne (P2P !), les connaissances se diffusant alors par cercle plus ou moins concentriques.

![6.png](6.png)
Le « réseau social » de l'association Goto10 basée à Poitiers

**Jardiland vs Caterpillar**

Aujourd'hui, si l'on considère internet comme un lieu public, on constate qu'il est fortement occupé par des marchands préoccupés par nos données personnelles et notre attention. Au regard de l'audience recueillie par les jeux vidéos ou le moteur de recherche de Google, à quoi doit ressembler la place publique dans cette culture numérique d'aujourd'hui ? Elle doit indubitablement constituer un réservoir de saveurs et de diversités, une source d'esprit critique, de capacités d'analyse et de détournements d'un futur à déterminer si possible collectivement.

**Mangeons du bio !**

J'ai peu jusqu'à maintenant évoqué l'intervention directe d'institutions publiques dans cette culture numérique, de l'école en passant par les ministères car il y a aussi dans ces lieux une multiplicité d'initiatives pour certaines florissantes, pour d'autres plus arides. Là aussi une coopération entre une diversité d'acteurs semble fertile, les « petits » pouvant apporter « aux gros » dans une relation symbiotique à équilibrer. Pour compliquer le tableau, il y a parfois des défricheurs au sein de grandes institutions publiques et des petits producteurs qui aspirent à devenir des multinationales, le tout restant peut être de nourrir notre cerveau avec des bons contenus savoureux et dont on connaisse la provenance, le producteur et de partager cette dégustation avec les plus jeunes et plus vieux d'entre nous pour conserver un rapport amical avec le genre informatique.

Benjamin CADON / Labomedia


---
Annexe Jeu – Concours :

Imaginons un bestiaire d'Espaces Publics Numériques nourris à base de *lab* :

EPN Fab*lab* : bricolage assisté par ordinateur
EPN Media*lab* : production audiovisuelle web mobile accompagnée
EPN Social*lab* : réseaux sociaux « réels » et « numériques »
EPN Hightech*lab* : expérimentations sociales critiques des dernières « nouveautés » high-tech
EPN Upcycle*lab* : réparation et recyclage, améliorations et embellissement d'objets
EPN Biblio*lab* : nouveaux usages culturels multimedia interactifs
EPN Anon*lab* : comment rendre sa vie numérique plus sereine
EPN Job*lab* : «se « vendre » sur Internet
EPN Edu*lab* : outils pédaogogiques et ressources
++ ?
À vous de jouer et d'imaginer votre EPN à base de *lab !
