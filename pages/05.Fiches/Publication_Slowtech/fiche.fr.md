---
title: '(S)LOWTECH, DÉPROGRAMMER L’OBSOLESCENCE'
media_order: Slowtech.jpeg
type_ressource: fichier
feature_image: Slowtech.jpeg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/publication_pfc
    -
        page: /fiches/publication_atelier_partagé
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
        - pratiques
        - recits
    tag:
        - réparation
        - tiers-lieux
        - slowtech
    author:
        - 'Thomas Bernardi'
        - 'Charlotte Rautureau'
        - 'Yanaita Araguas'
aura:
    pagetype: article
    description: 'À travers cette publication, PiNG partage l’expérience accumulée pendant cinq années d’exploration collective : un mode d’emploi pour celles et ceux qui souhaitent développer des ateliers de réparation citoyen'
    image: Slowtech.jpeg
show_breadcrumbs: true
metadata:
    description: '&Agrave; travers cette publication, PiNG partage l&rsquo;exp&eacute;rience accumul&eacute;e pendant cinq ann&eacute;es d&rsquo;exploration collective : un mode d&rsquo;emploi pour celles et ceux qui souhaitent d&eacute;velopper des ateliers de r&eacute;paration citoyen'
    'og:url': 'https://ressources.pingbase.net/fiches/publication_slowtech'
    'og:type': article
    'og:title': '(S)LOWTECH, D&Eacute;PROGRAMMER L&rsquo;OBSOLESCENCE | PiNG Ressources Num&eacute;riques'
    'og:description': '&Agrave; travers cette publication, PiNG partage l&rsquo;exp&eacute;rience accumul&eacute;e pendant cinq ann&eacute;es d&rsquo;exploration collective : un mode d&rsquo;emploi pour celles et ceux qui souhaitent d&eacute;velopper des ateliers de r&eacute;paration citoyen'
    'og:image': 'https://ressources.pingbase.net/fiches/publication_slowtech/Slowtech.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1000'
    'og:image:height': '649'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '(S)LOWTECH, D&Eacute;PROGRAMMER L&rsquo;OBSOLESCENCE | PiNG Ressources Num&eacute;riques'
    'twitter:description': '&Agrave; travers cette publication, PiNG partage l&rsquo;exp&eacute;rience accumul&eacute;e pendant cinq ann&eacute;es d&rsquo;exploration collective : un mode d&rsquo;emploi pour celles et ceux qui souhaitent d&eacute;velopper des ateliers de r&eacute;paration citoyen'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/publication_slowtech/Slowtech.jpeg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2021-01-12T12:12:36+01:00'
    'article:author': Ping
---

## Atelier de réparation citoyen, mode d’emploi

À travers cette publication, PiNG partage l’expérience accumulée pendant cinq années d’exploration collective : un mode d’emploi pour celles et ceux qui souhaitent développer des ateliers de réparation citoyen, une ressource pour qui s’intéresse à la question de l’obsolescence des objets électroniques et informatiques.

[TÉLÉCHARGER](https://station.pingbase.net/index.php/s/82WePdAT43GfPQf?classes=btn-down)

[ACHETER](https://ping.odoo.com/shop/product/livre-slow-tech-deprogrammer-l-obsolescence-12?classes=btn)

---
- Textes : Charlotte Rautureau & Thomas Bernardi
- Mise en page et illustration : Yanaita Araguas
