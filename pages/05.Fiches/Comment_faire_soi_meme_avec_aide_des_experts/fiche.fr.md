---
title: 'Comment faire soi-même… avec l''aide des experts'
media_order: article1_2018_2-870x550.jpg
type_ressource: text
feature_image: article1_2018_2-870x550.jpg
license: cc-by-nc-sa
date: '04-05-2018 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - fablab
        - 'agriculture urbaine'
        - biolab
        - 'recherche ouverte'
        - jardin
    author:
        - 'Michka Mélo'
aura:
    pagetype: article
    description: 'Pourquoi ne pas mutualiser l’équipement nécessaire aux analyses et apprendre à identifier et quantifier nous-mêmes les polluants de notre sol ?'
    image: article1_2018_2-870x550.jpg
metadata:
    description: 'Pourquoi ne pas mutualiser l&rsquo;&eacute;quipement n&eacute;cessaire aux analyses et apprendre &agrave; identifier et quantifier nous-m&ecirc;mes les polluants de notre sol ?'
    'og:url': 'https://ressources.pingbase.net/fiches/comment_faire_soi_meme_avec_aide_des_experts'
    'og:type': article
    'og:title': 'Comment faire soi-m&ecirc;me&hellip; avec l''aide des experts | PING Ressources Num&eacute;riques'
    'og:description': 'Pourquoi ne pas mutualiser l&rsquo;&eacute;quipement n&eacute;cessaire aux analyses et apprendre &agrave; identifier et quantifier nous-m&ecirc;mes les polluants de notre sol ?'
    'og:image': 'https://ressources.pingbase.net/fiches/comment_faire_soi_meme_avec_aide_des_experts/article1_2018_2-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Comment faire soi-m&ecirc;me&hellip; avec l''aide des experts | PING Ressources Num&eacute;riques'
    'twitter:description': 'Pourquoi ne pas mutualiser l&rsquo;&eacute;quipement n&eacute;cessaire aux analyses et apprendre &agrave; identifier et quantifier nous-m&ecirc;mes les polluants de notre sol ?'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/comment_faire_soi_meme_avec_aide_des_experts/article1_2018_2-870x550.jpg'
    'article:published_time': '2018-05-04T00:00:00+02:00'
    'article:modified_time': '2020-12-02T09:21:46+01:00'
    'article:author': Ping
---

## Faire soi-même…

Il y a 18 mois, nous publiions sur le site de PiNG [un article](http://www.pingbase.net/res-sources/agriculture-urbaine-et-fablab) esquissant le futur que nous souhaitons développer par notre [programme de recherche-action “agriculture urbaine et fablab”](http://www.pingbase.net/agricultureurbaineetfablab).
Dans ce futur en construction, de nombreux habitants de la ville s’emparent des outils du fablab et du jardinage pour répondre à leurs besoins. Alimentation, matériaux de construction, composants électroniques, etc. : le fablab est transformé en véritable atelier de production, avec pour matière première principale le foisonnement végétal urbain, dont le développement est soutenu par la mise en place d’infrastructures (produites au fablab) permettant de cultiver chaque recoin de la ville.

Pour amorcer la construction de ce futur, nous avons fait le choix de démarrer par la conception d’un [kit d’analyse du sol](http://fablabo.net/wiki/Analyse_du_sol_%C3%A0_faire_soi_m%C3%AAme) à destination des jardiniers. En effet, les discussions avec ces derniers ont fait rapidement remonter la problématique de la pollution du sol, et la difficulté à savoir si celui-ci était propre à la culture, à moins de ne procéder à de coûteuses analyses auprès de laboratoires privés.
Nous avons eu le sentiment que le fablab pouvait agir sur ce point. Pourquoi ne pas mutualiser l’équipement nécessaire aux analyses et apprendre à identifier et quantifier nous-mêmes les polluants de notre sol ?

Dans cette optique, nous avons organisé un atelier de recherche ouverte il y a un an, durant lequel nous avons transmis aux participants, pour l’essentiel non-scientifiques, [une méthode pour démarrer de zéro sur un sujet technique](http://www.pingbase.net/evenement/demarrer-de-zero-sur-un-sujet-technique) (tel que la pollution des sols). Nous avons ensuite passé la journée à explorer ensemble la littérature scientifique et technique disponible sur les méthodes d’analyse et de dépollution des sols. Ce qui nous a permis d’évaluer l’ampleur du travail à faire…
Les méthodes d’analyses et de dépollution sont nombreuses et complexes. Les interactions des polluants avec le sol et les plantes ainsi que leurs effets sur notre santé sont tout aussi difficiles à prédire avec précision. C’est un sujet très vaste, et nous ressentons très rapidement les limites de nos capacités de chercheurs autonomes. Nous pourrions potentiellement arriver à concevoir ce kit, mais cela prendrait énormément de temps et d’énergie.
Dans les semaines qui suivirent, nous avons été contactés par d’autres acteurs, particuliers ou même collectivités, tous très intéressés à disposer du kit, montrant [l’intérêt croissant sur cette question](http://www.lesonunique.com/content/lagriculture-urbaine). Mais, la route était encore longue avant de pouvoir leur proposer une solution clé en main…

Heureusement, une voie plus rapide vers notre objectif était en train de se dessiner. Quelques jours après cet atelier, nous avons été contactés par l’équipe de [Thierry Lebeau](http://www.univ-nantes.fr/site-de-l-universite-de-nantes/thierry-lebeau--445791.kjsp), chercheur au LPG de l’université de Nantes, et coordinateur du projet [Pollusols](http://www.osuna.univ-nantes.fr/pollusols-pollution-diffuse-de-la-terre-a-la-mer-1304013.kjsp). Les chercheurs professionnels de cet équipe montraient un intérêt pour notre démarche et avaient envie d’en savoir plus. Nous amorçons donc un dialogue avec eux pour voir comment nous pourrions travailler ensemble.

## …avec l’aide des experts

Notre objectif initial était de démontrer qu’un groupe de passionnés, avec du temps et de la méthode, pouvait faire de la recherche scientifique ou du développement technique de façon autonome et artisanale. Notre motivation à faire cette démonstration résidait dans l’envie de montrer que l’on peut faire soi-même, que l’on n’est pas forcément dépendant des acteurs en position de force ou d’autorité, et qu’il n’y a rien de divin dans la recherche scientifique et le développement technique : simplement des connaissances et des compétences à acquérir.
En réalité, notre autonomie était dès le départ toute relative : l’essentiel de notre travail consistait à parcourir la littérature scientifique et technique produite par d’autres, essentiellement des chercheurs professionnels. Ce que nous réalisons aujourd’hui, c’est que dialoguer avec les chercheurs professionnels directement (plutôt que d’éplucher patiemment leur littérature) représente un immense coup d’accélérateur vers notre objectif final.
En effet, les chercheurs professionnels, qui travaillent depuis de nombreuses années sur ces questions, disposent de la plupart des réponses aux questions que nous nous posons. Ils peuvent donc nous offrir en quelques minutes des réponses fiables, en nous économisant des heures de lecture. Nous pouvons en contrepartie leur offrir de nouveaux débouchés éventuels pour leur recherche, de nouvelles façons de les valoriser, en ligne directe avec les citoyens, au plus près de leurs besoins.

Nous avons donc demandé à [Béatrice Béchet](http://www.ifsttar.fr/menu-haut/annuaire/fiche-personnelle/personne/bechet-beatrice/) de l’[IFSTTAR](http://ifsttar.fr/) ainsi qu’à deux chercheurs du [LPG](https://lpg-umr6112.fr/index.php?lang=fr), tous spécialistes de la pollution du sol, si ils accepteraient de partager une journée avec des jardiniers pour répondre à toutes les questions qu’ils pourraient avoir sur la pollution du sol. Et à notre grande joie, ils ont accepté !
Nous avons donc le plaisir de proposer, le 25 mai prochain, un atelier, où nous invitons les jardiniers à venir poser toutes les questions qu’ils auraient sur la pollution du sol à ces spécialistes de la question.
Cet atelier sera suivi, en soirée, par une table-ronde durant laquelle les spécialistes présenteront leurs recherches en cours. Nous discuterons ensuite des possibilités d’appropriation de leur recherche par les habitants des villes, pour faire fructifier cette collaboration entre chercheurs et habitants.
Forts de ces informations précieuses transmises par les spécialistes, nous imaginerons ensemble, jardiniers, usagers du fablab et chercheurs professionnels, quel serait le meilleur dispositif à mettre en place au fablab pour aider les jardiniers à cultiver sainement sur les sols urbains.

Un article de Michka Mélo, bio-ingénieur et explorateur-associé de PiNG
