---
title: 'Manifeste pour la culture numérique par Alain Giffard'
media_order: img_7075.jpg
type_ressource: text
feature_image: img_7075.jpg
license: cc-by-sa
date: '15-05-2015 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'culture libre'
        - 'culture numérique'
        - médiation
    author:
        - 'Alain Giffard'
aura:
    pagetype: website
metadata:
    'og:url': 'https://ressources.pingbase.net/fiches/manifeste_pour_la_culture_numerique'
    'og:type': website
    'og:title': 'Manifeste pour la culture num&eacute;rique par Alain Giffard | Espace Ressources Num&eacute;riques'
    'og:image': 'https://ressources.pingbase.net/fiches/manifeste_pour_la_culture_numerique/img_7075.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Manifeste pour la culture num&eacute;rique par Alain Giffard | Espace Ressources Num&eacute;riques'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/manifeste_pour_la_culture_numerique/img_7075.jpg'
    'article:published_time': '2015-05-15T00:00:00+02:00'
    'article:modified_time': '2020-11-26T15:09:03+01:00'
    'article:author': Ping
---

*Un manifeste écrit par Alain Giffard, directeur du Groupement d’intérêt scientifique «Culture & Médias numériques» et membre d'Ars Industrialis. Alain Giffard a été le concepteur de la bibliothèque numérique de la Bibliothèque nationale de France et président de la Mission interministérielle pour l’accès public à l’Internet.*

Le numérique est partout : nous travaillons avec le numérique, communiquons avec le numérique, apprenons avec le numérique ; avec le numérique, nous faisons la guerre, des rencontres ou des affaires... la liste n’est pas prête d’être close, ni la ferveur avec laquelle nous soumettons nos activités, nos identités et nos vies à l’emprise du numérique.

Il est peut-être temps de poser la question : où apprenons-nous à comprendre ces technologies numériques ? À les anticiper ? À les détourner ? À nous les approprier ?

Cette question n’est rien d’autre que la question de ce que nous nommerons ici : *la culture numérique*.

Sans doute a-t-elle été relancée dans la période récente, en particulier à l’initiative de l’Administration Obama. En France, le Conseil National du Numérique a publié, en novembre 2013, un rapport dans le même sens.

Quinze ans après l’initiative d’Al Gore et ses répliques européennes, cette relance de la culture numérique officielle signe clairement l’échec de ce que l’on pourrait appeler la politique du rattrapage visant à combler le fossé numérique.

La culture numérique, souvent conçue, dans les politiques contre le fossé numérique, comme un simple moyen pour accompagner l’accès aux techniques et l’entrée sur le marché, fait aujourd’hui l’objet d’interrogations, notamment sur ses effets cognitifs et sa capacité à s’articuler avec la culture *«classique»*.

Avec le thème de la *«digital literacy»*, apparu pendant la décennie 2000, dans un contexte de politique publique, se dessine le nouveau paysage conceptuel de la culture numérique officielle.

Le point central de cette nouvelle approche est l’abandon de l’idée que la technologie, pouvait, en se banalisant, diffuser par son mouvement propre les savoirs et savoir-faire nécessaires.

On est cependant loin de disposer d’une conception véritablement unifiée de la culture numérique. En témoigne la variété des littératies parfois évoquées : media literacy, technology literacy, information, visual, communication, social literacies et... transliteracies.

La culture numérique, dans sa version officielle la plus récente, est donc une notion largement ouverte. Son contenu et sa composition, sa construction et sa consistance restent mal établies.

## Nos propositions

Le schéma que nous proposons ici est un cadre conceptuel général reposant sur plusieurs dimensions dont la combinaison permettrait de proposer une ou plusieurs listes qualifiées de ces savoirs et savoir-faire.

La technologie nous est apparue dangereusement absente - voire refoulée - de certains modèles de littératie numérique. Tout se passe comme si, après plusieurs années d’approche «orientée technologie», on était tenté de développer une littératie comme extérieure à la technique, et l’ignorant.

Or la conception de la technique est nécessairement le point de départ qui conditionne tout modèle et tout programme de culture numérique.

La société des hommes est médiatisée par la technique. Le système technique n’est pas un ensemble d’objets, mais une relation, non seulement entre objets techniques - des ordinateurs et des réseaux - mais aussi entre ces objets techniques et les hommes. Les technologies du numérique doivent être définies comme des technologies intellectuelles, cognitives et culturelles : des technologies de l’esprit.

La technique n’est pas un simple régime de moyens, auquel il serait possible d’affecter différentes finalités. Conditionnée par son économie et son industrie, elle relève au contraire d’un double régime de fins et de moyens. Autrement dit, qui souhaite utiliser le numérique à d’autres fins ne peut se dispenser - à un degré ou à un autre - de le modifier en tant que moyen, c’est-à-dire de se l’approprier.

Il est inconcevable d’imaginer une culture numérique qui serait seulement une entrée, une culture numérique élémentaire. La culture numérique se prolonge à l’horizontale par la pratique et, à la verticale, par le développement de la personne, dans l’optique de la promotion des capacités, ou «capabilités» humaines. Le plan ainsi formé par ces deux directions est celui de la vie de l’esprit dans une société qui s’individue autour de la technique et de la culture numérique. Ce sont là les fondements véritables d’une formation sérieuse à la culture numérique.

Il n’y a pas de confusion possible entre le savoir-faire initial qui conditionne la prise en mains par l’utilisateur de l’ordinateur connecté, et les premiers éléments du savoir spécifique de la culture numérique. Une démarche d’appropriation du numérique imposera, à un certain stade, la connaissance de notions élémentaires, ce qui suppose, au moins, que la formation initiale n’aura pas créé des obstacles à cette formation ultérieure.

Pourtant la littératie numérique reste une littératie. Autrement dit, la culture numérique n’est pas un univers de savoirs qu’une tendance centrifuge irrésistible éloignerait toujours plus du reste de la culture. La combinaison entre savoirs, savoir-faire et pratiques caractéristiques du numérique, et savoirs, savoir faire et pratiques culturelles classiques, est une orientation absolument centrale pour la littératie numérique.

En insistant sur la dimension «PRATIQUES» du schéma de la culture numérique, nous voulons d’abord rappeler que, comme toute culture technique, la culture numérique est empirique et expérimentale, et qu’elle progresse par l’exercice.
Nous voulons aussi souligner le rôle des pratiques culturelles numériques, dans le développement de la littératie numérique. Ce que veut signifier cette notion de pratiques culturelles, c’est l’autonomie de celui ou ceux - le public par exemple - que l’on considère comme destinataires soit des politiques publiques, soit de l’offre industrielle culturelle.
Pratique veut dire : le public n’est pas strictement déterminé par l’institution ou le marché. C’est cet écart entre une position de cible et une position active de sujet, qui révèle le projet d’appropriation culturelle.

## Un manifeste pour la culture numérique

**Nous appelons à reconnaître et favoriser la culture numérique en mouvement.**

Sans sous-estimer ce que peuvent apporter à la culture numérique, aussi bien les pouvoirs publics que les industries, nous devons souligner à quel point la culture numérique dépend d’abord des hommes et des femmes qui la créent, qui pourraient être appelés les lettrés du numérique.

Ce dont ont d’abord besoin les acteurs de la culture numérique, c’est tout simplement d’une ambiance suffisamment favorable, c’est-à-dire d’une certaine reconnaissance et d’une certaine continuité.

En proposant cette idée de culture numérique en mouvement, nous voulons regrouper plusieurs aspects de cet ensemble d’interrogations et d’avancées, de critiques et d’explorations qui nous semble caractériser la situation présente de la culture numérique.

La culture numérique est en mouvement dans le sens où elle est en formation. Elle ne préexiste pas à sa transmission. C’est l’existence de projets consciemment rapportés à cette orientation qui permet d’expliciter les savoirs et savoir-faire caractéristiques.

La culture numérique est aussi en mouvement parce qu’elle parcourt continuellement selon un processus itératif le passage du savoir des commencements au savoir approfondi, consciemment et délibérément organisé.

Enfin elle est en mouvement parce qu’elle court et traverse, à l’horizontale, l’activité, le savoir, l’éthique, l’imaginaire et les passions des personnes et des collectifs les plus divers.

**Nous prenons parti pour une culture numérique critique.**

Sans approche critique, pas de véritable formation à la littératie numérique qui se réduit alors à un discours d’accompagnement du marketing, à la préparation des consommateurs.

L’exigence d’une approche critique ne doit pas être formulée dans des termes d’une apparente fermeté mais d’une grande imprécision. Garantir l’esprit critique dans l’usage du numérique est un si bel objectif qu’il faut d’emblée s’efforcer de l’atteindre.

Aujourd’hui la direction dans laquelle la culture numérique doit assumer son potentiel critique est d’abord celui des risques que l’économie de l’attention fait peser sur le développement de la technologie, le formatage des usages et la culture elle-même.

Les acteurs de la culture numérique doivent être pleinement conscients de ces risques et mettre en œuvre des stratégies efficaces, par exemple en s’appuyant sur les acquis et le mouvement de l’économie de la contribution.


**Nous pensons que le développement de la culture numérique doit s’inscrire dans la perspective du renforcement des capacités des personnes et des collectifs, c’est-à-dire dans la perspective de la culture de soi.**

Dans une première période, l’informatique personnelle et le web se sont présentés et, jusqu’à un certain point, consistaient réellement en une technique d’*«empowerment»* avant l’heure.

La culture numérique comme culture de soi : délégation incontrôlée à la technique, narcissisme, addictions, surcharge cognitive, difficultés à contrôler son attention, oubli de l’exercice au profit de l’accès direct...

L’économie de l’attention se révèle un nouveau psycho-pouvoir suscitant une évidente toxicité du numérique. L’invention d’une culture de soi numérique est la meilleure réponse à ce psycho-pouvoir et le meilleur soin de cette toxicité.


**La culture numérique doit être réellement et largement démocratisée.**

L’orientation d’une culture numérique comme culture critique et culture de soi est la mieux à même de relancer sa démocratisation sur des bases plus satisfaisantes, c’est-à-dire plus larges et plus solides. Si nous récusons l’approche par le «rattrapage» et le seul «accès» aux technologies, nous restons fidèles à notre engagement initial de combattre les inégalités dans le domaine numérique.

Au travail comme dans la vie quotidienne, notre environnement est de plus en plus structuré par les technologies numériques. Certaines personnes ou certains groupes sont exclus de ce processus ; un nombre plus important est dans l’incapacité de com- prendre les nouveaux codes culturels et techniques de notre société et de ses artefacts technologiques ; et encore plus nombreux sont ceux qui doivent s’adapter à ces codes sans pouvoir se les approprier, c’est-à-dire sans participer à leur construction. En démocratie, la souveraineté du peuple devient une simple fiction si, face à un environnement qu’il ne comprend pas, qui le «dépasse», il ne peut acquérir l’autonomie suffisante pour comprendre les enjeux, identifier lesproblématiques et en fin de compte, s’étant approprié cet environnement, désirer exercer et exercer réellement son pouvoir. L’assujettissement du peuple à la technologie est une menace sur la démocratie.


**Nous préconisons d’associer culture numérique et culture du Libre, de construire la culture numérique comme un bien commun.**

La construction et la transmission de la culture numérique nécessite la mise en place d’une formation dans les cursus généraux de l’enseignement comme dans l’éducation populaire. Cet enseignement relève de la culture générale et ne peut être cantonné aux cursus «scientifiques» au sens étroit. Il faut également aménager des temps de débat sur la culture numérique afin d’activer l’appropriation sociale des technologies. Autrement dit, il faut faciliter l’appropriation de la culture numérique comme *«contenu»* et comme *«problème»*.
De manière générale - comme le terme de littératie nous y invite - nous n’opposons pas *«culture générale»* ou *«classique»* et *«culture numérique»* : la culture numérique est culture.

Mais elle est aussi technique et technologie. Et le numérique transforme en question politique centrale l’appropriation par toute la population d’une culture de type technique et technologique, où l’automatisation joue un rôle clé.
Si chacun est à même de comprendre les techniques de son temps, il faut encore avoir la possibilité d’accéder aux informations, savoirs, et savoir-faire caractéristiques de ces technologies. Il est donc nécessaire que les technologies soient et restent ouvertes et accessibles, de telle manière que non seulement les spécialistes ou les acteurs de la culture numérique, mais l’ensemble du public puisse accéder aux codes source, aux schémas électroniques et ainsi exercer ses compétences s’il le souhaite.

La culture du Libre et sa volonté de défendre l’ouverture des données et des informations, l’accès et la réutilisation des œuvres, le partage des connaissances et des ressources est donc une condition indispensable au développement de la culture numérique et au bon fonctionnement de la démocratie numérique. Plus généralement la valeur de la culture numérique et l’étendue de son appropriation démocratique sont étroitement corrélées à sa construction stratégique comme bien commun.

Les espaces publics numériques sont aujourd’hui, en 2014, parmi les rares endroits en France avec quelques autres lieux dédiés, où peuvent se mêler éducation populaire et culture numérique, où un public varié se rend pour échanger, discuter, apprendre au sujet du numérique.

Nous nous réunissons désormais, librement, avec les moyens qui sont les nôtres, pour impulser une dynamique de construction consciente de cette culture numérique.

On en trouvera ici la première initiative : un document sur la culture numérique que nous savons forcément inachevé et insuffisant, mais que nous imaginons à la fois conséquent et vivant, comme une [plate-forme problématique, ouverte aux contributions les plus variées](http://culturenum.net/index.php5?title=Accueil), intéressée par les implications des animateurs ou médiateurs numériques, orientée vers l’échange et le partage.

---
Vous pouvez retrouver l'intégralité de cet article au sein de la publication Parcours Numériques, publication téléchargeable [ici](https://station.pingbase.net/index.php/s/Q6Pn23jGkpXdo7o).
