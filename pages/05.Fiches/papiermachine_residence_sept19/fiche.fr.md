---
title: 'Résidence artistique Papier/Machine - septembre 19'
type_ressource: text
feature_image: IMG_4779.JPG
license: cc-by-nc-sa
date: '20-09-2019 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'papier machine'
        - Papier/Machine
        - micro-édition
        - livre-objet
        - résidence
    author:
        - 'Charlotte Rautureau'
aura:
    pagetype: article
    description: 'Retour sur la résidence artistique Papier/Machine accueillie au fablab Plateforme C du 9 au 13 septembre 2019.'
    image: IMG_4779.JPG
metadata:
    description: 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 9 au 13 septembre 2019.'
    'og:url': 'https://ressources.pingbase.net/fiches/papiermachine_residence_sept19'
    'og:type': article
    'og:title': 'R&eacute;sidence artistique Papier/Machine - septembre 19 | PiNG Ressources Num&eacute;riques'
    'og:description': 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 9 au 13 septembre 2019.'
    'og:image': 'https://ressources.pingbase.net/fiches/papiermachine_residence_sept19/IMG_4779.JPG'
    'og:image:type': image/jpeg
    'og:image:width': '5184'
    'og:image:height': '3456'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'R&eacute;sidence artistique Papier/Machine - septembre 19 | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Retour sur la r&eacute;sidence artistique Papier/Machine accueillie au fablab Plateforme C du 9 au 13 septembre 2019.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/papiermachine_residence_sept19/IMG_4779.JPG'
    'article:published_time': '2019-09-20T00:00:00+02:00'
    'article:modified_time': '2021-01-12T16:34:46+01:00'
    'article:author': Ping
---

## Propositions

À l’invitation de PiNG et de son exploratrice associée, Yanaita Araguas, 6 artistes d’univers différents sont venus au sein du fablab Plateforme C explorer en binômes le thème du livre-objet du 9 au 13 septembre 2019.

**Adelaïde Gaudéchoux, plasticienne, et Alice Rizio, artisan imprimeur**

Leur projet s’est fondé sur l’envie de travailler autour des possibilités du papier avec de la découpe, du piqué afin d’aller creuser dans le matériau. Le sujet choisi pour le livre est celui d’un carré de fouille autour du Kourgane Scythe, mixant couches de papier et dessins.
![IMG_4720.JPG](IMG_4720.JPG?forceresize=100%)
![IMG_4723.JPG](IMG_4723.JPG?forceresize=100%)
![IMG_4742.JPG](IMG_4742.JPG?forceresize=100%)
![IMG_4777.JPG](IMG_4777.JPG?forceresize=100%)

**Vincent Renaï, relieur d’art, et Billy Sérib, artiste sérigraphe**

Leur livre-objet est un écho à leurs engagements militants au service des mouvements trans/pédé/féministes. Un livre dans lequel des insultes se transforment en “emblèmes” des minorités de genre et sexuelles, révélés par leurs travaux à la découpeuse laser et à la brodeuse numérique, notamment.

[*Consulter la note d'intention de ce projet*](https://ressource.pingbase.net/wp-content/uploads/2020/03/Note-dintention-PING-Billy-Vincent.pdf)

![IMG_4706.JPG](IMG_4706.JPG?forceresize=100%)
![IMG_4760.JPG](IMG_4760.JPG?forceresize=100%)
![IMG_4765.JPG](IMG_4765.JPG?forceresize=100%)
![IMG_4767.JPG](IMG_4767.JPG?forceresize=100%)

**Yanaita Araguas, artiste imprimeur, et Marion Jdanoff, illustratrice**

« Tenter teinter » se présente sous la forme d’un nuancier textile grâce auquel on peut mettre en relation 3 bandelettes de tissus teints pour créer des correspondances. À la base de ce travail, l’envie pour ces deux artistes de trouver un terrain neutre pour fabriquer un livre ensemble en dehors de leur champ d’action habituel, bien qu’avec une certaine continuité (des couleurs pour Marion, des matières naturelles pour Yanaïta).

[*Consulter la note d'intention de ce projet*](https://ressource.pingbase.net/wp-content/uploads/2020/03/Note_intention_araguas_jdanoff_residence19.pdf)

![IMG_4776.JPG](IMG_4776.JPG?forceresize=100%)
![IMG_4774.JPG](IMG_4774.JPG?forceresize=100%)
![IMG_4741.JPG](IMG_4741.JPG?forceresize=100%)
![IMG_4732.JPG](IMG_4732.JPG?forceresize=100%)

```
