---
title: 'Un festival sous licence libre'
media_order: 'Cahierdes-chargesetudiants_scenographie_festD15.pdf,ComFestivalD2015.zip,FestivalD15_Appelbenevoles.pdf,FestivalD15_charte.pdf,FestivalD15_Repartitionprojetssceno.pdf,FestivalD15_Visuelsscenographie.pdf,FestivalD_com-recapdesactions.pdf,IMG_9162-750x330.jpg'
type_ressource: fichier
feature_image: IMG_9162-750x330.jpg
license: cc-by-nc-sa
date: '18-12-2015 15:12'
taxonomy:
    category:
        - pratiques
    tag:
        - 'festival d'
        - 'open source'
aura:
    pagetype: website
    description: 'Festival D est un festival sous licence libre.'
    image: IMG_9162-750x330.jpg
show_breadcrumbs: true
metadata:
    description: 'Festival D est un festival sous licence libre.'
    'og:url': 'https://ressources.pingbase.net/fiches/un_festival_sous_licence_libre'
    'og:type': website
    'og:title': 'Un festival sous licence libre | Espace Ressources Num&eacute;riques'
    'og:description': 'Festival D est un festival sous licence libre.'
    'og:image': 'https://ressources.pingbase.net/fiches/un_festival_sous_licence_libre/IMG_9162-750x330.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '750'
    'og:image:height': '330'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Un festival sous licence libre | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Festival D est un festival sous licence libre.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/un_festival_sous_licence_libre/IMG_9162-750x330.jpg'
    'article:published_time': '2015-12-18T15:12:00+01:00'
    'article:modified_time': '2020-12-02T09:22:53+01:00'
    'article:author': Ping
---

*Festival D est un festival sous licence [Creative Commons CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/) (paternité, pas d’utilisation commerciale, partage sous licence identique).*

Nous mettons à votre disposition ci-après des éléments vous permettant d’organiser Festival D chez vous. Ils sont issus de notre expérience de 2015 dont les grandes lignes sont les suivantes :
- Festival D 15 s’est déroulé sur deux jours, les 26 et 27 septembre,
- Au sein du lieu unique, partenaire clé de la rencontre, dans plusieurs de ses espaces,
- 35 projets ont été présentés par des exposants venus d’ici et d’ailleurs,
- 5 ateliers ont été proposés par des partenaires de la rencontre (Stéréolux, EDNA, Accoord),
- 1 discussion sur le thème “bricoler aujourd’hui” a été animée avec 3 intervenants invités pour éclairer le débat,
- Le samedi soir s’est voulu festif avec une programmation de deux groupes/performeurs musicaux et 1 DJ set made in PiNG,
- Cette rencontre a attiré 5600 visiteurs,
- Pour arriver à ce résultat, 1 personne salariée à temps plein sur 3 mois a assuré la coordination de la rencontre. Elle s’est vue assistée par une équipe de 7 personnes salariées à temps plein sur le temps de la rencontre à laquelle 22 bénévoles sont venus prêter main forte.

Avant de vous lancer, nous vous invitons à prendre connaissance de la charte de Festival D. Celle-ci vous éclaire sur la philosophie derrière la rencontre et les conditions à respecter pour l’organiser. Si, après lecture de celle-ci, vous êtes en accord avec cette charte, passez à l’étape suivante.

[Lire la charte](FestivalD15_charte.pdf?classes=btn)

## PROGRAMMATION

La programmation de Festival D doit, dans les grandes lignes, s’articuler autour d’une exposition / déambulation libre auprès des porteurs de projets, d’ateliers, d’un temps de débat/conférence et d’une programmation artistique en soirée (musique, démos…).

Les projets présentés ont répondu à un appel à projets, ouvert 2 mois et demi. Ils ont été ensuite sélectionnés par un comité de sélection, composé des partenaires du festival. 35 projets ont été retenus pour leur qualité, pour la diversité des propositions et dans un nombre limité pour s’assurer que les visiteurs puissent tous les voir.

Les ateliers, débat(s) et concerts ont fait l’objet d’une programmation de la part des organisateurs.

Pour vous aider sur cette partie, nous mettons à votre disposition :
- [le texte de l’appel à participation diffusé sur le site festivald.net et sur nos réseaux](FestivalD15_Texteappelparticipation.odt),
- [la fiche de renseignement des ateliers soumises à nos partenaires](FestivalD15_Ficherenseignementatelier.odt),
- [le formulaire de candidature pour les exposants](FestivalD15_formulairecandidature.odt),
- [la convention pour les exposants](FestivalD15_conventionexposant.odt),
- [l’autorisation parentale pour les exposants](FestivalD15_autorisationparentaleexposant.odt),
- [la convention pour les partenaires ateliers](FestivalD15_conventionpartenariatatelier.odt).

## LOGISTIQUE

Festival D, comme toute rencontre, c’est pas mal d’organisation et ce sur plusieurs mois. Un [rétroplanning type](FestivalD15_retroplanning.ods) vous aidera dans la mise en place de la rencontre en amont et un [planning d’organisation](FestivalD15_Planninglogistiquerencontre.ods) des forces vives dans la mise en place de la rencontre les derniers jours.

## SCÉNOGRAPHIE

Il est important de travailler sur cet aspect pour bien mettre en valeur les projets et faciliter la médiation comme la déambulation.

Pour cette édition, des étudiants du [DSAA Design d’Espace du lycée Livet](http://livet.paysdelaloire.e-lyco.fr/formation-superieure-en-design-espace-d-s-a-a-/) ont planché sur une première version de la scénographie lors d’un workshop en février 2015. Dans un second temps, une des propositions a servi de base à [Antoine Taillandier](http://antoinetaillandier.com/) pour proposer la scénographie de Festival D. La scénographie a ensuite été fabriquée au sein de Plateforme C par Antoine et des adhérents en août  2015.

Voici le cahier des charges soumis aux étudiants ainsi que quelques plans qui vous donnent à voir la scénographie et la répartition des projets dans l’espace :
- [Cahier des charges](Cahierdes-chargesetudiants_scenographie_festD15.pdf)
- [Répartition des projets](FestivalD15_Repartitionprojetssceno.pdf)
- [Visuels](FestivalD15_Visuelsscenographie.pdf)

## BÉNÉVOLES

Nous avons pu compter sur l’aide de 22 bénévoles pour rendre cette édition possible. Ils ont été particulièrement présents en amont de la rencontre sur la distribution d’affiches et de flyers, l’installation, pendant la rencontre sur la médiation et la logistique, et à l’issue du festival sur le rangement. Leur aide a été précieuse, chouchoutez vos bénévoles.

Pour vous aider à recruter vos bénévoles et les accompagner, voici quelques documents qui peuvent vous être utiles :
- [appel à bénévoles](FestivalD15_Appelbenevoles.pdf),
- [autorisation parentale pour les mineurs](FestivalD15_autorisationparentalebenevole.odt),
- [missions des bénévoles](FestivalD15_Missionsbenevoles.odt),
- [planning type des bénévoles](FestivalD15_Planningbenevoles.ods),
- [brief sur la médiation](FestivalD15_Briefmediation.odt).

## BUDGET

Comme tout budget, celui-ci n’a de valeur qu’éclairé par un contexte. C’est ainsi que nous vous le présentons dans [ce document](FestivalD15_Openbudget.odt).

## COMMUNICATION

Vous trouverez dans le .zip téléchargeable ci-dessous les éléments qui ont servis pour les supports imprimés, les relations presse et le site internet de l’édition 2015 :
– identité visuelle : logo, code couleur, visuel au format vectoriel en paysage et en portrait
– presse : le dossier de presse du festival et les différents communiqués (appel à bricoleurs, relance/invitation, bilan)
– print : les fichiers des différents supports imprimés (flyer, affiche, programme distribué pendant l’événement)
– web : éléments des appels à bricoleurs, favicon du site internet, newsletters

[Télécharger les éléments de communication](ComFestivalD2015.zip?classes=btn)

Le récapitulatif de l’ensemble des actions de communication réalisées pour le festival est à retrouver dans [ce document](FestivalD_com-recapdesactions.pdf).
