---
title: 'Fraiseuse Carvey'
media_order: 450px-Carvey.jpg
type_ressource: text
feature_image: 450px-Carvey.jpg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - fablab
        - machine
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Cette machine sert au fraisage.'
    image: 450px-Carvey.jpg
metadata:
    description: 'Cette machine sert au fraisage.'
    'og:url': 'https://ressources.pingbase.net/fiches/fraiseuse_carvey'
    'og:type': website
    'og:title': 'Fraiseuse Carvey | Espace Ressources Num&eacute;riques'
    'og:description': 'Cette machine sert au fraisage.'
    'og:image': 'https://ressources.pingbase.net/fiches/fraiseuse_carvey/450px-Carvey.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '450'
    'og:image:height': '600'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Fraiseuse Carvey | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cette machine sert au fraisage.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/fraiseuse_carvey/450px-Carvey.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:06:45+01:00'
    'article:author': Ping
---

*Cette machine sert au fraisage.*

[INFORMATIONS ET MODE D'EMPLOI DE LA MACHINE](https://fablabo.net/wiki/Fraiseuse_Carvey?classes=btn)
