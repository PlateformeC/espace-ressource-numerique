---
title: 'Y a-t-il un passager clandestin dans le fablab ?'
media_order: BP6Q7320.jpg
type_ressource: text
feature_image: BP6Q7320.jpg
license: cc-by-sa
date: '17-01-2017 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - 'propriété intellectuelle'
        - 'culture libre'
        - opensource
        - 'c libre'
        - fablab
    author:
        - 'Vladimir Ritz'
aura:
    pagetype: website
    description: 'Vladimir Ritz, doctorant en propriété intellectuelle et explorateur associé de PiNG impliqué dans le groupe de réflexion C LiBRE, nous explique comment le passager clandestin peut s’infiltrer dans les fablabs'
    image: BP6Q7320.jpg
metadata:
    description: 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous explique comment le passager clandestin peut s&rsquo;infiltrer dans les fablabs'
    'og:url': 'https://ressources.pingbase.net/fiches/passager_clandestin_dans_le_fablab'
    'og:type': website
    'og:title': 'Y a-t-il un passager clandestin dans le fablab ? | Espace Ressources Num&eacute;riques'
    'og:description': 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous explique comment le passager clandestin peut s&rsquo;infiltrer dans les fablabs'
    'og:image': 'https://ressources.pingbase.net/fiches/passager_clandestin_dans_le_fablab/BP6Q7320.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '800'
    'og:image:height': '533'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Y a-t-il un passager clandestin dans le fablab ? | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Vladimir Ritz, doctorant en propri&eacute;t&eacute; intellectuelle et explorateur associ&eacute; de PiNG impliqu&eacute; dans le groupe de r&eacute;flexion C LiBRE, nous explique comment le passager clandestin peut s&rsquo;infiltrer dans les fablabs'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/passager_clandestin_dans_le_fablab/BP6Q7320.jpg'
    'article:published_time': '2017-01-17T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:08:23+01:00'
    'article:author': Ping
---

*Vladimir Ritz, doctorant en propriété intellectuelle et explorateur associé de PiNG impliqué dans le groupe de réflexion C LiBRE, nous explique comment le passager clandestin peut s’infiltrer dans les fablabs.*

S’il n’y a pas de pilote dans l’avion, il y a peut-être un passager clandestin dans le fablab.

Qui est vraiment ce passager clandestin, ce free-rider ? Cette notion d’origine économique est plurale, chacun y va de sa définition et y voit ce qu’il veut y voir.
Parfois le passager clandestin est un acteur économique qui utilise un bien collectif sans en payer sa part (par exemple le passager du train qui ne paie pas son billet).
Parfois c’est une personne qui va bénéficier d’une campagne d’investissements sans y avoir participé (par exemple l’actionnaire d’une entreprise en faillite qui attend que les autres actionnaires remettent de l’argent dans les caisses pour remettre la société à flots).
Ou alors ce peut être une personne qui va bénéficier d’une action faite par un groupe mais là encore qui ne va pas y participer (par exemple du salarié qui bénéficie des acquis sociaux sans avoir participé au mouvement social).

## Dans toutes ces définitions, il y a une souche commune :

Tout d’abord, le passager clandestin a, de son point de vue, peu d’intérêt à agir pour le collectif. En effet, l’action va lui coûter de l’énergie, du temps ou de l’argent. Ainsi, lorsqu’il est certain de pouvoir bénéficier de l’action des autres il ne va pas nécessairement « jouer le jeu ». Si nous nous en tenons à ce critère alors nous sommes tous un peu des passagers clandestins, nous bénéficions tous d’un système de santé pour lequel nous n’œuvrons pas toujours, nous utilisons GNU/Linux sans pour autant y contribuer, nous bénéficions d’infrastructures publiques alors que nous ne payons pas toujours des impôts, etc.

Il y a donc plus : il faut que le comportement du passager clandestin soit considéré comme déviant par le groupe considéré. Prenons par exemple un chômeur qui bénéficierait gratuitement d’un service de transport. Cela semblerait aux yeux des usagers moins « illégitime » qu’un usager ne payant pas alors qu’il en a les moyens. Pourtant les deux sont usagers d’un service qu’ils ne paient pas… Le groupe a donc un rôle déterminant dans la définition de la notion car il va servir de référent pour déterminer ce qui est légitime et ce qui ne l’est pas.

**Le passager clandestin est donc une personne qui, par son comportement, va de manière non légitime bénéficier d’avantages sans participer à l’élaboration de ces avantages.**

Puisque la notion de passager clandestin est d’origine économique il n’est donc pas surprenant qu’elle refasse surface dans cette économie collaborative qui naît dans les fablabs. Dans cette économie collaborative, basée sur les biens communs de la connaissance, la somme des intérêts des membres ne suffit pas à faire l’intérêt collectif. Ce dernier est plus large. Il ne suffit pas que chacun sache de son coté, il faut savoir tous et ensemble. Par ailleurs, il est souvent apprécié voire exigé qu’il y ait un retour à la communauté. Celui qui ne respectera pas cette règle du jeu sera alors souvent vu comme un passager clandestin.

Il en existe au moins deux types :

  -  Il y a celui qui va discrètement épier un projet élaboré dans un fablab et qui va faire le nécessaire pour s’approprier tout avant tout le monde. C’est en quelque sorte de l’espionnage industriel au niveau du fablab.

  -  Et il y a celui qui va utiliser un travail partagé par les membres du fablab, l’améliorer et ne plus partager les améliorations qu’il va apporter à ce travail. C’est en quelque sorte une enclosure1. Nous pensons bien évidement à Mac OS qui a utilisé une base UNIX pour en faire un système d’exploitation propriétaire ou encore Makerbot qui a cessé de publier les plans de son imprimante 3D après avoir reçu maintes contributions.

**Le point commun entre ces deux types de passagers clandestins est qu’ils vont utiliser certaines règles de la propriété intellectuelle pour arriver à leurs fins.**

Prenons le cas du passager clandestin « espion ». D’apparence sympathique, il va venir donner un coup de main. Ensuite, il va revendiquer qu’il est le seul à avoir conçu ce projet. Pour bien faire les choses, il va se ménager une preuve de tout ça, il va aller à l’[INPI](https://www.data.gouv.fr/fr/organizations/institut-national-de-la-propriete-industrielle-inpi/#datasets) (Institut national de la propriété industrielle) et déposer une enveloppe Soleau2. Il aura ainsi la preuve qu’au jour du dépôt, il avait connaissance du projet. Pour contester cette preuve il faudra que les makers à l’origine dudit projet puissent prouver qu’ils en avaient connaissance avant le dépôt de l’enveloppe Soleau. Pour peu que le travail ne soit pas documenté, la tâche peut être difficile. La documentation a donc une réelle importance dans ce cas.

Dans l’autre cas, celui du passager clandestin « profiteur », il va utiliser la possibilité que lui donne les licences non copyleft. Lorsqu’un projet est partagé par l’intermédiaire de licences non copyleft, l’utilisateur peut utiliser et modifier le projet. Et, s’il le modifie, la licence ne lui impose pas de repartager à son tour ses modifications. Il peut alors en faire un usage en tant que propriétaire et ne pas rétribuer la communauté. C’est là, la grande différence entre les licences libres et les licences open source3. Il importe donc de choisir la bonne licence en gardant à l’esprit que de ce choix peut découler un certain nombre de conséquences.

## Mais alors, quel avenir pour la culture libre ? Sommes-nous totalement démunis face à ces passagers clandestins ?

Heureusement non, il existe des possibilités pour éviter ce genre de problèmes. Deux méthodes sont possibles : la méthode douce et la méthode forte.

 - La méthode forte consisterait à modifier [la charte des fablabs](http://www.plateforme-c.org/wp-content/uploads/chartefablabA4.png).

Cette charte est souvent considérée comme ambiguë, floue, voire peu propice à la culture libre. On trouve par exemple dans cette charte la disposition suivante :

***« Qui possède les inventions faites dans un Fablab ?
Les designs et les procédés développés dans les fablabs peuvent être protégés et vendus comme le souhaite leur inventeur, mais doivent rester disponibles de manière à ce que les individus puissent les utiliser et en apprendre. »***

Il y est question des inventions mais, il n’y a pas que des inventions dans les fablabs, il y a parfois des œuvres, des textes, des dessins, des designs… La charte est donc relativement incomplète. De plus, elle est assez peu précise sur la manière dont un inventeur peut à la fois commercialiser son invention tout en la laissant « disponible ».

Il serait donc envisageable de dépoussiérer cette charte. Il serait même souhaitable d’y préciser que les usagers du fablab s’engagent à ne pas s’approprier un projet au détriment de la communauté qui y a participé. Cette simple précision ainsi que l’engagement de chacun à respecter la charte suffirait à empêcher le passager clandestin « espion » de venir sévir dans les labs. Cette solution n’est bien évidement pas sans failles, il faut que les usagers respectent la charte et s’ils ne le font pas alors il faut envisager des moyens de coercition. De même, lors des temps ouverts il serait compliqué de faire signer la charte à chacun des visiteurs. Mais cela aurait au moins le mérite de clarifier les choses.

- La méthode souple consiste à utiliser la propriété intellectuelle pour contrer ce *free-rider*.

Nous avons souvent l’occasion de rappeler qu’il est important de documenter son travail et de s’intéresser à la propriété intellectuelle et aux licences libres. Ce n’est jamais aussi vrai que pour contrer le passager clandestin. En effet, documenter son travail permet en général d’avoir une « preuve » de la date à laquelle le projet a été effectué ou initié. Ainsi, face au passager clandestin « espion », il y aura possibilité d’opposer une « preuve » antérieure à l’enveloppe Soleau. Hors de la documentation point de salut ! Il faut documenter et le plus régulièrement possible. Notons toutefois que si la création documentée s’inscrit dans le secteur industriel (entendu au sens large) alors la développer en public c’est en faire don au domaine public. Ainsi, si c’est une invention pour laquelle un brevet est envisagé, dans la mesure où elle est faite dans un fablab la documentation n’enlève rien et même peut parfois apporter beaucoup : le simple fait de développer une invention aux yeux de tous bloque la possibilité d’obtenir un brevet alors que la documentation permet souvent d’identifier l’inventeur. Il est donc assez illusoire d’imaginer garder un monopole économique sur une invention dans un fablab.

De même, s’intéresser aux licences ouvertes et à leurs conséquences doit occuper une partie importante du projet et ce le plus tôt possible. Il existe aujourd’hui beaucoup de licences dans beaucoup de domaines (les inventions (domaine des brevets) n’en faisant pas encore partie). Il est parfois difficile de se repérer dans ce méandre. Le risque est d’opter pour une licence, un peu au hasard, et une fois cette licence choisie il sera compliqué de faire marche arrière. C’est la raison pour laquelle il faut s’y intéresser en amont afin d’éviter toute déconvenue. Le « copyleft », cette clause qui permet d’imposer aux utilisateurs d’une création de partager leurs travaux qui en sont issus suivant les mêmes conditions, est un bon moyen d’assurer la pérennité du partage et donc de bloquer les enclosures.

Bien sûr, il ne faut pas exagérer le phénomène, il y a peu d’exemple de passagers clandestins à l’échelle des fablabs. Il est probable que les convictions partagées par les makers suffisent à les garder éloignés des fablabs. C’est heureux car en l’état actuel, nous serions bien démunis face à de tels comportements illégitimes.

Vladimir Ritz, explorateur associé de PiNG, doctorant en propriété intellectuelle et associé au groupe de réflexion C LiBRE.


1 En référence au [mouvement des enclosures de l’Angleterre du 12ème siècle](https://fr.wikipedia.org/wiki/Mouvement_des_enclosures)

2 [http://prospiceis.free.fr/images/Soleau.jpg](http://prospiceis.free.fr/images/Soleau.jpg)

3 [https://www.gnu.org/philosophy/free-software-for-freedom.fr.html](https://www.gnu.org/philosophy/free-software-for-freedom.fr.html)
