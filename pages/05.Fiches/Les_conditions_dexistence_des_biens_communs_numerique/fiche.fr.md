---
title: 'Les conditions d''existence des biens communs numériques'
media_order: img_2580.jpg
type_ressource: text
feature_image: img_2580.jpg
license: cc-by-sa
date: '14-04-2014 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - Internet
        - communs
        - 'sobriété numérique'
    author:
        - 'Philippe Aigrain'
aura:
    pagetype: website
    description: 'On affirme souvent qu''internet et le Web sont des biens communs.'
    image: img_2580.jpg
metadata:
    description: 'On affirme souvent qu''internet et le Web sont des biens communs.'
    'og:url': 'https://ressources.pingbase.net/fiches/les_conditions_dexistence_des_biens_communs_numerique'
    'og:type': website
    'og:title': 'Les conditions d''existence des biens communs num&eacute;riques | Espace Ressources Num&eacute;riques'
    'og:description': 'On affirme souvent qu''internet et le Web sont des biens communs.'
    'og:image': 'https://ressources.pingbase.net/fiches/les_conditions_dexistence_des_biens_communs_numerique/img_2580.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '620'
    'og:image:height': '165'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les conditions d''existence des biens communs num&eacute;riques | Espace Ressources Num&eacute;riques'
    'twitter:description': 'On affirme souvent qu''internet et le Web sont des biens communs.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/les_conditions_dexistence_des_biens_communs_numerique/img_2580.jpg'
    'article:published_time': '2014-04-14T00:00:00+02:00'
    'article:modified_time': '2020-11-26T14:42:16+01:00'
    'article:author': Ping
---

## Internet comme bien commun ?

On affirme souvent qu'internet et le Web sont des biens communs. A priori, cela peut surprendre, dans la mesure où une grande partie des appareils, des infrastructures et des services y sont fournis par des acteurs privés.

Qu'est-ce donc qui est si précieux que nous souhaitions établir des chartes de droits pour le protéger, des lois pour empêcher que des acteurs privés recherchant des rentes en polluent le fonctionnement, des digues pour empêcher les acteurs publics et leurs prestataires d'y installer leur surveillance ?

La réponse tient en deux points étroitement liés : ce qui est précieux, ce sont les protocoles et l'architecture de ces réseaux, d'une part, et les usages qu'ils permettent lorsque nous reprenons le contrôle des dispositifs, des outils et des applications, d'autre part.

Si l'on voulait trouver un précédent d'une ressource essentielle à toutes les activités humaines, en permanente évolution et en même temps extérieure à chacun d'entre nous, on pourrait penser à la langue elle-même. Mais internet, le web et l'informatique qui les rend possibles sont des artefacts techniques, objet d'activités industrielles autant que lieu des pratiques de chacun. Cela pose des problèmes spécifiques pour protéger leur caractère de bien commun et pour garantir que chacun d'entre nous en bénéficie dans son développement humain et ses interactions sociales.

À la base d'internet et de tout ce qui se construit « au-dessus » d'internet, il y a une merveilleuse invention : la construction d'un réseau de « meilleur effort » où la transmission des informations sous forme de petits paquets n'est pas déterministe mais « fait de son mieux » pour garantir à chacun la bonne transmission d'un contenu, indépendamment de ce que ce contenu représente. Cette invention, esquissée dans les années 1960, déployée dans les années 1970 et théorisée au début des années 1980 rend possible quelque chose dont les tenants du « on va faire votre bonheur pour vous » ne sont pas encore revenus. Voilà un réseau universel « bête » (au sens où il n'essaye pas de garantir et contrôler la transmission de chaque contenu par exemple en lui donnant une priorité sur d'autres ou en réservant la bande passante nécessaire), bête donc en un certain sens, mais aussi très intelligent dans sa conception. Il donne à chacun qui est doté d'un ordinateur la possibilité d'imaginer de nouveaux usages, de devenir producteur et diffuseur de contenus ou de services, Les anciens monopolistes de ces fonctions (éditeurs, opérateurs téléphoniques, télédiffuseurs) qui ont été court-circuités ne ménageront aucun effort pour prendre leur revanche, aussi tardive soit-elle, contre cette émancipation.

Ce n'est qu'avec la naissance du web au début des années 1990 puis avec la multiplication d'autres applications tournant « au-dessus » du protocole IP d'internet que l'impact est devenu évident pour chacun d'entre nous, et non plus seulement les scientifiques ou quelques communautés spécialisées. Aujourd'hui, ce qui est évident, c'est que ce potentiel immense de développement humain et d'émancipation sociale et politique est gravement menacé par la recentralisation de nombreux services sur le Web et le développement des communications mobiles sur un modèle propriétaire ne respectant pas internet. Certains en concluent déjà qu'internet et l'informatique ouverte étaient des illusions, que ce serait la vraie nature d'un plan d'asservissement et de surveillance généralisée qui se révélerait, que bien commun, il n'y a jamais eu.

Or ce type de mise en danger des biens communs, loin d'être une exception, est leur lot … commun. Les biens communs de l'environnement (eau, air, terres cultivables, littoral) ne sont pas moins menacés par l'appropriation privée des ressources et de leur gestion, souvent avec la complicité des acteurs publics à travers les partenariats public-privé ou la transformation d'organismes publics en gestionnaires privés.

Allons-nous donc consentir, après moins d'une dizaine d'années de contre-révolution, à renoncer à la merveilleuse invention de l'internet et de ses usages parce que nous avons laissé quelques acteurs en accaparer le développement ? L'histoire nous apprend le coût de ces renoncements. Il a fallu trois siècles pour que l'invention de l'imprimerie soit vraiment porteuse d'émancipation au siècle des lumières. Au moins autant pour qu'on discerne de nouvelles voies entre les périls conjoints des enclosures privées et la propriété collective d'État pour les terres agricoles.

En réalité les dangers actuels que court internet comme bien commun proviennent d'une coalition entre des pouvoirs fragiles et menacés, ceux de classes politiques qui ont lié leur sort à celui d'une économie en perdition, d'une petite classe de très privilégiés, de quelques grands acteurs économiques et médiatiques. C'est bien parce que ces acteurs sentent le vent de l'émancipation politique des sociétés qu'ils défendent les acteurs centralisés (quitte à les critiquer lorsqu'ils émanent d'un autre pays), qu'ils développent censure, contrôle et surveillance, qu'ils privilégient les dispositifs et communications mobiles propriétaires et nous proposent de devenir de bons petits consommateurs numériques.

Il suffit que nous n'y consentions pas, que nous comprenions les enjeux de reprendre le contrôle sur les dispositifs que nous utilisons, les données et contenus que nous produisons et nos interactions sociales pour qu'internet soit remis sur la trajectoire des biens communs. Cela va demander de grands efforts mais qui sont immédiatement porteurs de grandes satisfactions.


## Reconstruction de l'autonomie et de la socialité numérique

Chaque petit pas que nous faisons pour reconstruire notre autonomie et notre souveraineté dans la sphère numérique est en même temps un pas pour échapper à une intense souffrance. Souffrance d'être investis dans un nouvel univers qui nous donne des satisfactions et des capacités irremplaçables et de subir cet univers comme quelque chose qui nous impose des rythmes, des buts, des formes que nous ne maîtrisons pas. Nous sommes à la fois libérés et asservis.

Une partie de cette souffrance résulte du fait que la définition et le devenir des appareils, des services, des applications nous échappe en grande partie et obéit aux stratégies de captation de la valeur économique par des acteurs industriels distants. C'est la souffrance de voir des réseaux soi-disant sociaux nous pousser dans une quête absurde du nombre d'amis et du nombre de leurs soi-disant témoignages d'attention pour pouvoir monétiser (très faiblement en réalité) notre propre temps d'attention. C'est la souffrance d'imaginer le livre papier qui nous appartient et dont nous faisons ce que nous voulons remplacé par un soi-disant livre numérique qui n'est qu'une location de droit d'usage sur un appareil qui décide pour nous et pour l'auteur de ce que nous avons le droit de faire du texte. C'est la souffrance de dispositifs mobiles qui nous rendent dépendants d'une géolocalisation panoptique, interrompent sans arrêt nos interactions avec d'autres dans l'espace physique et capturent toutes nos données pour en faire un maigre commerce ou une surveillance totalitaire. Tout cela, nous y avons consenti, et nous pouvons en faisant dissidence, reconstruire d'autres outils et applications qui nous en libéreront. Cela demande une révolte, mais dès qu'elle s'exprimera vraiment, l'effort nécessaire n'apparaîtra plus comme du temps perdu, ne sera plus « moins commode » que d'accepter l'esclavage.

Il y a aussi cependant, des souffrances qui ne viennent pas de l'appropriation privée des biens communs numériques, qui viennent de notre propre inadaptation à la nouvelle situation où tout un chacun est doté de nouvelles capacités. Après l'ivresse de pouvoir s'adresser à tous, de développer de nouvelles capacités à créer dans divers médias, vient la réalisation que lorsque beaucoup plus de gens le font, chaque production ne peut recevoir en moyenne qu'une attention très réduite en comparaison de celle que recevaient les créations de l'ère des industries culturelles, ère de la rareté des sources. C'est aussi la souffrance qui survient lorsque l'émission massive de messages par chacun (par exemples des courriels) surcharge nos boîtes de réception au point que nous vivons comme exposés à un reproche perpétuel, pas encore capables d'une écologie de la sobriété informationnelle.

La révolution des communs numériques sera donc une révolution politique, technique, une libération de l'économisme forcené, mais aussi une révolution des personnes. Cela fait beaucoup, mais c'est tout simplement l'enjeu majeur de notre époque.
