---
title: 'Les doigts dans la prise #6 : Le Cloud'
type_ressource: audio
feature_image: nuageafficheconfligth-1-768x768.jpg
license: cc-by-nc-sa
published: true
date: '17-06-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Thomas Bernardi'
aura:
    pagetype: article
    description: 'Dans cette micro-fiction, Thomas Bernardi vous rappelle que le Cloud est un ami qui ne vous veut pas toujours du bien...'
    image: nuageafficheconfligth-1-768x768.jpg
metadata:
    description: 'Dans cette micro-fiction, Thomas Bernardi vous rappelle que le Cloud est un ami qui ne vous veut pas toujours du bien...'
    'og:url': 'https://ressources.pingbase.net/fiches/sun06_juin19_nuages'
    'og:type': article
    'og:title': 'Les doigts dans la prise #6 : Le Cloud | Espace Ressources Num&eacute;riques'
    'og:description': 'Dans cette micro-fiction, Thomas Bernardi vous rappelle que le Cloud est un ami qui ne vous veut pas toujours du bien...'
    'og:image': 'https://ressources.pingbase.net/fiches/sun06_juin19_nuages/nuageafficheconfligth-1-768x768.jpg'
    'og:image:type': image/webp
    'og:image:width': '768'
    'og:image:height': '768'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #6 : Le Cloud | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Dans cette micro-fiction, Thomas Bernardi vous rappelle que le Cloud est un ami qui ne vous veut pas toujours du bien...'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun06_juin19_nuages/nuageafficheconfligth-1-768x768.jpg'
    'article:published_time': '2019-06-17T16:31:00+02:00'
    'article:modified_time': '2020-12-09T13:45:58+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Dans cette micro-fiction, Thomas Bernardi vous rappelle que le Cloud est un ami qui ne vous veut pas toujours du bien...

Pour écouter le podcast c’est par [ici](https://www.lesonunique.com/content/les-doigts-dans-la-prise-au-loin-les-nuages) !

![Les doigts dans la prise - le cloud](6%20-%20Les%20doigts%20dans%20la%20prise%20-%20et%20au%20loin%20les%20nuages.mp3)
