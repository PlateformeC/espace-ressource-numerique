---
title: 'Expérimentations autour de la cire'
media_order: experimentation-cire-web-3.jpg
date: '15-12-2020 00:00'
taxonomy:
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'decoupeuse laser'
    - 'decoupe laser'
    - 'carte postale'
    - 'gravure'
    - 'Expérimentation matériaux'
    - 'workshop'
    author:
        - 'Yanaïta Araguas'
aura:
    pagetype: article
    description: ‘Une expérimentation menée au fablab Plateforme C par une étudiante de l'ESBAN: une carte postale gravée à la découpeuse laser sur de la cire d'abeille’
    image: experimentation-cire-web-3.jpg
    category:
        - pratiques

---
## Expérimentations autour de la cire

![experimentation-cire-web-3.jpg](experimentation-cire-web-3.jpg?forceresize=100%)

Le projet d'exploration Papier/Machine a accueilli, en novembre 19, 22 étudiant·es de l’École des Beaux-Arts Nantes St Nazaire pour expérimenter les potentiels de création graphique du fablab autour de la série et de la carte postale.

Voici l'une des expérimentations réalisées.

Ici, il s'agit d'expérimenter autour d'un matériau qui fait son entrée dans le fablab: la cire d'abeille.
L'étudiante travaillait sur l'idée des tablettes de cire servant de support d'écriture dans l'Antiquité.
[Tablettes de cire- Wikipedia](https://fr.wikipedia.org/wiki/Tablette_de_cire)

Elle a donc réalisé un coffret/coffrage à la découpeuse laser* dans du contreplaqué de peuplier, constitué de deux plaques superposées, collées puis assemblées à l'aide de charnières.
Dans ce coffrage, elle a coulé de la cire pour constituer une surface d'écriture.
L'ensemble a ensuite été placé dans la découpeuse laser afin d'y graver en mode raster* le verso standard d'une carte postale.
Selon les réglages, réalisés en amont sur des « chutes », le laser fait plus ou moins fondre la cire et rend parfois le visuel illisible.
La plaque de cire semble absorber la chaleur du laser en refroidissant, en changeant très légèrement la teinte.
Cette technique fonctionne néanmoins plutôt bien.

![experimentation-cire-web-1.jpg](experimentation-cire-web-1.jpg?forceresize=100%)
![experimentation-cire-web-2.jpg](experimentation-cire-web-2.jpg?forceresize=100%)
![experimentation-cire-web-3.jpg](experimentation-cire-web-3.jpg?forceresize=100%)
![experimentation-cire-web-4.jpg](experimentation-cire-web-4.jpg?forceresize=100%)
![experimentation-cire-web-5.jpg](experimentation-cire-web-5.jpg?forceresize=100%)
![experimentation-cire-web-6.jpg](experimentation-cire-web-6.jpg?forceresize=100%)

* Le raster est un mode d'action que propose la découpeuse laser. On fournit à la machine un fichier image en niveaux de gris (noir et blanc). Le laser va balayer la surface de la zone à usiner (correspondant à la taille de l'image fournie) et sa puissance varie en fonction de la valeur des noirs et des blancs de l'image traitée, produisant ainsi des nuances sur le matériau (plus ou moins 'cramé').

* La découpeuse laser est un procédé de fabrication qui consiste à découper la matière grâce à une grande quantité d’énergie générée par un laser et concentrée sur une très faible surface. Cette technologie est majoritairement destinée aux chaînes de production industrielles mais peut également convenir aux boutiques, aux établissements professionnels et aux tiers-lieux de fabrication.
https://fr.wikipedia.org/wiki/Découpe_laser

### Autres ressources
Matériaux utilisés :
Cire d'abeille
Contreplaqué de peuplier
Charnières

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx
