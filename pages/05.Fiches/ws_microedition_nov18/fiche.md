---
title: 'Workshop micro-édition'
media_order:
type_ressource: text   
feature_image: IMG_2307.JPG
license: cc-by-nc-sa
serie:
date: '01-12-2018 00:00'
taxonomy:
    category:
    - recits
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'micro-édition'
    - 'livre-objet'
    - 'workshop'
    author:
    - 'Charlotte Rautureau'  
aura:
  pagetype: article
  description: 'Retour sur le workshop micro-édition autour du thème récits sensibles de la catastrophe environnementale'
  image: IMG_2294.JPG

---
## Si vous ne faites pas l'expérience de la catastrophe, vous ne pouvez comprendre ce qui se joue

**Récits sensibles de la catastrophe environnementale**
**Du 26 au 30 novembre 2018**
**À Plateforme C**

*Sous l’accompagnement bienveillant de Matthias Rischewski (École de Design Nantes Atlantique), Daniel Fabry (Graz University), Yanaïta Araguas (L’atelier dans l’ombre).*

[De 0.Camp à Récits-Nature](https://ressource.pingbase.net/arts-sciences-technologies-societe/), PiNG a questionné la notion d’anthropocène en invitant artistes et scientifiques à confronter leur(s) appréhension(s) de ce sujet.

**Qu’est-ce que l’Anthropocène ?** L’Anthropocène, soit l’Ère de l’Homme, est un terme relatif à la chronologie de la géologie proposé pour caractériser l’époque de l’histoire de la Terre qui a débuté lorsque les activités humaines ont eu un impact global significatif sur l’écosystème terrestre. L’Anthropocène serait la période durant laquelle l’influence de l’être humain sur la biosphère a atteint un tel niveau qu’elle est devenue une « force géologique » majeure capable de marquer la lithosphère [Source: Wikipedia](https://fr.wikipedia.org/wiki/Anthropoc%C3%A8ne).

Récits-Nature, nouvelle étape de nos recherches sur la crise environnementale, repose sur l’idée que mettre en récits sensibles les enjeux environnementaux actuels peut avoir un impact sur les esprits et comportements des humains.

Au cours de ce projet, des artistes et scientifiques ont créé des objets, performé des contes sonores, conçu un récit graphique. Afin de compléter ces travaux créatifs, PiNG a invité les étudiant·es de la filière graphique de l’École de Design Nantes Atlantique à créer leur propre récit de la catastrophe environnementale et à imaginer et fabriquer une micro-édition autour de ce(s) sujet(s).
Cette édition limitée devait être conçue comme :
- un objet de médiation sur ces sujets,
- en direction des jeunes enfants (entre 8 et 12 ans) ou des personnes nées avant la première crise pétrolière de 1973,
- permettant de vivre une expérience sensible de la catastrophe environnementale susceptible de provoquer des transformations de comportement.
Ces ouvrages ont été conçus et produits au sein du fablab Plateforme C, atelier local de micro-édition.

## Les productions

Les étudiant·es organisé·es en huit équipes de travail ont produit autant de micro-éditions sensibles dont ils font le récit à travers leurs carnets de bord compulsés sur le [wiki du fablab Plateforme C](https://fablabo.net/wiki/Accueil).

Carnet de bord du projet [Faire le bon choix](https://fablabo.net/wiki/Faire_le_bon_choix)
![IMG_2435.JPG](IMG_2435.JPG?forceresize=100%)
Carnet de bord du projet [Le livre de la jungle, Tome II](https://fablabo.net/wiki/Le_livre_de_la_jungle,_tome_II)
![IMG_2416.JPG](IMG_2416.JPG?forceresize=100%)
Carnet de bord du projet [Trendy Granny](https://fablabo.net/wiki/Trendy_Granny)
![IMG_2427.JPG](IMG_2427.JPG?forceresize=100%)
Carnet de bord du projet [Jean-Paul Ussion](https://fablabo.net/wiki/Une_journ%C3%A9e_dans_la_vie_de_Jean-Paul_Ussion)
![IMG_2411.JPG](IMG_2411.JPG?forceresize=100%)
Carnet de bord du projet [Avec des si on referait le monde](https://fablabo.net/wiki/Avec_des_si_on_referait_bien_le_monde)
![IMG_2438.JPG](IMG_2438.JPG?forceresize=100%)
Carnet de bord du projet [Kold](https://fablabo.net/wiki/Kold)
![IMG_2480.JPG](IMG_2480.JPG?forceresize=100%)
Carnet de bord du projet [L'Agence](https://fablabo.net/wiki/L%27agence)
![IMG_2404.JPG](IMG_2404.JPG?forceresize=100%)
Carnet de bord du projet [Gum Gum](https://fablabo.net/wiki/Gum_Gum)
![IMG_2397.JPG](IMG_2397.JPG?forceresize=100%)
