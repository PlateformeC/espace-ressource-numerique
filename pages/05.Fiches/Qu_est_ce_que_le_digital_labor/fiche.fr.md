---
title: 'Cardon, & Casilli, Qu''est ce que le digital labor ?'
media_order: Visuel_Digital_Labor.jpg
type_ressource: text
feature_image: Visuel_Digital_Labor.jpg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
    tag:
        - essai
        - données
        - culturenumérique
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: website
    description: 'Cet ouvrage est le résultat d''un dialogue entre Dominique Cardon et Antonio Casilli sur le thème du digital labor.'
    image: Visuel_Digital_Labor.jpg
show_breadcrumbs: true
metadata:
    description: 'Cet ouvrage est le r&eacute;sultat d''un dialogue entre Dominique Cardon et Antonio Casilli sur le th&egrave;me du digital labor.'
    'og:url': 'https://ressources.pingbase.net/fiches/qu_est_ce_que_le_digital_labor'
    'og:type': website
    'og:title': 'Cardon, &amp; Casilli, Qu''est ce que le digital labor ? | Espace Ressources Num&eacute;riques'
    'og:description': 'Cet ouvrage est le r&eacute;sultat d''un dialogue entre Dominique Cardon et Antonio Casilli sur le th&egrave;me du digital labor.'
    'og:image': 'https://ressources.pingbase.net/fiches/qu_est_ce_que_le_digital_labor/Visuel_Digital_Labor.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '660'
    'og:image:height': '330'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Cardon, &amp; Casilli, Qu''est ce que le digital labor ? | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Cet ouvrage est le r&eacute;sultat d''un dialogue entre Dominique Cardon et Antonio Casilli sur le th&egrave;me du digital labor.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/qu_est_ce_que_le_digital_labor/Visuel_Digital_Labor.jpg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T16:28:09+01:00'
    'article:author': Ping
---

## Résumé :

Comme l'indique la quatrième de couverture, cet ouvrage est le résultat d'un dialogue entre Dominique Cardon et Antonio Casilli sur le thème du digital labor, engagé en juin 2014 lors d'une séance des ateliers de recherche méthodologique de l'INA.

Le livre est découpée en deux parties, parfois antagonistes. Dans la première, Casilli apporte une définition du concept de digital labor. Il le resitue au regard des enjeux actuels de la recherche en Sciences de l'Information et de la Communication et de concepts proches (*audience commodity*, *prosumer*, travail du consommateur/des publics, etc.). Il trace aussi la génèse de ce terme dans le courant post-opéraïste (Negri, Tronti, Vercellone...) qui avait déjà vu naitre le concept de "capitalisme cognitif". Dans la seconde intervention, Cardon revient plus particulièrement ce que ce concept traduit du changement de climat dans les travaux de recherche sur Internet *"Internet était sympa, il ne l'est plus (…) Internet n'est plus une alternative, il est partout (…) Internet était une avant-garde innovante, il est devenu conformiste et commercial".* Il appelle à ne pas jeter le bébé avec l'eau du bain, et surtout à prendre garde du fait que les positions critiques déployées autour du digital labor ne creusent pas à nouveau *"la malheureuse séparation entre la posture des intellectuels et l'expérience des internautes que les premiers théoriciens d'Internet avaient justement cherché à abolir"*.

## Notes de Meven :

Sans revenir sur l'ensemble des arguments de l'ouvrage, je me permets de livrer ici quelques éléments de définition du digital labor, issus de la première partie d'Antonio Casilli. À la suite des concepts de travail immatériel, travail des publics et  travail cognitif, et nourrie par les nouveaux enjeux de la production de valeur sur internet et de la qualification des usages numériques ordinaires, l'étude du *digital labor* tente de renouveler les  réflexions autour d'une notion dont on parvient de moins en moins à  cerner les contours : le travail. Contrairement à ce que l'on pourrait  penser, cette forme de « travail numérique » ne désigne pas celui des  ouvriers des usines produisant les dispositifs techniques nécessaires au fonctionnement infrastructurel des nouvelles technologies. Selon  Antonio A. Casilli, nous devrions plutôt la comprendre comme le *« travail invisible réalisé pour des dispositifs qui captent l'attention pour la  réinjecter dans des logiques de marchandisation. »* :

*« En  revanche, nous devons nous situer en dehors des lieux classiques de la  production pour voir apparaître ce travail. C'est en nous penchant sur  les lieux de nos sociabilités ordinaires, sur nos interactions  quotidiennes médiatisées par les nouvelles technologies de l'information et la communication, que nous commençons à détecter des formes  d'activités assimilables au travail parce que productrices de valeur,  faisant l'objet d'un quelconque encadrement contractuel et soumises à  des métriques de performance. Nous appelons digital labor la réduction de nos « liaisons numériques » à un moment du rapport de production , la subsomption du social sous le marchand dans le contexte de nos usages  technologiques. »*

‍Défini plus clairement, le digital labor est pour Casilli, est la production de l'ensemble des  activités numériques quotidiennes des usagers des plateformes sociales,  d'objets connectés ou d'applications mobiles :

*« Chaque  post, chaque photo, chaque saisie, et même chaque connexion à ces  dispositifs remplit les conditions évoquées dans la définition, produire de la valeur (appropriée par les propriétaires des grandes entreprises  technologiques), encadrer la participation (mise en place d'obligations  et contraintes contractuelles à la contribution et la coopération  contenues dans les conditions générales d'usage), mesurer (indicateurs  de popularité, réputation, statut). »*



## Notes sur le digital labor, au-delà de l'ouvrage :

Dans une interview à la revue [*Période*](http://revueperiode.net/des-salaires-pour-facebooker-du-feminisme-a-la-cyber-exploitation-entretien-avec-kylie-jarrett/), Kylie Jarret, autrice de *Feminism, Labour and Digital Media: The Digital Housewife*, revient sur les distinctions entre travail et loisir, production et  consommation, travail productif et improductif, ou encore travail libre  et aliéné. Interrogée sur l'idée selon laquelle, le digital labor aurait fait s’effondrer ces frontières, elle répond que :

*« Ce que nous apprend l’histoire critique du travail domestique et de la  formation des subjectivités au sein de la division genrée, sexuelle et  raciale du travail, c’est que de telles distinctions ont toujours été  illusoires. Pour les femmes et les personnes racisées, il n’y a jamais  eu d’espace qui ne soit fondamentalement constitué par les manières qu’a eu le capitalisme d’incorporer les logiques patriarcales et racistes. Même la subjectivité individuelle est fondamentalement articulée en lien avec ces relations de pouvoir ; donc depuis un point de vue critique  féministe ou queer, ces distinctions entre travail et loisir ne tiennent pas. »*

[*‍*](http://fragments.webflow.io/#)Pour le dire autrement, Kylie Jarret considère que bien avant le *digital labor*, le *« travail invisible »* de reproduction sociale effectué notamment par les femmes, au sein du foyer, sans statut ni salaire, traduisait  l'étendue de la supercherie constituée par les distinctions entre  travail et loisir, ou travail productif et improductif (d'après l'OCDE  en France en 2011, le travail non rémunéré représentait 3 heures et 27  minutes par jour, soit 32% du PIB. Il s’élevait à 4h18 pour les femmes  contre 2h16 pour les hommes en moyenne).

Ceci peut être mis en regard avec les travaux de Jason W. Moore dans *Capitalism in the Web of Life*, considérant que *"le capitalisme est également un îlot de production et d’échange de  marchandises au sein d’un paysage plus large d’appropriation de  travail/énergie non-payé."*, il vise là autant le travail de reproduction sociale que le travail de la nature dans son ensemble *"celui des forêts, des océans, du climat, des sols ou des êtres humains"* (voir [La nature du capital : un entretien avec Jason W. Moore](http://revueperiode.net/la-nature-du-capital-un-entretien-avec-jason-w-moore/)).

## Citer l'ouvrage :
Cardon, & Casili. (2015). Qu’est-ce que le Digital Labor ? INA Éditions.
