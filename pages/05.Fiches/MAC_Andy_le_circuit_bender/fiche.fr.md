---
title: '[MAKER À COEUR] Andy, le circuit-bendeur'
media_order: andy.png
type_ressource: video
feature_image: andy.png
license: cc-by-nc-sa
date: '01-02-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Découvrez en vidéo Andy, un autre adepte de Plateforme C qui y développe ses travaux de circuit-bending !'
    image: andy.png
metadata:
    description: 'D&eacute;couvrez en vid&eacute;o Andy, un autre adepte de Plateforme C qui y d&eacute;veloppe ses travaux de circuit-bending !'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_andy_le_circuit_bender'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Andy, le circuit-bendeur | Espace Ressources Num&eacute;riques'
    'og:description': 'D&eacute;couvrez en vid&eacute;o Andy, un autre adepte de Plateforme C qui y d&eacute;veloppe ses travaux de circuit-bending !'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_andy_le_circuit_bender/andy.png'
    'og:image:type': image/png
    'og:image:width': '982'
    'og:image:height': '525'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Andy, le circuit-bendeur | Espace Ressources Num&eacute;riques'
    'twitter:description': 'D&eacute;couvrez en vid&eacute;o Andy, un autre adepte de Plateforme C qui y d&eacute;veloppe ses travaux de circuit-bending !'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_andy_le_circuit_bender/andy.png'
    'article:published_time': '2017-02-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T14:43:23+01:00'
    'article:author': Ping
---

*Découvrez en vidéo Andy, un autre adepte de Plateforme C qui y développe ses travaux de circuit-bending !*

<iframe width="100%" height="450" sandbox="allow-same-origin allow-scripts" src="https://medias.pingbase.net/videos/embed/165da3ec-52c9-40ae-8d38-7f028e75d0f0" frameborder="0" allowfullscreen></iframe>
