---
title: 'Les doigts dans la prise #2 : le jeu et le “je”'
type_ressource: audio
feature_image: ping_jeu_0_0_light-768x768.jpg
license: cc-by-nc-sa
published: true
date: '11-02-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Julien Bellanger'
aura:
    pagetype: article
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: ping_jeu_0_0_light-768x768.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://ressources.pingbase.net/fiches/sun02_fevrier19_lejeu'
    'og:type': article
    'og:title': 'Les doigts dans la prise #2 : le jeu et le &ldquo;je&rdquo; | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://ressources.pingbase.net/fiches/sun02_fevrier19_lejeu/ping_jeu_0_0_light-768x768.jpg'
    'og:image:type': image/webp
    'og:image:width': '768'
    'og:image:height': '768'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #2 : le jeu et le &ldquo;je&rdquo; | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun02_fevrier19_lejeu/ping_jeu_0_0_light-768x768.jpg'
    'article:published_time': '2019-02-11T16:31:00+01:00'
    'article:modified_time': '2020-12-09T13:37:53+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-le-jeu-le-je-0) !

![Les doigts dans la prise - Le jeu et le "je"](2%20-%20Les%20doigts%20dans%20la%20prise%20-%20le%20jeu%20et%20le%20je.mp3)

Je pensais devoir parler du je, alors qu’il s’agit de la semaine du jeu. Je vous propose donc de partir du je en montrant comment l’individu est au centre des outils numériques, notamment dans le jeu vidéo.

Une façon de parler de Fortnite, de Facebook, de Google sans les nommer. Prenons donc le jeu vidéo comme exemple d’interface numérique en réseau. Analysons les notions de temps réel, de territoires et surtout de société algorithmique tant qu’on y est.

Le traitement en temps réel par un ordinateur a été défini en 1974 « comme le mode d’exploitation autorisant l’introduction de données utilisateurs à un moment quelconque et l’obtention immédiate des résultats. » Ah mais qu’est-ce que le temps-réel ? Il y aurait un temps non réel ? Non !

Il y a à présent le temps numérique, qui diffère de l’ancien temps. Nous ne sommes plus à l’époque où chaque seconde pousse la roue des minutes. Le temps numérique est un temps simulé, synthétisé à partir de micro-durée. Il n’est plus orienté du passé vers l’avenir, comme le temps chronique du calendrier. Il répète indéfiniment le même micro-instant. Il autorise notamment cette forme d’échange immédiat entre l’homme et l’ordinateur, ou entre l’ordinateur et le monde extérieur, vers un territoire à explorer.

A l’origine de la communication, il y a la ville, son espace-quadrillé et orienté et son temps fléché, mesuré, compté. Ce qui est commun est ce qui se tient à l’intérieur des remparts, dans l’enceinte fortifiée de la ville. Communiquer, c’est d’abord partager un espace commun, retranché du reste.

Les jeux en ligne proposent des territoires à explorer aussi. On peut considérer qu’il y a deux types d’espaces de jeu/je, des espaces progressifs ou des espaces émergents :

- Dans le premier cas, cela s’apparente à un script de cinéma avec quelques embranchements, donnant une impression de liberté comme un puzzle programmé.
- Dans le second cas, les éléments du jeu sont produits en contexte, de manière générative, suivant des algorithmes qui laissent une grande place à l’aléa.

On parle ainsi d’explorateurs. Des explorateurs face aux algorithmes. Ah ! 2017-2018 auront été les années où nous avons cessé de les aimer, comme titrait Wired, pourtant connu pour son côté technophile. Que ce soit sur le débat du rôle joué par Facebook lors de l’élection de Trump ou l’affaire ‘APB’ pour les lycéens, la critique vise les “bulles partisanes” qu’ils engendrent. Sujet de société, nouveaux “mythes modernes”, des entités à la fois insaisissables et toutes-puissantes…

Pourtant, un algorithme est par principe simple à expliquer : ce serait une suite finie et non ambiguë d’opérations ou d’instructions permettant de résoudre un problème ou d’obtenir un résultat. Comme une recette de cuisine. Mais une bonne recette avec un mauvais chef cela ne marche pas. Avec un algorithme oui. Pourtant, c’est la neutralité des algorithmes qui est bien un leurre. En plus, il faut leur donner à manger, c’est-à-dire leur fournir des data, plutôt BIG. Il est vain de vouloir connaître le contenu des algorithmes sans vouloir aborder les données.

Doit-on rendre éthique et transparent ces dispositifs ? Comment les ausculter ?

En ouvrant ces boîtes noires souvent complexes, relevant du secret commercial, ou doit-on voir les algorithmes comme des objets sociaux, de l’extérieur, comme un jeu d’acteurs ?

L’essor des algorithmes personnalisés répond à un désir social d’individualisation très profond : c’est parce que les individus, les joueurs, les explorateurs ne voulaient pas être calculés par des moyennes, des statistiques mais exprimer leur singularité, leur personnalité que les algorithmes ont rencontré ce succès. Nous sommes à présent réduits à nos traces, aux dépens de nos goûts et expressions.

« Le futur de l’internaute est prédit par le passé de ceux qui lui ressemblent ».

C’est la condition nouvelle de l’homme-présent, qui se débat dans une contradiction permanente : à force de nier le temps, il finit par ne voir que son déferlement. C’est pourquoi, au lieu de penser sur le mode de l’espérance, il vit désormais sur le celui de l’urgence. Comment faire en sorte que les hommes-présent que nous sommes ne soient pas pris en compte uniquement en tant qu’agrégats temporaires de données exploitables en masse ? Gouverner n’est pas réductible à cette idée de “reprendre le pouvoir” sur les données, sur les algorithmes.

Peut-on se gouverner hors les normes sans se dissoudre dans les flux ? Dans les jeux ? Voilà quelques-unes des questions fondamentales auxquelles l’homme-présent est confronté avec panique et curiosité dans le jeu du je.

Julien Bellanger
