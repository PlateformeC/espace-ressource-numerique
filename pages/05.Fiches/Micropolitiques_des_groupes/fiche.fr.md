---
title: 'Vercauteren, Micropolitiques des groupes'
media_order: DmKpe63W4AIgk5j.jpeg
type_ressource: text
feature_image: DmKpe63W4AIgk5j.jpeg
license: cc-by-nc-sa
date: '01-01-2019 00:00'
taxonomy:
    category:
        - pratiques
        - recherches
    tag:
        - essai
        - tiers-lieux
        - collectif
    author:
        - 'Meven Marchand Guidevay'
aura:
    pagetype: website
    description: 'Partant du principe qu’« on ne naît pas groupe, on le devient »,  David Vercauteren examine les conditions de possibilité d’un véritable  fonctionnement collectif des collectifs militants.'
    image: DmKpe63W4AIgk5j.jpeg
show_breadcrumbs: true
metadata:
    description: 'Partant du principe qu&rsquo;&laquo; on ne na&icirc;t pas groupe, on le devient &raquo;,  David Vercauteren examine les conditions de possibilit&eacute; d&rsquo;un v&eacute;ritable  fonctionnement collectif des collectifs militants.'
    'og:url': 'https://ressources.pingbase.net/fiches/micropolitiques_des_groupes'
    'og:type': website
    'og:title': 'Vercauteren, Micropolitiques des groupes | Espace Ressources Num&eacute;riques'
    'og:description': 'Partant du principe qu&rsquo;&laquo; on ne na&icirc;t pas groupe, on le devient &raquo;,  David Vercauteren examine les conditions de possibilit&eacute; d&rsquo;un v&eacute;ritable  fonctionnement collectif des collectifs militants.'
    'og:image': 'https://ressources.pingbase.net/fiches/micropolitiques_des_groupes/DmKpe63W4AIgk5j.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1465'
    'og:image:height': '848'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Vercauteren, Micropolitiques des groupes | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Partant du principe qu&rsquo;&laquo; on ne na&icirc;t pas groupe, on le devient &raquo;,  David Vercauteren examine les conditions de possibilit&eacute; d&rsquo;un v&eacute;ritable  fonctionnement collectif des collectifs militants.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/micropolitiques_des_groupes/DmKpe63W4AIgk5j.jpeg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2020-11-26T15:11:15+01:00'
    'article:author': Ping
---

## Résumé :
David Vercauteren est un militant Bruxellois, passé des Verts pour une gauche Alternative (VeGA) au Collectif Sans Nom puis au Collectif Sans Ticket.

Partant du principe qu’*« on ne naît pas groupe, on le devient »*,  David Vercauteren examine les conditions de possibilité d’un véritable  fonctionnement *collectif* des collectifs militants. Il essaye d'élaborer un ensemble d'outils théoriques visant à nourrir l'émergence de nouvelles formes d'organisations politiques *"à distance des habitudes psychologisantes* (ndlr : et individualisantes)*, replis identitaires et autres passions tristes"*. À l'aide d'une *"culture des précédents"*, il expose les impensés récurrents des formations collectives et analyse un ensemble de *"situations problèmes"* afin de donner aux groupes les moyens de s'en prémunir.

## Notes de Meven :

Il est difficile de d'être exhaustif ou ni même concis à propos de ce livre qui est un large abécédaire de situations problèmes, à multiples entrées, d'*"Artifices"* à *"Théorie"* en passant par *"Évaluer"*, *"Parler"*, *"Rôles"*, ou encore *"Soucis de soi"*.

D'après le descriptif des *"mode(s) d'usage(s)"* suivant l'introduction, le lecteur est invité à entrer par le mot-clef qui l'intéresse puis à sauter de texte en texte selon sa recherche, la fin de chaque entrée en proposant d'autres pour la prolonger. Cependant deux circuits de lectures plus "balisés" sont aussi proposés : un *"itinéraires pour un groupe qui se forme"* et un *"itinéraire pour un groupe en crise"*.

Au-delà de ce parcours, le livre est accompagné d'un lexique, mais aussi d'une traduction (relue et corrigée par Isabelle Stengers) des rôles décrits par Starhawk dans *Truth or Dare*. Ces rôles, portraits éthologiques de *"rôles implicites"* ou *"(pré)acquis"* que l'on retrouve plus ou moins dans la plupart des groupes, sont des dispositifs devant permettre de se rendre sensibles à différents types d'aspects de la vie du groupe qui sinon passent à la trappe s'ils ne sont pas explicités. S'ajoutent un certain nombre de fonctions (le guetteur d'ambiance, le médiateur, le coordinateur, etc.) pouvant aider le groupe à résoudre des problèmes auxquels il se sent confronté, permettant à chacun·e d'avoir un aport particulier et reconnu par le groupe, et enfin, aidant chacun·e à prendre des distances avec la manière habituelle qu'il a de faire dans le groupe.

## Citer l'ouvrage :
Vercauteren, D. (2018). Micropolitiques des groupes : Pour une écologie des pratiques collectives. Amsterdam.
