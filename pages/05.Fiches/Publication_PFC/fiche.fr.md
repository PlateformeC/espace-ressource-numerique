---
title: 'PLATEFORME C,  FABLAB CITOYEN ET PÉDAGOGIQUE'
media_order: plateformec.jpeg
type_ressource: fichier
feature_image: plateformec.jpeg
license: cc-by-nc-sa
serie:
    -
        page: /fiches/publication_atelier_partagé
    -
        page: /fiches/publication_slowtech
date: '01-01-2019 00:00'
taxonomy:
    category:
        - recherches
        - pratiques
        - recits
    tag:
        - fablab
        - réparation
        - tiers-lieux
    author:
        - 'Julien Bellanger'
        - 'Charlotte Rautureau'
        - 'Yanaita Araguas'
aura:
    pagetype: article
    description: 'Cet ouvrage revient sur six années d’expérimentation de Plateforme C, un fablab que nous avons voulu à la fois ouvert sur la société civile et sur le monde de l’enseignement.'
    image: plateformec.jpeg
show_breadcrumbs: true
metadata:
    description: 'Cet ouvrage revient sur six ann&eacute;es d&rsquo;exp&eacute;rimentation de Plateforme C, un fablab que nous avons voulu &agrave; la fois ouvert sur la soci&eacute;t&eacute; civile et sur le monde de l&rsquo;enseignement.'
    'og:url': 'https://ressources.pingbase.net/fiches/publication_pfc'
    'og:type': article
    'og:title': 'PLATEFORME C,  FABLAB CITOYEN ET P&Eacute;DAGOGIQUE | PiNG Ressources Num&eacute;riques'
    'og:description': 'Cet ouvrage revient sur six ann&eacute;es d&rsquo;exp&eacute;rimentation de Plateforme C, un fablab que nous avons voulu &agrave; la fois ouvert sur la soci&eacute;t&eacute; civile et sur le monde de l&rsquo;enseignement.'
    'og:image': 'https://ressources.pingbase.net/fiches/publication_pfc/plateformec.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1024'
    'og:image:height': '755'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'PLATEFORME C,  FABLAB CITOYEN ET P&Eacute;DAGOGIQUE | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Cet ouvrage revient sur six ann&eacute;es d&rsquo;exp&eacute;rimentation de Plateforme C, un fablab que nous avons voulu &agrave; la fois ouvert sur la soci&eacute;t&eacute; civile et sur le monde de l&rsquo;enseignement.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/publication_pfc/plateformec.jpeg'
    'article:published_time': '2019-01-01T00:00:00+01:00'
    'article:modified_time': '2021-01-12T12:16:28+01:00'
    'article:author': Ping
---

## Mode d’emploi

Cet ouvrage revient sur six années d’expérimentation de Plateforme C, un fablab que nous avons voulu à la fois ouvert sur la société civile et sur le monde de l’enseignement. Espace d’innovation collaboratif, collectif, citoyen, commun, cet atelier augmenté a été un espace de test, d’expérimentation, de questionnements autour des pratiques numériques, des rapports entre technique et société, de la culture libre, de l’innovation pédagogique, des tiers-lieux numériques de 2013 à 2019.

[TÉLÉCHARGER](https://station.pingbase.net/index.php/s/3MZSfx3QnjZKiHq?classes=btn-down)

[ACHETER](https://ping.odoo.com/shop/product/livre-plateforme-c-fablab-citoyen-et-pedagogique-11?classes=btn)

---
- Textes : Charlotte Rautureau & Julien Bellanger
- Mise en page et illustration : Yanaita Araguas
