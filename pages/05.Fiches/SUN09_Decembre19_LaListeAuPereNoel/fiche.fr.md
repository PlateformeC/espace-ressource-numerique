---
title: 'Les doigts dans la prise #9 : La liste au Père Noël'
type_ressource: audio
feature_image: photo_2019-12-20_15-36-21.jpg
license: cc-by-nc-sa
published: true
date: '16-12-2019 16:31'
taxonomy:
    category:
        - recherches
    tag:
        - radio
        - podcast
        - SUN
        - 'Les doigts dans la prise'
    author:
        - 'Mona Jamois'
aura:
    pagetype: website
    description: 'Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne Le Fil de l’histoire.'
    image: photo_2019-12-20_15-36-21.jpg
metadata:
    description: 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:url': 'https://ressources.pingbase.net/fiches/sun09_decembre19_lalisteauperenoel'
    'og:type': website
    'og:title': 'Les doigts dans la prise #9 : La liste au P&egrave;re No&euml;l | Espace Ressources Num&eacute;riques'
    'og:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'og:image': 'https://ressources.pingbase.net/fiches/sun09_decembre19_lalisteauperenoel/photo_2019-12-20_15-36-21.jpg'
    'og:image:type': image/webp
    'og:image:width': '800'
    'og:image:height': '542'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Les doigts dans la prise #9 : La liste au P&egrave;re No&euml;l | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Tous les mois, l&rsquo;&eacute;quipe de PiNG met les doigts dans la prise et d&eacute;crypte le num&eacute;rique dans le cadre de la quotidienne Le Fil de l&rsquo;histoire.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/sun09_decembre19_lalisteauperenoel/photo_2019-12-20_15-36-21.jpg'
    'article:published_time': '2019-12-16T16:31:00+01:00'
    'article:modified_time': '2020-12-09T12:53:06+01:00'
    'article:author': Ping
---

*Tous les mois, l’équipe de PiNG met les doigts dans la prise et décrypte le numérique dans le cadre de la quotidienne [Le Fil de l’histoire](http://www.lesonunique.com/content/le-fil-lhistoire).*

Pour écouter le podcast c’est par [ici](http://www.lesonunique.com/content/les-doigts-dans-la-prise-la-liste-au-pere-noel) !

![Les doigts dans la prise - La liste au Père Noël](m-2019-12-16-18-30.mp3)

Bonjour à toutes et à tous et bienvenue pour cette nouvelle chronique Les doigts dans la prise, la chronique numérique de l’association PiNG !

Pour cet épisode spécial de Noël, je ne vais pas vous parler de la pollution engendrée par la fabrication et la mise en circulation de milliers d’objets technologiques neufs tout autour du globe, objets qui seront achetés dans un état de panique avancée, trois jours avant le réveillon, pour l’oncle Roger à qui on ne sait plus quoi offrir, objets que l’oncle Roger ne saura probablement pas faire fonctionner et qui finiront un jour ou l’autre sur Le bon coin.

Je ne vais pas non plus vous faire la liste des meilleures applis pour préparer Noël. Entre le compte à rebours interactif avec animations 3D de chute de neige, le simulateur des bruits du père Noël, le Santa Tracker pour savoir où est le Père Noël en ce moment, la ferme du père Noël ; dans laquelle on peut construire des usines de Noël ou même devenir un elfe de Noël, l’éditeur de photos pour incruster votre tête sur des corps en mode Sexy Santa (oui, ça existe), et bien sûr, l’incontournable fond d’écran feu de cheminée full HD. Je m’arrête là.

Non, je ne vais pas vous parler de tout ça car je sais que vous avez besoin d’aide. Parce qu’écrire sa liste au père Noël c’est souvent compliqué, je vous ai dégotté quelques idées, ne me remerciez pas, c’est cadeau.

Alors pour commencer, à mon avis, tout le monde a besoin d’un bon kit de défense numérique. Avec un TV-B gone, un petit outil formidable qui peut éteindre les télés à distance (très utile au bout du quatrième téléfilm de Noël), une clé USB killer qui permet de griller n’importe quel appareil muni d’un port USB, et l’indispensable Guide d’auto-défense numérique, ce sera un bon début.

Ensuite, vu que c’est bien connu le père Noël il peut tout faire, moi je serais vous je demanderais un compte sur WT:social. WT:social c’est un réseau social lancé par Wikipédia il y a quelques semaines, qui est sans pub, sans algorithme et qui n’utilise pas nos données personnelles. Évidemment, avec des arguments pareil la plateforme a envoyé tellement de paillettes qu’elle a été submergée de demandes, résultat : les demandes inscriptions sont pour l’instant sur liste d’attente.

Côté vidéo, je ne peux que vous recommander l’excellent documentaire Internet ou la révolution du partage : vous pourrez ainsi organiser votre propre soirée ciné-débat chez vous, pour sensibiliser tout votre entourage à la culture libre. Un bon vidéo-projecteur et du pop-corn : succès garanti.

Si vous aimez un peu la musique et tripoter des boutons, vous pouvez ajouter les yeux fermés n’importe quel synthé modulaire vendu par Bastl instruments. Attention, là je dois quand même vous avertir d’un risque d’addiction assez important.

Et pour finir, je pense que vous ne pouvez pas vous passer d’une Raspberry Pi. Comme son nom ne l’indique pas une Raspberry Pi c’est un nano-ordinateur qui peut être programmé pour faire.. et bien un peu ce qu’on veut!Par exemple créer un petit réseau de partage de fichier en wifi (une sorte d’AMAP de l’internet quoi), ou bien piloter vos guirlandes électroniques de Noël, ou encore fabriquer une station météo. Et coïncidence incroyable : nous organisons justement un atelier Raspberry Pi ce jeudi à partir de 16h à l’Atelier Partagé du Breil !

Bon voilà, je crois que vous avez de quoi faire. Vous pourrez retrouvez tous les liens de cette liste au Père Noël sous le podcast de cette chronique sur les sites de SUN et de PiNG.
À l’année prochaine pour de nouvelles aventures sur les sentiers numériques !

– Tv b gone : https://www.tvbgone.com/
– Clé usb killer : https://fr.usbkill.com/
– Guide d’auto-défense numérique : https://guide.boum.org/
– WT:Social : https://wt.social/
– Internet ou la révolution du partage : https://boutique.arte.tv/detail/internet_ou_la_revolution_du_partage
– Bastl instruments : https://bastl-instruments.com/
– Raspberry Pi : https://www.raspberrypi.org/
