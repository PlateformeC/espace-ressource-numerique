---
title: 'Lexique'
media_order: IMG_1270.JPG
date: '13-12-2020 00:00'
taxonomy:
    category:
    - pratiques
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'lexique'
    - 'fablab'

    author:
    - 'Yanaïta Araguas'
    aura:
    pagetype: article
    description: ‘Lexique technique utilisé au fablab dans le cadre du projet Papier/Machine’
    image: IMG_1270.JPG

---

## Lexique à usage des non-initié·es

* **Découpeuse laser**: la découpeuse laser est un procédé de fabrication qui consiste à découper la matière grâce à une grande quantité d’énergie générée par un laser et concentrée sur une très faible surface. Cette technologie est majoritairement destinée aux chaînes de production industrielles mais peut également convenir aux boutiques, aux établissements professionnels et aux tiers-lieux de fabrication.
[Plus d'infos sur la découpeuse laser - wikipédia] https://fr.wikipedia.org/wiki/Découpe_laser

À retrouver dans les articles :
- Expérimentations autour de la cire
- Peaux d'oranges et d'avocats tatouées au laser
- Découpeuse laser et plexi frost

* **Embossage** : l’embossage est une technique qui a pour objectif de créer des formes en relief dans du papier ou un autre matériau déformable. L’embossage est la version artisanale de l’emboutissage, qui s’applique le plus souvent à de la tôle.
[Plus d'infos sur l'embossage - wikipédia](https://fr.wikipedia.org/wiki/Embossage)

À retrouver dans les articles :
- Produire du texte à la découpeuse vinyle

* **Fraiseuse** : une fraiseuse est une machine-outil utilisée pour usiner tous types de pièces mécaniques, à l'unité ou en série, par enlèvement de matière à partir de blocs ou parfois d'ébauches estampées ou moulées, à l'aide d'un outil coupant nommé fraise. En-dehors de cet outil qui lui a donné son nom, une fraiseuse peut également être équipée de foret, de taraud ou d'alésoir.
[Plus d'infos sur la fraiseuse - wikipédia](https://fr.wikipedia.org/wiki/Fraiseuse)

À retrouver dans les articles :
- Gravure sur plexiglass pour éclairage led
- Découpeuse laser et plexi frost

* **Papier couché** : le papier ou carton couché est un papier ou carton dont la surface est recouverte d'une ou plusieurs couches généralement constituées de produits minéraux (pigments) en mélange avec des liants et des produits d'addition divers.
Quelque 40 % des papiers impression-écriture et près de 50 % des cartons sont couchés. L'opération de couchage consiste à déposer sur une ou sur les deux faces d'une feuille de papier ou de carton — appelée support — un enduit à base de pigments fins. Le but de cette opération est de permettre une meilleure reproduction des impressions en transformant la surface rugueuse et macroporeuse du papier en une face unie et microporeuse et d'améliorer la blancheur du papier ou du carton, son aspect (brillant, par exemple), son toucher.
[Tout savoir sur le papier couché - wikipédia](https://fr.wikipedia.org/wiki/Papier_couché)

À retrouver dans les articles :
- Produire du texte à la découpeuse vinyle

* **Plotter** : à la base, un plotter est une machine qui trace des lignes ("to plot" en anglais veut dire tracer), à la différence d'une imprimante dont le travail va être de couvrir toute une surface avec de l'encre. À ce titre, les plotters sont typiquement utilisés pour imprimer les patrons de couture, les schémas techniques en ingénierie ou en architecture. 
Un plotter fonctionne avec une tête qui vient se promener sur le matériau, tête qui dispensera de l'encre, mais qui pourra également être dotée d'une lame afin de découper le matériau choisi.

[Tout savoir sur le plotter de découpe - www.makerist.fr](https://www.makerist.fr/topics/Tout-savoir-sur-le-plotter-de-decoupe)

À retrouver dans les articles :
- Produire du texte à la découpeuse vinyle

* **Raster** : le raster est un mode d'action que propose la découpeuse laser. On fournit à la machine un fichier image en niveaux de gris (noir et blanc).Le laser va balayer la surface de la zone à usiner (correspondant à la taille de l'image fournie) et sa puissance varie en fonction de la valeur des noirs et des blancs de l'image traitée, produisant ainsi des nuances sur le matériau (plus ou moins 'cramé')

À retrouver dans les articles :
- Expérimentation autour de la cire
- Peaux d'oranges et d'avocats tatouées au laser
