---
title: 'Peaux d''oranges et d''avocats tatouées à la découpeuse laser'
type_ressource: text
feature_image: gravure-laser-sur-peaux-3.jpg
license: cc-by-nc-sa
date: '01-12-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - 'papier machine'
        - 'carte postale'
        - 'Expérimentation matériaux'
        - workshop
        - 'decoupeuse laser'
        - 'decoupe laser'
        - gravure
        - Papier/machine
        - 'peau orange'
        - avocat
    author:
        - 'Yanaïta Araguas'
aura:
    pagetype: article
    description: 'Graver des peaux d''oranges et d''avocats à la laser? Retours sur ces expérimentations inédites'
    image: gravure-laser-sur-peaux-3.jpg
metadata:
    description: 'Graver des peaux d''oranges et d''avocats &agrave; la laser? Retours sur ces exp&eacute;rimentations in&eacute;dites'
    'og:url': 'https://ressources.pingbase.net/fiches/peaux-d-orange-et-peau-d-avocats-tatouees-a-la-decoupeuse-laser'
    'og:type': article
    'og:title': 'Peaux d''oranges et d''avocats tatou&eacute;es &agrave; la d&eacute;coupeuse laser | PiNG Ressources Num&eacute;riques'
    'og:description': 'Graver des peaux d''oranges et d''avocats &agrave; la laser? Retours sur ces exp&eacute;rimentations in&eacute;dites'
    'og:image': 'https://ressources.pingbase.net/fiches/peaux-d-orange-et-peau-d-avocats-tatouees-a-la-decoupeuse-laser/gravure-laser-sur-peaux-3.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '984'
    'og:image:height': '654'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Peaux d''oranges et d''avocats tatou&eacute;es &agrave; la d&eacute;coupeuse laser | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Graver des peaux d''oranges et d''avocats &agrave; la laser? Retours sur ces exp&eacute;rimentations in&eacute;dites'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/peaux-d-orange-et-peau-d-avocats-tatouees-a-la-decoupeuse-laser/gravure-laser-sur-peaux-3.jpg'
    'article:published_time': '2019-12-01T00:00:00+01:00'
    'article:modified_time': '2021-01-12T16:53:03+01:00'
    'article:author': Ping
---

##  Peaux d'oranges et d'avocats tatouées au laser

![gravure-laser-sur-peaux.jpg](gravure-laser-sur-peaux.jpg?forceresize=100%)

Le projet d'exploration de PiNG Papier/Machine a accueilli 22 étudiant·es de l’École des Beaux-Arts de Nantes pour expérimenter les potentiels de création graphique du fablab, autour de la série et de la carte postale.

Voici l'une des expérimentations réalisées.

Une élève est arrivée avec ce projet un peu fou de tatouer une date sur des peaux de fruits ou de légumes. Ça tombait bien, nous nourrissions depuis longtemps déjà le fantasme d'essayer des trucs avec de la nourriture.

Ici des tests sur de la peau d'avocat séchée (raster) et de la peau de mandarine fraîche (gravure).
L'enjeu est d'arriver à maintenir à plat les peaux afin d'obtenir une gravure sur laquelle le texte ne soit pas trop déformé. Cela fonctionne bien, tant en raster qu'en gravure simple. On peut cependant remarquer que la peau sèche se prête mieux au traitement.

On peut entrevoir de nouvelles possibilités.

![gravure-laser-sur-peaux-1.jpg](gravure-laser-sur-peaux-1.jpg?forceresize=100%)
![gravure-laser-sur-peaux-2.jpg](gravure-laser-sur-peaux-2.jpg?forceresize=100%)
![gravure-laser-sur-peaux-3 copie.jpg](gravure-laser-sur-peaux-3 copie.jpg?forceresize=100%)
![gravure-laser-sur-peaux-4.jpg.jpg](gravure-laser-sur-peaux-4.jpg?forceresize=100%)


### Autres ressources
Matériaux utilisés :
Peau d'orange fraîche
Peau d'avocat séchée

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx

```