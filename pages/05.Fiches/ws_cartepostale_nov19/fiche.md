---
title: 'Workshop autour de la carte postale'
media_order:
type_ressource: text   
feature_image: IMG_0110.JPG
license: cc-by-nc-sa
serie:
date: '10-11-2019 00:00'
taxonomy:
    category:
    - recits
    tag:
    - 'papier machine'
    - 'Papier/Machine'
    - 'micro-édition'
    - 'carte postale'
    - 'workshop'
    author:
    - 'Charlotte Rautureau'  
aura:
  pagetype: article
  description: 'Telle une aparté dans le thème du livre-objet, nous avons accueilli 22 étudiant·es de l’École des Beaux-Arts de Nantes pour expérimenter les potentiels de création graphique du fablab, autour de la série et de la carte postale. Un premier temps de découverte réciproque, riche en perspectives pour les années à venir.'
  image: IMG_0110.JPG

---
Telle une aparté dans le thème du livre-objet, nous avons accueilli du 4 au 7 novembre 19, 22 étudiant·es de l’École des Beaux-Arts de Nantes pour expérimenter les potentiels de création graphique du fablab, autour de la série et de la carte postale. Un premier temps de découverte réciproque, riche en perspectives pour les années à venir, dont voici le retour en images.

![IMG_0150.JPG](IMG_0150.JPG?forceresize=100%)
![IMG_0115.JPG](IMG_0115.JPG?forceresize=100%)
![IMG_0121.JPG](IMG_0121.JPG?forceresize=100%)
![IMG_0107.JPG](IMG_0107.JPG?forceresize=100%)
![IMG_0131.JPG](IMG_0131.JPG?forceresize=100%)
![IMG_0135.JPG](IMG_0135.JPG?forceresize=100%)
![IMG_0141.JPG](IMG_0141.JPG?forceresize=100%)
![IMG_0104.JPG](IMG_0104.JPG?forceresize=100%)
![IMG_0061.JPG](IMG_0061.JPG?forceresize=100%)
![IMG_0099.JPG](IMG_0099.JPG?forceresize=100%)
