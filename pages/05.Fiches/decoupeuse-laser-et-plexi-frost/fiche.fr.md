---
title: 'Découpeuse laser et plexi frost'
media_order: plexi-frost-cailloux-2.jpg
type_ressource: text
license: none
date: '01-12-2019 00:00'
taxonomy:
    category:
        - pratiques
    tag:
        - 'papier machine'
        - Papier/Machine
        - 'carte postale'
        - 'Expérimentation matériaux'
        - workshop
        - papier
        - 'decoupeuse laser'
        - 'decoupe laser'
        - gravure
        - plexifrost
        - plexiglass
    author:
        - 'Yanaïta Araguas'
aura:
    pagetype: article
    description: 'Le plexi frost, un matériau intéressant avec la découpeuse laser. Quelques expérimentations menées à Plateforme C.'
    image: plexi-frost-cailloux-2.jpg
metadata:
    description: 'Le plexi frost, un mat&eacute;riau int&eacute;ressant avec la d&eacute;coupeuse laser. Quelques exp&eacute;rimentations men&eacute;es &agrave; Plateforme C.'
    'og:url': 'https://ressources.pingbase.net/fiches/decoupeuse-laser-et-plexi-frost'
    'og:type': article
    'og:title': 'D&eacute;coupeuse laser et plexi frost | PiNG Ressources Num&eacute;riques'
    'og:description': 'Le plexi frost, un mat&eacute;riau int&eacute;ressant avec la d&eacute;coupeuse laser. Quelques exp&eacute;rimentations men&eacute;es &agrave; Plateforme C.'
    'og:image': 'https://ressources.pingbase.net/fiches/decoupeuse-laser-et-plexi-frost/plexi-frost-cailloux-2.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '984'
    'og:image:height': '654'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'D&eacute;coupeuse laser et plexi frost | PiNG Ressources Num&eacute;riques'
    'twitter:description': 'Le plexi frost, un mat&eacute;riau int&eacute;ressant avec la d&eacute;coupeuse laser. Quelques exp&eacute;rimentations men&eacute;es &agrave; Plateforme C.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/decoupeuse-laser-et-plexi-frost/plexi-frost-cailloux-2.jpg'
    'article:published_time': '2019-12-01T00:00:00+01:00'
    'article:modified_time': '2021-01-12T13:56:31+01:00'
    'article:author': Ping
---

## Découpeuse laser et plexi Frost

![plexi-frost-cailloux-2.jpg](plexi-frost-cailloux-2.jpg?forceresize=100%)

Le projet d'exploration Papier/Machine a accueilli, en novembre 19, 22 étudiant·es de l’École des Beaux-Arts Nantes St Nazaire pour expérimenter les potentiels de création graphique du fablab autour de la série et de la carte postale.

Ici, nous parlons d'un matériau assez séduisant, le plexi Frost.
Plusieurs élèves ont été séduit.e.s par ses qualités plastiques (il ressemble au verre dépoli et, bien que très différent au toucher, il en présente certaines caractéristiques).
Il se découpe parfaitement à la découpeuse laser*. Il faut aller sur des réglages avec des vitesses lentes et une puissance pas trop forte.
Nous conseillons de faire un tout petit test de découpe/gravure (par ex. une petite croix gravée dans une découpe circulaire de 10 mm de diamètre pour éviter de gâcher du plexi qui coute quand même "une blinde" !)
Les découpes sont nettes et soyeuses. Le plexi fond très légèrement pendant l'exécution ce qui adoucit un peu le tranchant sans le déformer.
Le trait de gravure ou de découpe fait légèrement vibrer la couleur du plexi, produisant ainsi une bordure plus claire ce qui est assez intéressant.

Ici les travaux de deux élèves : l'une associe le plexi frost avec du fil, un travail très sensible sur le désir. L'autre joue avec les contrastes entre deux matériaux et procède à un échange subtil.

![plexi-frost-cailloux-1.jpg](plexi-frost-cailloux-1.jpg?forceresize=100%)
![plexi-frost-cailloux-2.jpg](plexi-frost-cailloux-2.jpg?forceresize=100%)
![plexi-frost-cailloux-3.jpg](plexi-frost-cailloux-3.jpg?forceresize=100%)
![plexi-frost-cailloux-4.jpg](plexi-frost-cailloux-4.jpg?forceresize=100%)
![plexi-frost-decoupe-mains1.jpg](plexi-frost-decoupe-mains1.jpg?forceresize=100%)
![plexi-frost-decoupe-mains2.jpg](plexi-frost-decoupe-mains2.jpg?forceresize=100%)

* La découpeuse laser est un procédé de fabrication qui consiste à découper la matière grâce à une grande quantité d’énergie générée par un laser et concentrée sur une très faible surface. Cette technologie est majoritairement destinée aux chaînes de production industrielles mais peut également convenir aux boutiques, aux établissements professionnels et aux tiers-lieux de fabrication.
https://fr.wikipedia.org/wiki/Découpe_laser


### Autres ressources
Matériaux utilisés :
Plexiglass PERSPEX FROST 5mm (VINK NANTES)
[lien vers le catalogue Vink](https://vink.fr/wp-content/uploads/2017/08/VINK-Interieur-ilovepdf-compressed.pdf)
Carton gris d'encadrement 5mm
[un ex au Géant des beaux-arts](https://www.geant-beaux-arts.fr/carton-gris-epais.html)

**▸ Documentation technique**

Retrouvez l’ensemble de la documentation sur le workshop ici:
xxxxxxx
[www.fablabo.net/wiki/Papier/Machine](http://fablabo.net/wiki/Papier/Machine)
xxxxxxx

```