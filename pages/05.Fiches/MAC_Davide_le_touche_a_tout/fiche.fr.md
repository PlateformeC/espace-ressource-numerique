---
title: '[MAKER À COEUR] De l''open-data aux matériaux de récup'' : Davide, le touche à tout'
media_order: IMG_1082_light-870x550.jpg
type_ressource: text
feature_image: IMG_1082_light-870x550.jpg
license: cc-by-nc-sa
date: '01-06-2017 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Davide Usai nous présente ses travaux au fablab Plateforme C.'
    image: IMG_1082_light-870x550.jpg
metadata:
    description: 'Davide Usai nous pr&eacute;sente ses travaux au fablab Plateforme C.'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_davide_le_touche_a_tout'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] De l''open-data aux mat&eacute;riaux de r&eacute;cup'' : Davide, le touche &agrave; tout | Espace Ressources Num&eacute;riques'
    'og:description': 'Davide Usai nous pr&eacute;sente ses travaux au fablab Plateforme C.'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_davide_le_touche_a_tout/IMG_1082_light-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] De l''open-data aux mat&eacute;riaux de r&eacute;cup'' : Davide, le touche &agrave; tout | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Davide Usai nous pr&eacute;sente ses travaux au fablab Plateforme C.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_davide_le_touche_a_tout/IMG_1082_light-870x550.jpg'
    'article:published_time': '2017-06-01T00:00:00+02:00'
    'article:modified_time': '2020-11-26T14:50:49+01:00'
    'article:author': Ping
---

**Bonjour Davide ! Qui es-tu ?**

Davide Usai, italien. J’ai quitté l’Italie en 2004 et j’ai voyagé pour les études : école d’ingénieur en Allemagne, un master au Brésil, puis je me suis installé en France en 2008.
Je suis ingénieur en électronique à la base et depuis 2008 je me suis reconverti en informatique. L’électronique c’était une passion mais je gagne pas ma vie avec ça. Je travaille pour une agence française dans l’informatique médicale. L’agence est sur Paris et je suis en télé-travail depuis 2011.
J’habite sur l’île de Nantes, sur la ligne C5 donc c’est génial ! En 10 minutes j’arrive au fablab !

**Avant de découvrir Plateforme C, connaissais-tu les fablabs ?**

Ce n’est pas un hasard que je sois arrivé ici : quand j’avais 7 ans, en 1987, je suis tombé dans une sorte de fablab. Mon papa était pilote d’avion et il était passionné par la construction et l’informatique. On avait monté une petite usine de construction d’avion, il y avait de l’espace, des machines, des outils…  On s’est dit qu’on pourrait partager tout ça avec d’autres ! Donc mon père a imaginé un système de location mutualisé : qui voulait pouvait louer cet espace avec les machines. C’était génial parce que c’était sur le même terrain que la maison de mes parents, et jusqu’à l’âge de 21 ans, j’ai participé à tout ce qui a été construit par la suite par des personnes que je ne connaissais même pas forcément ! Donc quand j’ai découvert le principe du fablab, ça me parlait parce que je connaissais déjà l’idée du partage des outils.

**Tu viens régulièrement au fablab : as-tu des projets en particulier sur lesquels tu travailles ?**

Aujourd’hui comme je suis en télé-travail, j’ai une certaine liberté dans mes horaires. Donc le jeudi après-midi j’ai pris l’habitude de venir ici depuis deux ans. Je n’ai pas de projet spécifique que je réalise forcément, mais c’est un vrai lieu de rencontre. Par exemple, au dernier apéro|projets j’ai présenté un projet que j’ai fait avec des copains en 2015 autour de l’open data dans le domaine viticole : cela permet de faire connaître encore plus le projet. Après l’apéro|projets plusieurs personnes sont venues me voir pour en parler, et c’était l’objectif ! Ça crée des liens et ça fait vivre le projet [OpenWines](http://openwines.eu/fr/).
Récemment j’ai réalisé un autre projet. Avec l’association des parents d’élèves de l’école Sainte Madeleine à République, on organise une kermesse une fois par an. Cette année, pour la dynamiser et y mettre du nouveau, on s’est lancés dans la construction d’un jeu de bois pour renouveler le stock. Les jeux changent peu, donc on s’est dit qu’on en ferait un nouveau et si ça plaît on en fera un par an. Ça nous permettrait de faire des rencontres régulières au cours de l’année, rencontrer les nouveaux parents et les encourager à prendre la suite. On a conçu ce jeu avec deux personnes extraordinaires : Hélène, qui est architecte et Thierry qui est ingénieur en mécanique !

![IMG_4107-e1499174416997.jpg](IMG_4107-e1499174416997.jpg)

**Tu peux nous parler un peu de ce jeu?**

C’est le jeu du plan incliné avec une bille : c’est un labyrinthe. Au début on voulait représenter l’Île de Nantes pour y mettre l’école, le fablab… mais nous n’avons pas pu à cause de problèmes de proportions de l’échelle : l’Île de Nantes elle est très longue mais pas très large. On s’est dit que la prochaine fois on ferait la Bretagne étant donné que l’école a une filière multilingue dont le Breton. Donc dans le jeu, les tasseaux représentent les quartiers, on a une bille de raquette de plage qui fait 4 cm et les boulevards font 8cm.
Ça a tellement cartonné durant la kermesse que tout le monde nous l’a demandé : une fête de quartier pas loin du fablab, une fête à Bouaye, la fête de l’île de Nantes le 24 septembre prochain.. Et le jeu sera aussi présenté à [Festival D](http://festivald.net/) à Angers ! À terme, on le donnera en cadeau à la [Maison des Jeux](http://maisondesjeux-nantes.org/).
Il faut préciser que la fabrication de ce jeu n’a rien coûtée : on a réussit à construire ce jeu, qui fait un 1m30 par 80cm, sans mettre un seul euro de notre poche, juste avec des matériaux de récupération ! C’était quelque chose qui me tenait à cœur : je ne voulais que les personnes à l’autre bout de la France qui iront voit la documentation sur [Fablabo](http://fablabo.net/) soient découragées par le prix de la construction du jeu. Là, au contraire je vais même donner des pistes dans la documentation pour savoir où récupérer du matériel

**Te souviens-tu de la première fois que tu es arrivé à Plateforme C ?**

Oui c’était il y a deux ans, j’ai découvert le lieu lors d’une visite hebdomadaire et le concept m’a vraiment plu. Je ne suis pas du tout attiré par les machines à commandes numériques (j’en ai utilisé plein lors de mon parcours universitaire et professionnel), mais plutôt par le lieu, les valeurs, l’ambiance, les personnes que l’on rencontre : cela va au delà de la technicité des outils ! Ici personnellement je n’utilise que la table, l’espace électronique (je me fais une pause réparation d’électroménager par semaine) et le thé !

**As-tu des idées de projets pour la suite ?**

Je fais du tirage photo en argentique et je sais qu’il y a deux autres passionnés à Plateforme C donc je me dis que je pourrais proposer cela l’année prochaine au fablab. En effet, il y a déjà ici tout le matériel pour développer des photos : un frigo, un évier, etc.
