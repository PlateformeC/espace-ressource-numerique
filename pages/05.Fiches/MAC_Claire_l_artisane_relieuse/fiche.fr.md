---
title: '[MAKER À COEUR] Claire, l''artisane relieuse'
media_order: IMG_0169-870x550.jpg
type_ressource: text
feature_image: IMG_0169-870x550.jpg
license: cc-by-nc-sa
date: '01-03-2018 00:00'
taxonomy:
    category:
        - recits
    tag:
        - 'maker à coeur'
    author:
        - PiNG
aura:
    pagetype: website
    description: 'Claire l''artisane relieuse, répond à quelques unes de nos questions au fablab Plateforme C.'
    image: IMG_0169-870x550.jpg
anchors:
    active: false
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_breadcrumbs: true
content:
    items:
        - '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    description: 'Claire l''artisane relieuse, r&eacute;pond &agrave; quelques unes de nos questions au fablab Plateforme C.'
    'og:url': 'https://ressources.pingbase.net/fiches/mac_claire_l_artisane_relieuse'
    'og:type': website
    'og:title': '[MAKER &Agrave; COEUR] Claire, l''artisane relieuse | Espace Ressources Num&eacute;riques'
    'og:description': 'Claire l''artisane relieuse, r&eacute;pond &agrave; quelques unes de nos questions au fablab Plateforme C.'
    'og:image': 'https://ressources.pingbase.net/fiches/mac_claire_l_artisane_relieuse/IMG_0169-870x550.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '870'
    'og:image:height': '550'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '[MAKER &Agrave; COEUR] Claire, l''artisane relieuse | Espace Ressources Num&eacute;riques'
    'twitter:description': 'Claire l''artisane relieuse, r&eacute;pond &agrave; quelques unes de nos questions au fablab Plateforme C.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/fiches/mac_claire_l_artisane_relieuse/IMG_0169-870x550.jpg'
    'article:published_time': '2018-03-01T00:00:00+01:00'
    'article:modified_time': '2020-11-24T15:04:04+01:00'
    'article:author': Ping
---

**Bonjour Claire, peux-tu te présenter ?**

Je m’appelle Claire Masfrand et je me suis lancée dans un projet de reconversion professionnelle suite à mon précédent emploi dans une boite d’édition de logiciels. J’ai commencé ma carrière en tant que développeuse et j’ai évolué dans mon entreprise en allant sur des postes de consultante ou d’avant-vente, donc, avec un un peu plus de rédactionnel. J’ai changé complètement de travail parce que j’avais envie d’un métier plus manuel dans lequel je puisse faire un peu plus ce dont j’avais envie : je me suis donc orientée vers la reliure en 2016.

J’ai suivi un CAP Arts de la Reliure / Dorure au Musée de l’imprimerie à Nantes, un jour par semaine, tout en travaillant. J’ai eu mon diplôme et suit donc artisane relieuse.

**Est-ce que c’était le désir de changer de métier ou d’être artisane qui t’a amené à cette reconversion ?**

Ce qui m’a vraiment poussé au départ c’était l’envie de faire autre chose. C’est ma manière de fonctionner : dans l’entreprise où j’étais avant j’ai eu trois postes différents en 8 ans et j’avais à peu près fait le tour de ce que l’on pouvait m’y proposer. On change aussi avec l’âge, je suis dans un désir de choses un peu plus lentes et moins dans la rapidité immédiate. Le fait d’avoir un travail manuel c’est permettre un temps de pratique qui amène, forcément, un temps de réflexion. Et également le sens du toucher, avoir le matériau dans les mains, créer et bouger son corps : c’est un désir de ne pas être statique.

**Comment est-ce que tu as découvert Plateforme C ?**

Je connaissais de nom. Je savais qu’il y avait des fablabs sur l’île de Nantes, et également par le biais de mon époux qui avait participé à Muséomix, événement sur lequel PiNG était présent. J’ai donc découvert Plateforme C comme ça. Je me suis vraiment renseigné au moment où j’ai voulu commencer à créer avec une découpe laser : où est-ce que je peux en trouver une ? Est-ce que je peux l’acheter ? Et, finalement, ce n’était pas valable niveau coût vis à vis de l’usage que j’en ai soit beaucoup de prototypage et pas un désir de faire de la série de masse. De ce fait, l’abonnement à Plateforme C me convient bien !

**Est-ce que tu te souviens de la première fois où tu es venue à Plateforme C ?**

Oui ! C’était pour prendre des informations (rire). La fois où je suis entrée et restée c’était pour une initiation découpe laser en novembre 2016 car, pendant ma formation en reliure, je m’étais dis que j’allais essayer de voir les croisements possibles. Finalement, passer un jour par semaine sur cette formation et avoir le diplôme était déjà un beau challenge et je ne pouvais pas en plus travailler sur du prototypage, ça aurait été trop lourd.

Du coup j’ai fait cette initiation mais je n’ai pas du tout pratiqué pendant un moment et j’ai refais une initiation découpe laser un an après parce que, évidement, j’avais tout oublié en ne pratiquant pas.

**Alors, maintenant que tu as ton diplôme, tu as pu faire du prototypage à Plateforme C ?**

Oui ! En novembre 2017 j’ai pris un abonnement un mois et j’ai pu en faire pas mal. Je me suis attardée sur la découpe laser mais on est toujours un peu trop optimiste et je me suis rendu compte assez rapidement que ça allait prendre plus de temps que ce que j’avais prévu. Il faut que je m’y mette avec plus de méthodologie en travaillant matériau par matériau pour tester toutes les possibilités de gravures, de vitesses, de puissances …

Mais ce qui était intéressant c’est que je suis parti par la suite sur des projets beaucoup plus simples comme, par exemple, des petits carnets pour Noël en récupérant des plaques de contreplaqué de Station-Service. J’ai fait deux marchés de Noël durant lesquels j’ai pu vendre ces carnets, et j’ai pu en profiter pour parler avec les personnes que je croisais de Plateforme C, des matériaux, de ce qu’on peut faire avec la découpe laser, etc.

**Ça permettait d’expliquer le chemin de création des objets qu’ils avaient directement sous les yeux ?**

Oui ! Je trouvais ça intéressant parce que je l’avais fait là-bas [à Plateforme C], c’était donc intéressant d’en parler, et de toute façon c’est imprimé dans le matériau lui-même : c’est brûlé, ça se voit et ça se sent donc le désir de création amène l’idée que l’on puisse expliquer le procédé de création. D’ailleurs, les gens aiment bien acheter des choses mais ils aiment surtout savoir comment ça a été fait.

**Pour conclure, quels sont tes projets à Plateforme C ?**

Et bien, là, j’aimerais bien y aller tout le temps ! Je vais reprendre un abonnement et, comme je le disais plus haut, travailler de manière un peu plus méthodologique : refaire des tests sur les cartons, les bois. En ce moment, je veux faire de la courbure, je veux que les matériaux soient solides sur les plats du livre [couvertures avant et arrière] et pouvoir faire en sorte que le dos se torde tout en restant solide. C’est là qu’est tout le problème : on doit pouvoir l’ouvrir plusieurs fois sans qu’il ne casse, et en même temps s’il est trop souple les plats seront souple comme du cuir et ça n’est pas l’idée. Je me suis rendu compte que c’était beaucoup de petites problématiques techniques, c’est là où ce n’était pas la bonne méthode que de vouloir les aborder toutes d’un coup, plutôt que par étapes.

Le problème c’est que je me disperse très facilement : on me parle de broderie et j’imagine comment l’utiliser pour recouvrir un livre de tissu avec la brodeuse numérique,. C’est la même chose pour les imprimantes 3D : j’en ai parlé avec l’équipe d’adhérents qui les présentaient à Festival D  et ils m’expliquaient, toujours avec cette dualité souple/rigide, qu’il existe des matériaux plastiques flexibles qui se tordent tout en restant solides que l’ont peut utiliser via une imprimante 3D. Pour résumer, pleins de projets pour 2018 à Plateforme C !
