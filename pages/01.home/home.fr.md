---
title: Ressources
media_order: ressources-min.jpeg
feature1: /home
feature2: /fiches/lieux-numeriques-entre-pratiques-populaires-et-re-appropriation-des-technologies
feature3: '/fiches/sun14_octobre20_instagram et censure des corps'
feature4: /fiches/publication_slowtech
feature5: /fiches/brodeuse_numerique_brothernv800e
feature6: /home
feature7: /fiches/publication_pfc
feature8: /fiches/archives_radio
feature9: /fiches/culture_libre_et_propriete_industrielle
feature10: /fiches/manifeste_pour_la_culture_numerique
feature11: /fiches/sun13_septembre20_aidants_numeriques
feature12: /fiches/mac_romain_le_mini_maker
body_classes: 'title-h1h2 header-dark header-transparent'
menu: Home
aura:
    pagetype: website
    description: 'Bienvenue sur le site ressource de l''association PiNG ! Vous y trouverez toute la documentation autour des projets de l’association : articles, photos, podcasts, vidéos, veille, etc.'
    image: ressources-min.jpeg
onpage_menu: false
content:
    items:
        -
            '@page.descendants': /fiches
    limit: 15
    order:
        by: date
        dir: desc
    pagination: true
recherches:
    items:
        -
            '@taxonomy.category': recherches
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
pratiques:
    items:
        -
            '@taxonomy.category': pratiques
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
metadata:
    description: 'Bienvenue sur le site ressource de l''association PiNG ! Vous y trouverez toute la documentation autour des projets de l&rsquo;association : articles, photos, podcasts, vid&eacute;os, veille, etc.'
    'og:url': 'https://ressources.pingbase.net/home'
    'og:type': website
    'og:title': 'Ressources | PING Ressources Num&eacute;riques'
    'og:description': 'Bienvenue sur le site ressource de l''association PiNG ! Vous y trouverez toute la documentation autour des projets de l&rsquo;association : articles, photos, podcasts, vid&eacute;os, veille, etc.'
    'og:image': 'https://ressources.pingbase.net/home/ressources-min.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '4096'
    'og:image:height': '2731'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Ressources | PING Ressources Num&eacute;riques'
    'twitter:description': 'Bienvenue sur le site ressource de l''association PiNG ! Vous y trouverez toute la documentation autour des projets de l&rsquo;association : articles, photos, podcasts, vid&eacute;os, veille, etc.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/home/ressources-min.jpeg'
    'article:published_time': '2021-01-12T10:08:44+01:00'
    'article:modified_time': '2021-01-12T10:08:44+01:00'
    'article:author': Ping
---

