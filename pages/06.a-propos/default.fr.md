---
title: 'À propos'
media_order: 1536-1024.jpg
aura:
    pagetype: website
    description: 'À propos de l''association PiNG et de son espace ressource.'
    image: 1536-1024.jpg
metadata:
    description: '&Agrave; propos de l''association PiNG et de son espace ressource.'
    'og:url': 'https://ressources.pingbase.net/a-propos'
    'og:type': website
    'og:title': '&Agrave; propos | PiNG Ressources Num&eacute;riques'
    'og:description': '&Agrave; propos de l''association PiNG et de son espace ressource.'
    'og:image': 'https://ressources.pingbase.net/a-propos/1536-1024.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1536'
    'og:image:height': '1024'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': '&Agrave; propos | PiNG Ressources Num&eacute;riques'
    'twitter:description': '&Agrave; propos de l''association PiNG et de son espace ressource.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/a-propos/1536-1024.jpg'
    'article:published_time': '2021-01-12T10:18:36+01:00'
    'article:modified_time': '2021-01-12T10:18:36+01:00'
    'article:author': Ping
---

# À propos

**PiNG est une association nantaise qui s’active depuis 2004. Sa mission ? Questionner le monde numérique dans lequel nous vivons et l’explorer ensemble avec la tête et les deux mains !**
**Ce site est le site ressource de l'association. Vous y trouverez toute la documentation autour des projets de PiNG : articles, documents, podcasts, vidéos, veille, etc.**

Comment le numérique transforme-t-il notre société ? Quel est son impact sur notre environnement, notre manière de vivre, de penser, de créer ? Comment favoriser, dans cet univers parfois opaque, la libre circulation des idées et des savoirs-faire ? 
Que vous soyez citoyen curieux ou professionnel averti, PiNG vous propose de partager ces questionnements lors de temps de découverte, de pratique et d’échange, pour se ré-approprier ensemble les technologies qui nous entourent.

*Rendez-vous sur [info.pingbase.net](info.pingbase.net) pour retrouver l'agenda des activités de PiNG.*


## Un espace ressource physique

![](1536-1024.jpg?forceresize=100%)

Notre association rassemble depuis 2013 des ouvrages autour de la culture numérique au sein d’un espace ressource installé au Pôle associatif du 38 Breil. Ce fond documentaire regroupe plus de 500 références. 
Cet espace est l’endroit idéal pour approfondir vos connaissances sur des sujets tels que l’histoire des sciences, la société et cultures numériques, la pédagogie, l’art et le design, mais on y trouve aussi des revues, des romans et des ouvrages techniques pour découvrir la fabrication numérique, l’électronique la sérigraphie…
L’ensemble des documents à disposition dans ce lieu est recensé [ici sur Zotero](https://www.zotero.org/groups/2368308/ping-bibliothque/items/NA5JFS77/library). 

## Recevoir "Dans l'oeil de PiNG" dans sa boite mail

« Dans l’œil de PiNG » est une lettre bimestrielle pour partager notre vision du numérique, ce qui nous a tapé dans l’œil et faire la mise au point sur des sujets à forts enjeux !

[S'abonner à la newsletter](https://info.pingbase.net/newsletters/?classes=btn)

---
Pour consulter les mentions légales de ce site, [rendez-vous sur cette page](https://ressources.pingbase.net/mentions-legales).