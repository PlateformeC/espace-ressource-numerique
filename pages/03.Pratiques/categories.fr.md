---
title: Pratiques
media_order: pratiques-min.jpeg
body_classes: 'title-h1h2 header-dark header-transparent'
aura:
    pagetype: website
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la catégorie "pratiques".'
    image: pratiques-min.jpeg
anchors:
    active: true
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_sidebar: false
show_searchsidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        -
            '@taxonomy.category': pratiques
    limit: null
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
metadata:
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;pratiques&quot;.'
    'og:url': 'https://ressources.pingbase.net/pratiques'
    'og:type': website
    'og:title': 'Pratiques | PING Ressources Num&eacute;riques'
    'og:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;pratiques&quot;.'
    'og:image': 'https://ressources.pingbase.net/pratiques/pratiques-min.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '2400'
    'og:image:height': '1600'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Pratiques | PING Ressources Num&eacute;riques'
    'twitter:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;pratiques&quot;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/pratiques/pratiques-min.jpeg'
    'article:published_time': '2021-01-12T10:13:15+01:00'
    'article:modified_time': '2021-01-12T10:13:15+01:00'
    'article:author': Ping
---

# Pratiques
