---
title: Recherches
media_order: recherches-min.jpeg
body_classes: 'title-h1h2 header-dark header-transparent'
aura:
    pagetype: website
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la catégorie "recherches".'
    image: recherches-min.jpeg
anchors:
    active: true
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_sidebar: false
show_searchsidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        -
            '@taxonomy.category': recherches
    limit: null
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
metadata:
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;recherches&quot;.'
    'og:url': 'https://ressources.pingbase.net/recherches'
    'og:type': website
    'og:title': 'Recherches | PING Ressources Num&eacute;riques'
    'og:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;recherches&quot;.'
    'og:image': 'https://ressources.pingbase.net/recherches/recherches-min.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '4096'
    'og:image:height': '2731'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'Recherches | PING Ressources Num&eacute;riques'
    'twitter:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;recherches&quot;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/recherches/recherches-min.jpeg'
    'article:published_time': '2020-12-02T09:21:42+01:00'
    'article:modified_time': '2021-01-12T10:14:13+01:00'
    'article:author': Ping
---

# Recherches
