---
title: Récits
media_order: recits-min.jpeg
body_classes: 'title-h1h2 header-dark header-transparent'
aura:
    pagetype: website
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la catégorie "récits".'
    image: recits-min.jpeg
anchors:
    active: true
tagtitle: h2
hero_overlay: true
hero_showsearch: true
show_sidebar: false
show_searchsidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        -
            '@taxonomy.category': recits
    limit: null
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
metadata:
    description: 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;r&eacute;cits&quot;.'
    'og:url': 'https://ressources.pingbase.net/recits'
    'og:type': website
    'og:title': 'R&eacute;cits | PING Ressources Num&eacute;riques'
    'og:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;r&eacute;cits&quot;.'
    'og:image': 'https://ressources.pingbase.net/recits/recits-min.jpeg'
    'og:image:type': image/jpeg
    'og:image:width': '1500'
    'og:image:height': '1000'
    'og:author': Ping
    'twitter:card': summary_large_image
    'twitter:title': 'R&eacute;cits | PING Ressources Num&eacute;riques'
    'twitter:description': 'Bienvenue sur le site ressource de PiNG ! Vous trouverez sur cette page l''ensemble des articles de la cat&eacute;gorie &quot;r&eacute;cits&quot;.'
    'twitter:site': '@assoPiNG'
    'twitter:creator': '@assoPiNG'
    'twitter:image': 'https://ressources.pingbase.net/recits/recits-min.jpeg'
    'article:published_time': '2020-12-11T14:21:51+01:00'
    'article:modified_time': '2021-01-12T10:10:52+01:00'
    'article:author': Ping
---

#RÉCITS
